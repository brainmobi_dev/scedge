 <script>
    function deleteItem(user_id) {
        var host = "{{URL::to('/')}}";
            bootbox.confirm("Are you sure you want to delete this data?", function(result) {
            if (result) {
                    $.ajax({
                        url: host+'/item/delete',
                        type: "post",
                        data: {"user_id": user_id,'_token':"{{ csrf_token()}}" },
                        success: function(response) {
                            $("#row_" + user_id).remove();
                        }
                    });
            }
        });    
    }
    
    function changeStatus(user_id, status){
        var host = "{{URL::to('/')}}";
            $.ajax({
                url: host+'/user/changestatus',
                type: "post",
                data: {"user_id": user_id, "status":status,'_token':"{{ csrf_token()}}"},
                success: function(response) {
                console.log(response);     
                var newStatus= (status)?0:1;
                if(response.error_code == 200){
                    if(status == "1"){
                        $("#user_"+user_id+ " span").removeClass("btn btn-success"); 
                        $("#user_"+user_id+ " span").addClass("btn btn-danger"); 
                        $("#user_"+user_id+ " span").text("Inactive"); 
                        $("#user_"+user_id).attr("href", "javascript:changeStatus("+user_id+", "+newStatus+")");
                    
                    } else {
                    
                        $("#user_"+user_id+ " span").removeClass("btn btn-danger"); 
                        $("#user_"+user_id+ " span").addClass("btn btn-success"); 
                        $("#user_"+user_id+ " span").text("Active"); 
                        $("#user_"+user_id).attr("href", "javascript:changeStatus("+user_id+", "+newStatus+")");
                    }
                }
                }
                });
    
    }
    </script>
