(function() {
  angular
    .module('validation.rule', ['validation'])
    .config(['$validationProvider', function($validationProvider) {
      var expression = {
        required: function(value) {
          return !!value;
        },
        url: /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)/,
        email: /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/,
        number: /^\d+$/,
        minlength: function(value, scope, element, attrs, param) {
          return value && value.length >= param;
        },
        maxlength: function(value, scope, element, attrs, param) {
          return !value || value.length <= param;
        },
        string : /^[a-zA-Z ]*$/,
      };

      var defaultMsg = {
        required: {
          error: 'This field be Required!!',
          success: ''
        },
        url: {
          error: 'This field should be Url',
          success: ''
        },
        email: {
          error: 'This field should be Email',
          success: ''
        },
        number: {
          error: 'This field should be Number',
          success: ''
        },
        minlength: {
          error: 'This field should be longer',
          success: ''
        },
        maxlength: {
          error: 'This field should be shorter',
          success: ''
        },
        string: {
          error: 'You can only use alphabets',
          success: ''
        },
      };
      $validationProvider.setExpression(expression).setDefaultMsg(defaultMsg);
    }]);
}).call(this);
