// JavaScript Document

$(".pop-tab a").click(function(){

	"use strict";

 	$(this).parent().addClass('active').siblings().removeClass('active');

	  var tab = $(this).attr('href');

	  $('.tab_content').not(tab).hide();

	  $(tab).show();
 });



$(".feature-slide").owlCarousel({
 	items: 3,
 	navigation: true,
 	itemsDesktop: [1199, 2],
 	itemsDesktopSmall: [1024, 1],
 	itemsTablet: [785, 1]
 });


 

//$("#creat-items-popup").modal("show");

//$("#create-items-free").modal("show");

//$("#personal").modal("show");

//$("#professional").modal("show");

//$("#reset-password").modal("show");

//$("#settings").modal("show");

//$("#edit-profile").modal("show");

$("#welcome").modal("show");

//$('#alert-popup').modal('show');
//$('#warning-popup').modal('show');
//$('#success-popup').modal('show');


function createPersonalItem() {

	$("#personal").modal("show");

}


$('.addtext').click(function(){
	$('.add-field').slideToggle();
  });

$(".addtext").click(function(){
      if ($(this).text() === 'Add new field') {
        $(this).text('Remove field');
    } else {
        $(this).text('Add new field');
    }

    return false;
});


function createProfessionalItem() {

	$("#professional").modal("show");

}





function gotopage(page) {

	window.location = page;

}



function toggleradio(id) {
	return;
	if (document.getElementById(id).checked) {

		$("#" + id).prop('checked', false);

	} else {
		$("#" + id).prop('checked', true);
	}
}

$(window).on("load",function(){
	 "use strict";
	   $(".content-m")
		 	.mCustomScrollbar({theme:"minimal"});

		 $(".content-x")
			 .mCustomScrollbar({theme:"minimal"});
		 //.mCustomScrollbar("scrollTo","bottom",{scrollInertia:0});
	 });


$(document).ready(function () {

	$('.check-input').on('change', function () {
		$('.check-input').not(this).prop('checked', false);
	});


	$('.check-input2').on('change', function () {
		$('.check-input2').not(this).prop('checked', false);
	});



	$(".confirm-pay").click(function (e) {
		e.preventDefault();
		$(".bill-details").removeClass("disable-content");
		$(".order-summary").addClass("disable-content");
		$(".step2").addClass("active");
	});
	$(".pay-now").click(function () {
		$(".step3").addClass("active");
	});


	function readURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function (e) {
				$('.blah').attr('src', e.target.result);
			}

			reader.readAsDataURL(input.files[0]);
		}
	}

	$(".imgInp").change(function () {
		readURL(this);
	});
});
