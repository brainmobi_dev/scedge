var gulp = require('gulp'),
    concat = require('gulp-concat'),
    cleanCSS = require('gulp-clean-css'),
    minify = require('gulp-minify');
    /*jshint = require('gulp-jshint')*/

gulp.task('minify-js', function() {
  	return gulp.src(['module/app-header.js',  'module/constants/*.js', 'module/services/modal.service.js',
  						'module/services/service.js', 'module/filters/filter.js', 'module/controllers/*.js', 'module/directives/modal.directive.js',
              'module/directives/directive.js', 'module/app.js', 'module/config.js'])
  		.pipe(concat('module.js'))
		.pipe(minify())
	  	.pipe(gulp.dest('build'));

});

gulp.task('minify-main-js', function() {
  	return gulp.src(['module/app-header.js',  'module/constants/*.js', 'module/services/modal.service.js',
  						'module/services/service-main.js', 'module/filters/filter-main.js', 'module/controllers/*.js', 'module/directives/modal.directive.js',
              'module/directives/directive-main.js', 'module/app.js', 'module/config-main.js'])
  		.pipe(concat('module.js'))
		  .pipe(minify())
	  	.pipe(gulp.dest('build'));

});

gulp.task('minify-css', function() {

	return gulp.src(['css/*.css'])
	    .pipe(concat('main.css'))
	    .pipe(cleanCSS({compatibility: 'ie8'}))
	    .pipe(gulp.dest('build'));

});

gulp.task('build', ['minify-js', 'minify-css']);
gulp.task('build-main', ['minify-main-js', 'minify-css']);

gulp.task('default', ['watch']);
//
// configure the jshint task
//gulp.task('jshint', function() {
//  return gulp.src('module/**/*.js')
//    .pipe(jshint())
//    .pipe(jshint.reporter('jshint-stylish'));
//});

// configure which files to watch and what tasks to use on file changes
//gulp.task('watch', function() {
//  gulp.watch('module/**/*.js', ['jshint']);
//});*/
