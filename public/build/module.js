var scedgeConstants     = angular.module('scedgeConstants', []);
var scedgeServices      = angular.module('scedgeServices', []);
var scedgeFilters       = angular.module('scedgeFilters', []);
var scedgeControllers   = angular.module('scedgeControllers', ['scedgeServices', 'validation', 'validation.rule']);
var scedgeDirectives    = angular.module('scedgeDirectives', []);

scedgeConstants
	.constant('LINK', {
		'local' :  'http://scedge.dev/',
		'server' : 'https://www.scedge.com/', //'http://scedge.apphosthub.com/',
		'public' : 'http://scedge.dev/',
	})
	.constant('MONTHS', {
		'NAME' : [
				{"JAN":1},
				{"FEB":2},
				{"MAR":3},
				{"APR":4},
				{"MAY":5},
				{"JUN":6},
				{"JUL":7},
				{"AUG":8},
				{"SEP":9},
				{"OCT":10},
				{"NOV":11},
				{"DEC":12}
		]
	})
	.constant('FILTERDATA', {
		'FIELDS' : [
			'location_address', 'location_city', 'location_province', 'location_country', 'cityName', 'provinceName',
			'countryName', 'price', 'quantity', 'discount', 'spacs_size', 'spacs_brand',
			'spacs_model', 'spacs_color', 'for_gender', 'for_age'
		]
	})
	.constant('API', (function(){
	    	return {
	    		'login' 	: 'api/v1/auth/login',
	    		'register'  : 'api/v1/auth/register',
	    		'socialRegister' : 'api/v1/auth/social-register',
	    		'subscription' : 'api/v1/auth/subscription',
	    		'activateUser' : 'api/v1/auth/activateUser',
	    		'processPayment' : 'api/v1/auth/process-payment',
	    		'getVerification' : 'api/v1/auth/get-verification',
	    		'resetPassword' :  'api/v1/auth/reset-password',
	    		'getProvinceAuth' : 'api/v1/auth/getProvince',
					'getCityAuth' : 'api/v1/auth/getCity',
					'resendActivationCode' : 'api/v1/auth/resend-activation-code',
					'checkVerificationCode' : 'api/v1/auth/check-verification-code',
					'updateAccountPassword' : 'api/v1/auth/update-account-password',

	    		'getItem'  : 'api/v1/item/get',
					'getShowcase'  : 'api/v1/profile/getShowcase',
					'createItem' : 'api/v1/item/create',
					'deleteItem' : 'api/v1/item/delete',
					'getCardGroup' : 'api/v1/item/group',
					'updateItem' : 'api/v1/item/update',
					'deleteAll'  : 'api/v1/item/delete-all',
					'getProvince' : 'api/v1/item/getProvince',
					'getCity' : 'api/v1/item/getCity',
					'getContacts' : 'api/v1/item/getContacts',
					'shareCard' : 'api/v1/item/share',
					'addItemGroup' 			: 'api/v1/item/addGroup',
					'updateItemOrder' 	: 'api/v1/item/updateOrder',
					'updateShowcaseOrder' : 'api/v1/item/updateShowcaseOrder',
					'updateSharedItem'  : 'api/v1/item/updateSharedItem',
					'getSharedItems' 		: 'api/v1/item/getSharedItems',
					'updateItemNotification' : 'api/v1/item/updateItemNotification',
					'shareItemViaEmail' : 'api/v1/item/shareItemViaEmail',

					'reportAbuseUser' : 'api/v1/contact/report-abuse',
					'blockUser' : 'api/v1/contact/block-user',

	    		'browse'  : 'api/v1/browse/get',
	    		'getReports' : 'api/v1/browse/getReports',
	    		'reportAbuse' : 'api/v1/browse/reportAbuse',

					'getContact'  : 'api/v1/contact/get',
					'getContactGroup' : 'api/v1/contact/get-group',
					'addContactGroup' : 'api/v1/contact/add-group',
					'addContact' : 'api/v1/contact/add-contact',
					'getUserContactGroup' : 'api/v1/contact/get-user-group',
					'deleteContact' : 'api/v1/contact/delete',
					'pendingRequest' : 'api/v1/contact/pendingRequest',
					'approveConnection' : 'api/v1/contact/approveConnection',
					'rejectConnection' : 'api/v1/contact/rejectConnection',
					'sendInvitation' : 'api/v1/contact/sendInvitation',

					'removeContactGroup' : 'api/v1/contact/removeContactGroup',

	    		'getProfile' : 'api/v1/profile/get',
					'getUserNotifications' : 'api/v1/profile/getUserNotifications',
					'removeUserNotifications' : 'api/v1/profile/removeUserNotifications',



	    		'updatePassword' : 'api/v1/profile/updatePassword',
	    		'updateSetting' : 'api/v1/profile/updateSetting',
	    		'updateCard' : 'api/v1/profile/updateCard',
					'deleteCard' : 'api/v1/profile/deleteCard',
	    		'updateProfile'  : 'api/v1/profile/updateProfile',
	    		'updateSubscription' : 'api/v1/profile/updateSubscription',
	    		'uploadCoverImage' : 'api/v1/profile/saveCoverImage'

	    	};
	})());

    function ImageUploader(modalFactory, FileUploader) {

     	 var openModal = function(id){
     			 modalFactory.Open(id);
       };

       var closeModal = function(id){
           uploader.clearQueue();
           modalFactory.Close(id);
       };


       var uploader = new FileUploader({
           url: '/api/v1/auth/upload-image'
       });

       uploader.filters.push({
           name: 'imageFilter',
           fn: function(item /*{File|FileLikeObject}*/, options) {
               var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
               return '|jpg|png|bmp|jpeg|gif|'.indexOf(type) !== -1;
           }
       });


       uploader.onBeforeUploadItem = function(item) {
         var blob = dataURItoBlob(item.croppedImage);
         item._file = blob;
       };

       var dataURItoBlob = function(dataURI) {
         var binary = atob(dataURI.split(',')[1]);
         var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
         var array = [];
         for(var i = 0; i < binary.length; i++) {
           array.push(binary.charCodeAt(i));
         }
         return new Blob([new Uint8Array(array)], {type: mimeString});
       };


       uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
           //console.info('onWhenAddingFileFailed', item, filter, options);
       };
       uploader.onAfterAddingAll = function(addedFileItems) {
           //console.info('onAfterAddingAll', addedFileItems);
           openModal('cropper-view-modal');
       };
       uploader.onProgressItem = function(fileItem, progress) {
           //console.info('onProgressItem', fileItem, progress);
       };
       uploader.onProgressAll = function(progress) {
           //console.info('onProgressAll', progress);
       };
       uploader.onSuccessItem = function(fileItem, response, status, headers) {
           //console.info('onSuccessItem', fileItem, response, status, headers);
       };
       uploader.onErrorItem = function(fileItem, response, status, headers) {
           //console.info('onErrorItem', fileItem, response, status, headers);
           uploader.clearQueue();
           closeModal('cropper-view-modal');
       };
       uploader.onCancelItem = function(fileItem, response, status, headers) {
          //console.info('onCancelItem', fileItem, response, status, headers);
           uploader.clearQueue();
           closeModal('cropper-view-modal');
       };
       uploader.onCompleteItem = function(fileItem, response, status, headers) {
           //console.info('onCompleteItem', fileItem, response, status, headers);
           uploader.uploadedImage = response;
           uploader.clearQueue();
           closeModal('cropper-view-modal');
       };
       uploader.onCompleteAll = function() {
           //console.info('onCompleteAll');
       };


       return uploader;
    }



scedgeServices
    .factory('ModalFactory', ['$document', '$timeout', function($document, $timeout){
        var modals = []; // array of modals on the page
        var service = {};
        service.Add = Add;
        service.Remove = Remove;
        service.Open = Open;
        service.Close = Close;
        return service;

        function Add(modal) {
            // add modal to array of active modals
            modals.push(modal);
        }

        function Remove(id) {
            // remove modal from array of active modals
            var modalToRemove = _.findWhere(modals, { id: id });
            modals = _.without(modals, modalToRemove);
        }

        function Open(id) {
            // open modal specified by id
            var modal = _.findWhere(modals, { id: id });
            modal.open();
        }

        function Close(id) {
            // close modal specified by id
            var modal = _.findWhere(modals, { id: id });
            modal.close();
            if(id == 'cropper-view-modal') {
              $timeout(function(){
                var body = angular.element(document).find('body');
                body[0].className = "modal-open";
                $document[0].body.className = "modal-open";
              }, 2000);
          }

        }
    }])
    .factory('ModalService',['$uibModal', function($uibModal){
        var defaultOption = {
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            size: 'lg',
            appendTo: undefined,
            backdrop: 'static',
            keyboard: false,
            controller : 'ModalInstanceCtrl',
            controllerAs: '$ctrl',
        }

        var modalData = {};

        function openModal(options){
             options = angular.extend(defaultOption, options);
             setModal(options.content);

             var modalInstance = $uibModal.open(options);
             return modalInstance.result;
        }


        function openSetting(options){
             options.templateUrl = "setting-content.html";
             options.keyboard = true;
             options.size = 'sm';
             options = angular.extend(defaultOption, options);
             setModal(options.content);

             var modalInstance = $uibModal.open(options);
             return modalInstance.result;
        }


        function openProfile(options){
             options.templateUrl = "profile-content.html";
             options.keyboard = true;
             options.size = 'sm';
             options = angular.extend(defaultOption, options);
             setModal(options.content);

             var modalInstance = $uibModal.open(options);
             return modalInstance.result;
        }

        function openPasswordReset(options){
             options.templateUrl = "password-reset-content.html";
             options.keyboard = true;
             options.size = 'sm';
             options = angular.extend(defaultOption, options);
             setModal(options.content);

             var modalInstance = $uibModal.open(options);
             return modalInstance.result;
        }


        function openCardSetting(options){
             options.templateUrl = "card-content.html";
             options.keyboard = true;
             options.size = 'sm';
             options = angular.extend(defaultOption, options);
             setModal(options.content);

             var modalInstance = $uibModal.open(options);
             return modalInstance.result;
        }


        function openContactPopup(options){
             options.templateUrl = "contact-content.html";
             options.keyboard = true;
             options.size = 'sm';
             options = angular.extend(defaultOption, options);
             setModal(options.content);

             var modalInstance = $uibModal.open(options);
             return modalInstance.result;
        }

        function openAddContactPopup(options) {
          options.templateUrl = "add-contact-content.html";
          options.keyboard = true;
          options.size = 'sm';
          options = angular.extend(defaultOption, options);
          setModal(options.content);

          var modalInstance = $uibModal.open(options);
          return modalInstance.result;
        }

        function openBrowseFilterPopup(options) {
          options.templateUrl = "filter-browse-content.html";
          options.keyboard = true;
          options.size = 'sm';
          options = angular.extend(defaultOption, options);
          setModal(options.content);

          var modalInstance = $uibModal.open(options);
          return modalInstance.result;
        }


        function setModal(data){
            modalData = data;
        }

        function getModal(){
            return modalData;
        }

        return {
            openModal: openModal,
            openSetting : openSetting,
            openProfile : openProfile,
            openCardSetting : openCardSetting,
            openPasswordReset : openPasswordReset,
            openContactPopup : openContactPopup,
            openAddContactPopup : openAddContactPopup,
            openBrowseFilterPopup: openBrowseFilterPopup,
            setModal : setModal,
            getModal : getModal
        }

    }])


    .factory("ImageUploader", ["ModalFactory", "FileUploader", ImageUploader])

function RequestHandler($http, $q, $state, storageServiceHandler){
        return{
            prepareRequest      : prepareRequest,
            preparePostRequest  : preparePostRequest,
            prepareGetRequest   : prepareGetRequest,
            prepareImageRequest : prepareImageRequest,
            prepareAttachmentRequest: prepareAttachmentRequest,
            prepareJsonRequest  : prepareJsonRequest
        }

        var postParam = {
            method  : 'POST',
            url     : '',
            data    : '',
            headers : {'Content-Type': 'application/x-www-form-urlencoded'},
            transformRequest : transformTicketRequest
        }

        var getParam = {
            method  : 'GET',
            url     : '',
        }


        function prepareRequest(method, param){
            var requestParam = (method.toUpperCase() == 'POST') ? $.extend({}, postParam, param) : $.extend({}, getParam, param);

            if(method.toUpperCase() == 'POST'){
                requestParam.data = (requestParam.data) ? $.param(requestParam.data) : "";
            }

            return $http(requestParam)
                .then(sendResponseData )
                .catch(sendResponseError);
        }

        function preparePostRequest($param) {
            $param.data = ($param.data) ? $.param($param.data) : "";
            return $http({
                method  : 'POST',
                url     : $param.url,
                data    : $param.data,
                headers : (typeof $param.header != 'undefined') ? angular.extend({'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'}, $param.header) : {'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'},
                transformRequest : angular.identity
            })
                .then(sendResponseData )
                .catch(sendResponseError )
        }


        function prepareImageRequest($param) {
            $param.data = ($param.data) ? $.param($param.data) : "";
            $param.url  =  $param.url + '?'+ $param.data;
            return $http({
                method: 'GET',
                url     : $param.url,
                dataType: 'binary',
                responseType: 'arraybuffer',
                headers: {'Content-Type':'image/png','X-Requested-With':'XMLHttpRequest'},
                processData: false,
                transformRequest: angular.identity
            })
                .then(sendResponseData )
                .catch(sendResponseError )
        }

        function prepareAttachmentRequest($param) {
            return $http({
                method  : 'POST',
                url     : $param.url,
                data    : $param.data,
                processData: false,
                contentType: false,
                headers:  {'Content-Type': undefined},
                transformRequest: angular.identity
            })
                .then(sendResponseData )
                .catch(sendResponseError )
        }

        function prepareJsonRequest($param){
            return $http.jsonp($param.url).
            success(function(status) {
                //console.log(status);
            }).
            error(function(status) {
                //console.log(status);
            });
        }


        function prepareGetRequest($param){
          $param.data = ($param.data) ? $.param($param.data) : "";
          $param.url  =  $param.url + '?'+ $param.data;
            return $http({
                method  : 'GET',
                url     : $param.url,
                headers: {
                    'Content-Type': 'json',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Methods': 'GET'
                }
            })
                .then(sendResponseData )
                .catch(sendResponseError )

        }


        function transformTicketRequest(data, headersGetter) {return data;}
        function transformEmailAttachmentsRequest(data, headersGetter) {
            var formData = new FormData();
            //need to convert our json object to a string version of json otherwise
            // the browser will do a 'toString()' on the object which will result
            // in the value '[Object object]' on the server.
            formData.append("email-param", angular.toJson(data['email-param']));
            //now add all of the assigned files

            formData.append("file", encodeURIComponent(data['email-param'].attachment));

            return formData;
        }
        function sendResponseData(response) {
            return response.data;
        }
        function sendResponseError(response) {
            if(response.status == 404){
              storageServiceHandler.removeValue('token');
              storageServiceHandler.removeValue('userDetail');
              storageServiceHandler.removeValue('notifications');
              storageServiceHandler.removeValue('uKey');

              $state.go('login', { nextState: '', nextParams: ''});
              return null;
            }
            return {'error_code' : 500, 'msg_string' : 'Some problem occured', 'result' : []};
            //storageServiceHandler.setValue('token', '');
            //$state.go('login', { nextState: '', nextParams: ''});
            //return null;
        }

    };


    function AuthServiceHandler($state, requestHandler, storageServiceHandler, link, api){
            return {
                 'isValidToken'      : isValidToken,
                 'isAuthenticated'   : isAuthenticated,
               'getUserDetail'     : getUserDetail,
               'getVerificationDetail' : getVerificationDetail,
               'login'             : login,
               'logout'            : logout,
               'getSubscription'   : getSubscription,
               'socialRegisterUser': socialRegisterUser,
               'registerUser'      : registerUser,
               'activeUser'        : activeUser,
               'processPaymentRequest'    : processPaymentRequest,
               'resetPassword'    : resetPassword,
               'getProvince' : getProvince,
               'getCity' : getCity,
               'resendActivationCode' : resendActivationCode,
               'checkVerificationCode' : checkVerificationCode,
               'updateAccountPassword' : updateAccountPassword
        		};


            function updateAccountPassword($params) {
               $params = ($params) ? $params : {};
               $params = angular.extend({
                   'url' : link.server + api.updateAccountPassword
               }, $params);
               return requestHandler.preparePostRequest($params);
            }

            function resendActivationCode($params) {
               $params = ($params) ? $params : {};
               $params = angular.extend({
                   'url' : link.server + api.resendActivationCode
               }, $params);
               return requestHandler.preparePostRequest($params);
            }

            function checkVerificationCode($params) {
               $params = ($params) ? $params : {};
               $params = angular.extend({
                   'url' : link.server + api.checkVerificationCode
               }, $params);
               return requestHandler.preparePostRequest($params);
            }


            function isValidToken($params){

                //requestHandler.preparePostRequest($params);
            }

            function isAuthenticated() {
                return storageServiceHandler.getValue('token');
            }

        function getVerificationDetail($params) {
             $params = angular.extend({
                'url' : link.server + api.getVerification
            }, $params);
            return requestHandler.preparePostRequest($params);
        }

        function resetPassword($params) {
             $params = angular.extend({
                'url' : link.server + api.resetPassword
            }, $params);
            return requestHandler.preparePostRequest($params);
        }

        function getUserDetail() {
            $params = angular.extend({
                'url' : link.server + api.getProfile,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, {});
            return requestHandler.preparePostRequest($params);
        }

        function login($params) {
            $params = angular.extend({'url' : link.server + api.login}, $params);
            return requestHandler.preparePostRequest($params);
        }

        function logout(){
            storageServiceHandler.removeValue('token');
            storageServiceHandler.removeValue('userDetail');
            storageServiceHandler.removeValue('notifications');
            storageServiceHandler.removeValue('uKey');

            $state.go('login', { nextState: '', nextParams: ''});
            return null;
        }

        function processPaymentRequest($params) {
            $params = angular.extend({'url' : link.server + api.processPayment}, $params);
            return requestHandler.preparePostRequest($params);
        }


        function getSubscription() {
            $params = angular.extend({'url' : link.server + api.subscription});
            return requestHandler.prepareGetRequest($params);
        }

        function socialRegisterUser($params){
            $params = angular.extend({'url' : link.server + api.socialRegister}, $params);
            return requestHandler.preparePostRequest($params);
        }

        function registerUser($params) {
            $params = angular.extend({'url' : link.server + api.register}, $params);
            return requestHandler.preparePostRequest($params);
        }

        function activeUser($params) {
            $params = angular.extend({'url' : link.server + api.activateUser}, $params);
            return requestHandler.preparePostRequest($params);
        }

        function getProvince($params){
               $params = ($params) ? $params : {};
            $params = angular.extend({
                'url' : link.server + api.getProvinceAuth
            }, $params);
            return requestHandler.preparePostRequest($params);

        }


        function getCity($params){
               $params = ($params) ? $params : {};
            $params = angular.extend({
                'url' : link.server + api.getCityAuth
            }, $params);
            return requestHandler.preparePostRequest($params);

        }
    }


    function StorageServiceHandler(localStorageService) {
        return {
            'setValue' : setValue,
            'getValue' : getValue,
            'removeValue' : removeValue
        };

        function setValue(key, val){
             localStorageService.set(key, val)
        }

        function getValue(key){
             return localStorageService.get(key);
        }

        function removeValue(key) {
           return localStorageService.remove(key);
        }

    }

    function ProfileServiceHandler($state, requestHandler, storageServiceHandler, link, api) {
        return {
            'notifications' : notifications,
            'updatePassword' :  updatePassword,
            'updateSetting' :   updateSetting,
            'updateProfile' :   updateProfile,
            'updateCard'    :   updateCard,
            'deleteCard'    :   deleteCard,
            'updateSubscription' : updateSubscription,
            'uploadCoverImage' : uploadCoverImage,
            'getUserNotifications' : getUserNotifications,
            'removeUserNotifications' : removeUserNotifications
        };

        function notifications(){
            return storageServiceHandler.getValue('notifications');
        }

        function updateSubscription($params){
           $params = ($params) ? $params : {};
           $params = angular.extend({
               'url' : link.server + api.updateSubscription,
               'header' : {
                   'Authorization' : storageServiceHandler.getValue('token')
                }
           }, $params);
           return requestHandler.preparePostRequest($params);
        }

        function deleteCard($params) {
           $params = ($params) ? $params : {};
           $params = angular.extend({
               'url' : link.server + api.deleteCard,
               'header' : {
                   'Authorization' : storageServiceHandler.getValue('token')
               }
           }, $params);
           return requestHandler.preparePostRequest($params);
        }

        function updateSetting($params) {
             $params = ($params) ? $params : {};
             $params = angular.extend({
                'url' : link.server + api.updateSetting,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);
        }

        function updateProfile($params) {
             $params = ($params) ? $params : {};
             $params = angular.extend({
                'url' : link.server + api.updateProfile,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);
        }

        function updateCard($params) {
             $params = ($params) ? $params : {};
             $params = angular.extend({
                'url' : link.server + api.updateCard,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);
        }

        function updatePassword($params) {
             $params = ($params) ? $params : {};
             $params = angular.extend({
                'url' : link.server + api.updatePassword,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);
        }

        function uploadCoverImage($params){
            $params = ($params) ? $params : {};
             $params = angular.extend({
                'url' : link.server + api.uploadCoverImage,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);
        }

        function getUserNotifications($params){
            $params = ($params) ? $params : {};
             $params = angular.extend({
                'url' : link.server + api.getUserNotifications,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);
        }

        function removeUserNotifications($params){
            $params = ($params) ? $params : {};
             $params = angular.extend({
                'url' : link.server + api.removeUserNotifications,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);
        }
    }


    function ContactServiceHandler($state, requestHandler, storageServiceHandler, link, api) {
        return {
            'getContacts'     : getContacts,
            'getContactGroup' : getContactGroup,

            'addContact'      : addContact,
            'addContactGroup' : addContactGroup,
            'removeContactGroup' : removeContactGroup,

            'getUserContactGroup' : getUserContactGroup,
            'filterResultByName' : filterResultByName,
            'deleteContact' : deleteContact,
            'pendingRequest' : pendingRequest,
            'approveConnection' : approveConnection,
            'rejectConnection' : rejectConnection,
            'sendInvitation' : sendInvitation,
            'reportAbuse'   : reportAbuse,
            'blockUser'     : blockUser
        };

        function removeContactGroup($params){
            $params = ($params) ? $params : {};
            $params = angular.extend({
                'url' : link.server + api.removeContactGroup,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);
        }


        function reportAbuse($params) {
          $params = ($params) ? $params : {};
          $params = angular.extend({
              'url' : link.server + api.reportAbuseUser,
              'header' : {
                  'Authorization' : storageServiceHandler.getValue('token')
              }
          }, $params);
          return requestHandler.preparePostRequest($params);
        }

        function blockUser($params) {
          $params = ($params) ? $params : {};
          $params = angular.extend({
              'url' : link.server + api.blockUser,
              'header' : {
                  'Authorization' : storageServiceHandler.getValue('token')
              }
          }, $params);
          return requestHandler.preparePostRequest($params);
        }

        function getUserContactGroup($params){
            $params = ($params) ? $params : {};
            $params = angular.extend({
                'url' : link.server + api.getUserContactGroup,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);
        }

        function addContactGroup($params){
            $params = ($params) ? $params : {};
            $params = angular.extend({
                'url' : link.server + api.addContactGroup,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);
        }

        function addContact($params){
            $params = ($params) ? $params : {};
            $params = angular.extend({
                'url' : link.server + api.addContact,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);
        }

        function getContacts($params){
            $params = ($params) ? $params : {};
            $params = angular.extend({
                'url' : link.server + api.getContact,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);
        }

        function deleteContact($params){
            $params = ($params) ? $params : {};
            $params = angular.extend({
                'url' : link.server + api.deleteContact,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);

        }

        function getContactGroup($params) {
          $params = ($params) ? $params : {};
            $params = angular.extend({
                'url' : link.server + api.getContactGroup,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);
        }

        function filterResultByName($params){

            $params = ($params) ? $params : {};
            $params = angular.extend({
                'url' : link.server + api.contactResultFilter,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);

        }

        function pendingRequest(){
            $params = angular.extend({
                'url' : link.server + api.pendingRequest,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, {});
            return requestHandler.preparePostRequest($params);
        }

        function approveConnection($params){
            $params = ($params) ? $params : {};
            $params = angular.extend({
                'url' : link.server + api.approveConnection,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);
        }

        function rejectConnection($params){
            $params = ($params) ? $params : {};
            $params = angular.extend({
                'url' : link.server + api.rejectConnection,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);
        }

        function sendInvitation($params){
            $params = ($params) ? $params : {};
            $params = angular.extend({
                'url' : link.server + api.sendInvitation,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);
        }
    }


    function ItemServiceHandler($state, requestHandler, storageServiceHandler, link, api) {
        return {
            'getItems' : getItems,
            'getShowcase' : getShowcase,
            'createItem' : createItem,
            'deleteItem' : deleteItem,
            'browseItems'   : browseItems,
            'getProfile' : getProfile,
            'getCardGroup' : getCardGroup,
            'updateItem' : updateItem,
            'deleteAll' : deleteAll,
            'getProvince' : getProvince,
            'getCity' : getCity,
            'getContacts' : getContacts,
            'getReports' :getReports,
            'reportAbuse' : reportAbuse,
            'shareCard' : shareCard,
            'addItemGroup' : addItemGroup,
            'updateItemOrder' : updateItemOrder,
            'updateShowcaseOrder' : updateShowcaseOrder,

            'getSharedItems' : getSharedItems,
            'updateSharedItem' : updateSharedItem,
            'updateItemNotification' : updateItemNotification,
            'shareItemViaEmail' : shareItemViaEmail
        };

        function shareItemViaEmail($params){
          $params = ($params) ? $params : {};
          $params = angular.extend({
              'url' : link.server + api.shareItemViaEmail,
              'header' : {
                  'Authorization' : storageServiceHandler.getValue('token')
              }
          }, $params);
          return requestHandler.preparePostRequest($params);
        }

        function updateItemNotification($params){
          $params = ($params) ? $params : {};
          $params = angular.extend({
              'url' : link.server + api.updateItemNotification,
              'header' : {
                  'Authorization' : storageServiceHandler.getValue('token')
              }
          }, $params);
          return requestHandler.preparePostRequest($params);
        }

        function getSharedItems($params) {
          $params = ($params) ? $params : {};
          $params = angular.extend({
              'url' : link.server + api.getSharedItems,
              'header' : {
                  'Authorization' : storageServiceHandler.getValue('token')
              }
          }, $params);
          return requestHandler.preparePostRequest($params);
        }

        function updateSharedItem($params) {
          $params = ($params) ? $params : {};
          $params = angular.extend({
              'url' : link.server + api.updateSharedItem,
              'header' : {
                  'Authorization' : storageServiceHandler.getValue('token')
              }
          }, $params);
          return requestHandler.preparePostRequest($params);
        }

        function updateShowcaseOrder($params) {
          $params = ($params) ? $params : {};
          $params = angular.extend({
              'url' : link.server + api.updateShowcaseOrder,
              'header' : {
                  'Authorization' : storageServiceHandler.getValue('token')
              }
          }, $params);
          return requestHandler.preparePostRequest($params);
        }

        function updateItemOrder($params) {
          $params = ($params) ? $params : {};
          $params = angular.extend({
              'url' : link.server + api.updateItemOrder,
              'header' : {
                  'Authorization' : storageServiceHandler.getValue('token')
              }
          }, $params);
          return requestHandler.preparePostRequest($params);
        }


        function getShowcase($params){
          $params = ($params) ? $params : {};
            $params = angular.extend({
                'url' : link.server + api.getShowcase,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);
        }



        function addItemGroup($params) {
          $params = ($params) ? $params : {};
          $params = angular.extend({
              'url' : link.server + api.addItemGroup,
              'header' : {
                  'Authorization' : storageServiceHandler.getValue('token')
              }
          }, $params);
          return requestHandler.preparePostRequest($params);
        }

        function getCardGroup($params) {
            $params = ($params) ? $params : {};
            $params = angular.extend({
                'url' : link.server + api.getCardGroup,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);
        }

        function getItems($params){
          $params = ($params) ? $params : {};
            $params = angular.extend({
                'url' : link.server + api.getItem,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);
        }



        function createItem($params){
            $params = ($params) ? $params : {};
            $params = angular.extend({
                'url' : link.server + api.createItem,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);

            return requestHandler.preparePostRequest($params);
        }

        function deleteItem($params){
            $params = ($params) ? $params : {};
            $params = angular.extend({
                'url' : link.server + api.deleteItem,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);

            return requestHandler.preparePostRequest($params);
        }

        function deleteAll($params){
            $params = ($params) ? $params : {};
            $params = angular.extend({
                'url' : link.server + api.deleteAll,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);

            return requestHandler.preparePostRequest($params);
        }

        function updateItem($params){
            $params = ($params) ? $params : {};
            $params = angular.extend({
                'url' : link.server + api.updateItem,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);

            return requestHandler.preparePostRequest($params);
        }


        function browseItems($params){
            $params = ($params) ? $params : {};
            $params = angular.extend({
                'url' : link.server + api.browse,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);
        }

        function getProfile($params){
            $params = angular.extend({
                'url' : link.server + api.getProfile,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, (typeof $params == 'undefined') ? {} : $params);
            return requestHandler.preparePostRequest($params);
        }

        function getProvince($params){
               $params = ($params) ? $params : {};
            $params = angular.extend({
                'url' : link.server + api.getProvince,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);

        }


        function getCity($params){
               $params = ($params) ? $params : {};
            $params = angular.extend({
                'url' : link.server + api.getCity,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);

        }

        function getContacts($params){
               $params = ($params) ? $params : {};
            $params = angular.extend({
                'url' : link.server + api.getContacts,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);
        }

        function getReports(){
            $params = angular.extend({
                'url' : link.server + api.getReports,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, {});
            return requestHandler.preparePostRequest($params);
        }

        function reportAbuse($params){
            $params = ($params) ? $params : {};
            $params = angular.extend({
                'url' : link.server + api.reportAbuse,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);
        }

        function shareCard($params){
            $params = ($params) ? $params : {};
            $params = angular.extend({
                'url' : link.server + api.shareCard,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);

        }
    }

scedgeServices
         .factory('RequestHandler', ['$http', '$q', '$state', 'StorageServiceHandler', RequestHandler])
         .factory('StorageServiceHandler', ['localStorageService', StorageServiceHandler])
         .factory('AuthServiceHandler', ['$state', 'RequestHandler', 'StorageServiceHandler', 'LINK', 'API', AuthServiceHandler])
         .factory('ItemServiceHandler', ['$state', 'RequestHandler', 'StorageServiceHandler', 'LINK', 'API', ItemServiceHandler])
         .factory('ContactServiceHandler', ['$state', 'RequestHandler', 'StorageServiceHandler', 'LINK', 'API', ContactServiceHandler])
         .factory('ProfileServiceHandler', ['$state', 'RequestHandler', 'StorageServiceHandler', 'LINK', 'API', ProfileServiceHandler]);

scedgeFilters
        .filter('truncate', function() {
            return function(mainString, desireLength) {
            	return mainString.substring(0, (typeof desireLength == 'undefined') ? 8 : desireLength)
            };
        })
        .filter('firstname', function() {
            return function(mainString) {
            	var stringArray =  mainString.split(/[ ,]+/);
            	return stringArray[0]
            };
        })
        .filter('getTitle', function() {
            return function(mainString) {
            	var stringArray =  mainString.split(/[ _]+/);
              if(typeof stringArray[1] != 'undefined') {
            	   return ((stringArray[1]).replace("Name", "")).toProperCase();
              } else {
                return (stringArray[0]) ? ((stringArray[0]).replace("Name", "")).toProperCase() : "";
              }

            };
        })
        .filter('getValue', function() {
            return function(value, titleString) {
              var title;
            	var stringArray =  titleString.split(/[ _]+/);
              if(typeof stringArray[1] != 'undefined') {
            	   title = ((stringArray[1]).replace("Name", "")).toUpperCase();
              } else {
                 title = (stringArray[0]) ? ((stringArray[0]).replace("Name", "")).toUpperCase() : "";
              }

              switch(title){
                case "DISCOUNT" : return value + "%";
                case "PRICE" : return "$" + value ;
                default : return value;
              }
            };
        })
        .filter('createImagePath',["LINK", function(link) {
            return function(mainString) {
                if(!mainString){
                    return '';
                }
                if(mainString.includes("http")) {
                  return mainString;
                } else if(mainString.includes("data:image")) {
                  return mainString;
                } else {
                  return link.server + 'public/'+mainString;
                }
            };
        }])
        .filter('isEmpty', function() {
            return function(data) {
            	if(data) {
                if(typeof data == 'object') {
                  return isEmpty(data);
                } else {
                  return data.length;
                }
              }
              return false;
            };
        })
        .filter('convertDateFormat', function(){
            return function(date){
                if(date)
                  return moment(date, "YYYY-MM-DD").format("D MMMM, YYYY");

                return "";
            }
        })
        .filter('convertTimeFormat', function(){
            return function(time){
                if(time){

                  time = "10-10-2000 " + time;
                  return moment(time, "MM-DD-YYYY HH:mm").format("h:mm");
                }
                return "";
            }
        })
        .filter('convertEventDateFormat', function(){
            return function(date, time){
                if(date && time){
                  var newDate = date + " " + time;
                  return newDate;
                  //return moment(newDate, "MM-DD-YYYY HH:mm").format("D MMMM, YYYY h:mm");
                }
                return "";
            }
        })
        .filter('alterAddress', function() {
            return function(address){
                if(address){
                    if(address.length < 35) {
                        return address;
                    }
                    return address.substr(0, 35) + "..";
                }
                return "";
            }
        })
        .filter('rawHtml', ['$sce', function($sce){
            return function(val) {
              return ($sce.trustAsHtml(val));
            };
        }])
        .filter('lastMessage', ['$sce', function($sce){
            return function(val) {
              if(val.length > 40) {
                val  = val.substring(0, 40) + "....";
              }
                return ($sce.trustAsHtml(val));

            };
        }])

function AuthController($scope, $rootScope, $state, $validation,modalFactory, modalService, authServiceHandler, storageServiceHandler){
	$scope.showPassword = false;
	if(authServiceHandler.isAuthenticated()) {
		return $state.go('myItem');
	}

	if(typeof $state.params != undefined && ($state.params.data != 'undefined' && $state.params.data)){
		$scope = angular.extend($scope, $state.params.data);
	} else {
		$scope.subscription_name = "FREE";
		$scope.subscription_type = "Monthly";
		$scope.subscription_amount = 0;
		$scope.subscription_id = 1;

	}

	$scope.loginError = "";
	$scope.forgotPasswordFlag = false;

	$scope.subscriptions = [{'id' : 'check', 'name' : 'FREE'}, {'id' : 'check1', 'name' : 'PERSONAL'}, {'id' : 'check2', 'name' : 'PROFESSIONAL'}];
	$scope.memberships = [{'id' : 'check4', 'name' : 'Monthly'}, {'id' : 'check5', 'name' : 'Annual'}];
	$scope.genderList = [{'id' : 0, 'value' : 'Gender'}, {'id' : 'male', 'value' : 'Male'}, {'id' : 'female', 'value' : 'Female'}];

	$scope.showUserPassword = function(value){
			$scope.showPassword = value;
	}

	$scope.login = function() {
		var loginRequest = authServiceHandler.login({'data' : {'email' : $scope.email, 'password' : $scope.password}});

		loginRequest.then(function(response) {
			$scope.handleLoginRequest(response);
		})
	}

	$scope.closeModal = function(modelId) {
		modalFactory.Close(modelId);
	}

	$scope.openModal = function(modelId) {
		modalFactory.Open(modelId);
	}

	$scope.activateForgotPassword = function() {
		$scope.forgotPasswordFlag = true;
	}

	$scope.deActivateForgotPassword = function() {
		$scope.forgotPasswordFlag = false;
	}


	$scope.updateSubscriptionType = function($subscriptionsType){
		$scope.subscription_name = $subscriptionsType.name;
		$scope.getSubscriptionCharges($scope.subscription_name, $scope.subscription_type);
	}

	$scope.updateSubscriptionPeriod = function($scubscriptionPeriod){
		$scope.subscription_type = $scubscriptionPeriod.name;
		$scope.getSubscriptionCharges($scope.subscription_name, $scope.subscription_type);
	}

	$scope.getSubscriptionCharges = function($subscriptionsType, $scubscriptionPeriod){

		var index = _.findLastIndex($scope.subscriptionList, {
			subscription_name : $subscriptionsType,
			subscription_type : ($scubscriptionPeriod == 'Annual') ? 'ANNUAL' : 'MONTHLY'
		});

		$amount = ((index != -1) ? $scope.subscriptionList[index].subscription_amount : "0");
		$scope.subscription_id = (index == -1) ? 1 : $scope.subscriptionList[index].id;

		switch($subscriptionsType) {
			case "PROFESSIONAL" : $scope.professionalAmount = $amount; break;
			case "PERSONAL" 	: $scope.personalAmount = $amount; break;
		}

		$scope.subscription_amount = $amount;
	}

	$scope.filterValues = function(formData) {
		$formData = {
			'subscription_id' : $scope.subscription_id
		};

		angular.forEach(formData, function(value, index){
			if(typeof value != 'function')
			$formData[index] = value;
		});

		angular.forEach(['registration_stage', 'user_id'], function(value, index){
			$formData[value] = (typeof $scope[value] != 'undefined') ? $scope[value] : 0;
		})

		return $formData;
	}

	$scope.getScedgeSubscription = function(formData) {
		authServiceHandler.getSubscription()
		.then(function(response){
			if(response.error_code == 200){
				$scope.subscriptionList = response.result;
			}
		});
	}


	$scope.form = {
		submit: function (form) {
			$validation.validate(form)
			.success(function(){
				authServiceHandler.registerUser({
					"data" : $scope.filterValues($scope.form)
				})
				.then(function(response) {
					$scope.groupAlertTitle = (typeof response.title != 'undefined') ? response.title : 'Welcome to scedge';
					$scope.groupAlertContent = response.msg_string;
					$scope.openModal('scedge-info-content');
				});

			})
			.error(function(){
				//console.log("invaid value");
			});
		}
	};

	$scope.gotoLogin = function(){
		$scope.closeModal('scedge-info-content');
		$state.go('login');
	}


	// Handle Login functionality
	$scope.handleLoginRequest = function(response) {
		if(response.error_code != 200) {
			if(response.error_code == 500) {
				// modalService.openModal({
				// 	templateUrl : 'scedge-info.html',
				// 	content : {
				// 		'modalMessage' : response.msg_string,
				// 		'modalTitle' : response.title,
				// 		'modalAction' : function(){
				// 			//$scope.handleRegistrationRequest(response);
				// 		}
				// 	}
				// });

				  $scope.groupAlertTitle = response.title;
				  $scope.groupAlertContent = response.msg_string;
					$scope.groupAlertAction = $scope.gotoJoin;
          $scope.openModal('scedge-logininfo-content');


			} else {
				$scope.loginError = response.msg_string;
			}
		} else {
			if(typeof response.token != 'undefined') {
				storageServiceHandler.setValue('token', 'Bearer ' + response.token);
				$state.go('myItem');
			} else {
				response.registration_stage = "" + response.registration_stage;
				switch(response.registration_stage) {
					case "1"  : $state.go('joinStep1', { verificationCode: response.verification_code, data: response.result}); break;
					case "2"  : $state.go('joinStep2', { verificationCode: response.verification_code, data: response.result}); break;
					case "3"  : $state.go('payment', { verificationCode: response.verification_code, data: response.result}); break;
					default   : $state.go('myItem'); break;
				}
			}
		}
	}

	$scope.gotoJoin = function(){
		$scope.closeModal('scedge-logininfo-content');
		$state.go('join');
	}


	// Handle Registration functionality
	$scope.handleRegistrationRequest = function(response) {
		if(response.error_code != 200) {
			if(response.error_code == 500) {
				/*modalService.openModal({
					templateUrl : 'scedge-info.html',
					content : {
							'modalMessage' : response.msg_string,
							'modalTitle' : response.title,
							'modalAction' : function(){
						}
					}
				});*/

				$scope.groupAlertTitle = response.title;
				$scope.groupAlertContent = response.msg_string;
				$scope.groupAlertAction = function(){};
				$scope.openModal('scedge-logininfo-content');

			} else {
				$scope.loginError = response.msg_string;
			}

		} else {
			if(typeof response.token != 'undefined') {
				storageServiceHandler.setValue('token', 'Bearer ' + response.token);
				$state.go('myItem');
			} else {
				response.registration_stage = "" + response.registration_stage;
				switch(response.registration_stage) {
					case "1"  : $state.go('joinStep1', { verificationCode: response.verification_code, data: []}); break;
					case "2"  : $state.go('joinStep2', { verificationCode: response.verification_code, data: []}); break;
					case "3"  : $state.go('payment', { verificationCode: response.verification_code, data: []}); break;
				}
			}
		}
	}

	// Reset password functionality
	$scope.resetPassword = function(){
		if($scope.resetPasswordEmail) {
			authServiceHandler.resetPassword({
				'data' : {
					'email' : $scope.resetPasswordEmail
				}
			}).then(function(response){
				if(response.error_code == 200) {
					$scope.resetPasswordEmail = "";
					/*modalService.openModal({
						templateUrl : 'scedge-info.html',
						size : 'lg',
						content : {
							'modalMessage' : response.msg_string,
							'modalTitle' : 'Successfully Processed',
							'modalAction' : function(){
								$modalInstance.close()
							}
						}
					});*/

					$scope.groupAlertTitle = response.title;
				  $scope.groupAlertContent = response.msg_string;
				  $scope.groupAlertAction =  function(){
							$scope.closeModal('scedge-logininfo-content');
							$state.go('login', { nextState: '', nextParams: ''}, {reload : true});
					};

					$scope.openModal('scedge-logininfo-content');


				} else {
					/*modalService.openModal({
						templateUrl : 'scedge-info.html',
						size : 'lg',
						content : {
							'modalMessage' : response.msg_string,
							'modalTitle' : response.title,
							'modalAction' : function($modalInstance){
								$modalInstance.close()
							}
						}
					});*/
					$scope.groupAlertTitle = response.title;
				  $scope.groupAlertContent = response.msg_string;
				  $scope.groupAlertAction =  function(){
							$scope.closeModal('scedge-logininfo-content');
							$state.go('login', { nextState: '', nextParams: ''}, {reload : true});
					};

					$scope.openModal('scedge-logininfo-content');
				}
			});
		}
	}


	if(typeof $rootScope.$$listeners['event:social-sign-in-success'] == 'undefined') {
		$rootScope.$on('event:social-sign-in-success', function(event, userDetails){
			if($state.current.name == 'login') {
				authServiceHandler.login({
					'data' : userDetails
				}).then(function(response){
					$scope.handleLoginRequest(response);
				});
			} else {
				authServiceHandler.socialRegisterUser({
					'data' : userDetails
				}).then(function(response){
					$scope.handleRegistrationRequest(response);
				});
			}
		});
	}

	if(typeof $rootScope.$$listeners['event:social-sign-out-success'] == 'undefined') {
		var deregisterSignOut = $rootScope.$on('event:social-sign-out-success', function(event, logoutStatus){

			socialLoginService.logout();
			authServiceHandler.logout({
				'data' : userDetails
			}).then(function(response){
				alert(response);
			});
		})
	}

	$scope.getScedgeSubscription();
}


function RProfileController($scope, $state, $stateParams, $validation, modalService, authServiceHandler, storageServiceHandler){
	if(typeof $stateParams.verificationCode != undefined){
		$scope.verification_code = $stateParams.verificationCode;
	} else {
		return $state.go('join');
	}

	$scope.genderList = [{
		'id' : 'Male',
		'value' : 'Male'
	},{
		'id' : 'Female',
		'value' : 'Female'
	}];

    var a = moment().format('YYYY');
    a = moment().subtract(15, 'years').format('YYYY');
    date = '12/31/'+a;
    $scope.currentDate = date;
    $scope.nowDate = new Date();
	$scope.nowDate = moment().subtract(15, 'years').calendar();
	$scope.activeProvince = false;
	$scope.activeCity = false;


	$scope.filterValues = function(formData) {
		$formData = {
			'subscription_id' :  $scope.user.subscription_id
		};

		angular.forEach(formData, function(value, index){
			if(typeof value != 'function') {
				$formData[index] = value;
			}
		});

		angular.forEach(['registration_stage', 'user_id'], function(value, index){
			$formData[value] = (typeof $scope.user[value] != 'undefined') ? $scope.user[value] : 0;
		});

		if(typeof  $scope.user['id'] != 'undefined') {
			$formData['user_id'] = $scope.user['id'];
		}

		return $formData;
	}

	$scope.getProvince = function(id){
		if(id){
			authServiceHandler.getProvince({
						'data' : {
							'country_id' : id
						}
					})
					.then(function(response){
						$scope.provinceList = (response.result.province.length) ? response.result.province : null;
						if(response.result.province.length){
							$scope.activeProvince = true;
						}
					});
		}
	}

	$scope.getCity = function(id){
		if(id){
			authServiceHandler.getCity({
						'data' : {
							'province_id' : id
						}
					})
					.then(function(response){
						$scope.cityList = (response.result.cities.length) ? response.result.cities : null;
						if(response.result.cities.length){
							$scope.activeCity = true;
						}
					});
		}
	}

	$scope.form = {
		submit: function (form) {
			$validation.validate(form)
			.success(function() {
				authServiceHandler.registerUser({
					"data" : $scope.filterValues($scope.form)
				})
				.then(function(response) {
					if(response.error_code == 200) {
						if(typeof response.token  != 'undefined') {
							storageServiceHandler.setValue('token', 'Bearer ' + response.token);
							return $state.go('myItem');
						} else {
							switch(response.registration_stage) {
								case 1  :  $state.go('joinStep1', { verificationCode: $scope.verification_code, data:''}); break;
								case 2  :  $state.go('joinStep2', { verificationCode: $scope.verification_code, data: ''}); break;
								case 3  :  $state.go('payment', { verificationCode: $scope.verification_code, data: ''}); break;
							}
						}
					} else {
						$scope.errorMessage = response.msg_string;
					}
				});

			})
			.error(function(){
				$scope.errorMessage = "Please fill the required field";
			});
		},
		skip: function(form) {
			authServiceHandler.registerUser({
				"data" : {
					'skip' : 1,
					'user_id' : $scope.user.id,
					'registration_stage' : $scope.user.registration_stage,
					'subscription_id' : $scope.user.subscription_id
				}
			})
			.then(function(response) {
				if(typeof response.token  != 'undefined') {
					storageServiceHandler.setValue('token', 'Bearer ' + response.token);
					return $state.go('myItem');
				} else {
					switch(Number(response.registration_stage)) {
						case 3  : response.registration_stage = 3;  $state.go('payment', {verificationCode: $scope.verification_code, data: ''}); break;
					}
				}
			});
		}
	};

	authServiceHandler.getVerificationDetail({
		'data' : {
			'verification_code' : $scope.verification_code
		}
	})
	.then(function(response){
		if(response.error_code == 200){
			$scope.user = response.result;
			$scope.countryList = response.others.countries;
		} else {
			$state.go('join');
		}
	});

	// Get subscription
	authServiceHandler.getSubscription()
	.then(function(response){
		if(response.error_code == 200){
			$scope.subscriptionList = response.result;
		}
	});
}

function VerificationController($scope, $rootScope, $state,
			$validation, modalFactory, modalService,
			authServiceHandler, storageServiceHandler){
	$scope.showPassword = false;
	if(authServiceHandler.isAuthenticated()) {
		return $state.go('myItem');
	}

	$scope.closeModal = function(modelId) {
		modalFactory.Close(modelId);
	}

	$scope.openModal = function(modelId) {
		modalFactory.Open(modelId);
	}

	$scope.gotoJoin = function(){
		$scope.closeModal('scedge-logininfo-content');
		$state.go('join');
	}

	$scope.gotoLogin = function(){
		$scope.closeModal('scedge-logininfo-content');
		$state.go('login');
	}

	$scope.resendActivationCode = function() {
			if($scope.email) {
					authServiceHandler.resendActivationCode({
						'data' : {
								'email' : $scope.email
						}
					})
					.then(function(response){
						switch(response.error_code){
							case 500 :   $scope.groupAlertTitle = response.title;
														$scope.groupAlertContent = response.msg_string;
														$scope.groupAlertAction = $scope.gotoJoin;
														$scope.openModal('scedge-logininfo-content');
														break;
							case 200 : 	$scope.groupAlertTitle = response.title;
														$scope.groupAlertContent = response.msg_string;
														$scope.groupAlertAction = $scope.gotoLogin;
														$scope.openModal('scedge-logininfo-content');
														break;
							default	 :	$state.go('login');
						}

					})
			}
	}


}


function ResetPasswordController($scope, $rootScope, $state,
			$validation, modalFactory, modalService,
			authServiceHandler, storageServiceHandler){

	$scope.validVerificationCode = false;
	if(authServiceHandler.isAuthenticated()) {
		return $state.go('myItem');
	}

	$scope.form = {};

	if(typeof $state.params != undefined && ($state.params.verificationCode != 'undefined' && $state.params.verificationCode)){
		$scope.verificationCode = $state.params.verificationCode;
	}


	$scope.closeModal = function(modelId) {
		modalFactory.Close(modelId);
	}

	$scope.openModal = function(modelId) {
		modalFactory.Open(modelId);
	}

	$scope.gotoJoin = function(){
		$scope.closeModal('scedge-logininfo-content');
		$state.go('join');
	}

	$scope.gotoLogin = function(){
		$scope.closeModal('scedge-logininfo-content');
		$state.go('login');
	}

	$scope.checkVerificationCode = function() {
			if($scope.verificationCode) {
					authServiceHandler.checkVerificationCode({
						'data' : {
								'verificationCode' : $scope.verificationCode
						}
					})
					.then(function(response){
						switch(response.error_code){
							case 500 :   $scope.groupAlertTitle = response.title;
														$scope.groupAlertContent = response.msg_string;
														$scope.groupAlertAction = $scope.gotoJoin;
														$scope.openModal('scedge-logininfo-content');
														break;
							case 200 :
													$scope.validVerificationCode = true;
													break;
							default	 :	$state.go('login');
						}

					})
			}
	}

	$scope.resetPassword = function() {
		  $scope.message = "";
			console.log($scope.form.newPassword);
			if(!$scope.form.newPassword) {
					$scope.message = "Please fill a valid password";
					return;
			}

			if(!$scope.form.repassword) {
					$scope.message = "Please retype your password";
					return;
			}

			if($scope.form.newPassword) {
					if($scope.form.newPassword != $scope.form.repassword) {
							$scope.message = "Password not matching";
							return
					}

					authServiceHandler.updateAccountPassword({
						'data' : {
								'activationCode' : $scope.verificationCode,
								'password' : $scope.form.newPassword
						}
					})
					.then(function(response){
						switch(response.error_code){
							case 500 :   $scope.groupAlertTitle = response.title;
														$scope.groupAlertContent = response.msg_string;
														$scope.groupAlertAction = $scope.gotoJoin;
														$scope.openModal('scedge-logininfo-content');
														break;

							case 200 :	storageServiceHandler.setValue('token', 'Bearer ' + response.token);
													$state.go('myItem');
													break;
							default	 :	$state.go('login');
						}

					})
			}
	}

	$scope.checkVerificationCode();


}

scedgeControllers
.controller('AuthController', ['$scope', '$rootScope', '$state', '$validation','ModalFactory', 'ModalService', 'AuthServiceHandler', 'StorageServiceHandler', AuthController])
.controller('VerificationController', ['$scope', '$rootScope', '$state', '$validation','ModalFactory', 'ModalService', 'AuthServiceHandler', 'StorageServiceHandler', VerificationController])
.controller('ResetPasswordController', ['$scope', '$rootScope', '$state', '$validation','ModalFactory', 'ModalService', 'AuthServiceHandler', 'StorageServiceHandler', ResetPasswordController])
.controller('RProfileController', ['$scope', '$state', '$stateParams', '$validation', 'ModalService', 'AuthServiceHandler', 'StorageServiceHandler', RProfileController])

function BrowseController($scope, $rootScope, $timeout, $state, modalFactory, modalService, itemServiceHandler, profileServiceHandler){

	$rootScope.notifications = profileServiceHandler.notifications();
	$timeout(function(){
		profileServiceHandler.removeUserNotifications({'data' : {'type' : 'browse'}})
							.then(function(response){
									$rootScope.notifications = response.result;
							})
	}, 1000);

	$scope.dateAsc = true;
	$scope.browseItems = {};
	$scope.filterData = {};
	$scope.currentPage = 1;
	$scope.activeSearchProvince = false;
	$scope.activeSearchCity = false;

	$scope.changeDateOrder = function(status) {
		$scope.dateAsc = status;
		$scope.searchBrowseItems(0);
	}



	$scope.openModal = function(id){
			$scope.reportAbuseError = "";
			modalFactory.Open(id);
	}

	$scope.closeModal = function(id){
			modalFactory.Close(id);
	}


	$scope.setPagination = function(result) {
			$scope.currentPage = result.current_page;
			$scope.totalPage = result.total;
	}

	$scope.resetBrowseItems = function() {
		$scope.filterData = {};
		$scope.searchText = "";
		$scope.closeModal('browse-filter-content');
		browseItemsList(null);
	}

	$scope.browseItems = [];

	function browseItemsList(params) {
		$scope.isRequestAllower = false;
		itemServiceHandler.browseItems(params)
						.then(function(response){
							$scope.isRequestAllower = true;
								$scope.setPagination(response.result)

								$scope.resultBrowseItems = (response.result.data.length) ? response.result.data : null;

								if($scope.browseItems.length) {
									for(var i=0; i<$scope.resultBrowseItems.length;i++) {
										$scope.browseItems.push($scope.resultBrowseItems[i]);
									}
								} else {
									$scope.browseItems = $scope.resultBrowseItems;
								}
								$scope.countryList = (response.others.countries.length) ? response.others.countries : null;
								$scope.userList = (response.others.users.length) ? response.others.users : null;
								$scope.closeModal('browse-filter-content');
						});
	}

	$scope.selectitem = function(selectedUserItem){
		$scope.selectedUserItem = selectedUserItem;
	}

	$scope.isRequestAllower = true;
	$scope.pageNumber = 1;

	$scope.filterBrowseItems = function(newPageNumber, oldPageNumber){
			if($scope.isRequestAllower) {
				 $scope.searchBrowseItems(++$scope.pageNumber);
			}
	}

	$scope.searchBrowseItems = function(pageNumber){

            $filter = {};

            if($scope.selectedUser){
            	$scope.filterData['user'] = $scope.selectedUser.title;
        	}

			if(pageNumber){
				$filter['page'] = pageNumber;
			}

			if($scope.searchText) {
					$scope.filterData['searchText'] = $scope.searchText;
			}

			if($scope.filterData) {
					$filter['filter'] = $scope.filterData;
			}

			$filter['sort'] = {
				'by_date' : ($scope.dateAsc) ? 'asc' : 'desc'
			}

			browseItemsList({
				'data' : $filter
			});
	}

	$scope.getProvince = function(id){
		if(id){
			itemServiceHandler.getProvince({
						'data' : {
							'country_id' : id
						}
					})
					.then(function(response){
						$scope.provinceList = (response.result.province.length) ? response.result.province : null;
						if(response.result.province.length){
							$scope.activeSearchProvince = true;
						}
					});
	    }
	}

    $scope.getCity = function(id){
	    if(id){
		    itemServiceHandler.getCity({
					'data' : {
						'province_id' : id
					}
				})
				.then(function(response){
					$scope.cityList = (response.result.cities.length) ? response.result.cities : null;
					if(response.result.cities.length){
						$scope.activeCity = true;
                        $scope.activeSearchCity = true;
					}
				});
	    }
    }

	browseItemsList($scope.filterData);
	itemServiceHandler.getCardGroup()
						.then(function(response){
							$scope.cardGroup = response.result;
						})
}


function BrowseDetailController($scope, $rootScope, $timeout, $state, modalFactory, modalService, itemServiceHandler, profileServiceHandler){
		if(typeof $state.params != undefined) {
			 if(typeof $state.params.cardId != 'undefined' && $state.params.cardId){
					$scope.cardId = $state.params.cardId;
 			}

		} else {
			$state.go("browse");
		}

		$rootScope.notifications = profileServiceHandler.notifications();
		$timeout(function(){
			profileServiceHandler.removeUserNotifications({'data' : {'type' : 'browse'}})
													 .then(function(response){
															$rootScope.notifications = response.result;
													 });
		}, 1000);

		$scope.openModal = function(id){
				modalFactory.Open(id);
		}

		$scope.closeModal = function(id){
				modalFactory.Close(id);
		}

		$scope.browseItems = {};
		$scope.filterData = {
			'notCardId' :  $scope.cardId
		};
		$scope.currentPage = 1;

		$scope.resetBrowseItems = function() {
			$scope.filterData = {
				'notCardId' :  $scope.cardId
			};
			$scope.closeModal('browse-filter-content');
			$scope.getOtherCards({
					'data' : {
						'filter' : $scope.filterData
					}
			})
		}

		$scope.resetAbuseList = function() {
			for(i=0;i<$scope.listAbuse.length;i++){
				if(document.getElementById($scope.listAbuse[i].id).checked == true){
					document.getElementById($scope.listAbuse[i].id).checked = false;
				}
			}

			$scope.closeModal('browse-abuse-content');

		}

		$scope.setPagination = function(result) {
				$scope.currentPage = result.current_page;
				$scope.totalPage = result.total;
		}

		function browseItems(params) {
				return itemServiceHandler.browseItems(params);
		}

		itemServiceHandler.getCardGroup()
				.then(function(response){
						$scope.cardGroup = response.result;
				})

		$scope.blockCard = function(card) {
			$scope.browse_card_id = card;
			itemServiceHandler.getReports()
				.then(function(response){
					$scope.listAbuse = response.result;
					$scope.openModal('browse-abuse-content');
				})

		}

		$scope.reportAbuse = function(){
			$scope.reportAbuseError = "";
			var card_id = $scope.browse_card_id;
			var abuseList = [];
			for(i=0;i<$scope.listAbuse.length;i++){
				if(document.getElementById($scope.listAbuse[i].id).checked == true){
					abuseList.push($scope.listAbuse[i].id);
				}
			}

			if(!abuseList.length) {
				$scope.reportAbuseError = "Please select a abuse type";
				return;
			}


			itemServiceHandler.reportAbuse({
				'data' : {
					'card_id' : card_id,
					'report_id' : abuseList
				}
			})
			.then(function(response){
				$scope.closeModal('browse-abuse-content');
				$scope.groupAlertTitle = 'SUCCESSFULLY REPORTED';
				$scope.groupAlertContent = 'ITEM HAS BEEN REPORTED SUCCESSFULLY';
				$scope.openModal('group-alert-content');
			});
		}



		browseItems({
				'data' : {
					'filter' : {
						'cardId' : $scope.cardId
					}
				}
			})
			.then(function(response){
					$scope.selectedCard = (response.result) ? response.result : null;
			});

	 $scope.getOtherCards = function($filter) {
			 browseItems($filter)
				 .then(function(response){
						 $scope.setPagination(response.result)
						 $scope.otherCard = (response.result.data.length) ? response.result.data : null;
						 $scope.closeModal('browse-filter-content');
				 });
	 }

		$scope.filterBrowseItems = function(newPageNumber, oldPageNumber) {
			  $filter = {
						'data' : {
							'page' : newPageNumber,
							'filter' : $scope.filterData
						}
				};
				$scope.getOtherCards($filter);
		}

		$scope.searchBrowseItems = function(){
				$scope.filterData["searchText"] = $scope.searchText;

				$scope.getOtherCards({
						'data' : {
								'filter' : $scope.filterData
						}
				});
		}

		$scope.getOtherCards({
				'data' : {
					'filter' : $scope.filterData
				}
		});
}

scedgeControllers
	.controller('BrowseDetailController', ['$scope', '$rootScope', '$timeout', '$state', 'ModalFactory', 'ModalService', 'ItemServiceHandler', 'ProfileServiceHandler', BrowseDetailController])
	.controller('BrowseController', ['$scope', '$rootScope', '$timeout', '$state','ModalFactory', 'ModalService', 'ItemServiceHandler', 'ProfileServiceHandler', BrowseController]);

function ChatController($scope, $rootScope, $timeout, $state, $location, $routeParams, $firebaseObject, authServiceHandler, storageServiceHandler, profileServiceHandler, ImageUploader, modalFactory){
		var chatRoomId = 0;
		$scope.chatRoomId = null;
		if(typeof $state.params != undefined){
			 if(typeof $state.params.chatroomId != 'undefined' && $state.params.chatroomId){
					$scope.chatRoomId = chatRoomId = $state.params.chatroomId;
			 }
		}


	  $scope.openModal = function(id) {
	    uploader.uploadedImage = "";
	    modalFactory.Open(id);
	  };

		$scope.closeModal = function(id) {
	    uploader.clearQueue();
	    modalFactory.Close(id);
	  };

		$rootScope.notifications = profileServiceHandler.notifications();
		$timeout(function(){
				profileServiceHandler.removeUserNotifications({'data' : {'type' : 'chat'}})
									.then(function(response){
											$rootScope.notifications = response.result;
								})
		}, 1000);

		$scope.isContactEmail = function(email) {
			return (_.indexOf($scope.authContactList, email) >= 0);
		}

		$scope.menuItems = [{
				text: "Copy Message",
				disabled: false
			}, {
				text:"Select All",
				disabled: false
			}, {
				text:"Remove Message",
				disabled: false
		}];

		var uploader = $scope.uploader = ImageUploader;

	  $scope.removeImageItems = function(){
	      console.log(uploader);
	  }

	  $scope.initializeUploader = function() {
	    uploader.onAfterAddingFile = function(item) {

	      item.croppedImage = '';
	      var reader = new FileReader();
	      reader.onload = function(event) {
	        $scope.$apply(function() {
	          item.image = event.target.result;
	        });
	      };
	      reader.readAsDataURL(item._file);
	    };
	  }

	  $scope.initializeUploader();

		//$scope.URL = "https://chatapp-f268c.firebaseio.com/";
		$scope.URL = "https://scedge-175411.firebaseio.com/";
                //$scope.URL = "https://scedge-dev.firebaseio.com/";

		$scope.userDetail = storageServiceHandler.getValue('userDetail');
		$scope.email = $scope.userDetail.email;
		//$scope.email = 'sunit@gmail.com';

		$scope.intialMessage = "Loading chat window...";
		$scope.intialButton = false;

		var userListRef = new Firebase($scope.URL + "userList");
		var userListRefObj = $firebaseObject(userListRef);
		userListRefObj.$bindTo($scope, "userList");

		userListRefObj.$loaded().then(function() {
			$authContactList = [];
			$validContactList = [];
			$authUserList = (_.where(userListRefObj, {'email' : $scope.email}));

			angular.forEach($authUserList[0].chatRoomMate , function(value, key){
					$authContactList.push(value.chatMate);
			});

			/*angular.forEach(userListRefObj , function(value, key){
					if(typeof value.email != 'undefined') {
						if(_.indexOf($authContactList, value.email) >= 0){
								$validContactList[key] = value;
						}
					}

			});*/

			$scope.authContactList = $authContactList;
			if(!$scope.authContactList.length) {
				$scope.intialMessage = "​ You have not added any contacts yet. Please add a contact to chat with them.";
				$scope.intialButton = true;
			}
		});



		userListRef.on("value", function(snapshot){
			var data = snapshot.val();

			var userIndex;
			var result = Object.keys(data).map(function(e, i){
				if(data[e].email == $scope.email){
					userIndex = e;
				}
				return [Object(e), data[e]];
			});
			$scope.userListData = result;

			for(var i=0;i<$scope.userListData.length;i++){
				if($scope.userListData[i][1].email == $scope.email){
					break;
				}
			}

			if(typeof $scope.userListData[i] != 'undefined')
				storageServiceHandler.setValue('uKey', btoa($scope.userListData[i][0].valueOf()));
		});

		var chatRoomRef = new Firebase($scope.URL + "chatRooms");
		var chatRoomRefObj = $firebaseObject(chatRoomRef);
		chatRoomRefObj.$bindTo($scope, "activeChatRooms");

		$scope.validUsers = [];
		chatRoomRefObj.$loaded().then(function() {
				_.each($scope.activeChatRooms, function(value, index){
						if(index == $scope.chatRoomId) {
								$scope.validUsers.push(value.createdBy);
								$scope.validUsers.push(value.createdFor);
						}
				});
		})

		var userListChatRoomMate = "";
		$scope.getLastMessage = function(item){
			userListChatRoomMate = _.findKey(item.chatRoomMate, {'chatMate' : $scope.email});
			if(typeof userListChatRoomMate == 'undefined'   || typeof $scope.activeChatRooms == 'undefined' || typeof $scope.activeChatRooms[item.chatRoomMate[userListChatRoomMate].chatRoom] == 'undefined'){
				return "";
			}
			else{
				if(typeof $scope.activeChatRooms[item.chatRoomMate[userListChatRoomMate].chatRoom].lastMessage == 'undefined'){
					return "";
				}
				else{
					return $scope.activeChatRooms[item.chatRoomMate[userListChatRoomMate].chatRoom].lastMessage.message;
				}
			}
		}

		$scope.getLastMessagePostedBy = function(item){
			userListChatRoomMate = _.findKey(item.chatRoomMate, {'chatMate' : $scope.email});
			if(typeof userListChatRoomMate == 'undefined'   || typeof $scope.activeChatRooms == 'undefined' || typeof $scope.activeChatRooms[item.chatRoomMate[userListChatRoomMate].chatRoom] == 'undefined'){
				return "";
			}
			else{
				if(typeof $scope.activeChatRooms[item.chatRoomMate[userListChatRoomMate].chatRoom].lastMessage == 'undefined'){
					return "";
				}
				else{
					return $scope.activeChatRooms[item.chatRoomMate[userListChatRoomMate].chatRoom].lastMessage.postedBy;
				}
			}
		}

		$scope.getLastMessageTime = function(item){
			userListChatRoomMate = _.findKey(item.chatRoomMate, {'chatMate' : $scope.email});
			if(typeof userListChatRoomMate == 'undefined' || typeof $scope.activeChatRooms == 'undefined' || typeof $scope.activeChatRooms[item.chatRoomMate[userListChatRoomMate].chatRoom] == 'undefined'){
				return "";
			}
			else{
				if(typeof $scope.activeChatRooms[item.chatRoomMate[userListChatRoomMate].chatRoom].lastMessage == 'undefined'){
					return "";
				}
				else{
					return $scope.activeChatRooms[item.chatRoomMate[userListChatRoomMate].chatRoom].lastMessage.postedDate;
				}
			}
		}

		$scope.unReadMessageCount = function(item){
			userListChatRoomMate = _.findKey(item.chatRoomMate, {'chatMate' : $scope.email});
			if(typeof userListChatRoomMate == 'undefined'  || typeof $scope.activeChatRooms == 'undefined' || typeof $scope.activeChatRooms[item.chatRoomMate[userListChatRoomMate].chatRoom] == 'undefined'){
				return "";
			}
			else{
				if(typeof $scope.activeChatRooms[item.chatRoomMate[userListChatRoomMate].chatRoom].chatMessages == 'undefined'){
					return "";
				} else {
					var count = 0;
					var data = Object.values($scope.activeChatRooms[item.chatRoomMate[userListChatRoomMate].chatRoom].chatMessages);
					for(var i=0; i<data.length; i++){
						if(data[i].isRead === false && data[i].postedBy !== $scope.email){
							count++;
						}
					}
					return count;
				}
			}
		}

		$scope.joinChat = function(index, item){
			$scope.uKey = atob(storageServiceHandler.getValue('uKey'));
			var chatRoomId = btoa($scope.email) + "-" + btoa(item.email);
			var chatRoomMateRef = new Firebase($scope.URL + "userList/" + $scope.uKey + "/chatRoomMate");
			var chatRoomMateBox = "";
			chatRoomMateRef.on('value', function(data){
				var data = data.val();
				chatRoomMateBox = (_.findKey(data, {"chatMate" :  item.email}));

				if(typeof chatRoomMateBox == 'undefined'){
					chatRoomMateRef.push({
						"chatMate" : item.email,
						"chatRoom" : chatRoomId
					});
				}	else {
					chatRoomId = data[chatRoomMateBox].chatRoom;
				}
			});

			var chatRoomMateRef = new Firebase($scope.URL + "userList/" + index + "/chatRoomMate");
			chatRoomMateRef.on('value', function(data){
				var data = data.val();
				chatRoomMateBox = (_.findKey(data, {"chatMate" : $scope.email}));

				if(typeof chatRoomMateBox == 'undefined'){
					chatRoomMateRef.push({
						"chatMate" : $scope.email,
						"chatRoom" : chatRoomId
					});
				} else {
					chatRoomId = data[chatRoomMateBox].chatRoom;
				}
			});

			var chatRoomIdRef = new Firebase($scope.URL + "chatRooms/" + chatRoomId);
			var chatRoomRef = new Firebase($scope.URL + "chatRooms/");
			var chatRoomBox = "";
			chatRoomRef.on('value', function(data){
				var data = data.val();
				chatRoomBox = (_.findKey(data, function(v, k){
					if(k === chatRoomId){
						return k;
					}
				}));

				if(typeof chatRoomBox == 'undefined'){
					chatRoomIdRef.set({
						createdBy: $scope.email,
						createdFor: item.email,
						createdDate: Date.now()
					});
				}
				else{
					chatRoomId = chatRoomBox;
				}
			});

			$location.path('message-inbox/' + chatRoomId);

		}

		$scope.getMessage = function(chatRoomId){
			var chatRoomIdRef = new Firebase($scope.URL + "chatRooms/" + chatRoomId);
			$scope.lastMessageRef = chatRoomIdRef.child('lastMessage');
			$scope.chatMessagesRef = chatRoomIdRef.child('chatMessages');
			var chatMessagesRefObj = $firebaseObject($scope.chatMessagesRef);
			chatMessagesRefObj.$bindTo($scope, "chatMessages");

			$scope.chatMessagesRef.on("value", function(snapshot){
				var data = snapshot.val();
				if(!data){
					return;
				}
				else{
					var result = Object.keys(data).map(function(e){
						return [Object(e), data[e]];
					});

					for(var i=0;i<result.length;i++){
						if(result[i][1].isRead == false && result[i][1].postedBy != $scope.email){
							$scope.myMessagesRef = chatRoomIdRef.child('chatMessages/' + result[i][0]);
							if(chatRoomId == $routeParams.roomId){
								$scope.myMessagesRef.update({
									isRead: true
								});
							}
							else{
								return;
							}
						}
					}
				}
			});
		}
		$scope.getMessage(chatRoomId);

		var pushMessage = function(){
			$scope.message = wdtEmojiBundle.render($scope.message);
			$scope.chatMessagesRef.push({
				postedBy: $scope.email,
				message: $scope.message,
				postedDate: Date.now(),
				isRead: false
			});
			$scope.lastMessageRef.update({
				postedBy: $scope.email,
				message: $scope.message,
				postedDate: Date.now()
			});
			$scope.message = '';
		}

		$scope.sendMessage = function($event){
			if(!($event.which == 13))
				return;
			if((!$scope.message) || $scope.message.length == 0)
				return;
			pushMessage();
		}

		$scope.sendOnClick = function(){
			if(!$scope.message || $scope.message.length == 0)
				return;
			pushMessage();
		}
}

scedgeControllers
	.controller('ChatController', ['$scope', '$rootScope', '$timeout', '$state', '$location', '$routeParams', '$firebaseObject' , 'AuthServiceHandler', 'StorageServiceHandler', 'ProfileServiceHandler', 'ImageUploader', 'ModalFactory', ChatController]);

function ContactController($scope, $rootScope, $timeout, $state, $timeout, modalFactory, modalService, itemServiceHandler, contactServiceHandler, profileServiceHandler){
	$scope.group = "";
	if(typeof $state.params != undefined){
	 	if(typeof $state.params.groupId != 'undefined' && $state.params.groupId) {
			$scope.group = Number($state.params.groupId) ? Number($state.params.groupId) : "";
			$scope.selectedItem = $scope.group;
		}
  }

	$rootScope.notifications = profileServiceHandler.notifications();

			$timeout(function(){
				profileServiceHandler.removeUserNotifications({'data' : {'type' : 'contact'}})
								.then(function(response){
										$rootScope.notifications = response.result;
							})
			}, 1000);

	$scope.contactItems = {};
	$scope.contactGroup = {};
	$scope.filterName = "";

	$scope.currentPage = 1;
	$scope.totalPage = 1;

	$scope.selectedSorting = "";
	$scope.sortingType = "asc"

	$scope.openModal = function(id) {
			modalFactory.Open(id);
	}

	$scope.openMenu = 0
	$scope.showHideMenu = function(id){
		  if($scope.openMenu != id) {
				$scope.openMenu = id;
				return;
			}

			$scope.openMenu = 0;
	}

	$scope.closeModal = function(id) {

		    if(id == 'contact-alert-content'){
		    	$state.reload();
		    }

		    if(id == 'group-alert-content'){
		    	$state.reload();
		    }

			modalFactory.Close(id);
	}

	$scope.setPagination = function(result) {
			$scope.currentPage = result.current_page;
			$scope.totalPage = result.total;

	}

	$scope.isEmpty = function(data){
		if(data) {
			if(typeof data == 'object') {
				return isEmpty(data);
			} else {
				return data.length;
			}
		}
		return true;
	}

	$scope.closeAlertPopup = function() {
		$scope.closeModal('group-alert-content');
	}

	$scope.closeAlertPopupAndReload = function() {
		$state.go('myContact', null, {reload: true, inherit: false});
		$scope.closeModal('group-alert-content');
	}

	$scope.deleteContactGroup = function(group) {
		$scope.closeModal('block-alert-content');
		$params = {
			'data' : {
				'group' : $scope.deleteGroupId
			}
		};

		 contactServiceHandler.removeContactGroup($params)
						.then(function(response){

								if(response.error_code == 200) {
											$scope.groupAlertTitle = 'Delete Group Request';
											$scope.groupAlertContent = 'Contact group has been deleted Successfully.';
											$scope.groupAlertAction = $scope.closeAlertPopupAndReload;
											$scope.openModal('group-alert-content');
								} else {
										$scope.groupAlertTitle = 'Problem Occured';
										$scope.groupAlertContent = 'Some problem occured, Please try after some time';
										$scope.groupAlertAction = $scope.closeAlertPopup;
										$scope.openModal('group-alert-content');
								}
						});
	}


	$scope.removeContactGroup = function(group){
		$scope.deleteGroupId = group;
		$scope.groupAlertTitle = 'Delete Group Request';
		$scope.groupAlertContent = 'Are you sure to delete your contact group?';
		$scope.groupAlertAction = $scope.deleteContactGroup;
		$scope.openModal('block-alert-content');
 }


	$scope.getContactGroup = function($params) {
		contactServiceHandler.getContactGroup($params)
						.then(function(response){
							$scope.contactGroup = (response.result.length) ? response.result : null;
						});
	}

	$scope.getContacts = function($params) {
		contactServiceHandler.getContacts($params)
						.then(function(response){
								$scope.setPagination(response.result);
								$scope.contactItems = (response.result.data.length) ? response.result.data : null;
						});
	}

	$scope.getPendingRequest = function(){
		contactServiceHandler.pendingRequest($params)
						.then(function(response){
							$scope.pendingData = response.result;
							// console.log($scope.pendingData);
						});
	}



    $scope.approveConnection = function(id){
    	// alert(id)
    	contactServiceHandler.approveConnection({
					'data' : {
						'userID' : id
					}
			})
			.then(function(response){
				$scope.groupAlertTitle = 'Approval Request';
				$scope.groupAlertContent = 'Contact request has been approved Successfully';
				$scope.groupAlertAction = $scope.closeAlertPopup;
				$scope.openModal('group-alert-content');
				$scope.getPendingRequest();
			});
    }

		$scope.rejectConnection = function(id){
    	// alert(id)
    	contactServiceHandler.rejectConnection({
											'data' : {
												'userID' : id
											}
										})
			.then(function(response){
				$scope.groupAlertTitle = 'Rejection Request';
				$scope.groupAlertContent = 'Contact request has been rejected Successfully';
				$scope.groupAlertAction = $scope.closeAlertPopup;
				$scope.openModal('group-alert-content');
				$scope.getPendingRequest();
			});
    }

	$scope.openDeleteContact = function(contact_group_id,id){

		 $scope.groupDeleteId = contact_group_id;
		 $scope.contactDeleteId = id;
		 $scope.openModal('delete-alert-content');
	}

	$scope.deleteContact = function(){
            contactServiceHandler.deleteContact({
			'data' : {
				'contact_group_id':$scope.groupDeleteId,
				id : $scope.contactDeleteId
			}
		    })
		    .then(function(response){
					$scope.closeModal('delete-alert-content');
					openModal('delete-success-content');
					$scope.getContacts(null);
					$state.reload();
		    })
	}


	$scope.openContactPopup = function() {
			$scope.mygroup = {
					'group_image' : '',
					'title' : '',
					'description': ''
			};
			$scope.openModal('contact-add-content');
	}

	$scope.addContactGroup = function() {
				contactServiceHandler.addContactGroup({
					'data' : $scope.mygroup
				})
				.then(function(response) {
						if(response.error_code == 200) {
								//$scope.contactGroup = (response.result.length) ? response.result : null;
								$scope.closeModal('contact-add-content');
								$scope.alertTitle = "Success";
								$scope.alertContent = "Group has been created";
								modalFactory.Open('contact-alert-content');
								// $state.go('myContact', { nextState: '', data: []});

						} else {
							$scope.createGroupError = response.msg_string;
						}
				})
	}

	$scope.filterContactItems = function(newPageNumber, oldPageNumber){
			$params = {
					'data' : {
							'filter' : {
								'group' : $scope.group,
								'name'	: $scope.filterName,
								'page' 	: newPageNumber
							}
					}
			}
			$scope.getContacts($params);
	}

	$scope.changeSortType = function() {
		$scope.sortingType = ($scope.sortingType  == 'asc') ? 'desc' : 'asc';

		if($scope.selectedSorting)
			$scope.sortContactItems($scope.selectedSorting)
	}


	$scope.filterContact = function(filterBy){
			if($scope.group) {
				filterBy.data.filter['group'] = $scope.group;
			}
			$scope.getContacts(filterBy);
	}

	$scope.sortContactItems = function(sort) {
		var sortBy = {};
		$scope.selectedSorting = sort;

		switch(sort) {
			case 'name' : sortBy.sortByName = $scope.sortingType;
		}

		var myJsonString = JSON.stringify(sortBy);
		$scope.filterContact({'data' : {'filter' : {'sortBy' : sortBy}}});
	}

	$scope.filterResultByGroup = function(group) {
		  if(group) {
				$scope.group = group;
				$scope.selectedItem = group;
				$scope.filterResult();
			} else {
				$scope.getContacts(null);
			}
	}

	$scope.filterGroupByType = function(filterType){
			contactServiceHandler.getContactGroup((filterType) ? {'data' : {'group' : filterType}} : null)
						.then(function(response){
								$statePar = null;
								if(response.result && response.result.length) {
									$statePar = { 'groupId' : response.result[0].parent_group_id };
								}

								$state.go($state.current, $statePar, {reload: true, inherit: false});
						});
	};

	$scope.filterResultByName = function(searchData){
		  alert(searchData);
	}

	$scope.filterResult = function() {
			$params = {
					'data' : {
							'filter' : {
									'group' : $scope.group,
									'name'	: $scope.filterName
							}
					}
			}
			$scope.getContacts($params)
	}

	$params = null;
	if($scope.group) {
		$params = {
				'data' : {
						'filter' : {
								'group' : $scope.group
						}
				}
		};

	}

	$scope.getContacts($params);
	$scope.getContactGroup({
			'data' : {
					'group' : $scope.group
			}
	});



	$scope.sendInvitation = function() {
			if($scope.friendEmail) {
					contactServiceHandler.sendInvitation(
								{'data' :
									{'email' : $scope.friendEmail}
								})
								.then(function(response){

									 if(response.error_code == 200) {
										 $scope.friendEmail = "";

										  $scope.groupAlertTitle = 'Invitation Send';
							 				$scope.groupAlertContent = response.msg_string;
											$scope.groupAlertAction = $scope.closeAlertPopup;
							 				$scope.openModal('group-alert-content');
									 } else {
										  $scope.groupAlertTitle = 'Inviation Error';
											$scope.groupAlertContent =  response.msg_string;
											$scope.groupAlertAction = $scope.closeAlertPopup;
											$scope.openModal('group-alert-content');
									 }
								});
			} else {
				$scope.groupAlertTitle = 'Inviation Error';
				$scope.groupAlertContent =  'Please enter a valid email';
				$scope.groupAlertAction = $scope.closeAlertPopup;
				$scope.openModal('group-alert-content');
			}
 }

	 $scope.reportAbuse = function(userId) {
		 $scope.reportedUserId = userId;
		 itemServiceHandler
		 	 .getReports()
			 .then(function(response){
				 $scope.listAbuse = response.result;
				 $scope.openModal('contact-abuse-content');
			 })
		}

		$scope.cancelReportAbuse = function(userId) {
				$scope.closeModal('contact-abuse-content');
		}

	  $scope.sendReportAbuse = function(userId) {
			 var abuseList = [];
			 for(i=0;i<$scope.listAbuse.length;i++){
				 if(document.getElementById($scope.listAbuse[i].id).checked == true){
					 abuseList.push($scope.listAbuse[i].id);
				 }
			 }

			 if(!abuseList.length) {
				 $scope.reportAbuseError = "Please select a abuse type";
				 return;
			 }

			 contactServiceHandler.reportAbuse({
					 'data' : {
						 'user_id' : $scope.reportedUserId,
						 'report_id' : abuseList
					 }
				})
			 	.then(function(response){
					$scope.closeModal('contact-abuse-content');
					$scope.groupAlertTitle = 'Report Abuse Request';
					$scope.groupAlertContent = 'User has been reported to scedge admin';
					$scope.groupAlertAction = $scope.closeAlertPopup;
					$scope.openModal('group-alert-content');
				});
		}


   $scope.processBlockRequest = function() {
				 	contactServiceHandler.blockUser({
						'data' : {
							'user_id' : $scope.blockUserId
						}
					})
					.then(function(response){
						  $scope.closeModal('block-alert-content');
							$scope.groupAlertTitle = 'Block User Request';
							$scope.groupAlertContent = response.msg_string;
							$scope.groupAlertAction = $scope.closeAlertPopup;
							$scope.openModal('group-alert-content');
					});
		}


	 $scope.blockUser = function(userId) {
		 $scope.blockUserId = userId;
		 $scope.groupAlertTitle = 'Block User Request';
		 $scope.groupAlertContent = 'Are you sure to block your contact?';
		 $scope.groupAlertAction = $scope.processBlockRequest;
		 $scope.openModal('block-alert-content');
   }

	$scope.getPendingRequest(null);
}

scedgeControllers
	.controller('ContactController', ['$scope', '$rootScope', '$timeout', '$state', '$timeout', 'ModalFactory', 'ModalService', 'ItemServiceHandler', 'ContactServiceHandler', 'ProfileServiceHandler', ContactController]);

function HomeController($scope, $state, modalFactory, authServiceHandler, storageServiceHandler, socialshare) {
	$scope.subscriptionType = false;
	$scope.currentTab = 'tab1';

	$scope.closeModal = function(modelId) {
		modalFactory.Close(modelId);
	}

	$scope.openModal = function(modelId) {
		modalFactory.Open(modelId);
	}

	$scope.facebookShare = function() {
		socialshare.share({
				'provider': 'facebook',
				'attrs': {
					'socialshareUrl': 'http://scedge.apphosthub.com'
				}
			});
	}

	$scope.twitterShare = function() {
		socialshare.share({
				'provider': 'twitter',
				'attrs': {
					'socialshareUrl': 'http://scedge.apphosthub.com'
				}
			});
	}

	$scope.linkedinShare = function() {
		socialshare.share({
				'provider': 'linkedin',
				'attrs': {
					'socialshareUrl': 'http://scedge.apphosthub.com'
				}
			});
	}

	$scope.pinterestShare = function() {
		socialshare.share({
				'provider': 'pinterest',
				'attrs': {
					'socialshareUrl': 'http://scedge.apphosthub.com'
				}
			});
	}
	$scope.contentType = 'pricing';
	$scope.changeContentType = function(contentType) {
		$scope.contentType = contentType
	}

	$scope.currentSection = function(section) {
		$scope.currentTab = section;
	}

	$scope.getSubscriptionSaving = function($subscriptionsType, $scubscriptionPeriod){
		$subscriptionType = ($scubscriptionPeriod) ? 'ANNUAL' : 'MONTHLY'
		if($subscriptionType == 'ANNUAL') {
			switch ($subscriptionsType) {
				case 'PERSONAL': return "(Save $10)";
				case 'PROFESSIONAL': return "(Save $30)";
			}
		}

		return "";
	}

	$scope.getSubscriptionCharges = function($subscriptionsType, $scubscriptionPeriod){

		var index = _.findLastIndex($scope.subscriptionList, {
			subscription_name : $subscriptionsType,
			subscription_type : ($scubscriptionPeriod) ? 'ANNUAL' : 'MONTHLY'
		});

		$amount = ((index != -1) ? $scope.subscriptionList[index].subscription_amount : "0");

		switch($subscriptionsType) {
			case "PROFESSIONAL" : $scope.professionalAmount = $amount; break;
			case "PERSONAL" 	: $scope.personalAmount = $amount; break;
		}

		return "$" + $amount;
	}

	authServiceHandler.getSubscription()
	.then(function(response){
		if(response.error_code == 200){
			$scope.subscriptionList = response.result;
		}
	});
}

function ActivateController($scope, $state, $stateParams, modalService,modalFactory, authService){

	$scope.closeModal = function(modelId) {
		modalFactory.Close(modelId);
	}

	$scope.openModal = function(modelId) {
		modalFactory.Open(modelId);
	}

	$scope.activateUser = function() {
		authService.activeUser({
			"data" : {
				'verification_code' : $stateParams.verificationCode
			}
		})
		.then(function(response) {
			var modalContent = {
				'modalMessage' : response.msg_string,
				'modalTitle'   : 'Welcome to Scedge',
				'modalAction' : ''
			};

			if(response.error_code == 200) {
				$scope.successResult = response.result;
				$scope.showAlertTitle = "Successfully Verified";
				$scope.showAlertContent = response.msg_string;
				$scope.openModal('activate-content');
				$scope.checkSuccess = true;
			} else {
				$scope.showAlertTitle = "Invalid Request";
				$scope.showAlertContent = response.msg_string;
				$scope.openModal('activate-content');
				$scope.checkSuccess = false;
			}
		});
	}

	$scope.gotoStep1 = function(){
		if($scope.checkSuccess){
			$state.go('joinStep1', {verificationCode: $stateParams.verificationCode, data: $scope.successResult});
		}else{
			$state.go('join', { nextState: '', data: []});
		}
		$scope.closeModal('activate-content');
	}

	if(typeof $stateParams.verificationCode != 'undefined') {
		$scope.activateUser();
	}
}


scedgeControllers.controller('ModalInstanceCtrl', function ($uibModalInstance,$state, ModalService) {
	var $ctrl = this;
	$ctrl.modalData =  ModalService.getModal();

	$ctrl.message = "";

	$ctrl.ok = function (data) {
		$uibModalInstance.close();
		$ctrl.modalData.modalAction();
		if(data == 'login'){
			$state.go('join');
		}
	};

	$ctrl.close = function () {
		$uibModalInstance.close();
	}

	$ctrl.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};

});



scedgeControllers
.controller('HomeController', ['$scope', '$state', 'ModalFactory', 'AuthServiceHandler', 'StorageServiceHandler', 'Socialshare', HomeController])
.controller('ActivateController', ['$scope', '$state', '$stateParams', 'ModalService','ModalFactory', 'AuthServiceHandler', ActivateController]);

function ItemController($scope, $rootScope, $timeout, $state, $window, $location, $timeout, modalService, modalFactory, authServiceHandler, itemServiceHandler, ImageUploader, profileServiceHandler, contactServiceHandler, toastr, socialshare, MONTHS) {
    $scope.groupId = "";
    $scope.gPlace = "";

	  $scope.form = {
      shareToContact : []
    };
	  $scope.filterData = {};
	  $scope.selectedFilter = 'all';
	  $scope.priorityList = ['Low', 'Medium', 'High'];
	  $scope.activeProvince = false;
	  $scope.activeCity = false;
	  $scope.activeSearchProvince = false;
	  $scope.activeSearchCity = false;
	  $scope.groupFlag = true;
	  $scope.showSelect = true;
	  $scope.currentDate = new Date();
	  $scope.selectedSorting = "";
	  $scope.sortingType = "asc";
	  $scope.slideDown = false;
	  $scope.slideDown1 = false;
	  $scope.checkedUserItem = [];

	  $scope.slideDown = false;
	  $scope.slideDown1 = false;
	  $scope.checkedUserItem = [];
	  $scope.availableMonths = MONTHS.NAME;
	  $scope.form.selectForShare
	  $scope.selectSharedFacebook = 0;
	  $scope.selectSharedTwitter = 0;

	  $scope.currentYear = (new Date()).getFullYear();
	  $scope.currentMonth = (new Date()).getMonth();
	  $scope.currentDate = (new Date()).getDate();

  $scope.uploadImage = function(){
    $scope.openModal('cropper-view-modal');
  }

  $scope.facebookShare = function(cardId) {
    socialshare.share({
      'provider': 'facebook',
      'attrs': {
        'socialshareUrl': 'http://scedge.apphosthub.com/item/' + cardId
      }
    });
  }

  $scope.twitterShare = function(cardId) {
    socialshare.share({
      'provider': 'twitter',
      'attrs': {
        'socialshareUrl': 'http://scedge.apphosthub.com/item/' + cardId
      }
    });
  }

  $scope.linkedinShare = function(cardId) {
    socialshare.share({
      'provider': 'linkedin',
      'attrs': {
        'socialshareUrl': 'http://scedge.apphosthub.com/item/' + cardId
      }
    });
  }

  function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }


  $scope.shareItemViaEmail = function(card) {
    if (!$scope.shareEmail) {
      toastr.error('Please provide a valid Email', 'Validation Error');
    } else {
      if (!validateEmail($scope.shareEmail)) {
        toastr.error('Please provide a valid Email', 'Validation Error');
      } else {
        itemServiceHandler.shareItemViaEmail({
            'data': {
              'email': $scope.shareEmail,
              'card': card.card_type_id
            }
          })
          .then(function(response) {
            toastr.success('Card has been shared with ' + $scope.shareEmail, 'Successfully Shared');
            $scope.closeModal('item-share-content');
          })
      }
    }
  }

  if (typeof $state.params != undefined) {
    if (typeof $state.params.groupId != 'undefined' && $state.params.groupId) {
      $scope.groupId = Number($state.params.groupId) ? Number($state.params.groupId) : "";
      $scope.selectedItem = $scope.groupId;
    }
  }

  $rootScope.notifications = profileServiceHandler.notifications();

  $timeout(function() {
    profileServiceHandler.removeUserNotifications({
        'data': {
          'type': 'item'
        }
      })
      .then(function(response) {
        $scope.notifications = $rootScope.notifications = response.result;
      })
  }, 1000);

  $scope.getMonthArray = function(monthStart, monthEnd, type) {
        var validMonths = {};

        if($scope.form.endDateYear && $scope.form.startDateYear) {
            if($scope.form.endDateYear == $scope.form.startDateYear) {
                switch(type) {
                  case "start" : if($scope.form.endDateMonth) {
                                      monthEnd = $scope.form.endDateMonth - 1;
                                 }

                  case "end" :   if($scope.form.startDateMonth) {
                                      monthStart = $scope.form.startDateMonth - 1;
                                 }
                }
            }
        }


        for (var i = monthStart; i <= monthEnd; i++) {
          for(var month in $scope.availableMonths[i]) {
              validMonths[month] = $scope.availableMonths[i][month];
          }
        }

        return validMonths;
  }

  $scope.getValidMonth = function(year, type) {
    if (year) {
      year = Number(year);
      var validMonths = $scope.availableMonths;
      var monthStart = 0;
      var monthEnd = $scope.availableMonths.length - 1;
      if (year == $scope.currentYear) {
        monthStart = $scope.currentMonth;
      }
      var validMonths = $scope.getMonthArray(monthStart, monthEnd, type);

      if (type == 'start') {
	        $scope.endYear = [];
	        for (var startYear = year; startYear <= (year + 100); startYear++) {
	          $scope.endYear.push(startYear);
	        }

	        $scope.startMonth = validMonths;
      }

      if (type == 'end') {
	        $scope.startYear = [];
	        for (var startYear = $scope.currentYear; startYear <= (year + 100); startYear++) {
	          $scope.startYear.push(startYear);
	        }
	        $scope.endMonth = validMonths;
      }
    }
  }


  $scope.createYears = function() {
      $scope.startYear = [];
      $scope.endYear = [];
      for (var i = $scope.currentYear; i <= ($scope.currentYear + 100); i++) {
        ($scope.startYear).push(i);
        ($scope.endYear).push(i);
      }
  }

  $scope.createTimeArray = function() {
    $scope.timeArray = [];
    for (var i = 1; i <= 12; i++) {
      for (var j = 0; j <= 59; j++) {
        $scope.timeArray.push(((i < 10) ? "0" + i : i) + ":" + ((j < 10) ? "0" + j : j));
      }
    }
  }

  $scope.form.timePeriod = "AM";



  $scope.isLeapYear = function(yearSelected) {
    return (((yearSelected % 4 == 0) && (yearSelected % 100 != 0)) ||
      (yearSelected % 400 == 0))
  }

  $scope.isAllowedToCompare = function(){
    reutrn (($scope.form.startDateYear && $scope.form.endDateYear) && ($scope.form.startDateMonth && $scope.form.startDateMonth))
  }

  $scope.getValidDays = function(type){

  }


  $scope.getValidTotalDays = function(yearSelected, monthSelected) {
    if (monthSelected == 2) {
      return ($scope.isLeapYear(yearSelected)) ? 28 : 29;
    }
    var monthWith31 = [1, 3, 5, 7, 8, 10, 12];
    return (monthWith31.indexOf(monthSelected) == -1) ? 30 : 31;
  }

  $scope.getDateArray = function(yearSelected, monthSelected) {
    if(yearSelected && monthSelected) {
      var totalDays = 0;
      var daysArray = [];
      var startFrom = 1;

      if ($scope.currentYear == yearSelected) {
        if (monthSelected == ($scope.currentMonth + 1)) {
          startFrom = $scope.currentDate;
        }
      }

      totalDays = $scope.getValidTotalDays(yearSelected, monthSelected);

      for (var j = startFrom; j <= totalDays; j++) {
        (daysArray).push(j);
      }
      return daysArray;
    }
  }

  $scope.getValidDates = function(type) {
    var monthStart = 0;
    var monthEnd = $scope.availableMonths.length -1;
    if (type == 'start') {
      $scope.startDays = $scope.getDateArray($scope.form.startDateYear, $scope.form.startDateMonth);
      monthStart = $scope.form.startDateMonth - 1;
      $scope.endMonth = $scope.getMonthArray(monthStart, monthEnd, type);
   } else {
      $scope.endDays = $scope.getDateArray($scope.form.endDateYear, $scope.form.endDateMonth);
      monthEnd = $scope.form.endDateMonth - 1;
      $scope.startMonth = $scope.getMonthArray(monthStart, monthEnd, type);
    }
  }



  $scope.createAge = function() {
    $scope.ageYears = [];
    for (var i = 10; i <= 60; i++) {
      ($scope.ageYears).push(i);
    }
  }

  $scope.createAge();


  $scope.disbledEmail = function() {
    if ($scope.form.shareToContact == undefined || $scope.form.shareToContact == null || !$scope.form.shareToContact) {
      $scope.emailShowValue = false;
    } else {
      $scope.emailShowValue = true;
    }
  }

  $scope.disableContactList = function() {

    if (!$scope.form.email) {
      $scope.contactShowValue = false;
    } else {
      $scope.contactShowValue = true;
    }
  }

  $scope.selectFacebook = function() {

    if ($scope.selectSharedFacebook == 0 || $scope.selectSharedFacebook == undefined) {
      $scope.selectSharedFacebook = 1;
      ($scope.form.selectForShare).push('Facebook')
    } else {
      $scope.selectSharedFacebook = 0;
      ($scope.form.selectForShare).pop('Facebook')
    }
  }

  $scope.selectTwitter = function() {
    if(!$scope.selectForShare)
        $scope.selectForShare = [];

    if ($scope.selectSharedTwitter == 0 || $scope.selectSharedTwitter == undefined) {
      $scope.selectSharedTwitter = 1;
      ($scope.selectForShare).push('Twitter')
    } else {
      $scope.selectSharedTwitter = 0;
      ($scope.selectForShare).pop('Twitter')
    }
  }


  $scope.getContacts = function($params) {
    contactServiceHandler.getContacts($params)
      .then(function(response) {
          $scope.setPagination(response.result);
          $scope.contactList = (response.result.data.length) ? response.result.data : null;

          $scope.contactItems = [];

          _.each($scope.contactList, function(value, index){
              $scope.contactItems.push({id : value.id, label : value.name});
          })
      });
  }


  $scope.getContacts();

  $scope.slideDownFun = function() {
    $scope.slideDown = !$scope.slideDown
  }
  $scope.slideDownFun1 = function() {
    $scope.slideDown1 = !$scope.slideDown1
  }



  $scope.createItem = function() {
    $scope.error = [];

    if (!$scope.form.title) {
      $scope.form.titleError = "Title is required";
      return;
    } else {
      $scope.form.titleError = "";
    }

    if ($scope.form.startDateYear || $scope.form.startDateMonth || $scope.form.startDayDate) {
        if (!$scope.form.startDateMonth) {
          $scope.error.push('Please select valid Month')

        }
        if (!$scope.form.startDayDate) {
          $scope.error.push('Please select valid Day')
        }
        if (!$scope.form.startDateYear) {
          $scope.error.push('Please select valid Year')
        }
    }

    if ($scope.userDetail.subscription_name != 'FREE') {
      if($scope.form.endDateYear || $scope.form.endDateMonth || $scope.form.endstartDayDate) {
        if (!$scope.form.endDateYear) {
          $scope.error.push('Please select valid Year')
        }
        if (!$scope.form.endDateMonth) {
          $scope.error.push('Please select valid Month')

        }
        if (!$scope.form.endDayDate) {
          $scope.error.push('Please select valid Day')
        }
      }
    }


    if ($scope.form.ageFrom || $scope.form.ageTo) {
      if (!$scope.form.ageFrom) {
        $scope.error.push('Please select Age From')
      }
      if (!$scope.form.ageTo) {
        $scope.error.push('Please select Age To')
      }
      if ($scope.form.ageTo <= $scope.form.ageFrom) {
        $scope.error.push('Age To must be greater Than Age From')
      }
    }

    if (($scope.error).length > 0) {
      for (var i = 0; i < ($scope.error.length); i++) {
        toastr.error($scope.error[i], 'Validation Error');
      }
      return;
    }

    if ($scope.form.startDateYear && $scope.form.startDateMonth && $scope.form.startDayDate) {
      $startDateValue = $scope.convertToDate(['startDateYear', 'startDateMonth', 'startDayDate']);
      if ($startDateValue) {
        $scope.form.start_date = $startDateValue;
      }
    }

    if ($scope.form.startTime && $scope.form.startTimePeriod) {
      $startTimeValue = $scope.convertToTime($scope.form.startTime, $scope.form.startTimePeriod);
      if ($startTimeValue) {
        $scope.form.start_time = $startTimeValue;
      }
    }


    if ($scope.userDetail.subscription_name != 'FREE') {
      if ($scope.form.endDateYear && $scope.form.endDateMonth && $scope.form.endDayDate) {
        $startDateValue = $scope.convertToDate(['endDateYear', 'endDateMonth', 'endDayDate']);
        if ($startDateValue) {
          $scope.form.start_date = $startDateValue;
        }
      }

      if ($scope.form.endTime && $scope.form.endTimePeriod) {
        $startTimeValue = $scope.convertToTime($scope.form.endTime, $scope.form.endTimePeriod);
        if ($startTimeValue) {
          $scope.form.start_time = $startTimeValue;
        }
      }
    }


    $scope.createItemResponse = "";
    if ($scope.userDetail.subscription_name == 'PROFESSIONAL') {
      if ($scope.form.live_item == true) {
        $scope.form.live_item = true;
      } else {
        $scope.form.live_item = false;
      }
    }

    if ($scope.userDetail.subscription_name == 'PERSONAL') {
      if ($scope.form.two_way_sync == true) {
        $scope.form.two_way_sync = true;
      } else {
        $scope.form.two_way_sync = false;
      }
    }


    if ($scope.form.share_on_showcase == true) {
      $scope.form.share_on_showcase = true;
    } else {
      $scope.form.share_on_showcase = false;
    }

    if($scope.groupId){
      $scope.form.card_type_id = $scope.groupId;
    }

    if (uploader.uploadedImage)
      $scope.form.card_image = uploader.uploadedImage


   itemServiceHandler.createItem({
        'data': {
          'item': $scope.form
        }
      })
      .then(function(response) {
        if (response.error_code == 200) {
          $scope.closeItem();
          //$scope.alertTitle = "Success";
          //$scope.alertContent = "Card has been created";
          //modalFactory.Open('item-alert-content');
          //
          $scope.getItems(null);
          toastr.success('Card has been created', 'Success', {
            onShown : function() {
              $scope.getItems(null);
              $state.reload();
            }
          });
        } else {
          toastr.error(response.msg_string, 'Error');
          $scope.createItemResponse = response.msg_string;
        }

        $scope.closeItem();
      });

  };

  $scope.editItem = function(formDataset) {
    $scope.createTimeArray();
    $scope.createYears();

    var userItems = _.extend({}, formDataset);
    userItems = validateValues(userItems);

    $scope.selectedItem = userItems;
    $scope.form = userItems;
    $scope.form.shareToContact = [];
    if (!$scope.form.selectForShare) {

    } else if ($scope.form.selectForShare.length > 0) {
      var indexFacebook = ($scope.form.selectForShare).indexOf('Facebook')
      $scope.selectSharedFacebook = (indexFacebook < 0) ? 0 : 1;
      var indexTwitter = ($scope.form.selectForShare).indexOf('Twitter')
      $scope.selectSharedTwitter = (indexTwitter < 0) ? 0 : 1;
    }
    $scope.form.location_country = ($scope.form.location_country) ? $scope.form.location_country.toString() : "";
    $scope.form.street_address = ($scope.form.street_address) ? $scope.form.street_address.toString() : "";
    $scope.form.location_province = ($scope.form.location_province) ? $scope.form.location_province.toString() :"";
    $scope.form.location_city = ($scope.form.location_city) ? $scope.form.location_city.toString() : "";
    $scope.form.discount = Number($scope.form.discount);

    if(userItems.start_date){
        var dateArray = (userItems.start_date).split("-");
        $scope.form.startDateYear = Number(dateArray[0]);
        $scope.form.startDateMonth = Number(dateArray[1]);
        $scope.form.startDayDate = Number(dateArray[2]);


        $scope.getValidMonth(dateArray[0], 'start');
        $scope.getValidDates('start');
    }

    if(userItems.start_time){
        $scope.form.startTime = userItems.start_time;
        $scope.form.startTimePeriod = 'AM';
    }

    if(userItems.end_date){
        var dateArray = (userItems.end_date).split("-");
        $scope.form.endDateYear = Number(dateArray[0]);
        $scope.form.endDateMonth = Number(dateArray[1]);
        $scope.form.endDayDate = Number(dateArray[2]);

        $scope.getValidMonth(dateArray[0], 'end');
        $scope.getValidDates('end');
    }

    if(userItems.end_time){
        $scope.form.endTime = userItems.end_time;
        $scope.form.endTimePeriod = 'AM';
    }



    if ($scope.userDetail.subscription_name == 'PROFESSIONAL') {
      if ($scope.form.live_item == true) {
        //document.getElementById("liveItem").checked = true;
      } else {
        // document.getElementById("liveItem").checked = false;
      }
    }

    if ($scope.userDetail.subscription_name == 'PERSONAL') {
      if (formDataset.two_way_sync == 1) {
        //document.getElementById("syncToWay").checked = true;
      } else {
        //document.getElementById("syncToWay").checked = false;
      }
    }

    if ($scope.form.share_on_showcase == 1) {
      //document.getElementById("shareOnShowcase").checked = true;
    }

    if (formDataset.share_on_showcase == 0) {
      //document.getElementById("shareOnShowcase").checked = false;
    }

    itemServiceHandler.getProvince({
        'data': {
          'country_id': formDataset.location_country
        }
      })
      .then(function(response) {
        $scope.provinceList = (response.result.province.length) ? response.result.province : null;
      });

    itemServiceHandler.getCity({
        'data': {
          'province_id': formDataset.location_province
        }
      })
      .then(function(response) {
        $scope.cityList = (response.result.cities.length) ? response.result.cities : null;
        $scope.openModal('item-form-modal');
      });


  }



  var uploader = $scope.uploader = ImageUploader;

  $scope.removeImageItems = function(){
      console.log(uploader);
  }

  $scope.initializeUploader = function() {
    uploader.onAfterAddingFile = function(item) {

      item.croppedImage = '';
      var reader = new FileReader();
      reader.onload = function(event) {
        $scope.$apply(function() {
          item.image = event.target.result;
        });
      };
      reader.readAsDataURL(item._file);
    };
  }

  $scope.initializeUploader();

  $scope.getView = function(item) {
    return 'nestable_item.html';
  }

  $scope.getChildView = function(item) {
    return 'nestable_child_item.html';
  }


  $scope.findOrderedItem = function(items) {
    var sortedItems = [];
    var childSortedItems = [];

    _.each(items, function(value, index) {
      childSortedItems = [];
      if (value.item.length) {
        childSortedItems = $scope.findOrderedItem(value.item);
      }
      sortedItems.push({
        card_order: (index + 1),
        id: value.id,
        card_type_id: value.card_type_id,
        child_item: childSortedItems
      });
    });

    return sortedItems;
  }

  $scope.sortableChildOptions = {
    connectWith: ".apps-container"
  }

  $scope.sortableOptions = {
    connectWith: ".apps-container",
    update: function(e, ui) {
      $timeout(function() {
        var sortedItems = $scope.findOrderedItem($scope.userItems);
        itemServiceHandler.updateItemOrder({
          'data': {
            'cardOrder': sortedItems
          }
        })
      }, 1000);
    }
  };

  $scope.openModal = function(id) {
    $scope.itemGroupMessage = "";
    $scope.createItemResponse = "";
    uploader.uploadedImage = "";
    modalFactory.Open(id);
  };

  $scope.closeModal = function(id) {
    if (id == 'group-alert-content') {
      $statePar = {};

      if ($scope.groupId) {
        $statePar = {
          'groupId': $scope.groupId
        };
      }

      $state.go($state.current, $statePar, {
        reload: true,
        inherit: false
      });
      return modalFactory.Close(id);
    }

    uploader.clearQueue();
    modalFactory.Close(id);
  };

  $scope.slideDownFun = function() {
    $scope.slideDown = !$scope.slideDown
  }
  $scope.slideDownFun1 = function() {
    $scope.slideDown1 = !$scope.slideDown1
  }


  $scope.resetItems = function() {
    $scope.filterData = {};
    $scope.closeModal('item-filter-content');
    $scope.getItems(null);
  };

  $scope.resetSortItems = function() {
    document.getElementById("date").checked = false;
    document.getElementById("title").checked = false;
    document.getElementById("description").checked = false;
    document.getElementById("priority").checked = false;
    $scope.closeModal('item-filter-content');
    $scope.getItems(null);

  };

  $scope.resetShareItems = function() {
    for (i = 0; i < $scope.contactList.length; i++) {
      document.getElementById($scope.contactList[i].userId).checked = false;
    }
    $scope.closeModal('item-share-content');
  };

  $scope.setPagination = function(result) {
    $scope.currentPage = result.current_page;
    $scope.totalPage = result.total;
  };

  $scope.updateItemNotification = function(item) {
    if (typeof item.data == 'object') {
      itemServiceHandler.updateItemNotification({
          'data': {
            'itemId': item.data.id,
            'notification': item.notification
          }
        })
        .then(function(response) {
          //$scope.alertTitle = "Success";
          //  $scope.alertContent = "Card notification has been updated";
          //  modalFactory.Open('item-alert-content');
          toastr.success('Card notification has been updated', 'Success');
        });
    }
  }

    $scope.quickAdd = function(){

      if($scope.quickAddTitle){

      $scope.form.title =  $scope.quickAddTitle;
			if($scope.groupId) {
				  $scope.form.card_type_id = $scope.groupId;
			}
  	  itemServiceHandler.createItem({
							'data' : {
								'item' : $scope.form
							}
						})
						.then(function(response){
								if(response.error_code == 200) {
									$scope.closeItem();
                  toastr.success('Card has been created successfully', 'Success');
									$state.reload();
								} else {
                  toastr.success('Some problem occured', 'Error');
								}

						}, function(response){
							  $scope.closeItem();
						});
		}else{
      toastr.success('Please Enter the Title first and then click on quick add button', 'Alert');
		}
  }

  $scope.closeAlertPage = function() {
    $scope.closeModal('item-alert-content');
    // $state.go($state.current, {groupId : response.group[0].id}, {reload: true, inherit: false});
    $state.reload();
  }

  $scope.updateItem = function() {
    $scope.error = [];
    if (!$scope.form.title) {
      $scope.form.titleError = "Title is required";
      return;
    } else {
      $scope.form.titleError = "";
    }

    if ($scope.form.startDateYear || $scope.form.startDateMonth || $scope.form.startDayDate) {
        if (!$scope.form.startDateMonth) {
          $scope.error.push('Please select valid Month')

        }
        if (!$scope.form.startDayDate) {
          $scope.error.push('Please select valid Day')
        }
        if (!$scope.form.startDateYear) {
          $scope.error.push('Please select valid Year')
        }
    }

    if ($scope.userDetail.subscription_name != 'FREE') {
      if($scope.form.endDateYear || $scope.form.endDateMonth || $scope.form.endstartDayDate) {
        if (!$scope.form.endDateYear) {
          $scope.error.push('Please select valid Year')
        }
        if (!$scope.form.endDateMonth) {
          $scope.error.push('Please select valid Month')

        }
        if (!$scope.form.endstartDayDate) {
          $scope.error.push('Please select valid Day')
        }
      }
    }


    if ($scope.form.ageFrom || $scope.form.ageTo) {
      if (!$scope.form.ageFrom) {
        $scope.error.push('Please select Age From')
      }
      if (!$scope.form.ageTo) {
        $scope.error.push('Please select Age To')
      }
      if ($scope.form.ageTo <= $scope.form.ageFrom) {
        $scope.error.push('Age To must be greater Than Age From')
      }
    }

    if (($scope.error).length > 0) {
      for (var i = 0; i < ($scope.error.length); i++) {
        toastr.error($scope.error[i], 'Validation Error');
      }
      return;
    }

    if (document.getElementById("showcase").checked == true) {
      $scope.form.share_on_showcase = true;
    } else {
      $scope.form.share_on_showcase = false;
    }

    if ($scope.userDetail.subscription_name == 'PROFESSIONAL') {
      if (document.getElementById("liveItem").checked == true) {
        $scope.form.live_item = true;
      } else {
        $scope.form.live_item = false;
      }
    }

    if ($scope.userDetail.subscription_name == 'PERSONAL') {
      if (document.getElementById("syncTwoWay").checked == true) {
        $scope.form.two_way_sync = true;
      } else {
        $scope.form.two_way_sync = false;
      }
    }

    if ($scope.form.location_address) {
      if (typeof $scope.form.location_address.address_components != 'undefined') {
        var addressData = [];
        angular.forEach($scope.form.location_address.address_components, function(value, index) {
          if (addressData.indexOf(value.long_name) == -1)
            addressData.push(value.long_name);
        })

        $scope.form.location_address = addressData.join();
      }
    }


    if ($scope.form.startDateYear && $scope.form.startDateMonth && $scope.form.startDayDate) {
      $startDateValue = $scope.convertToDate(['startDateYear', 'startDateMonth', 'startDayDate']);
      if ($startDateValue) {
        $scope.form.start_date = $startDateValue;
      }
    }

    if ($scope.form.startTime && $scope.form.startTimePeriod) {
      $startTimeValue = $scope.convertToTime($scope.form.startTime, $scope.form.startTimePeriod);
      if ($startTimeValue) {
        $scope.form.start_time = $startTimeValue;
      }
    }


    if ($scope.userDetail.subscription_name != 'FREE') {
      if ($scope.form.endDateYear && $scope.form.endDateMonth && $scope.form.endDayDate) {
        $startDateValue = $scope.convertToDate(['endDateYear', 'endDateMonth', 'endDayDate']);
        if ($startDateValue) {
          $scope.form.start_date = $startDateValue;
        }
      }

      if ($scope.form.endTime && $scope.form.endTimePeriod) {
        $startTimeValue = $scope.convertToTime($scope.form.endTime, $scope.form.endTimePeriod);
        if ($startTimeValue) {
          $scope.form.start_time = $startTimeValue;
        }
      }
    }


    if ($scope.form.$$hashKey)
      delete($scope.form.$$hashKey);

    if ($scope.form.owner_id)
      delete($scope.form.owner_id);

    if (uploader.uploadedImage)
      $scope.form.card_image = uploader.uploadedImage

    itemServiceHandler.updateItem({
        'data': {
          'id': $scope.form.id,
          'item': $scope.form
        }
      })
      .then(function(response) {
        $scope.closeItem();
        //$scope.alertTitle = "Success";
        //$scope.alertContent = "Card has been updated";
        //modalFactory.Open('item-alert-content');
        toastr.success('Card has been updated', 'Success');
        $scope.getItems(null);
        $state.reload();
      });
  };

  $scope.convertToTime = function(time, format) {
    var timeValue = "";
    if(time.hasOwnProperty('split')) {
      var providedTime = time.split(":");


      if (providedTime.length == 2) {
        if (format == 'pm') {
          providedTime[0] = Number(providedTime[0]) + 12;
        }
        _.each(providedTime, function(value) {
          if (timeValue) timeValue += ":";

          timeValue += (Number(value) < 10) ? ("0" + Number(value)) : Number(value);
        });
      }
    }
    return timeValue;
  }

  $scope.convertToDate = function(fields) {
    $startDateValue = "";
    $validDate = true;
    _.each(fields, function(value) {
      if ($startDateValue) {
        $startDateValue += "-";
      }
      if ($scope.form[value]) {
        $startDateValue += ($scope.form[value] < 10) ? ("0" + $scope.form[value]) : $scope.form[value];
      } else {
        $validDate = false;
      }
    });

    return ($validDate) ? $startDateValue : "";
  }

  $scope.changeTab = function(tabId) {
    $scope.activeTab = tabId;
  };

  $scope.viewItem = function(formDataset) {
    $scope.openModal('item-view-modal');
    $scope.viewDetails = formDataset;
  };

  $scope.openDeleteItem = function(formDataset) {
    $scope.singleDeleteId = formDataset.id;
    $scope.openModal('delete-alert-content');
  };

  $scope.deleteItem = function() {
    itemServiceHandler.deleteItem({
      'data': {
        'id': $scope.singleDeleteId
      }
    }).then(function(response) {
      $scope.closeItem();
      //$scope.alertTitle = "Success";
    //  $scope.alertContent = "Card has been deleted";
    //  modalFactory.Open('item-alert-content');
      $scope.getItems(null);
      toastr.success('Card has been deleted', 'Success', {
        onShown : function() {
          $scope.getItems(null);
          $state.reload();
        }
      });
      $scope.closeModal('delete-alert-content');

    }, function(response) {
      $scope.closeItem();
    });
  }

  $scope.shareItem = function(formDataset) {
    $scope.contact_id = formDataset.id;
    itemServiceHandler.getContacts({
        'data': {
          'card_id': formDataset.id
        }
      })
      .then(function(response) {
        $scope.contactList = response.result;
        if ($scope.contactList.length > 0) {
          $scope.shareableCardDetail = formDataset;
          $scope.shareEmail = "";
          $scope.openModal('item-share-content');
        } else {
          $scope.showAlertTitle = 'Unable to Share Item';
          $scope.showAlertContent = 'You cannot share this item yet, as you do not have any contacts. Add contacts and try this again.';
          $scope.openModal('show-alert-content');
        }

      });

  };

  $scope.shareItemShare = function() {

    var id = $scope.contact_id;
    var to_users = [];
    for (i = 0; i < $scope.contactList.length; i++) {
      if (document.getElementById($scope.contactList[i].userId).checked == true) {
        to_users.push($scope.contactList[i].userId);
      }
    }

    itemServiceHandler.shareCard({
        'data': {
          'id': id,
          'to_users': to_users
        }
      })
      .then(function(response) {
        $scope.closeModal('item-share-content');
        $scope.showAlertTitle = 'SUCCESS';
        $scope.showAlertContent = 'Successfully Shared.';
        $scope.openModal('show-alert-content');
      });
  };

  $scope.openDeleteAll = function() {

    var deleteUser = [];
    for (i = 0; i < $scope.checkedUserItem.length; i++) {
      deleteUser.push($scope.checkedUserItem[i].id);
    }

    if (deleteUser.length === 0) {
      $scope.openModal('allDelete-selectAlert-content');
    } else {
      $scope.openModal('allDelete-alert-content');
    }


  };

  $scope.deleteAll = function() {

    var deleteUser = [];
    for (i = 0; i < $scope.checkedUserItem.length; i++) {
        deleteUser.push($scope.checkedUserItem[i].id);
    }
    itemServiceHandler.deleteAll({
      'data': {
        'cardIds': deleteUser
      }
    }).then(function(response) {
      $scope.closeModal('allDelete-alert-content');
      $scope.closeItem();
      //$scope.alertTitle = "Success";
      //$scope.alertContent = "Card has been deleted";
      //modalFactory.Open('item-alert-content');
      $scope.getItems(null);
      toastr.success('Card has been deleted', 'Success', {
        onShown : function() {
          $scope.getItems(null);
          $state.reload();
        }
      });

    }, function(response) {
      $scope.closeItem();
    });
  }

  $scope.closeItem = function() {
    $scope.closeModal('item-form-modal');
  };

  $scope.openGroup = function() {
    $scope.selectedItem = null;
    $scope.form = {
      shareToContact : []
    };
    $scope.closeModal('add-section-content');
    $scope.openModal('item-group-modal');
  }

  $scope.openItem = function(formDataset) {
    $scope.createTimeArray();
    $scope.createYears();

    // if($scope.userDetail.subscription_name == 'PROFESSIONAL'){
    // 	document.getElementById("liveItem").checked = false;
    //    }

    //       if($scope.userDetail.subscription_name == 'PERSONAL'){
    // 	document.getElementById("syncToWay").checked = false;
    //    }

    // document.getElementById("shareOnShowcase").checked = false;

    $scope.selectedItem = null;
    $scope.form = {
      shareToContact : []
    };
    $scope.closeModal('add-section-content');
    $scope.openModal('item-form-modal');
  };

  $scope.checkAndMove = function(item) {
    var userItemIndex = ($scope.userItems.indexOf(item));

    var index = ($scope.checkedUserItem).indexOf(item);
    if (index == -1) {
      ($scope.checkedUserItem).push(item)
      $scope.userItems[userItemIndex].hideItem = true;
    } else {
      ($scope.checkedUserItem).pop(item)
      $scope.userItems[userItemIndex].hideItem = false;
    }
  }

  /*$scope.editItem = function(formDataset) {
			var userItems = _.extend({}, formDataset);
			userItems = validateValues(userItems);

			$scope.selectedItem = userItems;
			$scope.form = userItems;
			$scope.form.location_country = ($scope.form.location_country) ? $scope.form.location_country.toString() : 0;
			$scope.form.location_province = ($scope.form.location_province) ? $scope.form.location_province.toString() : 0;
			$scope.form.location_city = ($scope.form.location_city) ? $scope.form.location_city.toString() : 0;
			$scope.form.discount = Number($scope.form.discount);

            if($scope.userDetail.subscription_name == 'PROFESSIONAL'){
				if(formDataset.live_item == 1){
					document.getElementById("liveItem").checked = true;
				}else{
					document.getElementById("liveItem").checked = false;
				}
            }

            if($scope.userDetail.subscription_name == 'PERSONAL'){
				if(formDataset.two_way_sync == 1){
					document.getElementById("syncToWay").checked = true;
				}else{
					document.getElementById("syncToWay").checked = false;
				}
            }


			if(formDataset.share_on_showcase == 1){
				document.getElementById("shareOnShowcase").checked = true;
			}

			if(formDataset.share_on_showcase == 0){
				document.getElementById("shareOnShowcase").checked = false;
			}

            itemServiceHandler.getProvince({
						'data' : {
							'country_id' : formDataset.location_country
						}
					})
					.then(function(response){
						$scope.provinceList = (response.result.province.length) ? response.result.province : null;
					});

			itemServiceHandler.getCity({
						'data' : {
							'province_id' : formDataset.location_province
						}
					})
					.then(function(response){
						$scope.cityList = (response.result.cities.length) ? response.result.cities : null;
						    $scope.openModal('item-form-modal');
					});
	}*/

  $scope.userItems = {};

  $scope.getSharedItems = function($params) {
    itemServiceHandler.getSharedItems($params)
      .then(function(response) {
        $scope.otherData = response.others;
        $scope.setPagination(response.result);
        $scope.sharedItems = (response.result.data.length) ? response.result.data : null;
      });
  }

  $scope.rejectItem = function(selectedUserItem) {
    itemServiceHandler.updateSharedItem({
        'data': {
          'accept_request': 0,
          'card_id': selectedUserItem
        }
      })
      .then(function(response) {
        $scope.groupAlertTitle = "Success";
        $scope.groupAlertContent = "Item has been rejected";
        modalFactory.Open('group-alert-content');
        $scope.getSharedItems(null);
        $scope.getItems(($scope.groupId) ? {
          'data': {
            'filter': {
              'type': $scope.groupId
            }
          }
        } : null);
      });
  }

  $scope.acceptItem = function(selectedUserItem) {
    itemServiceHandler.updateSharedItem({
        'data': {
          'accept_request': 1,
          'card_id': selectedUserItem.id
        }
      })
      .then(function(response) {
        $scope.groupAlertTitle = "Success";
        $scope.groupAlertContent = "Item has been accepted";
        modalFactory.Open('group-alert-content');
        $scope.getSharedItems(null);
        $scope.getItems(($scope.groupId) ? {
          'data': {
            'filter': {
              'type': $scope.groupId
            }
          }
        } : null);
      });
  }

  $scope.getItems = function($params) {
    itemServiceHandler.getItems($params)
      .then(function(response) {
        $scope.otherData = response.others;
        $scope.setPagination(response.result);
        $scope.userItems = (response.result.data.length) ? response.result.data : null;


        $scope.userItems = $scope.userItems.map(function(value) {
          value.item = [];
          return value;
        });

        if ($scope.groupId) {
          var selectedUserItem = _.where($scope.userItems, {
            'card_type_id': $scope.groupId
          });
          if (selectedUserItem.length)
            $scope.selectedUserItem = selectedUserItem[0];
        }

        $scope.closeModal('item-filter-content');
        $scope.closeModal('item-sort-content');

        if (response.group.length) {
          $state.go($state.current, {
            groupId: response.group[0].id
          }, {
            reload: true,
            inherit: false
          });
          //location.reload();
          //$location.url($location.path() + "/" + response.group[0].id);
          //$window.location.href = $location.path() + response.group[0].id;
        }
      });
  };


  $scope.filterGroupByType = function(filterType) {
    itemServiceHandler.getCardGroup((filterType) ? {
        'data': {
          'groupId': filterType
        }
      } : null)
      .then(function(response) {
        $statePar = null;
        if (response.result && response.result.length) {
          $statePar = {
            'groupId': response.result[0].parent_group_id
          };
        }

        $state.go($state.current, $statePar, {
          reload: true,
          inherit: false
        });
      });
  };

  function validateValues(userItems) {
    if (userItems) {
      if (userItems.price) {
        userItems.price = Number(userItems.price);
      }

      if (userItems.quantity) {
        userItems.quantity = Number(userItems.quantity);
      }

      if (userItems.card_time) {
        userItems.card_time = new Date("10-10-2017 " + userItems.card_time);
      }

      if (userItems.start_time) {
        userItems.start_time = new Date("10-10-2017 " + userItems.start_time);
      }

      if (userItems.end_time) {
        userItems.end_time = new Date("10-10-2017 " + userItems.end_time);
      }
    }
    return userItems;
  }


  authServiceHandler.getUserDetail()
    .then(function(response) {
      $scope.userDetail = response.result;
    });

  itemServiceHandler.getCardGroup(($scope.groupId) ? {
      'data': {
        'groupId': $scope.groupId
      }
    } : null)
    .then(function(response) {
      $scope.cardGroup = response.result;
    });

  $scope.selectItem = function(selectedUserItem) {
    $scope.selectedUserItem = selectedUserItem;
  };

  $scope.filterItem = function(filterType) {
    $scope.selectedFilter = filterType;
    if ($scope.groupId) {
      var card_id = $scope.groupId;
    } else {
      card_id = '';
    }
    switch (filterType) {
      case 'all':
        $scope.getItems({
          'data': {
            'filter': {
              'getitem': 'all'
            }
          }
        });
        break;
      case 'name':
        $scope.getItems({
          'data': {
            'filter': {
              'searchText': $scope.filterName,
              'card_type_id': card_id
            }
          }
        });
        break;
      case 'today':
        $scope.getItems({
          'data': {
            'filter': {
              'date': 'now'
            }
          }
        });
        break;
      case 'search':
        $scope.openModal('item-filter-content');
        break;
      case 'sort':
        $scope.openModal('item-sort-content');
        break;
    }
  };

  $scope.searchItems = function() {
    if (document.getElementById("searchRadio").checked == true) {
      $scope.filterData.share_on_showcase = 1;
    } else {
      $scope.filterData.share_on_showcase = 0;
    }

    if (document.getElementById("searchLiveItem").checked == true) {
      $scope.filterData.live_item = 1;
    } else {
      $scope.filterData.live_item = 0;
    }

    if (document.getElementById("searchTwoWaySync").checked == true) {
      $scope.filterData.two_way_sync = 1;
    } else {
      $scope.filterData.two_way_sync = 0;
    }


    if ($scope.groupId) {
      $scope.filterData.card_type_id = $scope.groupId;
    }

    $scope.getItems({
      'data': {
        'filter': $scope.filterData
      }
    });
  };

  $scope.changeSortType = function() {
    $scope.sortingType = ($scope.sortingType == 'asc') ? 'desc' : 'asc';

    if ($scope.selectedSorting)
      $scope.sortItems($scope.selectedSorting)
  }

  $scope.sortItems = function(sort) {
    var sortBy = {};
    $scope.selectedSorting = sort;

    switch (sort) {
      case "date":
        sortBy.sortByDate = $scope.sortingType;
        break;
      case "title":
        sortBy.sortByTitle = $scope.sortingType;
        break;
      case "priority":
        sortBy.sortByPriority = $scope.sortingType;
        break;
      default:
        sortBy.sortByTitle = $scope.sortingType;
        break;
    }

    /*if(document.getElementById("date").checked == true){
			sortBy.sortByDate = 'asc';
		}

		if(document.getElementById("title").checked == true){
			sortBy.sortByTitle = 'asc';
		}

		if(document.getElementById("description").checked == true){
			sortBy.sortByDescription = 'asc';
		}

		if(document.getElementById("priority").checked == true){
			sortBy.sortByPriority = 'asc';
		}

		if($scope.groupId){
           	sortBy.sortByCardTypeID = $scope.groupId;
    }*/

    var myJsonString = JSON.stringify(sortBy);
    $scope.getItems({
      'data': {
        'filter': {
          'sortBy': sortBy
        }
      }
    });

    //$scope.closeModal('item-sort-content');
  };

  $scope.filterItemByType = function(filterType) {
    $(".item").removeClass('active');
    $("#item-group-" + filterType).addClass('active');
    $scope.selectedItem = filterType;
    if (filterType) {
      $scope.getItems({
        'data': {
          'filter': {
            'type': filterType
          },
          'group': filterType
        }
      });
    } else {
      $scope.selectedUserItem = "";
      $scope.getItems(null);
    }

  };


  $scope.allSelect = function() {
    $scope.showSelect = false;
    for (i = 0; i < $scope.userItems.length; i++) {
      //if (document.getElementById("checkbx_" + $scope.userItems[i].id).checked == false) {
      //  document.getElementById("checkbx_" + $scope.userItems[i].id).checked = true;
        //$scope.userItems[i].hideItem = true;
          $scope.checkAndMove($scope.userItems[i]);
    //  }
    }
  };

  $scope.unselect = function() {
    $scope.showSelect = true;
    for (i = 0; i < $scope.userItems.length; i++) {
      //if (document.getElementById("checkbx_" + $scope.userItems[i].id).checked == true) {
        //document.getElementById("checkbx_" + $scope.userItems[i].id).checked = false;
        $scope.userItems[i].hideItem = false;
        $scope.checkedUserItem = [];
        //$scope.checkAndMove($scope.userItems[i]);
      //}
    }
  };


  $scope.getProvince = function(id) {
    if (id) {
      itemServiceHandler.getProvince({
          'data': {
            'country_id': id
          }
        })
        .then(function(response) {
          $scope.provinceList = (response.result.province.length) ? response.result.province : null;
          if (response.result.province.length) {
            $scope.activeProvince = true;
            $scope.activeSearchProvince = true;
          }
        });
    }
  };

  $scope.getCity = function(id) {
    if (id) {
      itemServiceHandler.getCity({
          'data': {
            'province_id': id
          }
        })
        .then(function(response) {
          $scope.cityList = (response.result.cities.length) ? response.result.cities : null;
          if (response.result.cities.length) {
            $scope.activeCity = true;
            $scope.activeSearchCity = true;
          }
        });
    }
  };


  $scope.addItemGroup = function() {
    if ($scope.groupId) {
      $scope.mygroup.groupId = $scope.groupId;
    }

    itemServiceHandler.addItemGroup({
        'data': $scope.mygroup
      })
      .then(function(response) {
        if (response.error_code == 500) {
          $scope.itemGroupMessage = response.msg_string;
        } else {
          $scope.closeModal('item-group-modal');
          $scope.groupAlertTitle = "Success";
          $scope.groupAlertContent = "Item Group has been created";
          modalFactory.Open('group-alert-content');
        }
      });
  }

  $scope.refreshMainItem = function() {
    $scope.getItems(null);
  }

  $scope.getSharedItems(null);
  $scope.getItems(($scope.groupId) ? {
    'data': {
      'filter': {
        'type': $scope.groupId
      }
    }
  } : null);
}

scedgeControllers
  .controller('ItemController', ['$scope', '$rootScope', '$timeout', '$state', '$window', '$location', '$timeout', 'ModalService', 'ModalFactory', 'AuthServiceHandler', 'ItemServiceHandler', 'ImageUploader', 'ProfileServiceHandler', 'ContactServiceHandler', 'toastr', 'Socialshare', 'MONTHS', ItemController]);

function PaymentController($scope, $state, $stateParams, $validation, modalService,modalFactory, authServiceHandler, storageServiceHandler){
	if(typeof $stateParams.verificationCode != 'undefined') {
		$scope.verification_code = $stateParams.verificationCode;
	} else {
		return $state.go('join');
	}

	$scope.paymentStep = 1;

	$scope.confirmPayment = function() { 
		$scope.paymentStep = 2;
	}

	$scope.openModal = function(id){
        modalFactory.Open(id);
    };

    $scope.closeModal = function(id){
	    modalFactory.Close(id);
    }

	$scope.filterValues = function(formData) {
		$formData = {
			'subscription_id' : $scope.user.subscription_id
		};
		
		angular.forEach(formData, function(value, index){
			if(typeof value != 'function') {
				$formData[index] = value;
			}
		});

		angular.forEach(['registration_stage', 'user_id'], function(value, index){
			$formData[value] = (typeof $scope.user[value] != 'undefined') ? $scope.user[value] : 0;
		});

		if(typeof  $scope.user['id'] != 'undefined') {
			$formData['user_id'] = $scope.user['id'];
		}

		return $formData;
	}

	$scope.form = {
        submit: function (form) {
        	$validation.validate(form)
                .success(function() {
                	
                	return authServiceHandler.processPaymentRequest({
                		"data" : $scope.filterValues($scope.form)
                	})
                	.then(function(response) {
                		if(response.error_code  == 200) {
                			storageServiceHandler.setValue('token', 'Bearer ' + response.token); 

							$scope.openModal('success-process');
                		} else {
	                	   	$scope.errorMessage = response.msg_string;
						}
	            	});

                })
                .error(function(){
                	$scope.errorMessage = "Please fill required field";
                });
        }
    }

    $scope.completePayment = function(){
    	$scope.closeModal('success-process');
    	$state.go('myItem', { nextState: '', data: []});
    }

    authServiceHandler.getVerificationDetail({
    		'data' : {
    			'verification_code' : $scope.verification_code
    		}
    	})
		.then(function(response){
			if(response.error_code == 200){
				$scope.user = response.result;
			} else {
				$state.go('join');
			}
		});


    authServiceHandler.getSubscription()
		.then(function(response){
			if(response.error_code == 200){
				$scope.subscriptionList = response.result;
			}
		});

}

scedgeControllers
	.controller('PaymentController', ['$scope', '$state', '$stateParams', '$validation', 'ModalService','ModalFactory', 'AuthServiceHandler', 'StorageServiceHandler', PaymentController])
function ProfileController( $scope, $rootScope, $timeout, $state, profileService, modalFactory, modalService, itemServiceHandler, authServiceHandler, ImageUploader){
	$scope.pageNumber = 1;
	if(typeof $state.params != undefined){
	 	if(typeof $state.params.data != 'undefined' && $state.params.data) {
			$scope = angular.extend($scope, $state.params.data);
		}
	} else {
		$scope.profile_id = "0";
	}

	$rootScope.notifications = profileService.notifications();

	$scope.uploadImage = function(){
		angular.element('#upload-cover-photo-button').trigger('click');
	}

	$scope.sortableOptions = {
    update: function(e, ui) {
			$timeout(function() {
					var sortedItems = [];

					angular.element(".sorted-showcase-item").each(function(index, element){
						sortedItems.push({
							showcase_order : (index + 1),
							id : $(element).attr('showcaseId')
						});
					})

					itemServiceHandler.updateShowcaseOrder({
						'data' : {
							'showcaseOrder' : sortedItems
						}
					})

			}, 500);
		},

	};

	$timeout(function(){
		profileService.removeUserNotifications({'data' : {'type' : 'profile'}})
					.then(function(response){
							$rootScope.notifications = response.result;
				})
	}, 1000);


    var a = moment().format('YYYY');
    a = moment().subtract(15, 'years').format('YYYY');
    date = '12/31/'+a;
    $scope.currentDate = date;

	$scope.setPagination = function(result) {
			$scope.currentPage = result.current_page;
			$scope.totalPage = result.total;
	}

	$scope.profileDetail = {};
	$scope.contactGroup = [];
	$scope.profileDetailBackup = {};


	var uploader = $scope.uploader = ImageUploader;

 	uploader.onAfterAddingFile = function(item) {

 		item.croppedImage = '';
 		var reader = new FileReader();
 		reader.onload = function(event) {
 			$scope.$apply(function(){
 				item.image = event.target.result;
 			});
 		};
 		reader.readAsDataURL(item._file);
 	};


	$scope.closeModal = function(modelId) {
		$scope.formSuccessMessage = "";
		$scope.formErrorMessage = "";
		modalFactory.Close(modelId);
		if(modelId == 'update-alert-content'){
			getProfile();
		}
	}

	$scope.openModal = function(modelId) {
		$scope.formSuccessMessage = "";
		$scope.formErrorMessage = "";
		modalFactory.Open(modelId);
	}

	function getProfile(){
		itemServiceHandler.getProfile()
					.then(function(response) {
						   $scope.profileDetail = (response.result);

						   $scope.profileDetail.country = $scope.profileDetail.country.toString();
						   $scope.profileDetail.province = $scope.profileDetail.province.toString();
						   $scope.profileDetail.city = $scope.profileDetail.city.toString();

							$scope.countryList = response.others.countries;
							$scope.authUserDetail = _.extend({}, $scope.profileDetail);
							$scope.profileDetailBackup = _.extend({}, $scope.profileDetail);
							switch(profileDetail.subscription_id) {
									case 1:
										$scope.profileDetail.subscription_name = 'FREE';
										$scope.profileDetail.subscription_type = 'ALL';
									break;
									case 2:
										$scope.profileDetail.subscription_name = 'PERSONAL';
										$scope.profileDetail.subscription_type = 'MONTHLY';
									break;
									case 3:
										$scope.profileDetail.subscription_name = 'PROFESSIONAL';
										$scope.profileDetail.subscription_type = 'MONTHLY';
									break;
									case 4:
										$scope.profileDetail.subscription_name = 'PERSONAL';
										$scope.profileDetail.subscription_type = 'ANNUAL';
									break;
									case 5:
										$scope.profileDetail.subscription_name = 'PROFESSIONAL';
										$scope.profileDetail.subscription_type = 'ANNUAL';
									break;
							}
					});
	}
	getProfile();


 $scope.isRequestAllower = true;

 $scope.getUserItems = function($params){
	 $scope.isRequestAllower = false;

    itemServiceHandler.getShowcase($params)
						.then(function(response) {
							$scope.isRequestAllower = true;

							$scope.setPagination(response.result);
							$scope.userOriginalItems = (response.result.data.length) ? response.result.data : null;

							if($scope.userOriginalItems) {
								if(!$scope.userItems) {
									$scope.userItems = $scope.userOriginalItems;
								} else {
									for(var i=0; i<$scope.userOriginalItems.length;i++) {
										$scope.userItems.push($scope.userOriginalItems[i]);
									}
								}
							}
						});
	}

	$scope.getUserItems(null);

	$scope.filterUserItems = function(newPageNumber, oldPageNumber) {

		 if($scope.isRequestAllower) {
				$scope.getUserItems({
					'data' : {
						'page' : (++$scope.pageNumber)
					}
				});
			}
	}

	$scope.selectitem = function(selectedUserItem) {
		$scope.selectedUserItem = selectedUserItem;
	}

  $scope.openSetting = function() {
		$scope.userSubscription = $rootScope.authUserDetail['subscription_name'];
		$scope.openModal('setting-content');
	}

	$scope.logoutSession = function() {

		authServiceHandler.logout();
		$scope.closeModal('setting-content');
	}

	$scope.disableAccount = function() {
		if(confirm("Are you sure you want to disable your account?")) {
				profileService.disableAccount(null)
					.then( function(response) {
							if(response.error_code == 200){
									$scope.formSuccessMessage = response.msg_string;
									authServiceHandler.logout();
							} else {
									$scope.formErrorMessage = response.msg_string;
							}
					});
		}
	}

	$scope.updateProfile = function() {
		if(uploader.uploadedImage)
			$scope.profileDetail.profile_image = uploader.uploadedImage;

		profileService.updateProfile({
			'data' : $scope.profileDetail
		}).then( function(response) {
			if(response.error_code == 200){
				$scope.formSuccessMessage = response.msg_string;
				$scope.closeModal('profile-content');
				$scope.groupAlertTitle = 'SUCCESS';
				$scope.groupAlertContent = 'Your profile has been updated Successfully !!';
				$scope.openModal('update-alert-content');
				$scope.getUserItems(null);
			} else {
				$scope.formErrorMessage = response.msg_string;
				$scope.closeModal('profile-content');
			}
		});
	}

	$scope.openProfile = function() {

		itemServiceHandler.getProvince({
						'data' : {
							'country_id' : $scope.profileDetail.country
						}
					})
					.then(function(response){
						$scope.provinceList = (response.result.province.length) ? response.result.province : null;
					});

		itemServiceHandler.getCity({
						'data' : {
							'province_id' : $scope.profileDetail.province
						}
					})
					.then(function(response){
						$scope.cityList = (response.result.cities.length) ? response.result.cities : null;
						    $scope.openModal('profile-content');
					});

	}

	$scope.updateCard = function() {
		profileService.updateCard({
			'data' : $scope.cardDetail
		}).then( function(response) {
			if(response.error_code == 200){
				$scope.formErrorMessage = response.msg_string;
				$scope.groupAlertTitle = 'ALERT';
				$scope.groupAlertContent = 'Your card has been updated successfully';
				$scope.closeModal('card-content');
				$scope.openModal('update-alert-content');
			} else {
				$scope.formErrorMessage = response.msg_string;
			}
		});
	}

	$scope.openDeleteCard = function() {
        $scope.closeModal('setting-content');
		$scope.openModal('delete-card-content');
	}

	$scope.deleteCard = function(){
			profileService.deleteCard(null)
				.then( function(response) {
					if(response.error_code == 200){
						$scope.formErrorMessage = response.msg_string;
						$scope.closeModal('delete-card-content');
						$scope.groupAlertTitle = 'ALERT';
						$scope.groupAlertContent = 'Successfully Deleted';
						$scope.openModal('update-alert-content');
					} else {
						$scope.formErrorMessage = response.msg_string;
					}
			 });
	}

	$scope.openCard = function() {
		$scope.closeModal('setting-content');
		$scope.cardDetail = {
				'name' : $scope.profileDetail.name,
				'number' : $scope.profileDetail.number,
				'exp_date' : $scope.profileDetail.exp_date,
				'cvc' : $scope.profileDetail.cvc
		};

		$scope.openModal('card-content');
	}

	$scope.resetPassword = function() {
			if($scope.passwordData.newPassword != $scope.passwordData.repeatPassword){
				$scope.formErrorMessage = "Repeat password not matching";
				return;
			}
			profileService.updatePassword({
				'data' : $scope.passwordData
			}).then( function(response) {
				if(response.error_code == 200){
					$scope.formSuccessMessage = response.msg_string;
					$scope.closeModal('password-reset-content');
				} else {
					$scope.formErrorMessage = response.msg_string;
				}
			});
	}

	$scope.openPasswordReset = function() {
			$scope.closeModal('profile-content');
			$scope.passwordData = {
				'id' : $scope.profileDetail.user_id,
				'oldPassword' 	: '',
				'newPassword' 	: '',
				'repeatPassword': ''
			};
			$scope.openModal('password-reset-content');
	}

	$scope.getProvince = function(id){
		if(id){
			itemServiceHandler.getProvince({
						'data' : {
							'country_id' : id
						}
					})
					.then(function(response){
						$scope.provinceList = (response.result.province.length) ? response.result.province : null;
						if(response.result.province.length){
							$scope.activeProvince = true;
						}
					});
		}
	}

	$scope.getCity = function(id){
		if(id){
			itemServiceHandler.getCity({
						'data' : {
							'province_id' : id
						}
					})
					.then(function(response){
						$scope.cityList = (response.result.cities.length) ? response.result.cities : null;
						if(response.result.cities.length){
							$scope.activeCity = true;
						}
					});
		}
	}

	$scope.uploadCoverImage = function(image){
       profileService.uploadCoverImage({
						'data' : {
							'cover_image' : image
						}
					})
					.then(function(response){
				    	$state.reload();
					});
	}

	$scope.updateSubscription = function() {

		profileService.updateSetting({
						'data' : {
							'updateSubscription_name' : $scope.profileDetail.subscription_name,
							'updateSubscription_type' : $scope.profileDetail.subscription_type
						}
					})
					.then(function(response){
						$scope.closeModal('setting-content');
						$scope.groupAlertTitle = 'SUCCESS';
						$scope.groupAlertContent = 'Your setting has been updated Successfully.';
						$scope.openModal('update-alert-content');
					});
	}

	$scope.updateSubscriptionPeriod = function() {
		if($scope.profileDetail.subscription_type && $scope.profileDetail.subscription_type != 'ALL') {
				if($scope.profileDetail.subscription_name == $scope.profileDetailBackup.subscription_name){
					$scope.groupAlertTitle = 'No Changes';
					$scope.groupAlertContent = 'Nothing to update.';
					$scope.openModal('update-alert-content');
					return;
				}

				profileService.updateSetting({
								'data' : {
									'updateSubscription_name' : $scope.profileDetail.subscription_name,
									'updateSubscription_type' : $scope.profileDetail.subscription_type
								}
							})
							.then(function(response){
								$scope.closeModal('setting-content');
								$scope.groupAlertTitle = 'SUCCESS';
								$scope.groupAlertContent = 'Your setting has been updated Successfully.';
								$scope.openModal('update-alert-content');
							});
		} else {
			$scope.groupAlertTitle = 'Invalid Request';
			$scope.groupAlertContent = 'Please select valid subscription.';
			$scope.openModal('update-alert-content');
		}

	}
}

function OtherProfileController($scope, $rootScope, $timeout, $state, profileService, modalFactory, modalService, itemServiceHandler, contactServiceHandler) {
	if(typeof $state.params != undefined){
		if(typeof $state.params.profileId != 'undefined' && $state.params.profileId){
			$scope.profile_id = $state.params.profileId;
		}

	} else {
			return $state.go('contact');
	}

	$rootScope.notifications = profileService.notifications();

			$timeout(function(){
				profileService.removeUserNotifications({'data' : {'type' : 'profile'}})
							.then(function(response){
									$rootScope.notifications = response.result;
						})
			}, 1000);

	$scope.openModal = function(id) {
		$scope.modalErrorMessage = "";
		$scope.modalSuccessMessage = "";
		modalFactory.Open(id);
	}

	$scope.closeModal = function(id) {
		$scope.modalErrorMessage = "";
		$scope.modalSuccessMessage = "";
		modalFactory.Close(id);
	}

	$scope.setPagination = function(result) {
			$scope.currentPage = result.current_page;
			$scope.totalPage = result.total;
	}
	$scope.profileDetail = {};

	$scope.getUserProfile = function() {
		itemServiceHandler.getProfile({
							'data' : {
								'id' : $scope.profile_id
							}
						})
						.then(function(response) {
							 $scope.profileDetail = (response.result);

						});
  }

  itemServiceHandler.getShowcase({
							'data' : {
								'user_id' : $scope.profile_id
							}
						})
						.then(function(response) {
								$scope.setPagination(response.result);
								$scope.userOriginalItems = (response.result.data.length) ? response.result.data : null;

								var i,j,temparray = [],chunk = 4;
								for (i=0,j=$scope.userOriginalItems.length; i<j; i+=chunk) {
								    	temparray.push($scope.userOriginalItems.slice(i, i+chunk));
								}

								$scope.userItems = temparray;
						});


		contactServiceHandler.getContactGroup()
						.then(function(respose){
								$scope.contactGroup = respose.result;
						});

		contactServiceHandler.getUserContactGroup({
								'data' : {
										'userId' : $scope.profile_id
								}
						})
						.then(function(respose){
								$scope.userContactGroup = respose.result;
						});

	  $scope.addContact = function(){
			
			var selectedGroup = [];
			angular.forEach($scope.selectedGroup, function(value, index){
				if(value){
						selectedGroup.push(index);
				};
			});

			if(selectedGroup.length) {
				var param = {
					'data' : {
							group : selectedGroup,
							contactId : $scope.profile_id
					}
				};

				contactServiceHandler.addContact(param)
						.then(function(response){
								if(response.error_code == 200) {
									$scope.userContactGroup = response.result;
									$scope.modalSuccessMessage = response.msg_string;
									$scope.closeModal('add-contact-content');
									$scope.groupAlertTitle = 'Success';
									$scope.groupAlertContent = 'Your request has been processed successfully';
									$scope.openModal('profile-alert-content');
								} else {
									$scope.modalErrorMessage = response.msg_string;
								}
						});
			} else {
				$scope.modalErrorMessage = "Please select a contact group";
			}
		}

		$scope.openAddContactPopup = function() {
			$scope.selectedGroup = [];
			angular.forEach($scope.contactGroup, function(value, index){
				hasElement	= _.where($scope.userContactGroup, {'id' : value.id});
				if(hasElement.length) {
					$scope.selectedGroup[value.id] = true;
				} else {
					$scope.selectedGroup[value.id] = false;
				}
			})

			$scope.openModal('add-contact-content');
		}

		$scope.getUserProfile();
}

scedgeControllers
	.controller('ProfileController', ['$scope', '$rootScope', '$timeout', '$state', 'ProfileServiceHandler', 'ModalFactory', 'ModalService', 'ItemServiceHandler', 'AuthServiceHandler', 'ImageUploader', ProfileController])
	.controller('OtherProfileController', ['$scope', '$rootScope', '$timeout', '$state', 'ProfileServiceHandler', 'ModalFactory', 'ModalService', 'ItemServiceHandler', 'ContactServiceHandler', OtherProfileController]);

scedgeDirectives.directive('modal', ['ModalFactory', Directive]);

function Directive(ModalService) {
    return {
        link: function (scope, element, attrs) {
            // ensure id attribute exists
            if (!attrs.id) {
                // console.error('modal must have an id');
                return;
            }

            // move element to bottom of page (just before </body>) so it can be displayed above everything else
            element.appendTo('body');

            // close modal on background click
            element.on('click', function (e) {

                var target = $(e.target);
                if (!target.closest('.modal-body').length) {
                    console.log(target);

                    if(target.attr('type') == 'file')
                      return;

                    if(target.hasClass('edit-profile-user-header'))
                        return;

                    if(target.hasClass('edit-profile-user-image'))
                            return;

                    if(target.hasClass('alert-popup'))
                      return;

                    if(target.hasClass('cropper-view-popup'))
                      return;

                    scope.$evalAsync(Close);
                }
            });

            // add self (this modal instance) to the modal service so it's accessible from controllers
            var modal = {
                id: attrs.id,
                open: Open,
                close: Close
            };
            ModalService.Add(modal);

            // remove self from modal service when directive is destroyed
            scope.$on('$destroy', function() {
                ModalService.Remove(attrs.id);
                element.remove();
            });

            // open modal
            function Open() {

                element.show("slow", function(){
                    $(this).slideDown();
                    $('body').addClass('modal-open');
                });

            }

            // close modal
            function Close() {
                element.hide("slow", function(){
                    $(this).slideUp();
                    $('body').removeClass('modal-open');
                });
            }
        }
    };
}

scedgeDirectives
    .directive('footerSection', ["LINK", function (link) {
        return {
            restrict: 'E',
            templateUrl : link.server + 'public/pages/footer.html'
        }
    }])
    .directive('bannerSection', ["LINK", function (link) {
        return {
            restrict: 'E',
            templateUrl : link.server + 'public/pages/banner.html'
        }
     }])
    .directive('nevbarSection', ["LINK", function (link) {
        return {
            restrict: 'E',
            templateUrl : link.server + 'public/pages/nevbar.html'
        }
     }])
    .directive('nevbarMainSection', ["LINK", function (link) {
        return {
            restrict: 'E',
            templateUrl : link.server + 'public/pages/nevmainbar.html'
        }
     }])
    .directive('sharedCard', ["LINK", "FILTERDATA", function (link, filterData) {
          return {
              restrict: 'A',
              scope: {
                  data : '=data',
                  rejectItem: '&',
                  acceptItem: '&'
              },
              templateUrl : link.server + 'public/pages/item/shared-card.html',
              link: function (scope, element, attrs) {
                scope.dataToDisplay = {};

                scope.itemsFieldList = filterData.FIELDS;

                scope.filterDisplayData = function(){
                  var counter = 0;
                  angular.forEach(scope.itemsFieldList, function(value, index){
                      if( !($.trim(scope.data[value]) == '') && (counter < 3)) {
                          scope.dataToDisplay[value] = scope.data[value];
                          counter++;
                      }
                  })

                  if(!counter) {
                    scope.dataToDisplay = "";
                  }
                }
                scope.filterDisplayData();
              }
          }
       }])
    .directive('itemCard', ["LINK", "FILTERDATA", function (link, filterData) {
        return {
            restrict: 'A',
            scope: {
                data : '=data',
                selectItem: '&',
                editItem: '&',
                openDeleteItem: '&',
                updateItemNotification: '&',
                viewItem: '&',
                shareItem: '&',
                checkAndMove : '&',
                sortableOptions: '=',
                getView: '&'
            },
            templateUrl : link.server + 'public/pages/item/item-card.html',
            link: function (scope, element, attrs) {
              scope.dataToDisplay = {};


              scope.itemsFieldList =  filterData.FIELDS;

              scope.filterDisplayData = function(){
                var counter = 0;
                angular.forEach(scope.itemsFieldList, function(value, index){
                    if( (!($.trim(scope.data[value]) == '')) && (counter < 3)) {
                        scope.dataToDisplay[value] = scope.data[value];
                        counter++;
                    }
                })

                if(!counter) {
                  scope.dataToDisplay = "";
                }
              }
              scope.filterDisplayData();
            }
        }
     }])


          .directive('itemCardChild', ["LINK", "FILTERDATA", function (link, filterData) {
              return {
                  restrict: 'A',
                  scope: {
                      data : '=data',
                      selectItem: '&',
                      editItem: '&',
                      openDeleteItem: '&',
                      updateItemNotification: '&',
                      viewItem: '&',
                      shareItem: '&',
                      checkAndMove : '&',
                      sortableOptions: '=',
                      getView: '&'
                  },
                  templateUrl : link.server + 'public/pages/item/item-card-child.html',
                  link: function (scope, element, attrs) {
                    scope.dataToDisplay = {};

                    scope.itemsFieldList =  filterData.FIELDS;

                    scope.filterDisplayData = function(){
                      var counter = 0;
                      angular.forEach(scope.itemsFieldList, function(value, index){
                          if( !($.trim(scope.data[value]) == '') && (counter < 3)) {
                              scope.dataToDisplay[value] = scope.data[value];
                              counter++;
                          }
                      })

                      if(!counter) {
                        scope.dataToDisplay = "";
                      }
                    }
                    scope.filterDisplayData();
                  }
              }
           }])

    .directive('itemSelectedCard', ["LINK", "FILTERDATA", function (link, filterData) {
        return {
            restrict: 'A',
            scope: {
                data : '=data',
                selectItem: '&',
                editItem: '&',
                openDeleteItem: '&',
                updateItemNotification: '&',
                viewItem: '&',
                shareItem: '&',
                checkAndMove : '&',
                sortableOptions: '=',
                getView: '&'
            },
            templateUrl : link.server + 'public/pages/item/item-selected-card.html',
            link: function (scope, element, attrs) {
              scope.dataToDisplay = {};

              scope.itemsFieldList =  filterData.FIELDS;

              scope.filterDisplayData = function(){
                var counter = 0;
                angular.forEach(scope.itemsFieldList, function(value, index){
                    if( !($.trim(scope.data[value]) == '') && (counter < 3)) {
                        scope.dataToDisplay[value] = scope.data[value];
                        counter++;
                    }
                })

                if(!counter) {
                  scope.dataToDisplay = "";
                }
              }
              scope.filterDisplayData();
            }
        }
     }])
    .directive('itemCardMain', ["LINK", function (link) {
        return {
            restrict: 'A',
            scope: {
                data : '=data',
                selectItem: '&',
                editItem: '&',
                openDeleteItem: '&',
                viewItem: '&',
                shareItem: '&',
                refreshMainItem : '&'
            },
            templateUrl : link.server + 'public/pages/item/item-card-main.html',
            link: function (scope, element, attrs) {

            }
        }
     }])
    .directive('browseCard', ["LINK", "FILTERDATA", function (link, filterData) {
        return {
            restrict: 'A',
            scope: {
                data : '=data',
                selectitem: '=selectitem'
            },
            templateUrl : link.server + 'public/pages/browse/browse-item.html',
            link: function (scope, element, attrs) {
              scope.dataToDisplay = {};

              scope.itemsFieldList = filterData.FIELDS;

              scope.filterDisplayData = function(){
                var counter = 0;
                angular.forEach(scope.itemsFieldList, function(value, index){
                    if( (!($.trim(scope.data[value]) == '')) && (counter < 3)) {
                        scope.dataToDisplay[value] = scope.data[value];
                        counter++;
                    }
                });

                if(!counter) {
                  scope.dataToDisplay = "";
                }
              }
              scope.filterDisplayData();
            }
        }
    }])
    .directive('mainBrowseItem', ["LINK", function (link) {
        return {
            restrict: 'EA',
            scope: {
              browseItem : '=',
              blockCard : '&'
            },
            templateUrl : link.server + 'public/pages/browse/main-browse-item.html',
            link: function (scope, element, attrs) {

            }
        }
    }])
    .directive('listBrowseItem', ["LINK", function (link) {
        return {
            restrict: 'EA',
            scope: {
                browseItem : '=',
                blockCard : '&'
            },
            templateUrl : link.server + 'public/pages/browse/list-browse-item.html',
            link: function (scope, element, attrs) {

            }
        }
    }])
    .directive('contactCard', ["LINK", function (link) {
        return {
            restrict: 'A',
            scope: {
              data : '=data',
              openMenu : '=openMenu',
              selectitem: '=selectitem',
              openDeleteContact : '&',
              blockUser : '&',
              reportAbuse : '&',
              showHideMenu : '&'
            },
            templateUrl : link.server + 'public/pages/contact/contact-card.html',
            link: function (scope, element, attrs) {

            }
        }
     }])
    .directive('profileCard', ["LINK", function (link) {
        return {
            restrict: 'A',
            scope: {
                data : '=data',
                selectitem: '=selectitem',
                otherUser: '=otherUser'
            },
            templateUrl : link.server + 'public/pages/profile/profile-card.html',
            link: function (scope, element, attrs) {
                scope.otherUser = Number(scope.otherUser);
            }
        }
     }])
    .directive('profileDetail', ["LINK", function (link) {
        return {
            restrict: 'E',
            scope: {
                profileDetail : '=',
                selectitem: '=selectitem',
                authUserDetail: '=',
                uploadCoverImage : '&',
                openProfile: '&',
                openSetting : '&',
                uploader : '=uploader'
            },
            templateUrl : link.server + 'public/pages/profile/profile-detail.html',
            link: function (scope, element, attrs) {
              scope.uploadImage = function(){
                angular.element('#upload-profile-photo-button').trigger('click');
              }
            }
        }
    }])
    .directive('otherProfileDetail', ["LINK", function (link) {
        return {
            restrict: 'E',
            scope: {
                profileDetail : '=',
                selectitem: '=selectitem',
                openPopup: '&',
                openProfile: '&',
                openSetting : '&'
            },
            templateUrl : link.server + 'public/pages/profile/other-profile-detail.html',
            link: function (scope, element, attrs) {

            }
        }
     }])
    .directive('itemFilterSection', ["LINK", "$timeout", function (link, $timeout) {
        return {
            restrict: 'E',
            templateUrl : link.server + 'public/pages/item/item-filter.html',
            link: function( $scope, elem, attrs ) {
                  elem.ready(function(){
                        $(".feature-slide").owlCarousel({
                            items: 3,
                            navigation: true,
 								responsive:{
							0:{  items:1 },
							1024:{ items:2 },
							1199:{ items:3 }
							}

                        });

                        $timeout(function () { $('#quick-add-input').focus(); });
                  })
           }
        }
    }])

    .directive('pendingContactSection', ["$timeout",  "LINK", function ($timeout, link) {
        return {
            restrict: 'E',
            scope: {
                 groupItem : '=',
                 selectedItem: '=',
                 hasGroup: '=',
                 filterResult: '&',
                 filterGroup: '&',
                 approveConnection: '&',
                 rejectConnection: '&'
             },
            templateUrl : link.server + 'public/pages/pending-contact-slider.html',
            link: function( $scope, elem, attrs ) {
                  elem.ready(function(){
                    $timeout(function(){
                      if($(".contact-slider")) {
                           $(".contact-slider").owlCarousel({
                               items: 4,
                               margin:30,
                               navigation: true,

                   responsive:{
                 0:{  items:1 },
                 600:{  items:2 },
                 1024:{ items:2 },
                 1199:{ items:4 }
               },
                           });
                         }
                   }, 500);
                  })
           }
        }
    }])

    .directive('contactFilterSection', ["LINK", function (link) {
        return {
            restrict: 'E',
            templateUrl : link.server + 'public/pages/contact/contact-filter-section.html',

        }
     }])

     .directive('groupSlider', ["$timeout", "LINK", function ($timeout, link) {
         return {
             restrict: 'E',
             replace: true,
             scope: {
                 groupItem : '=',
                 selectedItem: '=',
                 hasGroup: '=',
                 filterResult: '&',
                 filterGroup: '&'
             },
             templateUrl : link.server + 'public/pages/group-slider.html',

             link: function( scope, elem, attrs ) {
               function initOwlCarousel(){
                 $timeout(function(){
                     if($(".feature-slide")) {
                        var totalItems = $('.item').length;
                        var isNav = true;
                        if (totalItems <= 3) {
                           isNav = false;
                        }

                       var owl = $(".feature-slide").owlCarousel({
                           items: 3,
                           navigation:false,
                           responsive:{
                           0:{  items:1 },
                           1024:{ items:2 },
                           1199:{ items:3 }
                         },
                           callbacks: true,
                           onInitialized: function(event){
                             totalItems = event.item.count;
                             $timeout(function(){
                               if(Number(totalItems)) {
                                 $(".feature-slide").trigger('to.owl.carousel', Number(totalItems));
                               }
                             }, 1000);
                           }
                       });
                       $(".item").show();
                     }
                   }, 1000);
               }

               /*function renderOwlCarousel(){
                 var items = [];
                 var counter = 0;
                 $(".item").each(function(){
                    items.push($(this).html());
                    $(this).remove();
                 });

                 $(".owl-item").each(function(){
                   if(!$(this).html()) {
                      $(this).html(items[counter++])
                   }
                 });

                 $(".item").show();
               }*/

               elem.ready(function(){
                  initOwlCarousel();
               });

               scope.$watch('groupItem', function(oldvalue, newvalue) {
                  /*if(oldvalue != newvalue) {
                    renderOwlCarousel();
                  }*/
               }, true);
             }
         }
    }])
    .directive('contactSlider', ["$timeout", "LINK", function ($timeout, link) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                groupItem : '=',
                selectedItem: '=',
                hasGroup: '=',
                filterResult: '&',
                filterGroup: '&',
                removeGroup: '&'
            },
            templateUrl : link.server + 'public/pages/contact-slider.html',

            link: function( scope, elem, attrs ) {
              function initOwlCarousel(){
                $timeout(function(){
                    if($(".feature-slide")) {
                       var totalItems = $('.item').length;
                       var isNav = true;
                       if (totalItems <= 3) {
                          isNav = false;
                       }

                      var owl = $(".feature-slide").owlCarousel({
                          items: 3,
                          navigation:false,
                          responsive:{
                          0:{  items:1 },
                          1024:{ items:2 },
                          1199:{ items:3 }
                        },
                          callbacks: true,
                      });
                      $(".item").show();
                    }
                  }, 1000);
              }

              /*function renderOwlCarousel(){
                var items = [];
                var counter = 0;
                $(".item").each(function(){
                   items.push($(this).html());
                   $(this).remove();
                });

                $(".owl-item").each(function(){
                  if(!$(this).html()) {
                     $(this).html(items[counter++])
                  }
                });

                $(".item").show();
              }*/

              elem.ready(function(){
                 initOwlCarousel();
              });

              scope.$watch('groupItem', function(oldvalue, newvalue) {
                 /*if(oldvalue != newvalue) {
                   renderOwlCarousel();
                 }*/
              }, true);
            }
        }
   }])


    .directive('noResultFound', ["LINK", function (link) {
        return {
            restrict: 'E',
            scope: {
              message : '@'
            },
            templateUrl : link.server + 'public/pages/no-result-found.html',
        }
     }])

     .directive('noContactFound', ["LINK", function (link) {
        return {
            restrict: 'E',
            templateUrl : link.server + 'public/pages/no-contact-found.html',
        }
     }])

    .directive('noItemFound', ["LINK", function (link) {
        return {
            restrict: 'E',
            templateUrl : link.server + 'public/pages/no-item-found.html',
        }
     }])

    .directive('noShowcaseFound', ["LINK", function (link) {
        return {
            restrict: 'E',
            templateUrl : link.server + 'public/pages/no-showcase-found.html',
        }
     }])

    .directive('subscriptionCheckbox', ["LINK", function (link) {
        return {
            restrict: 'A',
            link: function( $scope, elem, attrs ) {
                  elem.ready(function(){
                        $('.check-input').on('change', function () {
                            $('.check-input').not(this).prop('checked', false);
                        });
                  })
            }
        }
     }])

    .directive('periodCheckbox', ["LINK", function (link) {
        return {
            restrict: 'A',
            link: function( $scope, elem, attrs ) {
                  elem.ready(function(){
                        $('.check-input2').on('change', function () {
                            $('.check-input2').not(this).prop('checked', false);
                        });
                  })
            }
        }
    }])
    .directive('popupTab', ["LINK", function (link) {
        return {
            restrict: 'A',
            link: function( $scope, elem, attrs ) {
                  elem.ready(function(){
                      $(".pop-tab span").click(function(){
                        $(this).parent().addClass('active').siblings().removeClass('active');
                        var tab = $(this).attr('t-href');
                        $('.tab_content').not(tab).hide();
                        $(tab).show();
                      });
                  })
            }
        }
    }])
    .directive('keyEnter', function () {
        return function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if(event.which === 13) {
                    scope.$apply(function (){
                        scope.$eval(attrs.keyEnter);
                    });

                    event.preventDefault();
                }
            });
        };
    })
    .directive('appFilereader', function($q) {
        var slice = Array.prototype.slice;

        return {
            restrict: 'A',
            require: '?ngModel',
            link: function(scope, element, attrs, ngModel) {
                    if (!ngModel) return;

                    ngModel.$render = function() {};

                    element.bind('change', function(e) {
                        var element = e.target;

                        $q.all(slice.call(element.files, 0).map(readFile))
                            .then(function(values) {
                                if (element.multiple) ngModel.$setViewValue(values);
                                else ngModel.$setViewValue(values.length ? values[0] : null);
                            });

                        function readFile(file) {
                            var deferred = $q.defer();

                            var reader = new FileReader();
                            reader.onload = function(e) {
                                deferred.resolve(e.target.result);
                            };
                            reader.onerror = function(e) {
                                deferred.reject(e);
                            };
                            reader.readAsDataURL(file);

                            return deferred.promise;
                        }

                    }); //change

                } //link
        }; //return
    })

    .directive("leftmsgDirective",  ["LINK", function (link) {
    	return{
    		restrict: 'EA',
        replace:true,
    		templateUrl: link.server + 'public/pages/chat/left-chat.html',
    		scope: {
    			userList: '=',
          authContactList: '=',
    			email: '=',
          validUsers: "=",
    			click: '&',
    			getLastMessage: '&',
    			getLastMessagePostedBy: '&',
    			getLastMessageTime: '&',
    			unReadMessageCount: '&',
          isContactEmail: '&'
    		}
    	};
    }])
    .directive("rightmsgDirective",  ["LINK", "$timeout", function (link, $timeout) {
    	return{
    		restrict: 'EA',
        replace:true,
    		templateUrl: link.server + 'public/pages/chat/right-chat.html',
    		scope: {
    			chatMessages: '=',
    			email: '=',
          authContactList: '='
    		},
    		link: function($scope, elem, attrs){
          elem.ready(function() {
            $timeout(function(){
        			if($(".content-m")){
        				$(".content-m").mCustomScrollbar({
        					theme: "minimal"
        				});
                $(".content-m").mCustomScrollbar("scrollTo", "bottom");
        			}

              if($(".content-x")){
                $(".content-x")
           			 .mCustomScrollbar({theme:"minimal"});
              };

            }, 3000);

			  $(".menu-chat").click(function(){
				  $("body").toggleClass("user-hide");
			  });
          });

          $scope.$watch('chatMessages', function(oldvalue, newvalue) {
             $timeout(function(){
               $(".content-m").mCustomScrollbar("scrollTo", "bottom");
             }, 1000);
          }, true);
    		}
    	};
    }])
    .directive("emojiSpan", function(){
      return {
        restrict: 'EA',
        template: '<span class="select-symbol" ng-right-click="" menu-items="menuItems"><i class="fa fa-smile-o" aria-hidden="true" smilies-selector="message" smilies-placement="right" smilies-title="Smilies"></i></span>',
        link: function($scope, elem, attrs) {
           elem.ready(function() {
               wdtEmojiBundle.init('.chat-input');
               elem.bind("click", function(){
                 $('.wdt-emoji-picker').trigger('click');
               });
            });
        }
      };
    })
    .directive("clockPicker", function(){
      return {
        restrict: 'EA',
        link: function($scope, elem, attrs) {
           elem.ready(function() {
             elem.clockpicker({
                placement: 'top',
                align: 'left',
                donetext: 'Done'
              });
            });
        }
      };
    })

    .directive('imageDimensions', function() {
        return {
            restrict: 'A',
            link: function(scope, element, attrs, model) {
                element.on('load', function(){
                    //console.log(element[0].offsetHeight, element[0].naturalWidth);
                    //if(element[0].offsetHeight < 93){
                      //  element.css({'margin-top' : ((93-element[0].offsetHeight)/2)});
                    //}
                });
            }
        };
    })

    .directive('imageDimensionsMain', function() {
        return {
            restrict: 'EA',
            link: function(scope, element, attrs, model) {
                /*element.on('load', function(){
                    console.log(element[0].offsetHeight, element[0].naturalWidth);
                    if(element[0].offsetHeight < 218){
                        element.css({'margin-top' : ((218-element[0].offsetHeight)/2)});
                    }
                });*/
            }
        };
    })
    .directive('sizeCalculator', function(){
        return {
            restrict: 'EA',
            link: function(scope, element, attrs, model) {
                /*element.on('load', function(){
                    var offsetHeight = element[0].offsetHeight;
                    var offsetWidth = element[0].offsetWidth;
                    var parentElement =  element.parent();

                    if(element[0].naturalWidth < 142) {
                        element[0].style.width = element[0].naturalWidth + "px !important";
                    }

                    parentElement[0].style.marginTop = "-" + ( (offsetHeight / 2) + 36 ) + "px";

                });*/
            }
        };
    })
    .directive("ngFileSelect", function(){

      return {
        link: function($scope,el){

          el.bind("change", function(e){

            $scope.file = (e.srcElement || e.target).files[0];
            $scope.getFile();
          })

        }

      }
    });

'use strict';
/* App Module */

if(typeof String.prototype.trim !== 'function') {
  String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g, '');
  }
}

if(typeof String.prototype.toProperCase !== 'function') {
  String.prototype.toProperCase = function () {
      return this.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
  };
}

function isEmpty(obj) {

    // null and undefined are "empty"
    if (obj == null) return true;

    // Assume if it has a length property with a non-zero value
    // that that property is correct.
    if (obj.length > 0)    return false;
    if (obj.length === 0)  return true;

    // Otherwise, does it have any properties of its own?
    // Note that this doesn't handle
    // toString and valueOf enumeration bugs in IE < 9
    for (var key in obj) {
        if (hasOwnProperty.call(obj, key)) return false;
    }

    return true;
}

(function () {

      angular.module('tg.dynamicDirective', [])
        .directive('tgDynamicDirective', ['$compile',
            function($compile) {
                'use strict';

                function templateUrlProvider(getView, ngModelItem) {
                    if (getView) {
                        if (typeof getView === 'function') {
                            var templateUrl = getView(ngModelItem) || '';
                            if (templateUrl) {
                                return templateUrl;
                            }
                        } else if (typeof getView === 'string' && getView.length) {
                            return getView;
                        }
                    }
                    return '';
                }

                return {
                    restrict: 'E',
                    require: '^ngModel',
                    scope: true,
                    template: '<div ng-include="templateUrl"></div>',
                    link: function(scope, element, attrs, ngModel) {

                        scope.$watch(function() {
                            var ngModelItem = scope.$eval(attrs.ngModel);
                            var getView = scope.$eval(attrs.tgDynamicDirectiveView);
                            scope.ngModelItem = ngModelItem;
                            return templateUrlProvider(getView, ngModelItem);
                        }, function(newValue, oldValue) {
                            scope.templateUrl = newValue;
                        });
                    }
                };
            }
        ]);



    angular.module('scedgeApp', [
        'ui.router',
        'ui.bootstrap',
        'ui.sortable',
        'ngRoute',
        'ngResource',
        'ngAnimate',
        'ngCookies',
        'ngMessages',
        'validation',
        'validation.rule',
        '720kb.datepicker',
        '720kb.socialshare',
        'socialLogin',
        'angularUtils.directives.dirPagination',
        'firebase',
        'ngRightClick',
        'luegg.directives',
        'google.places',
        /*'ngAnimate',
        'toastr',*/

        'angular-loading-bar',
        'LocalStorageModule',
        'angucomplete-alt',

        // Scedge Local Modules
        'scedgeConstants',
        'scedgeFilters',
        'scedgeServices',
        'scedgeControllers',
        'scedgeDirectives',
        'duScroll',

        'angularFileUpload',
        'ngImgCrop',
        'infinite-scroll',
        'tg.dynamicDirective',
        'toastr',
        'angularjs-dropdown-multiselect'
        // '720kb.tooltips'
    ]);
})();

function randomIdentifier(){
    return (new Date().getTime());
}

function config($stateProvider, $urlRouterProvider, $locationProvider, $validationProvider, $qProvider, localStorageServiceProvider, paginationTemplateProvider, socialProvider, link){
    $urlRouterProvider.otherwise("/home");

    $stateProvider
        .state('home',{
            url : "/home",
            templateUrl : link.server + "public/pages/home.html?refresh=" + randomIdentifier(),
            authenticate: false,
            controller : 'HomeController',
            authenticate : false
        })
        .state('login',{
            url : "/login",
            templateUrl : link.server + "public/pages/auth/login.html?refresh=" + randomIdentifier(),
            controller: 'AuthController',
            authenticate: false,
            params: {
                'data': ''
            }
        })
        .state('verification',{
            url : "/resend-verification",
            templateUrl : link.server + "pages/auth/verification.html?refresh=" + randomIdentifier(),
            controller: 'VerificationController',
            authenticate: false,
            params: {
                'data': ''
            }
        })
        .state('resetPassword',{
            url : "/reset-password/:verificationCode",
            templateUrl : link.server + "pages/auth/reset-password.html?refresh=" + randomIdentifier(),
            controller: 'ResetPasswordController',
            authenticate: false,
            params: {
                'data': '',
                'verificationCode' : ''
            }
        })
        .state('activate',{
            url : "/activate/:verificationCode",
            templateUrl : link.server + "public/pages/auth/activate.html?refresh=" + randomIdentifier(),
            controller: 'ActivateController',
            authenticate: false,
            params: {
                'data': ''
            }
        })
        .state('join',{
            url : "/join",
            templateUrl : link.server + "public/pages/auth/join.html?refresh=" + randomIdentifier(),
            controller: 'AuthController',
            authenticate: false,
            params: {
                'data': ''
            }
        })
        .state('joinStep1',{
            url : "/join-step-1/:verificationCode",
            templateUrl : link.server + "public/pages/auth/join-step-1.html?refresh=" + randomIdentifier(),
            controller: 'RProfileController',
            authenticate: false,
            params: {
                'data': '',
                'verificationCode' : ''
            }
        })
        .state('joinStep2',{
            url : "/join-step-2/:verificationCode",
            templateUrl : link.server + "public/pages/auth/join-step-2.html?refresh=" + randomIdentifier(),
            controller: 'RProfileController',
            authenticate: false,
            params: {
                'data': '',
                'verificationCode' : ''
            }
        })
        .state('payment',{
            url : "/payment/:verificationCode",
            templateUrl : link.server + "public/pages/auth/payment.html?refresh=" + randomIdentifier(),
            controller: 'PaymentController',
            authenticate: false,
            params: {
                'data': '',
                'verificationCode' : ''
            }
        })
        .state('about',{
            url : "/about",
            templateUrl : link.server + "public/pages/about.html?refresh=" + randomIdentifier(),
            authenticate: false
        })
        .state('contact',{
            url : "/contact",
            templateUrl : link.server + "public/pages/contact.html?refresh=" + randomIdentifier(),
            authenticate: false

        })

        .state('myItem',{
            url : "/my-item/:groupId?",
            templateUrl : link.server + "public/pages/item/my-item.html?refresh=" + randomIdentifier(),
            controller: 'ItemController',
            authenticate: true,
            params: {
                'groupId' : ''
            }

        })
        .state('profile',{
            url : "/profile",
            templateUrl : link.server + "public/pages/profile/profile.html?refresh=" + randomIdentifier(),
            authenticate: true,
            controller: 'ProfileController'

        })
        .state('userProfile',{
            url : "/user-profile/:profileId",
            templateUrl : link.server + "public/pages/profile/user-profile.html?refresh=" + randomIdentifier(),
            authenticate: true,
            controller: 'OtherProfileController',
            params: {
                'data': '',
                'profileId' : ''
            }

        })
        .state('myMessage',{
            url : "/my-message",
            templateUrl : link.server + "public/pages/chat/my-message.html?refresh=" + randomIdentifier(),
            authenticate: true,
            controller: 'ChatController',
            params: {
                'data': ''
            }

        })
        .state('messageInbox',{
            url : "/message-inbox/:chatroomId",
            templateUrl : link.server + "public/pages/chat/my-message.html?refresh=" + randomIdentifier(),
            authenticate: true,
            controller: 'ChatController',
            params: {
                'data': '',
                'chatroomId' : ''
            }

        })
        .state('myContact',{
            url : "/my-contact/:groupId?",
            templateUrl : link.server + "public/pages/contact/my-contact.html?refresh=" + randomIdentifier(),
            authenticate: true,
            controller: 'ContactController',
            params: {
                'groupId' : ''
            }

        })
        .state('browse',{
            url : "/browse",
            templateUrl : link.server + "public/pages/browse/browse.html?refresh=" + randomIdentifier(),
            authenticate: true,
            controller: 'BrowseController'

        })
        .state('browseDetail',{
              url : "/browse-detail/:cardId",
              templateUrl : link.server + "public/pages/browse/browse-detail.html?refresh=" + randomIdentifier(),
              authenticate: true,
              controller: 'BrowseDetailController',
              params: {
                  'data': '',
                  'cardId' : ''
              }
          });

        $urlRouterProvider.otherwise("/home");

        localStorageServiceProvider
            .setPrefix('scedge-web')
            .setStorageType('localStorage');

        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });

        angular.extend($validationProvider, {
            validCallback: function (element){
                $(element).parents('.form-group:first').removeClass('has-error');
            },
            invalidCallback: function (element) {
                $(element).parents('.form-group:first').addClass('has-error');
            }
        });

        paginationTemplateProvider.setPath('../pages/pagination.html');

        socialProvider.setGoogleKey("458613000830-jt0uuijmmad1c1p7ioblbbs6gdj2c85r.apps.googleusercontent.com");
        socialProvider.setLinkedInKey("86a5bi0s0jeyxw");
        socialProvider.setFbKey({appId: "1948768705335398", apiVersion: "v2.10"});

        $qProvider.errorOnUnhandledRejections(false);
}

angular
    .module('scedgeApp')
    .config(["$stateProvider", "$urlRouterProvider", "$locationProvider", "$validationProvider", "$qProvider", "localStorageServiceProvider", "paginationTemplateProvider",
        "socialProvider", "LINK", config])
    .run(['$rootScope', '$state', '$stateParams', '$transitions', 'AuthServiceHandler', 'StorageServiceHandler', 'ProfileServiceHandler',
        function($rootScope, $state, $stateParams, $transitions, authServiceHandler, storageServiceHandler, profileServiceHandler){
            $transitions.onEnter( {}, function($transition$) {
                 var toState = $transition$.to();
                 if ( authServiceHandler.isAuthenticated() ) {
                    profileServiceHandler.getUserNotifications()
                              .then(function(response){
                                  $rootScope.notifications = response.result;
                                  storageServiceHandler.setValue('notifications', response.result);
                              });

                 }
            });

            $transitions.onStart( {}, function($transition$) {
                 var toState = $transition$.to();
                 var nextParams = $transition$.params();
                 $rootScope.authUserDetail = null;
                 if(authServiceHandler.isAuthenticated()) {
                   authServiceHandler.getUserDetail()
                     .then(function(response){
                        $rootScope.authUserDetail = response.result;
                        storageServiceHandler.setValue('userDetail', $rootScope.authUserDetail);
                      });
                 }

                 if (toState.authenticate && !authServiceHandler.isAuthenticated()){
                      return $state.go('login', { nextState: toState.name, nextParams: nextParams});
                 }

                 if ( !toState.authenticate && authServiceHandler.isAuthenticated()) {
                     return $state.go('myItem', { nextState: toState.name, nextParams: nextParams});
                 }
            });
     }]);
