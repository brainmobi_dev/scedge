scedgeFilters
        .filter('truncate', function() {
            return function(mainString, desireLength) {
            	return mainString.substring(0, (typeof desireLength == 'undefined') ? 8 : desireLength)
            };
        })
        .filter('firstname', function() {
            return function(mainString) {
            	var stringArray =  mainString.split(/[ ,]+/);
            	return stringArray[0]
            };
        })
        .filter('getTitle', function() {
            return function(mainString) {
            	var stringArray =  mainString.split(/[ _]+/);
              if(typeof stringArray[1] != 'undefined') {
            	   return ((stringArray[1]).replace("Name", "")).toProperCase();
              } else {
                return (stringArray[0]) ? ((stringArray[0]).replace("Name", "")).toProperCase() : "";
              }

            };
        })
        .filter('getValue', function() {
            return function(value, titleString) {
              var title;
            	var stringArray =  titleString.split(/[ _]+/);
              if(typeof stringArray[1] != 'undefined') {
            	   title = ((stringArray[1]).replace("Name", "")).toUpperCase();
              } else {
                 title = (stringArray[0]) ? ((stringArray[0]).replace("Name", "")).toUpperCase() : "";
              }

              switch(title){
                case "DISCOUNT" : return value + "%";
                case "PRICE" : return "$" + value ;
                default : return value;
              }
            };
        })
        .filter('createImagePath',["LINK", function(link) {
            return function(mainString) {
                if(!mainString){
                    return '';
                }
                if(mainString.includes("http")) {
                  return mainString;
                } else if(mainString.includes("data:image")) {
                  return mainString;
                } else {
                  return link.server + 'public/'+mainString;
                }
            };
        }])
        .filter('isEmpty', function() {
            return function(data) {
            	if(data) {
                if(typeof data == 'object') {
                  return isEmpty(data);
                } else {
                  return data.length;
                }
              }
              return false;
            };
        })
        .filter('convertDateFormat', function(){
            return function(date){
                if(date)
                  return moment(date, "YYYY-MM-DD").format("D MMMM, YYYY");

                return "";
            }
        })
        .filter('convertTimeFormat', function(){
            return function(time){
                if(time){

                  time = "10-10-2000 " + time;
                  return moment(time, "MM-DD-YYYY HH:mm").format("h:mm");
                }
                return "";
            }
        })
        .filter('convertEventDateFormat', function(){
            return function(date, time){
                if(date && time){
                  var newDate = date + " " + time;
                  return newDate;
                  //return moment(newDate, "MM-DD-YYYY HH:mm").format("D MMMM, YYYY h:mm");
                }
                return "";
            }
        })
        .filter('alterAddress', function() {
            return function(address){
                if(address){
                    if(address.length < 35) {
                        return address;
                    }
                    return address.substr(0, 35) + "..";
                }
                return "";
            }
        })
        .filter('rawHtml', ['$sce', function($sce){
            return function(val) {
              return ($sce.trustAsHtml(val));
            };
        }])
        .filter('lastMessage', ['$sce', function($sce){
            return function(val) {
              if(val.length > 40) {
                val  = val.substring(0, 40) + "....";
              }
                return ($sce.trustAsHtml(val));

            };
        }])
