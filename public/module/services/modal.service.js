    function ImageUploader(modalFactory, FileUploader) {

     	 var openModal = function(id){
     			 modalFactory.Open(id);
       };

       var closeModal = function(id){
           uploader.clearQueue();
           modalFactory.Close(id);
       };


       var uploader = new FileUploader({
           url: '/api/v1/auth/upload-image'
       });

       uploader.filters.push({
           name: 'imageFilter',
           fn: function(item /*{File|FileLikeObject}*/, options) {
               var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
               return '|jpg|png|bmp|jpeg|gif|'.indexOf(type) !== -1;
           }
       });


       uploader.onBeforeUploadItem = function(item) {
         var blob = dataURItoBlob(item.croppedImage);
         item._file = blob;
       };

       var dataURItoBlob = function(dataURI) {
         var binary = atob(dataURI.split(',')[1]);
         var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
         var array = [];
         for(var i = 0; i < binary.length; i++) {
           array.push(binary.charCodeAt(i));
         }
         return new Blob([new Uint8Array(array)], {type: mimeString});
       };


       uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
           //console.info('onWhenAddingFileFailed', item, filter, options);
       };
       uploader.onAfterAddingAll = function(addedFileItems) {
           //console.info('onAfterAddingAll', addedFileItems);
           openModal('cropper-view-modal');
       };
       uploader.onProgressItem = function(fileItem, progress) {
           //console.info('onProgressItem', fileItem, progress);
       };
       uploader.onProgressAll = function(progress) {
           //console.info('onProgressAll', progress);
       };
       uploader.onSuccessItem = function(fileItem, response, status, headers) {
           //console.info('onSuccessItem', fileItem, response, status, headers);
       };
       uploader.onErrorItem = function(fileItem, response, status, headers) {
           //console.info('onErrorItem', fileItem, response, status, headers);
           uploader.clearQueue();
           closeModal('cropper-view-modal');
       };
       uploader.onCancelItem = function(fileItem, response, status, headers) {
          //console.info('onCancelItem', fileItem, response, status, headers);
           uploader.clearQueue();
           closeModal('cropper-view-modal');
       };
       uploader.onCompleteItem = function(fileItem, response, status, headers) {
           //console.info('onCompleteItem', fileItem, response, status, headers);
           uploader.uploadedImage = response;
           uploader.clearQueue();
           closeModal('cropper-view-modal');
       };
       uploader.onCompleteAll = function() {
           //console.info('onCompleteAll');
       };


       return uploader;
    }



scedgeServices
    .factory('ModalFactory', ['$document', '$timeout', function($document, $timeout){
        var modals = []; // array of modals on the page
        var service = {};
        service.Add = Add;
        service.Remove = Remove;
        service.Open = Open;
        service.Close = Close;
        return service;

        function Add(modal) {
            // add modal to array of active modals
            modals.push(modal);
        }

        function Remove(id) {
            // remove modal from array of active modals
            var modalToRemove = _.findWhere(modals, { id: id });
            modals = _.without(modals, modalToRemove);
        }

        function Open(id) {
            // open modal specified by id
            var modal = _.findWhere(modals, { id: id });
            modal.open();
        }

        function Close(id) {
            // close modal specified by id
            var modal = _.findWhere(modals, { id: id });
            modal.close();
            if(id == 'cropper-view-modal') {
              $timeout(function(){
                var body = angular.element(document).find('body');
                body[0].className = "modal-open";
                $document[0].body.className = "modal-open";
              }, 2000);
          }

        }
    }])
    .factory('ModalService',['$uibModal', function($uibModal){
        var defaultOption = {
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            size: 'lg',
            appendTo: undefined,
            backdrop: 'static',
            keyboard: false,
            controller : 'ModalInstanceCtrl',
            controllerAs: '$ctrl',
        }

        var modalData = {};

        function openModal(options){
             options = angular.extend(defaultOption, options);
             setModal(options.content);

             var modalInstance = $uibModal.open(options);
             return modalInstance.result;
        }


        function openSetting(options){
             options.templateUrl = "setting-content.html";
             options.keyboard = true;
             options.size = 'sm';
             options = angular.extend(defaultOption, options);
             setModal(options.content);

             var modalInstance = $uibModal.open(options);
             return modalInstance.result;
        }


        function openProfile(options){
             options.templateUrl = "profile-content.html";
             options.keyboard = true;
             options.size = 'sm';
             options = angular.extend(defaultOption, options);
             setModal(options.content);

             var modalInstance = $uibModal.open(options);
             return modalInstance.result;
        }

        function openPasswordReset(options){
             options.templateUrl = "password-reset-content.html";
             options.keyboard = true;
             options.size = 'sm';
             options = angular.extend(defaultOption, options);
             setModal(options.content);

             var modalInstance = $uibModal.open(options);
             return modalInstance.result;
        }


        function openCardSetting(options){
             options.templateUrl = "card-content.html";
             options.keyboard = true;
             options.size = 'sm';
             options = angular.extend(defaultOption, options);
             setModal(options.content);

             var modalInstance = $uibModal.open(options);
             return modalInstance.result;
        }


        function openContactPopup(options){
             options.templateUrl = "contact-content.html";
             options.keyboard = true;
             options.size = 'sm';
             options = angular.extend(defaultOption, options);
             setModal(options.content);

             var modalInstance = $uibModal.open(options);
             return modalInstance.result;
        }

        function openAddContactPopup(options) {
          options.templateUrl = "add-contact-content.html";
          options.keyboard = true;
          options.size = 'sm';
          options = angular.extend(defaultOption, options);
          setModal(options.content);

          var modalInstance = $uibModal.open(options);
          return modalInstance.result;
        }

        function openBrowseFilterPopup(options) {
          options.templateUrl = "filter-browse-content.html";
          options.keyboard = true;
          options.size = 'sm';
          options = angular.extend(defaultOption, options);
          setModal(options.content);

          var modalInstance = $uibModal.open(options);
          return modalInstance.result;
        }


        function setModal(data){
            modalData = data;
        }

        function getModal(){
            return modalData;
        }

        return {
            openModal: openModal,
            openSetting : openSetting,
            openProfile : openProfile,
            openCardSetting : openCardSetting,
            openPasswordReset : openPasswordReset,
            openContactPopup : openContactPopup,
            openAddContactPopup : openAddContactPopup,
            openBrowseFilterPopup: openBrowseFilterPopup,
            setModal : setModal,
            getModal : getModal
        }

    }])


    .factory("ImageUploader", ["ModalFactory", "FileUploader", ImageUploader])
