function RequestHandler($http, $q, $state, storageServiceHandler){
        return{
            prepareRequest      : prepareRequest,
            preparePostRequest  : preparePostRequest,
            prepareGetRequest   : prepareGetRequest,
            prepareImageRequest : prepareImageRequest,
            prepareAttachmentRequest: prepareAttachmentRequest,
            prepareJsonRequest  : prepareJsonRequest
        }

        var postParam = {
            method  : 'POST',
            url     : '',
            data    : '',
            headers : {'Content-Type': 'application/x-www-form-urlencoded'},
            transformRequest : transformTicketRequest
        }

        var getParam = {
            method  : 'GET',
            url     : '',
        }


        function prepareRequest(method, param){
            var requestParam = (method.toUpperCase() == 'POST') ? $.extend({}, postParam, param) : $.extend({}, getParam, param);

            if(method.toUpperCase() == 'POST'){
                requestParam.data = (requestParam.data) ? $.param(requestParam.data) : "";
            }

            return $http(requestParam)
                .then(sendResponseData )
                .catch(sendResponseError);
        }

        function preparePostRequest($param) {
            $param.data = ($param.data) ? $.param($param.data) : "";
            return $http({
                method  : 'POST',
                url     : $param.url,
                data    : $param.data,
                headers : (typeof $param.header != 'undefined') ? angular.extend({'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'}, $param.header) : {'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'},
                transformRequest : angular.identity
            })
                .then(sendResponseData )
                .catch(sendResponseError )
        }


        function prepareImageRequest($param) {
            $param.data = ($param.data) ? $.param($param.data) : "";
            $param.url  =  $param.url + '?'+ $param.data;
            return $http({
                method: 'GET',
                url     : $param.url,
                dataType: 'binary',
                responseType: 'arraybuffer',
                headers: {'Content-Type':'image/png','X-Requested-With':'XMLHttpRequest'},
                processData: false,
                transformRequest: angular.identity
            })
                .then(sendResponseData )
                .catch(sendResponseError )
        }

        function prepareAttachmentRequest($param) {
            return $http({
                method  : 'POST',
                url     : $param.url,
                data    : $param.data,
                processData: false,
                contentType: false,
                headers:  {'Content-Type': undefined},
                transformRequest: angular.identity
            })
                .then(sendResponseData )
                .catch(sendResponseError )
        }

        function prepareJsonRequest($param){
            return $http.jsonp($param.url).
            success(function(status) {
                //console.log(status);
            }).
            error(function(status) {
                //console.log(status);
            });
        }


        function prepareGetRequest($param){
          $param.data = ($param.data) ? $.param($param.data) : "";
          $param.url  =  $param.url + '?'+ $param.data;
            return $http({
                method  : 'GET',
                url     : $param.url,
                headers: {
                    'Content-Type': 'json',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Methods': 'GET'
                }
            })
                .then(sendResponseData )
                .catch(sendResponseError )

        }


        function transformTicketRequest(data, headersGetter) {return data;}
        function transformEmailAttachmentsRequest(data, headersGetter) {
            var formData = new FormData();
            //need to convert our json object to a string version of json otherwise
            // the browser will do a 'toString()' on the object which will result
            // in the value '[Object object]' on the server.
            formData.append("email-param", angular.toJson(data['email-param']));
            //now add all of the assigned files

            formData.append("file", encodeURIComponent(data['email-param'].attachment));

            return formData;
        }
        function sendResponseData(response) {
            return response.data;
        }
        function sendResponseError(response) {
            if(response.status == 404){
              storageServiceHandler.removeValue('token');
              storageServiceHandler.removeValue('userDetail');
              storageServiceHandler.removeValue('notifications');
              storageServiceHandler.removeValue('uKey');

              $state.go('login', { nextState: '', nextParams: ''});
              return null;
            }
            return {'error_code' : 500, 'msg_string' : 'Some problem occured', 'result' : []};
            //storageServiceHandler.setValue('token', '');
            //$state.go('login', { nextState: '', nextParams: ''});
            //return null;
        }

    };


    function AuthServiceHandler($state, requestHandler, storageServiceHandler, link, api){
            return {
                 'isValidToken'      : isValidToken,
                 'isAuthenticated'   : isAuthenticated,
               'getUserDetail'     : getUserDetail,
               'getVerificationDetail' : getVerificationDetail,
               'login'             : login,
               'logout'            : logout,
               'getSubscription'   : getSubscription,
               'socialRegisterUser': socialRegisterUser,
               'registerUser'      : registerUser,
               'activeUser'        : activeUser,
               'processPaymentRequest'    : processPaymentRequest,
               'resetPassword'    : resetPassword,
               'getProvince' : getProvince,
               'getCity' : getCity,
               'resendActivationCode' : resendActivationCode,
               'checkVerificationCode' : checkVerificationCode,
               'updateAccountPassword' : updateAccountPassword
        		};


            function updateAccountPassword($params) {
               $params = ($params) ? $params : {};
               $params = angular.extend({
                   'url' : link.server + api.updateAccountPassword
               }, $params);
               return requestHandler.preparePostRequest($params);
            }

            function resendActivationCode($params) {
               $params = ($params) ? $params : {};
               $params = angular.extend({
                   'url' : link.server + api.resendActivationCode
               }, $params);
               return requestHandler.preparePostRequest($params);
            }

            function checkVerificationCode($params) {
               $params = ($params) ? $params : {};
               $params = angular.extend({
                   'url' : link.server + api.checkVerificationCode
               }, $params);
               return requestHandler.preparePostRequest($params);
            }


            function isValidToken($params){

                //requestHandler.preparePostRequest($params);
            }

            function isAuthenticated() {
                return storageServiceHandler.getValue('token');
            }

        function getVerificationDetail($params) {
             $params = angular.extend({
                'url' : link.server + api.getVerification
            }, $params);
            return requestHandler.preparePostRequest($params);
        }

        function resetPassword($params) {
             $params = angular.extend({
                'url' : link.server + api.resetPassword
            }, $params);
            return requestHandler.preparePostRequest($params);
        }

        function getUserDetail() {
            $params = angular.extend({
                'url' : link.server + api.getProfile,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, {});
            return requestHandler.preparePostRequest($params);
        }

        function login($params) {
            $params = angular.extend({'url' : link.server + api.login}, $params);
            return requestHandler.preparePostRequest($params);
        }

        function logout(){
            storageServiceHandler.removeValue('token');
            storageServiceHandler.removeValue('userDetail');
            storageServiceHandler.removeValue('notifications');
            storageServiceHandler.removeValue('uKey');

            $state.go('login', { nextState: '', nextParams: ''});
            return null;
        }

        function processPaymentRequest($params) {
            $params = angular.extend({'url' : link.server + api.processPayment}, $params);
            return requestHandler.preparePostRequest($params);
        }


        function getSubscription() {
            $params = angular.extend({'url' : link.server + api.subscription});
            return requestHandler.prepareGetRequest($params);
        }

        function socialRegisterUser($params){
            $params = angular.extend({'url' : link.server + api.socialRegister}, $params);
            return requestHandler.preparePostRequest($params);
        }

        function registerUser($params) {
            $params = angular.extend({'url' : link.server + api.register}, $params);
            return requestHandler.preparePostRequest($params);
        }

        function activeUser($params) {
            $params = angular.extend({'url' : link.server + api.activateUser}, $params);
            return requestHandler.preparePostRequest($params);
        }

        function getProvince($params){
               $params = ($params) ? $params : {};
            $params = angular.extend({
                'url' : link.server + api.getProvinceAuth
            }, $params);
            return requestHandler.preparePostRequest($params);

        }


        function getCity($params){
               $params = ($params) ? $params : {};
            $params = angular.extend({
                'url' : link.server + api.getCityAuth
            }, $params);
            return requestHandler.preparePostRequest($params);

        }
    }


    function StorageServiceHandler(localStorageService) {
        return {
            'setValue' : setValue,
            'getValue' : getValue,
            'removeValue' : removeValue
        };

        function setValue(key, val){
             localStorageService.set(key, val)
        }

        function getValue(key){
             return localStorageService.get(key);
        }

        function removeValue(key) {
           return localStorageService.remove(key);
        }

    }

    function ProfileServiceHandler($state, requestHandler, storageServiceHandler, link, api) {
        return {
            'notifications' : notifications,
            'updatePassword' :  updatePassword,
            'updateSetting' :   updateSetting,
            'updateProfile' :   updateProfile,
            'updateCard'    :   updateCard,
            'deleteCard'    :   deleteCard,
            'updateSubscription' : updateSubscription,
            'uploadCoverImage' : uploadCoverImage,
            'getUserNotifications' : getUserNotifications,
            'removeUserNotifications' : removeUserNotifications
        };

        function notifications(){
            return storageServiceHandler.getValue('notifications');
        }

        function updateSubscription($params){
           $params = ($params) ? $params : {};
           $params = angular.extend({
               'url' : link.server + api.updateSubscription,
               'header' : {
                   'Authorization' : storageServiceHandler.getValue('token')
                }
           }, $params);
           return requestHandler.preparePostRequest($params);
        }

        function deleteCard($params) {
           $params = ($params) ? $params : {};
           $params = angular.extend({
               'url' : link.server + api.deleteCard,
               'header' : {
                   'Authorization' : storageServiceHandler.getValue('token')
               }
           }, $params);
           return requestHandler.preparePostRequest($params);
        }

        function updateSetting($params) {
             $params = ($params) ? $params : {};
             $params = angular.extend({
                'url' : link.server + api.updateSetting,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);
        }

        function updateProfile($params) {
             $params = ($params) ? $params : {};
             $params = angular.extend({
                'url' : link.server + api.updateProfile,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);
        }

        function updateCard($params) {
             $params = ($params) ? $params : {};
             $params = angular.extend({
                'url' : link.server + api.updateCard,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);
        }

        function updatePassword($params) {
             $params = ($params) ? $params : {};
             $params = angular.extend({
                'url' : link.server + api.updatePassword,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);
        }

        function uploadCoverImage($params){
            $params = ($params) ? $params : {};
             $params = angular.extend({
                'url' : link.server + api.uploadCoverImage,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);
        }

        function getUserNotifications($params){
            $params = ($params) ? $params : {};
             $params = angular.extend({
                'url' : link.server + api.getUserNotifications,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);
        }

        function removeUserNotifications($params){
            $params = ($params) ? $params : {};
             $params = angular.extend({
                'url' : link.server + api.removeUserNotifications,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);
        }
    }


    function ContactServiceHandler($state, requestHandler, storageServiceHandler, link, api) {
        return {
            'getContacts'     : getContacts,
            'getContactGroup' : getContactGroup,

            'addContact'      : addContact,
            'addContactGroup' : addContactGroup,
            'removeContactGroup' : removeContactGroup,

            'getUserContactGroup' : getUserContactGroup,
            'filterResultByName' : filterResultByName,
            'deleteContact' : deleteContact,
            'pendingRequest' : pendingRequest,
            'approveConnection' : approveConnection,
            'rejectConnection' : rejectConnection,
            'sendInvitation' : sendInvitation,
            'reportAbuse'   : reportAbuse,
            'blockUser'     : blockUser
        };

        function removeContactGroup($params){
            $params = ($params) ? $params : {};
            $params = angular.extend({
                'url' : link.server + api.removeContactGroup,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);
        }


        function reportAbuse($params) {
          $params = ($params) ? $params : {};
          $params = angular.extend({
              'url' : link.server + api.reportAbuseUser,
              'header' : {
                  'Authorization' : storageServiceHandler.getValue('token')
              }
          }, $params);
          return requestHandler.preparePostRequest($params);
        }

        function blockUser($params) {
          $params = ($params) ? $params : {};
          $params = angular.extend({
              'url' : link.server + api.blockUser,
              'header' : {
                  'Authorization' : storageServiceHandler.getValue('token')
              }
          }, $params);
          return requestHandler.preparePostRequest($params);
        }

        function getUserContactGroup($params){
            $params = ($params) ? $params : {};
            $params = angular.extend({
                'url' : link.server + api.getUserContactGroup,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);
        }

        function addContactGroup($params){
            $params = ($params) ? $params : {};
            $params = angular.extend({
                'url' : link.server + api.addContactGroup,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);
        }

        function addContact($params){
            $params = ($params) ? $params : {};
            $params = angular.extend({
                'url' : link.server + api.addContact,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);
        }

        function getContacts($params){
            $params = ($params) ? $params : {};
            $params = angular.extend({
                'url' : link.server + api.getContact,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);
        }

        function deleteContact($params){
            $params = ($params) ? $params : {};
            $params = angular.extend({
                'url' : link.server + api.deleteContact,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);

        }

        function getContactGroup($params) {
          $params = ($params) ? $params : {};
            $params = angular.extend({
                'url' : link.server + api.getContactGroup,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);
        }

        function filterResultByName($params){

            $params = ($params) ? $params : {};
            $params = angular.extend({
                'url' : link.server + api.contactResultFilter,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);

        }

        function pendingRequest(){
            $params = angular.extend({
                'url' : link.server + api.pendingRequest,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, {});
            return requestHandler.preparePostRequest($params);
        }

        function approveConnection($params){
            $params = ($params) ? $params : {};
            $params = angular.extend({
                'url' : link.server + api.approveConnection,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);
        }

        function rejectConnection($params){
            $params = ($params) ? $params : {};
            $params = angular.extend({
                'url' : link.server + api.rejectConnection,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);
        }

        function sendInvitation($params){
            $params = ($params) ? $params : {};
            $params = angular.extend({
                'url' : link.server + api.sendInvitation,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);
        }
    }


    function ItemServiceHandler($state, requestHandler, storageServiceHandler, link, api) {
        return {
            'getItems' : getItems,
            'getShowcase' : getShowcase,
            'createItem' : createItem,
            'deleteItem' : deleteItem,
            'browseItems'   : browseItems,
            'getProfile' : getProfile,
            'getCardGroup' : getCardGroup,
            'updateItem' : updateItem,
            'deleteAll' : deleteAll,
            'getProvince' : getProvince,
            'getCity' : getCity,
            'getContacts' : getContacts,
            'getReports' :getReports,
            'reportAbuse' : reportAbuse,
            'shareCard' : shareCard,
            'addItemGroup' : addItemGroup,
            'updateItemOrder' : updateItemOrder,
            'updateShowcaseOrder' : updateShowcaseOrder,

            'getSharedItems' : getSharedItems,
            'updateSharedItem' : updateSharedItem,
            'updateItemNotification' : updateItemNotification,
            'shareItemViaEmail' : shareItemViaEmail
        };

        function shareItemViaEmail($params){
          $params = ($params) ? $params : {};
          $params = angular.extend({
              'url' : link.server + api.shareItemViaEmail,
              'header' : {
                  'Authorization' : storageServiceHandler.getValue('token')
              }
          }, $params);
          return requestHandler.preparePostRequest($params);
        }

        function updateItemNotification($params){
          $params = ($params) ? $params : {};
          $params = angular.extend({
              'url' : link.server + api.updateItemNotification,
              'header' : {
                  'Authorization' : storageServiceHandler.getValue('token')
              }
          }, $params);
          return requestHandler.preparePostRequest($params);
        }

        function getSharedItems($params) {
          $params = ($params) ? $params : {};
          $params = angular.extend({
              'url' : link.server + api.getSharedItems,
              'header' : {
                  'Authorization' : storageServiceHandler.getValue('token')
              }
          }, $params);
          return requestHandler.preparePostRequest($params);
        }

        function updateSharedItem($params) {
          $params = ($params) ? $params : {};
          $params = angular.extend({
              'url' : link.server + api.updateSharedItem,
              'header' : {
                  'Authorization' : storageServiceHandler.getValue('token')
              }
          }, $params);
          return requestHandler.preparePostRequest($params);
        }

        function updateShowcaseOrder($params) {
          $params = ($params) ? $params : {};
          $params = angular.extend({
              'url' : link.server + api.updateShowcaseOrder,
              'header' : {
                  'Authorization' : storageServiceHandler.getValue('token')
              }
          }, $params);
          return requestHandler.preparePostRequest($params);
        }

        function updateItemOrder($params) {
          $params = ($params) ? $params : {};
          $params = angular.extend({
              'url' : link.server + api.updateItemOrder,
              'header' : {
                  'Authorization' : storageServiceHandler.getValue('token')
              }
          }, $params);
          return requestHandler.preparePostRequest($params);
        }


        function getShowcase($params){
          $params = ($params) ? $params : {};
            $params = angular.extend({
                'url' : link.server + api.getShowcase,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);
        }



        function addItemGroup($params) {
          $params = ($params) ? $params : {};
          $params = angular.extend({
              'url' : link.server + api.addItemGroup,
              'header' : {
                  'Authorization' : storageServiceHandler.getValue('token')
              }
          }, $params);
          return requestHandler.preparePostRequest($params);
        }

        function getCardGroup($params) {
            $params = ($params) ? $params : {};
            $params = angular.extend({
                'url' : link.server + api.getCardGroup,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);
        }

        function getItems($params){
          $params = ($params) ? $params : {};
            $params = angular.extend({
                'url' : link.server + api.getItem,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);
        }



        function createItem($params){
            $params = ($params) ? $params : {};
            $params = angular.extend({
                'url' : link.server + api.createItem,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);

            return requestHandler.preparePostRequest($params);
        }

        function deleteItem($params){
            $params = ($params) ? $params : {};
            $params = angular.extend({
                'url' : link.server + api.deleteItem,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);

            return requestHandler.preparePostRequest($params);
        }

        function deleteAll($params){
            $params = ($params) ? $params : {};
            $params = angular.extend({
                'url' : link.server + api.deleteAll,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);

            return requestHandler.preparePostRequest($params);
        }

        function updateItem($params){
            $params = ($params) ? $params : {};
            $params = angular.extend({
                'url' : link.server + api.updateItem,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);

            return requestHandler.preparePostRequest($params);
        }


        function browseItems($params){
            $params = ($params) ? $params : {};
            $params = angular.extend({
                'url' : link.server + api.browse,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);
        }

        function getProfile($params){
            $params = angular.extend({
                'url' : link.server + api.getProfile,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, (typeof $params == 'undefined') ? {} : $params);
            return requestHandler.preparePostRequest($params);
        }

        function getProvince($params){
               $params = ($params) ? $params : {};
            $params = angular.extend({
                'url' : link.server + api.getProvince,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);

        }


        function getCity($params){
               $params = ($params) ? $params : {};
            $params = angular.extend({
                'url' : link.server + api.getCity,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);

        }

        function getContacts($params){
               $params = ($params) ? $params : {};
            $params = angular.extend({
                'url' : link.server + api.getContacts,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);
        }

        function getReports(){
            $params = angular.extend({
                'url' : link.server + api.getReports,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, {});
            return requestHandler.preparePostRequest($params);
        }

        function reportAbuse($params){
            $params = ($params) ? $params : {};
            $params = angular.extend({
                'url' : link.server + api.reportAbuse,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);
        }

        function shareCard($params){
            $params = ($params) ? $params : {};
            $params = angular.extend({
                'url' : link.server + api.shareCard,
                'header' : {
                    'Authorization' : storageServiceHandler.getValue('token')
                }
            }, $params);
            return requestHandler.preparePostRequest($params);

        }
    }

scedgeServices
         .factory('RequestHandler', ['$http', '$q', '$state', 'StorageServiceHandler', RequestHandler])
         .factory('StorageServiceHandler', ['localStorageService', StorageServiceHandler])
         .factory('AuthServiceHandler', ['$state', 'RequestHandler', 'StorageServiceHandler', 'LINK', 'API', AuthServiceHandler])
         .factory('ItemServiceHandler', ['$state', 'RequestHandler', 'StorageServiceHandler', 'LINK', 'API', ItemServiceHandler])
         .factory('ContactServiceHandler', ['$state', 'RequestHandler', 'StorageServiceHandler', 'LINK', 'API', ContactServiceHandler])
         .factory('ProfileServiceHandler', ['$state', 'RequestHandler', 'StorageServiceHandler', 'LINK', 'API', ProfileServiceHandler]);
