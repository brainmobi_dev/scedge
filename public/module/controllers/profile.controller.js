function ProfileController( $scope, $rootScope, $timeout, $state, profileService, modalFactory, modalService, itemServiceHandler, authServiceHandler, ImageUploader){
	$scope.pageNumber = 1;
	if(typeof $state.params != undefined){
	 	if(typeof $state.params.data != 'undefined' && $state.params.data) {
			$scope = angular.extend($scope, $state.params.data);
		}
	} else {
		$scope.profile_id = "0";
	}

	$rootScope.notifications = profileService.notifications();

	$scope.uploadImage = function(){
		angular.element('#upload-cover-photo-button').trigger('click');
	}

	$scope.sortableOptions = {
    update: function(e, ui) {
			$timeout(function() {
					var sortedItems = [];

					angular.element(".sorted-showcase-item").each(function(index, element){
						sortedItems.push({
							showcase_order : (index + 1),
							id : $(element).attr('showcaseId')
						});
					})

					itemServiceHandler.updateShowcaseOrder({
						'data' : {
							'showcaseOrder' : sortedItems
						}
					})

			}, 500);
		},

	};

	$timeout(function(){
		profileService.removeUserNotifications({'data' : {'type' : 'profile'}})
					.then(function(response){
							$rootScope.notifications = response.result;
				})
	}, 1000);


    var a = moment().format('YYYY');
    a = moment().subtract(15, 'years').format('YYYY');
    date = '12/31/'+a;
    $scope.currentDate = date;

	$scope.setPagination = function(result) {
			$scope.currentPage = result.current_page;
			$scope.totalPage = result.total;
	}

	$scope.profileDetail = {};
	$scope.contactGroup = [];
	$scope.profileDetailBackup = {};


	var uploader = $scope.uploader = ImageUploader;

 	uploader.onAfterAddingFile = function(item) {

 		item.croppedImage = '';
 		var reader = new FileReader();
 		reader.onload = function(event) {
 			$scope.$apply(function(){
 				item.image = event.target.result;
 			});
 		};
 		reader.readAsDataURL(item._file);
 	};


	$scope.closeModal = function(modelId) {
		$scope.formSuccessMessage = "";
		$scope.formErrorMessage = "";
		modalFactory.Close(modelId);
		if(modelId == 'update-alert-content'){
			getProfile();
		}
	}

	$scope.openModal = function(modelId) {
		$scope.formSuccessMessage = "";
		$scope.formErrorMessage = "";
		modalFactory.Open(modelId);
	}

	function getProfile(){
		itemServiceHandler.getProfile()
					.then(function(response) {
						   $scope.profileDetail = (response.result);

						   $scope.profileDetail.country = $scope.profileDetail.country.toString();
						   $scope.profileDetail.province = $scope.profileDetail.province.toString();
						   $scope.profileDetail.city = $scope.profileDetail.city.toString();

							$scope.countryList = response.others.countries;
							$scope.authUserDetail = _.extend({}, $scope.profileDetail);
							$scope.profileDetailBackup = _.extend({}, $scope.profileDetail);
							switch(profileDetail.subscription_id) {
									case 1:
										$scope.profileDetail.subscription_name = 'FREE';
										$scope.profileDetail.subscription_type = 'ALL';
									break;
									case 2:
										$scope.profileDetail.subscription_name = 'PERSONAL';
										$scope.profileDetail.subscription_type = 'MONTHLY';
									break;
									case 3:
										$scope.profileDetail.subscription_name = 'PROFESSIONAL';
										$scope.profileDetail.subscription_type = 'MONTHLY';
									break;
									case 4:
										$scope.profileDetail.subscription_name = 'PERSONAL';
										$scope.profileDetail.subscription_type = 'ANNUAL';
									break;
									case 5:
										$scope.profileDetail.subscription_name = 'PROFESSIONAL';
										$scope.profileDetail.subscription_type = 'ANNUAL';
									break;
							}
					});
	}
	getProfile();


 $scope.isRequestAllower = true;

 $scope.getUserItems = function($params){
	 $scope.isRequestAllower = false;

    itemServiceHandler.getShowcase($params)
						.then(function(response) {
							$scope.isRequestAllower = true;

							$scope.setPagination(response.result);
							$scope.userOriginalItems = (response.result.data.length) ? response.result.data : null;

							if($scope.userOriginalItems) {
								if(!$scope.userItems) {
									$scope.userItems = $scope.userOriginalItems;
								} else {
									for(var i=0; i<$scope.userOriginalItems.length;i++) {
										$scope.userItems.push($scope.userOriginalItems[i]);
									}
								}
							}
						});
	}

	$scope.getUserItems(null);

	$scope.filterUserItems = function(newPageNumber, oldPageNumber) {

		 if($scope.isRequestAllower) {
				$scope.getUserItems({
					'data' : {
						'page' : (++$scope.pageNumber)
					}
				});
			}
	}

	$scope.selectitem = function(selectedUserItem) {
		$scope.selectedUserItem = selectedUserItem;
	}

  $scope.openSetting = function() {
		$scope.userSubscription = $rootScope.authUserDetail['subscription_name'];
		$scope.openModal('setting-content');
	}

	$scope.logoutSession = function() {

		authServiceHandler.logout();
		$scope.closeModal('setting-content');
	}

	$scope.disableAccount = function() {
		if(confirm("Are you sure you want to disable your account?")) {
				profileService.disableAccount(null)
					.then( function(response) {
							if(response.error_code == 200){
									$scope.formSuccessMessage = response.msg_string;
									authServiceHandler.logout();
							} else {
									$scope.formErrorMessage = response.msg_string;
							}
					});
		}
	}

	$scope.updateProfile = function() {
		if(uploader.uploadedImage)
			$scope.profileDetail.profile_image = uploader.uploadedImage;

		profileService.updateProfile({
			'data' : $scope.profileDetail
		}).then( function(response) {
			if(response.error_code == 200){
				$scope.formSuccessMessage = response.msg_string;
				$scope.closeModal('profile-content');
				$scope.groupAlertTitle = 'SUCCESS';
				$scope.groupAlertContent = 'Your profile has been updated Successfully !!';
				$scope.openModal('update-alert-content');
				$scope.getUserItems(null);
			} else {
				$scope.formErrorMessage = response.msg_string;
				$scope.closeModal('profile-content');
			}
		});
	}

	$scope.openProfile = function() {

		itemServiceHandler.getProvince({
						'data' : {
							'country_id' : $scope.profileDetail.country
						}
					})
					.then(function(response){
						$scope.provinceList = (response.result.province.length) ? response.result.province : null;
					});

		itemServiceHandler.getCity({
						'data' : {
							'province_id' : $scope.profileDetail.province
						}
					})
					.then(function(response){
						$scope.cityList = (response.result.cities.length) ? response.result.cities : null;
						    $scope.openModal('profile-content');
					});

	}

	$scope.updateCard = function() {
		profileService.updateCard({
			'data' : $scope.cardDetail
		}).then( function(response) {
			if(response.error_code == 200){
				$scope.formErrorMessage = response.msg_string;
				$scope.groupAlertTitle = 'ALERT';
				$scope.groupAlertContent = 'Your card has been updated successfully';
				$scope.closeModal('card-content');
				$scope.openModal('update-alert-content');
			} else {
				$scope.formErrorMessage = response.msg_string;
			}
		});
	}

	$scope.openDeleteCard = function() {
        $scope.closeModal('setting-content');
		$scope.openModal('delete-card-content');
	}

	$scope.deleteCard = function(){
			profileService.deleteCard(null)
				.then( function(response) {
					if(response.error_code == 200){
						$scope.formErrorMessage = response.msg_string;
						$scope.closeModal('delete-card-content');
						$scope.groupAlertTitle = 'ALERT';
						$scope.groupAlertContent = 'Successfully Deleted';
						$scope.openModal('update-alert-content');
					} else {
						$scope.formErrorMessage = response.msg_string;
					}
			 });
	}

	$scope.openCard = function() {
		$scope.closeModal('setting-content');
		$scope.cardDetail = {
				'name' : $scope.profileDetail.name,
				'number' : $scope.profileDetail.number,
				'exp_date' : $scope.profileDetail.exp_date,
				'cvc' : $scope.profileDetail.cvc
		};

		$scope.openModal('card-content');
	}

	$scope.resetPassword = function() {
			if($scope.passwordData.newPassword != $scope.passwordData.repeatPassword){
				$scope.formErrorMessage = "Repeat password not matching";
				return;
			}
			profileService.updatePassword({
				'data' : $scope.passwordData
			}).then( function(response) {
				if(response.error_code == 200){
					$scope.formSuccessMessage = response.msg_string;
					$scope.closeModal('password-reset-content');
				} else {
					$scope.formErrorMessage = response.msg_string;
				}
			});
	}

	$scope.openPasswordReset = function() {
			$scope.closeModal('profile-content');
			$scope.passwordData = {
				'id' : $scope.profileDetail.user_id,
				'oldPassword' 	: '',
				'newPassword' 	: '',
				'repeatPassword': ''
			};
			$scope.openModal('password-reset-content');
	}

	$scope.getProvince = function(id){
		if(id){
			itemServiceHandler.getProvince({
						'data' : {
							'country_id' : id
						}
					})
					.then(function(response){
						$scope.provinceList = (response.result.province.length) ? response.result.province : null;
						if(response.result.province.length){
							$scope.activeProvince = true;
						}
					});
		}
	}

	$scope.getCity = function(id){
		if(id){
			itemServiceHandler.getCity({
						'data' : {
							'province_id' : id
						}
					})
					.then(function(response){
						$scope.cityList = (response.result.cities.length) ? response.result.cities : null;
						if(response.result.cities.length){
							$scope.activeCity = true;
						}
					});
		}
	}

	$scope.uploadCoverImage = function(image){
       profileService.uploadCoverImage({
						'data' : {
							'cover_image' : image
						}
					})
					.then(function(response){
				    	$state.reload();
					});
	}

	$scope.updateSubscription = function() {

		profileService.updateSetting({
						'data' : {
							'updateSubscription_name' : $scope.profileDetail.subscription_name,
							'updateSubscription_type' : $scope.profileDetail.subscription_type
						}
					})
					.then(function(response){
						$scope.closeModal('setting-content');
						$scope.groupAlertTitle = 'SUCCESS';
						$scope.groupAlertContent = 'Your setting has been updated Successfully.';
						$scope.openModal('update-alert-content');
					});
	}

	$scope.updateSubscriptionPeriod = function() {
		if($scope.profileDetail.subscription_type && $scope.profileDetail.subscription_type != 'ALL') {
				if($scope.profileDetail.subscription_name == $scope.profileDetailBackup.subscription_name){
					$scope.groupAlertTitle = 'No Changes';
					$scope.groupAlertContent = 'Nothing to update.';
					$scope.openModal('update-alert-content');
					return;
				}

				profileService.updateSetting({
								'data' : {
									'updateSubscription_name' : $scope.profileDetail.subscription_name,
									'updateSubscription_type' : $scope.profileDetail.subscription_type
								}
							})
							.then(function(response){
								$scope.closeModal('setting-content');
								$scope.groupAlertTitle = 'SUCCESS';
								$scope.groupAlertContent = 'Your setting has been updated Successfully.';
								$scope.openModal('update-alert-content');
							});
		} else {
			$scope.groupAlertTitle = 'Invalid Request';
			$scope.groupAlertContent = 'Please select valid subscription.';
			$scope.openModal('update-alert-content');
		}

	}
}

function OtherProfileController($scope, $rootScope, $timeout, $state, profileService, modalFactory, modalService, itemServiceHandler, contactServiceHandler) {
	if(typeof $state.params != undefined){
		if(typeof $state.params.profileId != 'undefined' && $state.params.profileId){
			$scope.profile_id = $state.params.profileId;
		}

	} else {
			return $state.go('contact');
	}

	$rootScope.notifications = profileService.notifications();

			$timeout(function(){
				profileService.removeUserNotifications({'data' : {'type' : 'profile'}})
							.then(function(response){
									$rootScope.notifications = response.result;
						})
			}, 1000);

	$scope.openModal = function(id) {
		$scope.modalErrorMessage = "";
		$scope.modalSuccessMessage = "";
		modalFactory.Open(id);
	}

	$scope.closeModal = function(id) {
		$scope.modalErrorMessage = "";
		$scope.modalSuccessMessage = "";
		modalFactory.Close(id);
	}

	$scope.setPagination = function(result) {
			$scope.currentPage = result.current_page;
			$scope.totalPage = result.total;
	}
	$scope.profileDetail = {};

	$scope.getUserProfile = function() {
		itemServiceHandler.getProfile({
							'data' : {
								'id' : $scope.profile_id
							}
						})
						.then(function(response) {
							 $scope.profileDetail = (response.result);

						});
  }

  itemServiceHandler.getShowcase({
							'data' : {
								'user_id' : $scope.profile_id
							}
						})
						.then(function(response) {
								$scope.setPagination(response.result);
								$scope.userOriginalItems = (response.result.data.length) ? response.result.data : null;

								var i,j,temparray = [],chunk = 4;
								for (i=0,j=$scope.userOriginalItems.length; i<j; i+=chunk) {
								    	temparray.push($scope.userOriginalItems.slice(i, i+chunk));
								}

								$scope.userItems = temparray;
						});


		contactServiceHandler.getContactGroup()
						.then(function(respose){
								$scope.contactGroup = respose.result;
						});

		contactServiceHandler.getUserContactGroup({
								'data' : {
										'userId' : $scope.profile_id
								}
						})
						.then(function(respose){
								$scope.userContactGroup = respose.result;
						});

	  $scope.addContact = function(){
			
			var selectedGroup = [];
			angular.forEach($scope.selectedGroup, function(value, index){
				if(value){
						selectedGroup.push(index);
				};
			});

			if(selectedGroup.length) {
				var param = {
					'data' : {
							group : selectedGroup,
							contactId : $scope.profile_id
					}
				};

				contactServiceHandler.addContact(param)
						.then(function(response){
								if(response.error_code == 200) {
									$scope.userContactGroup = response.result;
									$scope.modalSuccessMessage = response.msg_string;
									$scope.closeModal('add-contact-content');
									$scope.groupAlertTitle = 'Success';
									$scope.groupAlertContent = 'Your request has been processed successfully';
									$scope.openModal('profile-alert-content');
								} else {
									$scope.modalErrorMessage = response.msg_string;
								}
						});
			} else {
				$scope.modalErrorMessage = "Please select a contact group";
			}
		}

		$scope.openAddContactPopup = function() {
			$scope.selectedGroup = [];
			angular.forEach($scope.contactGroup, function(value, index){
				hasElement	= _.where($scope.userContactGroup, {'id' : value.id});
				if(hasElement.length) {
					$scope.selectedGroup[value.id] = true;
				} else {
					$scope.selectedGroup[value.id] = false;
				}
			})

			$scope.openModal('add-contact-content');
		}

		$scope.getUserProfile();
}

scedgeControllers
	.controller('ProfileController', ['$scope', '$rootScope', '$timeout', '$state', 'ProfileServiceHandler', 'ModalFactory', 'ModalService', 'ItemServiceHandler', 'AuthServiceHandler', 'ImageUploader', ProfileController])
	.controller('OtherProfileController', ['$scope', '$rootScope', '$timeout', '$state', 'ProfileServiceHandler', 'ModalFactory', 'ModalService', 'ItemServiceHandler', 'ContactServiceHandler', OtherProfileController]);
