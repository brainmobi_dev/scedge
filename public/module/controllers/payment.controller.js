function PaymentController($scope, $state, $stateParams, $validation, modalService,modalFactory, authServiceHandler, storageServiceHandler){
	if(typeof $stateParams.verificationCode != 'undefined') {
		$scope.verification_code = $stateParams.verificationCode;
	} else {
		return $state.go('join');
	}

	$scope.paymentStep = 1;

	$scope.confirmPayment = function() { 
		$scope.paymentStep = 2;
	}

	$scope.openModal = function(id){
        modalFactory.Open(id);
    };

    $scope.closeModal = function(id){
	    modalFactory.Close(id);
    }

	$scope.filterValues = function(formData) {
		$formData = {
			'subscription_id' : $scope.user.subscription_id
		};
		
		angular.forEach(formData, function(value, index){
			if(typeof value != 'function') {
				$formData[index] = value;
			}
		});

		angular.forEach(['registration_stage', 'user_id'], function(value, index){
			$formData[value] = (typeof $scope.user[value] != 'undefined') ? $scope.user[value] : 0;
		});

		if(typeof  $scope.user['id'] != 'undefined') {
			$formData['user_id'] = $scope.user['id'];
		}

		return $formData;
	}

	$scope.form = {
        submit: function (form) {
        	$validation.validate(form)
                .success(function() {
                	
                	return authServiceHandler.processPaymentRequest({
                		"data" : $scope.filterValues($scope.form)
                	})
                	.then(function(response) {
                		if(response.error_code  == 200) {
                			storageServiceHandler.setValue('token', 'Bearer ' + response.token); 

							$scope.openModal('success-process');
                		} else {
	                	   	$scope.errorMessage = response.msg_string;
						}
	            	});

                })
                .error(function(){
                	$scope.errorMessage = "Please fill required field";
                });
        }
    }

    $scope.completePayment = function(){
    	$scope.closeModal('success-process');
    	$state.go('myItem', { nextState: '', data: []});
    }

    authServiceHandler.getVerificationDetail({
    		'data' : {
    			'verification_code' : $scope.verification_code
    		}
    	})
		.then(function(response){
			if(response.error_code == 200){
				$scope.user = response.result;
			} else {
				$state.go('join');
			}
		});


    authServiceHandler.getSubscription()
		.then(function(response){
			if(response.error_code == 200){
				$scope.subscriptionList = response.result;
			}
		});

}

scedgeControllers
	.controller('PaymentController', ['$scope', '$state', '$stateParams', '$validation', 'ModalService','ModalFactory', 'AuthServiceHandler', 'StorageServiceHandler', PaymentController])