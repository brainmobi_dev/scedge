function BrowseController($scope, $rootScope, $timeout, $state, modalFactory, modalService, itemServiceHandler, profileServiceHandler){

	$rootScope.notifications = profileServiceHandler.notifications();
	$timeout(function(){
		profileServiceHandler.removeUserNotifications({'data' : {'type' : 'browse'}})
							.then(function(response){
									$rootScope.notifications = response.result;
							})
	}, 1000);

	$scope.dateAsc = true;
	$scope.browseItems = {};
	$scope.filterData = {};
	$scope.currentPage = 1;
	$scope.activeSearchProvince = false;
	$scope.activeSearchCity = false;

	$scope.changeDateOrder = function(status) {
		$scope.dateAsc = status;
		$scope.searchBrowseItems(0);
	}



	$scope.openModal = function(id){
			$scope.reportAbuseError = "";
			modalFactory.Open(id);
	}

	$scope.closeModal = function(id){
			modalFactory.Close(id);
	}


	$scope.setPagination = function(result) {
			$scope.currentPage = result.current_page;
			$scope.totalPage = result.total;
	}

	$scope.resetBrowseItems = function() {
		$scope.filterData = {};
		$scope.searchText = "";
		$scope.closeModal('browse-filter-content');
		browseItemsList(null);
	}

	$scope.browseItems = [];

	function browseItemsList(params) {
		$scope.isRequestAllower = false;
		itemServiceHandler.browseItems(params)
						.then(function(response){
							$scope.isRequestAllower = true;
								$scope.setPagination(response.result)

								$scope.resultBrowseItems = (response.result.data.length) ? response.result.data : null;

								if($scope.browseItems.length) {
									for(var i=0; i<$scope.resultBrowseItems.length;i++) {
										$scope.browseItems.push($scope.resultBrowseItems[i]);
									}
								} else {
									$scope.browseItems = $scope.resultBrowseItems;
								}
								$scope.countryList = (response.others.countries.length) ? response.others.countries : null;
								$scope.userList = (response.others.users.length) ? response.others.users : null;
								$scope.closeModal('browse-filter-content');
						});
	}

	$scope.selectitem = function(selectedUserItem){
		$scope.selectedUserItem = selectedUserItem;
	}

	$scope.isRequestAllower = true;
	$scope.pageNumber = 1;

	$scope.filterBrowseItems = function(newPageNumber, oldPageNumber){
			if($scope.isRequestAllower) {
				 $scope.searchBrowseItems(++$scope.pageNumber);
			}
	}

	$scope.searchBrowseItems = function(pageNumber){

            $filter = {};

            if($scope.selectedUser){
            	$scope.filterData['user'] = $scope.selectedUser.title;
        	}

			if(pageNumber){
				$filter['page'] = pageNumber;
			}

			if($scope.searchText) {
					$scope.filterData['searchText'] = $scope.searchText;
			}

			if($scope.filterData) {
					$filter['filter'] = $scope.filterData;
			}

			$filter['sort'] = {
				'by_date' : ($scope.dateAsc) ? 'asc' : 'desc'
			}

			browseItemsList({
				'data' : $filter
			});
	}

	$scope.getProvince = function(id){
		if(id){
			itemServiceHandler.getProvince({
						'data' : {
							'country_id' : id
						}
					})
					.then(function(response){
						$scope.provinceList = (response.result.province.length) ? response.result.province : null;
						if(response.result.province.length){
							$scope.activeSearchProvince = true;
						}
					});
	    }
	}

    $scope.getCity = function(id){
	    if(id){
		    itemServiceHandler.getCity({
					'data' : {
						'province_id' : id
					}
				})
				.then(function(response){
					$scope.cityList = (response.result.cities.length) ? response.result.cities : null;
					if(response.result.cities.length){
						$scope.activeCity = true;
                        $scope.activeSearchCity = true;
					}
				});
	    }
    }

	browseItemsList($scope.filterData);
	itemServiceHandler.getCardGroup()
						.then(function(response){
							$scope.cardGroup = response.result;
						})
}


function BrowseDetailController($scope, $rootScope, $timeout, $state, modalFactory, modalService, itemServiceHandler, profileServiceHandler){
		if(typeof $state.params != undefined) {
			 if(typeof $state.params.cardId != 'undefined' && $state.params.cardId){
					$scope.cardId = $state.params.cardId;
 			}

		} else {
			$state.go("browse");
		}

		$rootScope.notifications = profileServiceHandler.notifications();
		$timeout(function(){
			profileServiceHandler.removeUserNotifications({'data' : {'type' : 'browse'}})
													 .then(function(response){
															$rootScope.notifications = response.result;
													 });
		}, 1000);

		$scope.openModal = function(id){
				modalFactory.Open(id);
		}

		$scope.closeModal = function(id){
				modalFactory.Close(id);
		}

		$scope.browseItems = {};
		$scope.filterData = {
			'notCardId' :  $scope.cardId
		};
		$scope.currentPage = 1;

		$scope.resetBrowseItems = function() {
			$scope.filterData = {
				'notCardId' :  $scope.cardId
			};
			$scope.closeModal('browse-filter-content');
			$scope.getOtherCards({
					'data' : {
						'filter' : $scope.filterData
					}
			})
		}

		$scope.resetAbuseList = function() {
			for(i=0;i<$scope.listAbuse.length;i++){
				if(document.getElementById($scope.listAbuse[i].id).checked == true){
					document.getElementById($scope.listAbuse[i].id).checked = false;
				}
			}

			$scope.closeModal('browse-abuse-content');

		}

		$scope.setPagination = function(result) {
				$scope.currentPage = result.current_page;
				$scope.totalPage = result.total;
		}

		function browseItems(params) {
				return itemServiceHandler.browseItems(params);
		}

		itemServiceHandler.getCardGroup()
				.then(function(response){
						$scope.cardGroup = response.result;
				})

		$scope.blockCard = function(card) {
			$scope.browse_card_id = card;
			itemServiceHandler.getReports()
				.then(function(response){
					$scope.listAbuse = response.result;
					$scope.openModal('browse-abuse-content');
				})

		}

		$scope.reportAbuse = function(){
			$scope.reportAbuseError = "";
			var card_id = $scope.browse_card_id;
			var abuseList = [];
			for(i=0;i<$scope.listAbuse.length;i++){
				if(document.getElementById($scope.listAbuse[i].id).checked == true){
					abuseList.push($scope.listAbuse[i].id);
				}
			}

			if(!abuseList.length) {
				$scope.reportAbuseError = "Please select a abuse type";
				return;
			}


			itemServiceHandler.reportAbuse({
				'data' : {
					'card_id' : card_id,
					'report_id' : abuseList
				}
			})
			.then(function(response){
				$scope.closeModal('browse-abuse-content');
				$scope.groupAlertTitle = 'SUCCESSFULLY REPORTED';
				$scope.groupAlertContent = 'ITEM HAS BEEN REPORTED SUCCESSFULLY';
				$scope.openModal('group-alert-content');
			});
		}



		browseItems({
				'data' : {
					'filter' : {
						'cardId' : $scope.cardId
					}
				}
			})
			.then(function(response){
					$scope.selectedCard = (response.result) ? response.result : null;
			});

	 $scope.getOtherCards = function($filter) {
			 browseItems($filter)
				 .then(function(response){
						 $scope.setPagination(response.result)
						 $scope.otherCard = (response.result.data.length) ? response.result.data : null;
						 $scope.closeModal('browse-filter-content');
				 });
	 }

		$scope.filterBrowseItems = function(newPageNumber, oldPageNumber) {
			  $filter = {
						'data' : {
							'page' : newPageNumber,
							'filter' : $scope.filterData
						}
				};
				$scope.getOtherCards($filter);
		}

		$scope.searchBrowseItems = function(){
				$scope.filterData["searchText"] = $scope.searchText;

				$scope.getOtherCards({
						'data' : {
								'filter' : $scope.filterData
						}
				});
		}

		$scope.getOtherCards({
				'data' : {
					'filter' : $scope.filterData
				}
		});
}

scedgeControllers
	.controller('BrowseDetailController', ['$scope', '$rootScope', '$timeout', '$state', 'ModalFactory', 'ModalService', 'ItemServiceHandler', 'ProfileServiceHandler', BrowseDetailController])
	.controller('BrowseController', ['$scope', '$rootScope', '$timeout', '$state','ModalFactory', 'ModalService', 'ItemServiceHandler', 'ProfileServiceHandler', BrowseController]);
