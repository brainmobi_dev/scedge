function ChatController($scope, $rootScope, $timeout, $state, $location, $routeParams, $firebaseObject, authServiceHandler, storageServiceHandler, profileServiceHandler, ImageUploader, modalFactory){
		var chatRoomId = 0;
		$scope.chatRoomId = null;
		if(typeof $state.params != undefined){
			 if(typeof $state.params.chatroomId != 'undefined' && $state.params.chatroomId){
					$scope.chatRoomId = chatRoomId = $state.params.chatroomId;
			 }
		}


	  $scope.openModal = function(id) {
	    uploader.uploadedImage = "";
	    modalFactory.Open(id);
	  };

		$scope.closeModal = function(id) {
	    uploader.clearQueue();
	    modalFactory.Close(id);
	  };

		$rootScope.notifications = profileServiceHandler.notifications();
		$timeout(function(){
				profileServiceHandler.removeUserNotifications({'data' : {'type' : 'chat'}})
									.then(function(response){
											$rootScope.notifications = response.result;
								})
		}, 1000);

		$scope.isContactEmail = function(email) {
			return (_.indexOf($scope.authContactList, email) >= 0);
		}

		$scope.menuItems = [{
				text: "Copy Message",
				disabled: false
			}, {
				text:"Select All",
				disabled: false
			}, {
				text:"Remove Message",
				disabled: false
		}];

		var uploader = $scope.uploader = ImageUploader;

	  $scope.removeImageItems = function(){
	      console.log(uploader);
	  }

	  $scope.initializeUploader = function() {
	    uploader.onAfterAddingFile = function(item) {

	      item.croppedImage = '';
	      var reader = new FileReader();
	      reader.onload = function(event) {
	        $scope.$apply(function() {
	          item.image = event.target.result;
	        });
	      };
	      reader.readAsDataURL(item._file);
	    };
	  }

	  $scope.initializeUploader();

		//$scope.URL = "https://chatapp-f268c.firebaseio.com/";
		$scope.URL = "https://scedge-175411.firebaseio.com/";

		$scope.userDetail = storageServiceHandler.getValue('userDetail');
		$scope.email = $scope.userDetail.email;
		//$scope.email = 'sunit@gmail.com';

		$scope.intialMessage = "Loading chat window...";
		$scope.intialButton = false;

		var userListRef = new Firebase($scope.URL + "userList");
		var userListRefObj = $firebaseObject(userListRef);
		userListRefObj.$bindTo($scope, "userList");

		userListRefObj.$loaded().then(function() {
			$authContactList = [];
			$validContactList = [];
			$authUserList = (_.where(userListRefObj, {'email' : $scope.email}));

			angular.forEach($authUserList[0].chatRoomMate , function(value, key){
					$authContactList.push(value.chatMate);
			});

			/*angular.forEach(userListRefObj , function(value, key){
					if(typeof value.email != 'undefined') {
						if(_.indexOf($authContactList, value.email) >= 0){
								$validContactList[key] = value;
						}
					}

			});*/

			$scope.authContactList = $authContactList;
			if(!$scope.authContactList.length) {
				$scope.intialMessage = "​ You have not added any contacts yet. Please add a contact to chat with them.";
				$scope.intialButton = true;
			}
		});



		userListRef.on("value", function(snapshot){
			var data = snapshot.val();

			var userIndex;
			var result = Object.keys(data).map(function(e, i){
				if(data[e].email == $scope.email){
					userIndex = e;
				}
				return [Object(e), data[e]];
			});
			$scope.userListData = result;

			for(var i=0;i<$scope.userListData.length;i++){
				if($scope.userListData[i][1].email == $scope.email){
					break;
				}
			}

			if(typeof $scope.userListData[i] != 'undefined')
				storageServiceHandler.setValue('uKey', btoa($scope.userListData[i][0].valueOf()));
		});

		var chatRoomRef = new Firebase($scope.URL + "chatRooms");
		var chatRoomRefObj = $firebaseObject(chatRoomRef);
		chatRoomRefObj.$bindTo($scope, "activeChatRooms");

		$scope.validUsers = [];
		chatRoomRefObj.$loaded().then(function() {
				_.each($scope.activeChatRooms, function(value, index){
						if(index == $scope.chatRoomId) {
								$scope.validUsers.push(value.createdBy);
								$scope.validUsers.push(value.createdFor);
						}
				});
		})

		var userListChatRoomMate = "";
		$scope.getLastMessage = function(item){
			userListChatRoomMate = _.findKey(item.chatRoomMate, {'chatMate' : $scope.email});
			if(typeof userListChatRoomMate == 'undefined'   || typeof $scope.activeChatRooms == 'undefined' || typeof $scope.activeChatRooms[item.chatRoomMate[userListChatRoomMate].chatRoom] == 'undefined'){
				return "";
			}
			else{
				if(typeof $scope.activeChatRooms[item.chatRoomMate[userListChatRoomMate].chatRoom].lastMessage == 'undefined'){
					return "";
				}
				else{
					return $scope.activeChatRooms[item.chatRoomMate[userListChatRoomMate].chatRoom].lastMessage.message;
				}
			}
		}

		$scope.getLastMessagePostedBy = function(item){
			userListChatRoomMate = _.findKey(item.chatRoomMate, {'chatMate' : $scope.email});
			if(typeof userListChatRoomMate == 'undefined'   || typeof $scope.activeChatRooms == 'undefined' || typeof $scope.activeChatRooms[item.chatRoomMate[userListChatRoomMate].chatRoom] == 'undefined'){
				return "";
			}
			else{
				if(typeof $scope.activeChatRooms[item.chatRoomMate[userListChatRoomMate].chatRoom].lastMessage == 'undefined'){
					return "";
				}
				else{
					return $scope.activeChatRooms[item.chatRoomMate[userListChatRoomMate].chatRoom].lastMessage.postedBy;
				}
			}
		}

		$scope.getLastMessageTime = function(item){
			userListChatRoomMate = _.findKey(item.chatRoomMate, {'chatMate' : $scope.email});
			if(typeof userListChatRoomMate == 'undefined' || typeof $scope.activeChatRooms == 'undefined' || typeof $scope.activeChatRooms[item.chatRoomMate[userListChatRoomMate].chatRoom] == 'undefined'){
				return "";
			}
			else{
				if(typeof $scope.activeChatRooms[item.chatRoomMate[userListChatRoomMate].chatRoom].lastMessage == 'undefined'){
					return "";
				}
				else{
					return $scope.activeChatRooms[item.chatRoomMate[userListChatRoomMate].chatRoom].lastMessage.postedDate;
				}
			}
		}

		$scope.unReadMessageCount = function(item){
			userListChatRoomMate = _.findKey(item.chatRoomMate, {'chatMate' : $scope.email});
			if(typeof userListChatRoomMate == 'undefined'  || typeof $scope.activeChatRooms == 'undefined' || typeof $scope.activeChatRooms[item.chatRoomMate[userListChatRoomMate].chatRoom] == 'undefined'){
				return "";
			}
			else{
				if(typeof $scope.activeChatRooms[item.chatRoomMate[userListChatRoomMate].chatRoom].chatMessages == 'undefined'){
					return "";
				} else {
					var count = 0;
					var data = Object.values($scope.activeChatRooms[item.chatRoomMate[userListChatRoomMate].chatRoom].chatMessages);
					for(var i=0; i<data.length; i++){
						if(data[i].isRead === false && data[i].postedBy !== $scope.email){
							count++;
						}
					}
					return count;
				}
			}
		}

		$scope.joinChat = function(index, item){
			$scope.uKey = atob(storageServiceHandler.getValue('uKey'));
			var chatRoomId = btoa($scope.email) + "-" + btoa(item.email);
			var chatRoomMateRef = new Firebase($scope.URL + "userList/" + $scope.uKey + "/chatRoomMate");
			var chatRoomMateBox = "";
			chatRoomMateRef.on('value', function(data){
				var data = data.val();
				chatRoomMateBox = (_.findKey(data, {"chatMate" :  item.email}));

				if(typeof chatRoomMateBox == 'undefined'){
					chatRoomMateRef.push({
						"chatMate" : item.email,
						"chatRoom" : chatRoomId
					});
				}	else {
					chatRoomId = data[chatRoomMateBox].chatRoom;
				}
			});

			var chatRoomMateRef = new Firebase($scope.URL + "userList/" + index + "/chatRoomMate");
			chatRoomMateRef.on('value', function(data){
				var data = data.val();
				chatRoomMateBox = (_.findKey(data, {"chatMate" : $scope.email}));

				if(typeof chatRoomMateBox == 'undefined'){
					chatRoomMateRef.push({
						"chatMate" : $scope.email,
						"chatRoom" : chatRoomId
					});
				} else {
					chatRoomId = data[chatRoomMateBox].chatRoom;
				}
			});

			var chatRoomIdRef = new Firebase($scope.URL + "chatRooms/" + chatRoomId);
			var chatRoomRef = new Firebase($scope.URL + "chatRooms/");
			var chatRoomBox = "";
			chatRoomRef.on('value', function(data){
				var data = data.val();
				chatRoomBox = (_.findKey(data, function(v, k){
					if(k === chatRoomId){
						return k;
					}
				}));

				if(typeof chatRoomBox == 'undefined'){
					chatRoomIdRef.set({
						createdBy: $scope.email,
						createdFor: item.email,
						createdDate: Date.now()
					});
				}
				else{
					chatRoomId = chatRoomBox;
				}
			});

			$location.path('message-inbox/' + chatRoomId);

		}

		$scope.getMessage = function(chatRoomId){
			var chatRoomIdRef = new Firebase($scope.URL + "chatRooms/" + chatRoomId);
			$scope.lastMessageRef = chatRoomIdRef.child('lastMessage');
			$scope.chatMessagesRef = chatRoomIdRef.child('chatMessages');
			var chatMessagesRefObj = $firebaseObject($scope.chatMessagesRef);
			chatMessagesRefObj.$bindTo($scope, "chatMessages");

			$scope.chatMessagesRef.on("value", function(snapshot){
				var data = snapshot.val();
				if(!data){
					return;
				}
				else{
					var result = Object.keys(data).map(function(e){
						return [Object(e), data[e]];
					});

					for(var i=0;i<result.length;i++){
						if(result[i][1].isRead == false && result[i][1].postedBy != $scope.email){
							$scope.myMessagesRef = chatRoomIdRef.child('chatMessages/' + result[i][0]);
							if(chatRoomId == $routeParams.roomId){
								$scope.myMessagesRef.update({
									isRead: true
								});
							}
							else{
								return;
							}
						}
					}
				}
			});
		}
		$scope.getMessage(chatRoomId);

		var pushMessage = function(){
			$scope.message = wdtEmojiBundle.render($scope.message);
			$scope.chatMessagesRef.push({
				postedBy: $scope.email,
				message: $scope.message,
				postedDate: Date.now(),
				isRead: false
			});
			$scope.lastMessageRef.update({
				postedBy: $scope.email,
				message: $scope.message,
				postedDate: Date.now()
			});
			$scope.message = '';
		}

		$scope.sendMessage = function($event){
			if(!($event.which == 13))
				return;
			if((!$scope.message) || $scope.message.length == 0)
				return;
			pushMessage();
		}

		$scope.sendOnClick = function(){
			if(!$scope.message || $scope.message.length == 0)
				return;
			pushMessage();
		}
}

scedgeControllers
	.controller('ChatController', ['$scope', '$rootScope', '$timeout', '$state', '$location', '$routeParams', '$firebaseObject' , 'AuthServiceHandler', 'StorageServiceHandler', 'ProfileServiceHandler', 'ImageUploader', 'ModalFactory', ChatController]);
