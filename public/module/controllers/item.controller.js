function ItemController($scope, $rootScope, $timeout, $state, $window, $location, $timeout, modalService, modalFactory, authServiceHandler, itemServiceHandler, ImageUploader, profileServiceHandler, contactServiceHandler, toastr, socialshare, MONTHS) {
    $scope.groupId = "";
    $scope.gPlace = "";

	  $scope.form = {
      shareToContact : []
    };
	  $scope.filterData = {};
	  $scope.selectedFilter = 'all';
	  $scope.priorityList = ['Low', 'Medium', 'High'];
	  $scope.activeProvince = false;
	  $scope.activeCity = false;
	  $scope.activeSearchProvince = false;
	  $scope.activeSearchCity = false;
	  $scope.groupFlag = true;
	  $scope.showSelect = true;
	  $scope.currentDate = new Date();
	  $scope.selectedSorting = "";
	  $scope.sortingType = "asc";
	  $scope.slideDown = false;
	  $scope.slideDown1 = false;
	  $scope.checkedUserItem = [];

	  $scope.slideDown = false;
	  $scope.slideDown1 = false;
	  $scope.checkedUserItem = [];
	  $scope.availableMonths = MONTHS.NAME;
	  $scope.form.selectForShare
	  $scope.selectSharedFacebook = 0;
	  $scope.selectSharedTwitter = 0;

	  $scope.currentYear = (new Date()).getFullYear();
	  $scope.currentMonth = (new Date()).getMonth();
	  $scope.currentDate = (new Date()).getDate();

  $scope.uploadImage = function(){
    $scope.openModal('cropper-view-modal');
  }

  $scope.facebookShare = function(cardId) {
    socialshare.share({
      'provider': 'facebook',
      'attrs': {
        'socialshareUrl': 'http://scedge.apphosthub.com/item/' + cardId
      }
    });
  }

  $scope.twitterShare = function(cardId) {
    socialshare.share({
      'provider': 'twitter',
      'attrs': {
        'socialshareUrl': 'http://scedge.apphosthub.com/item/' + cardId
      }
    });
  }

  $scope.linkedinShare = function(cardId) {
    socialshare.share({
      'provider': 'linkedin',
      'attrs': {
        'socialshareUrl': 'http://scedge.apphosthub.com/item/' + cardId
      }
    });
  }

  function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }


  $scope.shareItemViaEmail = function(card) {
    if (!$scope.shareEmail) {
      toastr.error('Please provide a valid Email', 'Validation Error');
    } else {
      if (!validateEmail($scope.shareEmail)) {
        toastr.error('Please provide a valid Email', 'Validation Error');
      } else {
        itemServiceHandler.shareItemViaEmail({
            'data': {
              'email': $scope.shareEmail,
              'card': card.card_type_id
            }
          })
          .then(function(response) {
            toastr.success('Card has been shared with ' + $scope.shareEmail, 'Successfully Shared');
            $scope.closeModal('item-share-content');
          })
      }
    }
  }

  if (typeof $state.params != undefined) {
    if (typeof $state.params.groupId != 'undefined' && $state.params.groupId) {
      $scope.groupId = Number($state.params.groupId) ? Number($state.params.groupId) : "";
      $scope.selectedItem = $scope.groupId;
    }
  }

  $rootScope.notifications = profileServiceHandler.notifications();

  $timeout(function() {
    profileServiceHandler.removeUserNotifications({
        'data': {
          'type': 'item'
        }
      })
      .then(function(response) {
        $scope.notifications = $rootScope.notifications = response.result;
      })
  }, 1000);

  $scope.getMonthArray = function(monthStart, monthEnd, type) {
        var validMonths = {};

        if($scope.form.endDateYear && $scope.form.startDateYear) {
            if($scope.form.endDateYear == $scope.form.startDateYear) {
                switch(type) {
                  case "start" : if($scope.form.endDateMonth) {
                                      monthEnd = $scope.form.endDateMonth - 1;
                                 }

                  case "end" :   if($scope.form.startDateMonth) {
                                      monthStart = $scope.form.startDateMonth - 1;
                                 }
                }
            }
        }


        for (var i = monthStart; i <= monthEnd; i++) {
          for(var month in $scope.availableMonths[i]) {
              validMonths[month] = $scope.availableMonths[i][month];
          }
        }

        return validMonths;
  }

  $scope.getValidMonth = function(year, type) {
    if (year) {
      year = Number(year);
      var validMonths = $scope.availableMonths;
      var monthStart = 0;
      var monthEnd = $scope.availableMonths.length - 1;
      if (year == $scope.currentYear) {
        monthStart = $scope.currentMonth;
      }
      var validMonths = $scope.getMonthArray(monthStart, monthEnd, type);

      if (type == 'start') {
	        $scope.endYear = [];
	        for (var startYear = year; startYear <= (year + 100); startYear++) {
	          $scope.endYear.push(startYear);
	        }

	        $scope.startMonth = validMonths;
      }

      if (type == 'end') {
	        $scope.startYear = [];
	        for (var startYear = $scope.currentYear; startYear <= (year + 100); startYear++) {
	          $scope.startYear.push(startYear);
	        }
	        $scope.endMonth = validMonths;
      }
    }
  }


  $scope.createYears = function() {
      $scope.startYear = [];
      $scope.endYear = [];
      for (var i = $scope.currentYear; i <= ($scope.currentYear + 100); i++) {
        ($scope.startYear).push(i);
        ($scope.endYear).push(i);
      }
  }

  $scope.createTimeArray = function() {
    $scope.timeArray = [];
    for (var i = 1; i <= 12; i++) {
      for (var j = 0; j <= 59; j++) {
        $scope.timeArray.push(((i < 10) ? "0" + i : i) + ":" + ((j < 10) ? "0" + j : j));
      }
    }
  }

  $scope.form.timePeriod = "AM";



  $scope.isLeapYear = function(yearSelected) {
    return (((yearSelected % 4 == 0) && (yearSelected % 100 != 0)) ||
      (yearSelected % 400 == 0))
  }

  $scope.isAllowedToCompare = function(){
    reutrn (($scope.form.startDateYear && $scope.form.endDateYear) && ($scope.form.startDateMonth && $scope.form.startDateMonth))
  }

  $scope.getValidDays = function(type){

  }


  $scope.getValidTotalDays = function(yearSelected, monthSelected) {
    if (monthSelected == 2) {
      return ($scope.isLeapYear(yearSelected)) ? 28 : 29;
    }
    var monthWith31 = [1, 3, 5, 7, 8, 10, 12];
    return (monthWith31.indexOf(monthSelected) == -1) ? 30 : 31;
  }

  $scope.getDateArray = function(yearSelected, monthSelected) {
    if(yearSelected && monthSelected) {
      var totalDays = 0;
      var daysArray = [];
      var startFrom = 1;

      if ($scope.currentYear == yearSelected) {
        if (monthSelected == ($scope.currentMonth + 1)) {
          startFrom = $scope.currentDate;
        }
      }

      totalDays = $scope.getValidTotalDays(yearSelected, monthSelected);

      for (var j = startFrom; j <= totalDays; j++) {
        (daysArray).push(j);
      }
      return daysArray;
    }
  }

  $scope.getValidDates = function(type) {
    var monthStart = 0;
    var monthEnd = $scope.availableMonths.length -1;
    if (type == 'start') {
      $scope.startDays = $scope.getDateArray($scope.form.startDateYear, $scope.form.startDateMonth);
      monthStart = $scope.form.startDateMonth - 1;
      $scope.endMonth = $scope.getMonthArray(monthStart, monthEnd, type);
   } else {
      $scope.endDays = $scope.getDateArray($scope.form.endDateYear, $scope.form.endDateMonth);
      monthEnd = $scope.form.endDateMonth - 1;
      $scope.startMonth = $scope.getMonthArray(monthStart, monthEnd, type);
    }
  }



  $scope.createAge = function() {
    $scope.ageYears = [];
    for (var i = 10; i <= 60; i++) {
      ($scope.ageYears).push(i);
    }
  }

  $scope.createAge();


  $scope.disbledEmail = function() {
    if ($scope.form.shareToContact == undefined || $scope.form.shareToContact == null || !$scope.form.shareToContact) {
      $scope.emailShowValue = false;
    } else {
      $scope.emailShowValue = true;
    }
  }

  $scope.disableContactList = function() {

    if (!$scope.form.email) {
      $scope.contactShowValue = false;
    } else {
      $scope.contactShowValue = true;
    }
  }

  $scope.selectFacebook = function() {

    if ($scope.selectSharedFacebook == 0 || $scope.selectSharedFacebook == undefined) {
      $scope.selectSharedFacebook = 1;
      ($scope.form.selectForShare).push('Facebook')
    } else {
      $scope.selectSharedFacebook = 0;
      ($scope.form.selectForShare).pop('Facebook')
    }
  }

  $scope.selectTwitter = function() {
    if(!$scope.selectForShare)
        $scope.selectForShare = [];

    if ($scope.selectSharedTwitter == 0 || $scope.selectSharedTwitter == undefined) {
      $scope.selectSharedTwitter = 1;
      ($scope.selectForShare).push('Twitter')
    } else {
      $scope.selectSharedTwitter = 0;
      ($scope.selectForShare).pop('Twitter')
    }
  }


  $scope.getContacts = function($params) {
    contactServiceHandler.getContacts($params)
      .then(function(response) {
          $scope.setPagination(response.result);
          $scope.contactList = (response.result.data.length) ? response.result.data : null;

          $scope.contactItems = [];

          _.each($scope.contactList, function(value, index){
              $scope.contactItems.push({id : value.id, label : value.name});
          })
      });
  }


  $scope.getContacts();

  $scope.slideDownFun = function() {
    $scope.slideDown = !$scope.slideDown
  }
  $scope.slideDownFun1 = function() {
    $scope.slideDown1 = !$scope.slideDown1
  }



  $scope.createItem = function() {
    $scope.error = [];

    if (!$scope.form.title) {
      $scope.form.titleError = "Title is required";
      return;
    } else {
      $scope.form.titleError = "";
    }

    if ($scope.form.startDateYear || $scope.form.startDateMonth || $scope.form.startDayDate) {
        if (!$scope.form.startDateMonth) {
          $scope.error.push('Please select valid Month')

        }
        if (!$scope.form.startDayDate) {
          $scope.error.push('Please select valid Day')
        }
        if (!$scope.form.startDateYear) {
          $scope.error.push('Please select valid Year')
        }
    }

    if ($scope.userDetail.subscription_name != 'FREE') {
      if($scope.form.endDateYear || $scope.form.endDateMonth || $scope.form.endstartDayDate) {
        if (!$scope.form.endDateYear) {
          $scope.error.push('Please select valid Year')
        }
        if (!$scope.form.endDateMonth) {
          $scope.error.push('Please select valid Month')

        }
        if (!$scope.form.endDayDate) {
          $scope.error.push('Please select valid Day')
        }
      }
    }


    if ($scope.form.ageFrom || $scope.form.ageTo) {
      if (!$scope.form.ageFrom) {
        $scope.error.push('Please select Age From')
      }
      if (!$scope.form.ageTo) {
        $scope.error.push('Please select Age To')
      }
      if ($scope.form.ageTo <= $scope.form.ageFrom) {
        $scope.error.push('Age To must be greater Than Age From')
      }
    }

    if (($scope.error).length > 0) {
      for (var i = 0; i < ($scope.error.length); i++) {
        toastr.error($scope.error[i], 'Validation Error');
      }
      return;
    }

    if ($scope.form.startDateYear && $scope.form.startDateMonth && $scope.form.startDayDate) {
      $startDateValue = $scope.convertToDate(['startDateYear', 'startDateMonth', 'startDayDate']);
      if ($startDateValue) {
        $scope.form.start_date = $startDateValue;
      }
    }

    if ($scope.form.startTime && $scope.form.startTimePeriod) {
      $startTimeValue = $scope.convertToTime($scope.form.startTime, $scope.form.startTimePeriod);
      if ($startTimeValue) {
        $scope.form.start_time = $startTimeValue;
      }
    }


    if ($scope.userDetail.subscription_name != 'FREE') {
      if ($scope.form.endDateYear && $scope.form.endDateMonth && $scope.form.endDayDate) {
        $startDateValue = $scope.convertToDate(['endDateYear', 'endDateMonth', 'endDayDate']);
        if ($startDateValue) {
          $scope.form.start_date = $startDateValue;
        }
      }

      if ($scope.form.endTime && $scope.form.endTimePeriod) {
        $startTimeValue = $scope.convertToTime($scope.form.endTime, $scope.form.endTimePeriod);
        if ($startTimeValue) {
          $scope.form.start_time = $startTimeValue;
        }
      }
    }


    $scope.createItemResponse = "";
    if ($scope.userDetail.subscription_name == 'PROFESSIONAL') {
      if ($scope.form.live_item == true) {
        $scope.form.live_item = true;
      } else {
        $scope.form.live_item = false;
      }
    }

    if ($scope.userDetail.subscription_name == 'PERSONAL') {
      if ($scope.form.two_way_sync == true) {
        $scope.form.two_way_sync = true;
      } else {
        $scope.form.two_way_sync = false;
      }
    }


    if ($scope.form.share_on_showcase == true) {
      $scope.form.share_on_showcase = true;
    } else {
      $scope.form.share_on_showcase = false;
    }

    if($scope.groupId){
      $scope.form.card_type_id = $scope.groupId;
    }

    if (uploader.uploadedImage)
      $scope.form.card_image = uploader.uploadedImage


   itemServiceHandler.createItem({
        'data': {
          'item': $scope.form
        }
      })
      .then(function(response) {
        if (response.error_code == 200) {
          $scope.closeItem();
          //$scope.alertTitle = "Success";
          //$scope.alertContent = "Card has been created";
          //modalFactory.Open('item-alert-content');
          //
          $scope.getItems(null);
          toastr.success('Card has been created', 'Success', {
            onShown : function() {
              $scope.getItems(null);
              $state.reload();
            }
          });
        } else {
          toastr.error(response.msg_string, 'Error');
          $scope.createItemResponse = response.msg_string;
        }

        $scope.closeItem();
      });

  };

  $scope.editItem = function(formDataset) {
    $scope.createTimeArray();
    $scope.createYears();

    var userItems = _.extend({}, formDataset);
    userItems = validateValues(userItems);

    $scope.selectedItem = userItems;
    $scope.form = userItems;
    $scope.form.shareToContact = [];
    if (!$scope.form.selectForShare) {

    } else if ($scope.form.selectForShare.length > 0) {
      var indexFacebook = ($scope.form.selectForShare).indexOf('Facebook')
      $scope.selectSharedFacebook = (indexFacebook < 0) ? 0 : 1;
      var indexTwitter = ($scope.form.selectForShare).indexOf('Twitter')
      $scope.selectSharedTwitter = (indexTwitter < 0) ? 0 : 1;
    }
    $scope.form.location_country = ($scope.form.location_country) ? $scope.form.location_country.toString() : "";
    $scope.form.street_address = ($scope.form.street_address) ? $scope.form.street_address.toString() : "";
    $scope.form.location_province = ($scope.form.location_province) ? $scope.form.location_province.toString() :"";
    $scope.form.location_city = ($scope.form.location_city) ? $scope.form.location_city.toString() : "";
    $scope.form.discount = Number($scope.form.discount);

    if(userItems.start_date){
        var dateArray = (userItems.start_date).split("-");
        $scope.form.startDateYear = Number(dateArray[0]);
        $scope.form.startDateMonth = Number(dateArray[1]);
        $scope.form.startDayDate = Number(dateArray[2]);


        $scope.getValidMonth(dateArray[0], 'start');
        $scope.getValidDates('start');
    }

    if(userItems.start_time){
        $scope.form.startTime = userItems.start_time;
        $scope.form.startTimePeriod = 'AM';
    }

    if(userItems.end_date){
        var dateArray = (userItems.end_date).split("-");
        $scope.form.endDateYear = Number(dateArray[0]);
        $scope.form.endDateMonth = Number(dateArray[1]);
        $scope.form.endDayDate = Number(dateArray[2]);

        $scope.getValidMonth(dateArray[0], 'end');
        $scope.getValidDates('end');
    }

    if(userItems.end_time){
        $scope.form.endTime = userItems.end_time;
        $scope.form.endTimePeriod = 'AM';
    }



    if ($scope.userDetail.subscription_name == 'PROFESSIONAL') {
      if ($scope.form.live_item == true) {
        //document.getElementById("liveItem").checked = true;
      } else {
        // document.getElementById("liveItem").checked = false;
      }
    }

    if ($scope.userDetail.subscription_name == 'PERSONAL') {
      if (formDataset.two_way_sync == 1) {
        //document.getElementById("syncToWay").checked = true;
      } else {
        //document.getElementById("syncToWay").checked = false;
      }
    }

    if ($scope.form.share_on_showcase == 1) {
      //document.getElementById("shareOnShowcase").checked = true;
    }

    if (formDataset.share_on_showcase == 0) {
      //document.getElementById("shareOnShowcase").checked = false;
    }

    itemServiceHandler.getProvince({
        'data': {
          'country_id': formDataset.location_country
        }
      })
      .then(function(response) {
        $scope.provinceList = (response.result.province.length) ? response.result.province : null;
      });

    itemServiceHandler.getCity({
        'data': {
          'province_id': formDataset.location_province
        }
      })
      .then(function(response) {
        $scope.cityList = (response.result.cities.length) ? response.result.cities : null;
        $scope.openModal('item-form-modal');
      });


  }



  var uploader = $scope.uploader = ImageUploader;

  $scope.removeImageItems = function(){
      console.log(uploader);
  }

  $scope.initializeUploader = function() {
    uploader.onAfterAddingFile = function(item) {

      item.croppedImage = '';
      var reader = new FileReader();
      reader.onload = function(event) {
        $scope.$apply(function() {
          item.image = event.target.result;
        });
      };
      reader.readAsDataURL(item._file);
    };
  }

  $scope.initializeUploader();

  $scope.getView = function(item) {
    return 'nestable_item.html';
  }

  $scope.getChildView = function(item) {
    return 'nestable_child_item.html';
  }


  $scope.findOrderedItem = function(items) {
    var sortedItems = [];
    var childSortedItems = [];

    _.each(items, function(value, index) {
      childSortedItems = [];
      if (value.item.length) {
        childSortedItems = $scope.findOrderedItem(value.item);
      }
      sortedItems.push({
        card_order: (index + 1),
        id: value.id,
        card_type_id: value.card_type_id,
        child_item: childSortedItems
      });
    });

    return sortedItems;
  }

  $scope.sortableChildOptions = {
    connectWith: ".apps-container"
  }

  $scope.sortableOptions = {
    connectWith: ".apps-container",
    update: function(e, ui) {
      $timeout(function() {
        var sortedItems = $scope.findOrderedItem($scope.userItems);
        itemServiceHandler.updateItemOrder({
          'data': {
            'cardOrder': sortedItems
          }
        })
      }, 1000);
    }
  };

  $scope.openModal = function(id) {
    $scope.itemGroupMessage = "";
    $scope.createItemResponse = "";
    uploader.uploadedImage = "";
    modalFactory.Open(id);
  };

  $scope.closeModal = function(id) {
    if (id == 'group-alert-content') {
      $statePar = {};

      if ($scope.groupId) {
        $statePar = {
          'groupId': $scope.groupId
        };
      }

      $state.go($state.current, $statePar, {
        reload: true,
        inherit: false
      });
      return modalFactory.Close(id);
    }

    uploader.clearQueue();
    modalFactory.Close(id);
  };

  $scope.slideDownFun = function() {
    $scope.slideDown = !$scope.slideDown
  }
  $scope.slideDownFun1 = function() {
    $scope.slideDown1 = !$scope.slideDown1
  }


  $scope.resetItems = function() {
    $scope.filterData = {};
    $scope.closeModal('item-filter-content');
    $scope.getItems(null);
  };

  $scope.resetSortItems = function() {
    document.getElementById("date").checked = false;
    document.getElementById("title").checked = false;
    document.getElementById("description").checked = false;
    document.getElementById("priority").checked = false;
    $scope.closeModal('item-filter-content');
    $scope.getItems(null);

  };

  $scope.resetShareItems = function() {
    for (i = 0; i < $scope.contactList.length; i++) {
      document.getElementById($scope.contactList[i].userId).checked = false;
    }
    $scope.closeModal('item-share-content');
  };

  $scope.setPagination = function(result) {
    $scope.currentPage = result.current_page;
    $scope.totalPage = result.total;
  };

  $scope.updateItemNotification = function(item) {
    if (typeof item.data == 'object') {
      itemServiceHandler.updateItemNotification({
          'data': {
            'itemId': item.data.id,
            'notification': item.notification
          }
        })
        .then(function(response) {
          //$scope.alertTitle = "Success";
          //  $scope.alertContent = "Card notification has been updated";
          //  modalFactory.Open('item-alert-content');
          toastr.success('Card notification has been updated', 'Success');
        });
    }
  }

    $scope.quickAdd = function(){

      if($scope.quickAddTitle){

      $scope.form.title =  $scope.quickAddTitle;
			if($scope.groupId) {
				  $scope.form.card_type_id = $scope.groupId;
			}
  	  itemServiceHandler.createItem({
							'data' : {
								'item' : $scope.form
							}
						})
						.then(function(response){
								if(response.error_code == 200) {
									$scope.closeItem();
                  toastr.success('Card has been created successfully', 'Success');
									$state.reload();
								} else {
                  toastr.success('Some problem occured', 'Error');
								}

						}, function(response){
							  $scope.closeItem();
						});
		}else{
      toastr.success('Please Enter the Title first and then click on quick add button', 'Alert');
		}
  }

  $scope.closeAlertPage = function() {
    $scope.closeModal('item-alert-content');
    // $state.go($state.current, {groupId : response.group[0].id}, {reload: true, inherit: false});
    $state.reload();
  }

  $scope.updateItem = function() {
    $scope.error = [];
    if (!$scope.form.title) {
      $scope.form.titleError = "Title is required";
      return;
    } else {
      $scope.form.titleError = "";
    }

    if ($scope.form.startDateYear || $scope.form.startDateMonth || $scope.form.startDayDate) {
        if (!$scope.form.startDateMonth) {
          $scope.error.push('Please select valid Month')

        }
        if (!$scope.form.startDayDate) {
          $scope.error.push('Please select valid Day')
        }
        if (!$scope.form.startDateYear) {
          $scope.error.push('Please select valid Year')
        }
    }

    if ($scope.userDetail.subscription_name != 'FREE') {
      if($scope.form.endDateYear || $scope.form.endDateMonth || $scope.form.endstartDayDate) {
        if (!$scope.form.endDateYear) {
          $scope.error.push('Please select valid Year')
        }
        if (!$scope.form.endDateMonth) {
          $scope.error.push('Please select valid Month')

        }
        if (!$scope.form.endstartDayDate) {
          $scope.error.push('Please select valid Day')
        }
      }
    }


    if ($scope.form.ageFrom || $scope.form.ageTo) {
      if (!$scope.form.ageFrom) {
        $scope.error.push('Please select Age From')
      }
      if (!$scope.form.ageTo) {
        $scope.error.push('Please select Age To')
      }
      if ($scope.form.ageTo <= $scope.form.ageFrom) {
        $scope.error.push('Age To must be greater Than Age From')
      }
    }

    if (($scope.error).length > 0) {
      for (var i = 0; i < ($scope.error.length); i++) {
        toastr.error($scope.error[i], 'Validation Error');
      }
      return;
    }

    if (document.getElementById("showcase").checked == true) {
      $scope.form.share_on_showcase = true;
    } else {
      $scope.form.share_on_showcase = false;
    }

    if ($scope.userDetail.subscription_name == 'PROFESSIONAL') {
      if (document.getElementById("liveItem").checked == true) {
        $scope.form.live_item = true;
      } else {
        $scope.form.live_item = false;
      }
    }

    if ($scope.userDetail.subscription_name == 'PERSONAL') {
      if (document.getElementById("syncTwoWay").checked == true) {
        $scope.form.two_way_sync = true;
      } else {
        $scope.form.two_way_sync = false;
      }
    }

    if ($scope.form.location_address) {
      if (typeof $scope.form.location_address.address_components != 'undefined') {
        var addressData = [];
        angular.forEach($scope.form.location_address.address_components, function(value, index) {
          if (addressData.indexOf(value.long_name) == -1)
            addressData.push(value.long_name);
        })

        $scope.form.location_address = addressData.join();
      }
    }


    if ($scope.form.startDateYear && $scope.form.startDateMonth && $scope.form.startDayDate) {
      $startDateValue = $scope.convertToDate(['startDateYear', 'startDateMonth', 'startDayDate']);
      if ($startDateValue) {
        $scope.form.start_date = $startDateValue;
      }
    }

    if ($scope.form.startTime && $scope.form.startTimePeriod) {
      $startTimeValue = $scope.convertToTime($scope.form.startTime, $scope.form.startTimePeriod);
      if ($startTimeValue) {
        $scope.form.start_time = $startTimeValue;
      }
    }


    if ($scope.userDetail.subscription_name != 'FREE') {
      if ($scope.form.endDateYear && $scope.form.endDateMonth && $scope.form.endDayDate) {
        $startDateValue = $scope.convertToDate(['endDateYear', 'endDateMonth', 'endDayDate']);
        if ($startDateValue) {
          $scope.form.start_date = $startDateValue;
        }
      }

      if ($scope.form.endTime && $scope.form.endTimePeriod) {
        $startTimeValue = $scope.convertToTime($scope.form.endTime, $scope.form.endTimePeriod);
        if ($startTimeValue) {
          $scope.form.start_time = $startTimeValue;
        }
      }
    }


    if ($scope.form.$$hashKey)
      delete($scope.form.$$hashKey);

    if ($scope.form.owner_id)
      delete($scope.form.owner_id);

    if (uploader.uploadedImage)
      $scope.form.card_image = uploader.uploadedImage

    itemServiceHandler.updateItem({
        'data': {
          'id': $scope.form.id,
          'item': $scope.form
        }
      })
      .then(function(response) {
        $scope.closeItem();
        //$scope.alertTitle = "Success";
        //$scope.alertContent = "Card has been updated";
        //modalFactory.Open('item-alert-content');
        toastr.success('Card has been updated', 'Success');
        $scope.getItems(null);
        $state.reload();
      });
  };

  $scope.convertToTime = function(time, format) {
    var timeValue = "";
    if(time.hasOwnProperty('split')) {
      var providedTime = time.split(":");


      if (providedTime.length == 2) {
        if (format == 'pm') {
          providedTime[0] = Number(providedTime[0]) + 12;
        }
        _.each(providedTime, function(value) {
          if (timeValue) timeValue += ":";

          timeValue += (Number(value) < 10) ? ("0" + Number(value)) : Number(value);
        });
      }
    }
    return timeValue;
  }

  $scope.convertToDate = function(fields) {
    $startDateValue = "";
    $validDate = true;
    _.each(fields, function(value) {
      if ($startDateValue) {
        $startDateValue += "-";
      }
      if ($scope.form[value]) {
        $startDateValue += ($scope.form[value] < 10) ? ("0" + $scope.form[value]) : $scope.form[value];
      } else {
        $validDate = false;
      }
    });

    return ($validDate) ? $startDateValue : "";
  }

  $scope.changeTab = function(tabId) {
    $scope.activeTab = tabId;
  };

  $scope.viewItem = function(formDataset) {
    $scope.openModal('item-view-modal');
    $scope.viewDetails = formDataset;
  };

  $scope.openDeleteItem = function(formDataset) {
    $scope.singleDeleteId = formDataset.id;
    $scope.openModal('delete-alert-content');
  };

  $scope.deleteItem = function() {
    itemServiceHandler.deleteItem({
      'data': {
        'id': $scope.singleDeleteId
      }
    }).then(function(response) {
      $scope.closeItem();
      //$scope.alertTitle = "Success";
    //  $scope.alertContent = "Card has been deleted";
    //  modalFactory.Open('item-alert-content');
      $scope.getItems(null);
      toastr.success('Card has been deleted', 'Success', {
        onShown : function() {
          $scope.getItems(null);
          $state.reload();
        }
      });
      $scope.closeModal('delete-alert-content');

    }, function(response) {
      $scope.closeItem();
    });
  }

  $scope.shareItem = function(formDataset) {
    $scope.contact_id = formDataset.id;
    itemServiceHandler.getContacts({
        'data': {
          'card_id': formDataset.id
        }
      })
      .then(function(response) {
        $scope.contactList = response.result;
        if ($scope.contactList.length > 0) {
          $scope.shareableCardDetail = formDataset;
          $scope.shareEmail = "";
          $scope.openModal('item-share-content');
        } else {
          $scope.showAlertTitle = 'Unable to Share Item';
          $scope.showAlertContent = 'You cannot share this item yet, as you do not have any contacts. Add contacts and try this again.';
          $scope.openModal('show-alert-content');
        }

      });

  };

  $scope.shareItemShare = function() {

    var id = $scope.contact_id;
    var to_users = [];
    for (i = 0; i < $scope.contactList.length; i++) {
      if (document.getElementById($scope.contactList[i].userId).checked == true) {
        to_users.push($scope.contactList[i].userId);
      }
    }

    itemServiceHandler.shareCard({
        'data': {
          'id': id,
          'to_users': to_users
        }
      })
      .then(function(response) {
        $scope.closeModal('item-share-content');
        $scope.showAlertTitle = 'SUCCESS';
        $scope.showAlertContent = 'Successfully Shared.';
        $scope.openModal('show-alert-content');
      });
  };

  $scope.openDeleteAll = function() {

    var deleteUser = [];
    for (i = 0; i < $scope.checkedUserItem.length; i++) {
      deleteUser.push($scope.checkedUserItem[i].id);
    }

    if (deleteUser.length === 0) {
      $scope.openModal('allDelete-selectAlert-content');
    } else {
      $scope.openModal('allDelete-alert-content');
    }


  };

  $scope.deleteAll = function() {

    var deleteUser = [];
    for (i = 0; i < $scope.checkedUserItem.length; i++) {
        deleteUser.push($scope.checkedUserItem[i].id);
    }
    itemServiceHandler.deleteAll({
      'data': {
        'cardIds': deleteUser
      }
    }).then(function(response) {
      $scope.closeModal('allDelete-alert-content');
      $scope.closeItem();
      //$scope.alertTitle = "Success";
      //$scope.alertContent = "Card has been deleted";
      //modalFactory.Open('item-alert-content');
      $scope.getItems(null);
      toastr.success('Card has been deleted', 'Success', {
        onShown : function() {
          $scope.getItems(null);
          $state.reload();
        }
      });

    }, function(response) {
      $scope.closeItem();
    });
  }

  $scope.closeItem = function() {
    $scope.closeModal('item-form-modal');
  };

  $scope.openGroup = function() {
    $scope.selectedItem = null;
    $scope.form = {
      shareToContact : []
    };
    $scope.closeModal('add-section-content');
    $scope.openModal('item-group-modal');
  }

  $scope.openItem = function(formDataset) {
    $scope.createTimeArray();
    $scope.createYears();

    // if($scope.userDetail.subscription_name == 'PROFESSIONAL'){
    // 	document.getElementById("liveItem").checked = false;
    //    }

    //       if($scope.userDetail.subscription_name == 'PERSONAL'){
    // 	document.getElementById("syncToWay").checked = false;
    //    }

    // document.getElementById("shareOnShowcase").checked = false;

    $scope.selectedItem = null;
    $scope.form = {
      shareToContact : []
    };
    $scope.closeModal('add-section-content');
    $scope.openModal('item-form-modal');
  };

  $scope.checkAndMove = function(item) {
    var userItemIndex = ($scope.userItems.indexOf(item));

    var index = ($scope.checkedUserItem).indexOf(item);
    if (index == -1) {
      ($scope.checkedUserItem).push(item)
      $scope.userItems[userItemIndex].hideItem = true;
    } else {
      ($scope.checkedUserItem).pop(item)
      $scope.userItems[userItemIndex].hideItem = false;
    }
  }

  /*$scope.editItem = function(formDataset) {
			var userItems = _.extend({}, formDataset);
			userItems = validateValues(userItems);

			$scope.selectedItem = userItems;
			$scope.form = userItems;
			$scope.form.location_country = ($scope.form.location_country) ? $scope.form.location_country.toString() : 0;
			$scope.form.location_province = ($scope.form.location_province) ? $scope.form.location_province.toString() : 0;
			$scope.form.location_city = ($scope.form.location_city) ? $scope.form.location_city.toString() : 0;
			$scope.form.discount = Number($scope.form.discount);

            if($scope.userDetail.subscription_name == 'PROFESSIONAL'){
				if(formDataset.live_item == 1){
					document.getElementById("liveItem").checked = true;
				}else{
					document.getElementById("liveItem").checked = false;
				}
            }

            if($scope.userDetail.subscription_name == 'PERSONAL'){
				if(formDataset.two_way_sync == 1){
					document.getElementById("syncToWay").checked = true;
				}else{
					document.getElementById("syncToWay").checked = false;
				}
            }


			if(formDataset.share_on_showcase == 1){
				document.getElementById("shareOnShowcase").checked = true;
			}

			if(formDataset.share_on_showcase == 0){
				document.getElementById("shareOnShowcase").checked = false;
			}

            itemServiceHandler.getProvince({
						'data' : {
							'country_id' : formDataset.location_country
						}
					})
					.then(function(response){
						$scope.provinceList = (response.result.province.length) ? response.result.province : null;
					});

			itemServiceHandler.getCity({
						'data' : {
							'province_id' : formDataset.location_province
						}
					})
					.then(function(response){
						$scope.cityList = (response.result.cities.length) ? response.result.cities : null;
						    $scope.openModal('item-form-modal');
					});
	}*/

  $scope.userItems = {};

  $scope.getSharedItems = function($params) {
    itemServiceHandler.getSharedItems($params)
      .then(function(response) {
        $scope.otherData = response.others;
        $scope.setPagination(response.result);
        $scope.sharedItems = (response.result.data.length) ? response.result.data : null;
      });
  }

  $scope.rejectItem = function(selectedUserItem) {
    itemServiceHandler.updateSharedItem({
        'data': {
          'accept_request': 0,
          'card_id': selectedUserItem
        }
      })
      .then(function(response) {
        $scope.groupAlertTitle = "Success";
        $scope.groupAlertContent = "Item has been rejected";
        modalFactory.Open('group-alert-content');
        $scope.getSharedItems(null);
        $scope.getItems(($scope.groupId) ? {
          'data': {
            'filter': {
              'type': $scope.groupId
            }
          }
        } : null);
      });
  }

  $scope.acceptItem = function(selectedUserItem) {
    itemServiceHandler.updateSharedItem({
        'data': {
          'accept_request': 1,
          'card_id': selectedUserItem.id
        }
      })
      .then(function(response) {
        $scope.groupAlertTitle = "Success";
        $scope.groupAlertContent = "Item has been accepted";
        modalFactory.Open('group-alert-content');
        $scope.getSharedItems(null);
        $scope.getItems(($scope.groupId) ? {
          'data': {
            'filter': {
              'type': $scope.groupId
            }
          }
        } : null);
      });
  }

  $scope.getItems = function($params) {
    itemServiceHandler.getItems($params)
      .then(function(response) {
        $scope.otherData = response.others;
        $scope.setPagination(response.result);
        $scope.userItems = (response.result.data.length) ? response.result.data : null;


        $scope.userItems = $scope.userItems.map(function(value) {
          value.item = [];
          return value;
        });

        if ($scope.groupId) {
          var selectedUserItem = _.where($scope.userItems, {
            'card_type_id': $scope.groupId
          });
          if (selectedUserItem.length)
            $scope.selectedUserItem = selectedUserItem[0];
        }

        $scope.closeModal('item-filter-content');
        $scope.closeModal('item-sort-content');

        if (response.group.length) {
          $state.go($state.current, {
            groupId: response.group[0].id
          }, {
            reload: true,
            inherit: false
          });
          //location.reload();
          //$location.url($location.path() + "/" + response.group[0].id);
          //$window.location.href = $location.path() + response.group[0].id;
        }
      });
  };


  $scope.filterGroupByType = function(filterType) {
    itemServiceHandler.getCardGroup((filterType) ? {
        'data': {
          'groupId': filterType
        }
      } : null)
      .then(function(response) {
        $statePar = null;
        if (response.result && response.result.length) {
          $statePar = {
            'groupId': response.result[0].parent_group_id
          };
        }

        $state.go($state.current, $statePar, {
          reload: true,
          inherit: false
        });
      });
  };

  function validateValues(userItems) {
    if (userItems) {
      if (userItems.price) {
        userItems.price = Number(userItems.price);
      }

      if (userItems.quantity) {
        userItems.quantity = Number(userItems.quantity);
      }

      if (userItems.card_time) {
        userItems.card_time = new Date("10-10-2017 " + userItems.card_time);
      }

      if (userItems.start_time) {
        userItems.start_time = new Date("10-10-2017 " + userItems.start_time);
      }

      if (userItems.end_time) {
        userItems.end_time = new Date("10-10-2017 " + userItems.end_time);
      }
    }
    return userItems;
  }


  authServiceHandler.getUserDetail()
    .then(function(response) {
      $scope.userDetail = response.result;
    });

  itemServiceHandler.getCardGroup(($scope.groupId) ? {
      'data': {
        'groupId': $scope.groupId
      }
    } : null)
    .then(function(response) {
      $scope.cardGroup = response.result;
    });

  $scope.selectItem = function(selectedUserItem) {
    $scope.selectedUserItem = selectedUserItem;
  };

  $scope.filterItem = function(filterType) {
    $scope.selectedFilter = filterType;
    if ($scope.groupId) {
      var card_id = $scope.groupId;
    } else {
      card_id = '';
    }
    switch (filterType) {
      case 'all':
        $scope.getItems({
          'data': {
            'filter': {
              'getitem': 'all'
            }
          }
        });
        break;
      case 'name':
        $scope.getItems({
          'data': {
            'filter': {
              'searchText': $scope.filterName,
              'card_type_id': card_id
            }
          }
        });
        break;
      case 'today':
        $scope.getItems({
          'data': {
            'filter': {
              'date': 'now'
            }
          }
        });
        break;
      case 'search':
        $scope.openModal('item-filter-content');
        break;
      case 'sort':
        $scope.openModal('item-sort-content');
        break;
    }
  };

  $scope.searchItems = function() {
    if (document.getElementById("searchRadio").checked == true) {
      $scope.filterData.share_on_showcase = 1;
    } else {
      $scope.filterData.share_on_showcase = 0;
    }

    if (document.getElementById("searchLiveItem").checked == true) {
      $scope.filterData.live_item = 1;
    } else {
      $scope.filterData.live_item = 0;
    }

    if (document.getElementById("searchTwoWaySync").checked == true) {
      $scope.filterData.two_way_sync = 1;
    } else {
      $scope.filterData.two_way_sync = 0;
    }


    if ($scope.groupId) {
      $scope.filterData.card_type_id = $scope.groupId;
    }

    $scope.getItems({
      'data': {
        'filter': $scope.filterData
      }
    });
  };

  $scope.changeSortType = function() {
    $scope.sortingType = ($scope.sortingType == 'asc') ? 'desc' : 'asc';

    if ($scope.selectedSorting)
      $scope.sortItems($scope.selectedSorting)
  }

  $scope.sortItems = function(sort) {
    var sortBy = {};
    $scope.selectedSorting = sort;

    switch (sort) {
      case "date":
        sortBy.sortByDate = $scope.sortingType;
        break;
      case "title":
        sortBy.sortByTitle = $scope.sortingType;
        break;
      case "priority":
        sortBy.sortByPriority = $scope.sortingType;
        break;
      default:
        sortBy.sortByTitle = $scope.sortingType;
        break;
    }

    /*if(document.getElementById("date").checked == true){
			sortBy.sortByDate = 'asc';
		}

		if(document.getElementById("title").checked == true){
			sortBy.sortByTitle = 'asc';
		}

		if(document.getElementById("description").checked == true){
			sortBy.sortByDescription = 'asc';
		}

		if(document.getElementById("priority").checked == true){
			sortBy.sortByPriority = 'asc';
		}

		if($scope.groupId){
           	sortBy.sortByCardTypeID = $scope.groupId;
    }*/

    var myJsonString = JSON.stringify(sortBy);
    $scope.getItems({
      'data': {
        'filter': {
          'sortBy': sortBy
        }
      }
    });

    //$scope.closeModal('item-sort-content');
  };

  $scope.filterItemByType = function(filterType) {
    $(".item").removeClass('active');
    $("#item-group-" + filterType).addClass('active');
    $scope.selectedItem = filterType;
    if (filterType) {
      $scope.getItems({
        'data': {
          'filter': {
            'type': filterType
          },
          'group': filterType
        }
      });
    } else {
      $scope.selectedUserItem = "";
      $scope.getItems(null);
    }

  };


  $scope.allSelect = function() {
    $scope.showSelect = false;
    for (i = 0; i < $scope.userItems.length; i++) {
      //if (document.getElementById("checkbx_" + $scope.userItems[i].id).checked == false) {
      //  document.getElementById("checkbx_" + $scope.userItems[i].id).checked = true;
        //$scope.userItems[i].hideItem = true;
          $scope.checkAndMove($scope.userItems[i]);
    //  }
    }
  };

  $scope.unselect = function() {
    $scope.showSelect = true;
    for (i = 0; i < $scope.userItems.length; i++) {
      //if (document.getElementById("checkbx_" + $scope.userItems[i].id).checked == true) {
        //document.getElementById("checkbx_" + $scope.userItems[i].id).checked = false;
        $scope.userItems[i].hideItem = false;
        $scope.checkedUserItem = [];
        //$scope.checkAndMove($scope.userItems[i]);
      //}
    }
  };


  $scope.getProvince = function(id) {
    if (id) {
      itemServiceHandler.getProvince({
          'data': {
            'country_id': id
          }
        })
        .then(function(response) {
          $scope.provinceList = (response.result.province.length) ? response.result.province : null;
          if (response.result.province.length) {
            $scope.activeProvince = true;
            $scope.activeSearchProvince = true;
          }
        });
    }
  };

  $scope.getCity = function(id) {
    if (id) {
      itemServiceHandler.getCity({
          'data': {
            'province_id': id
          }
        })
        .then(function(response) {
          $scope.cityList = (response.result.cities.length) ? response.result.cities : null;
          if (response.result.cities.length) {
            $scope.activeCity = true;
            $scope.activeSearchCity = true;
          }
        });
    }
  };


  $scope.addItemGroup = function() {
    if ($scope.groupId) {
      $scope.mygroup.groupId = $scope.groupId;
    }

    itemServiceHandler.addItemGroup({
        'data': $scope.mygroup
      })
      .then(function(response) {
        if (response.error_code == 500) {
          $scope.itemGroupMessage = response.msg_string;
        } else {
          $scope.closeModal('item-group-modal');
          $scope.groupAlertTitle = "Success";
          $scope.groupAlertContent = "Item Group has been created";
          modalFactory.Open('group-alert-content');
        }
      });
  }

  $scope.refreshMainItem = function() {
    $scope.getItems(null);
  }

  $scope.getSharedItems(null);
  $scope.getItems(($scope.groupId) ? {
    'data': {
      'filter': {
        'type': $scope.groupId
      }
    }
  } : null);
}

scedgeControllers
  .controller('ItemController', ['$scope', '$rootScope', '$timeout', '$state', '$window', '$location', '$timeout', 'ModalService', 'ModalFactory', 'AuthServiceHandler', 'ItemServiceHandler', 'ImageUploader', 'ProfileServiceHandler', 'ContactServiceHandler', 'toastr', 'Socialshare', 'MONTHS', ItemController]);
