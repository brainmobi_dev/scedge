function AuthController($scope, $rootScope, $state, $validation,modalFactory, modalService, authServiceHandler, storageServiceHandler){
	$scope.showPassword = false;
	if(authServiceHandler.isAuthenticated()) {
		return $state.go('myItem');
	}

	if(typeof $state.params != undefined && ($state.params.data != 'undefined' && $state.params.data)){
		$scope = angular.extend($scope, $state.params.data);
	} else {
		$scope.subscription_name = "FREE";
		$scope.subscription_type = "Monthly";
		$scope.subscription_amount = 0;
		$scope.subscription_id = 1;

	}

	$scope.loginError = "";
	$scope.forgotPasswordFlag = false;

	$scope.subscriptions = [{'id' : 'check', 'name' : 'FREE'}, {'id' : 'check1', 'name' : 'PERSONAL'}, {'id' : 'check2', 'name' : 'PROFESSIONAL'}];
	$scope.memberships = [{'id' : 'check4', 'name' : 'Monthly'}, {'id' : 'check5', 'name' : 'Annual'}];
	$scope.genderList = [{'id' : 0, 'value' : 'Gender'}, {'id' : 'male', 'value' : 'Male'}, {'id' : 'female', 'value' : 'Female'}];

	$scope.showUserPassword = function(value){
			$scope.showPassword = value;
	}

	$scope.login = function() {
		var loginRequest = authServiceHandler.login({'data' : {'email' : $scope.email, 'password' : $scope.password}});

		loginRequest.then(function(response) {
			$scope.handleLoginRequest(response);
		})
	}

	$scope.closeModal = function(modelId) {
		modalFactory.Close(modelId);
	}

	$scope.openModal = function(modelId) {
		modalFactory.Open(modelId);
	}

	$scope.activateForgotPassword = function() {
		$scope.forgotPasswordFlag = true;
	}

	$scope.deActivateForgotPassword = function() {
		$scope.forgotPasswordFlag = false;
	}


	$scope.updateSubscriptionType = function($subscriptionsType){
		$scope.subscription_name = $subscriptionsType.name;
		$scope.getSubscriptionCharges($scope.subscription_name, $scope.subscription_type);
	}

	$scope.updateSubscriptionPeriod = function($scubscriptionPeriod){
		$scope.subscription_type = $scubscriptionPeriod.name;
		$scope.getSubscriptionCharges($scope.subscription_name, $scope.subscription_type);
	}

	$scope.getSubscriptionCharges = function($subscriptionsType, $scubscriptionPeriod){

		var index = _.findLastIndex($scope.subscriptionList, {
			subscription_name : $subscriptionsType,
			subscription_type : ($scubscriptionPeriod == 'Annual') ? 'ANNUAL' : 'MONTHLY'
		});

		$amount = ((index != -1) ? $scope.subscriptionList[index].subscription_amount : "0");
		$scope.subscription_id = (index == -1) ? 1 : $scope.subscriptionList[index].id;

		switch($subscriptionsType) {
			case "PROFESSIONAL" : $scope.professionalAmount = $amount; break;
			case "PERSONAL" 	: $scope.personalAmount = $amount; break;
		}

		$scope.subscription_amount = $amount;
	}

	$scope.filterValues = function(formData) {
		$formData = {
			'subscription_id' : $scope.subscription_id
		};

		angular.forEach(formData, function(value, index){
			if(typeof value != 'function')
			$formData[index] = value;
		});

		angular.forEach(['registration_stage', 'user_id'], function(value, index){
			$formData[value] = (typeof $scope[value] != 'undefined') ? $scope[value] : 0;
		})

		return $formData;
	}

	$scope.getScedgeSubscription = function(formData) {
		authServiceHandler.getSubscription()
		.then(function(response){
			if(response.error_code == 200){
				$scope.subscriptionList = response.result;
			}
		});
	}


	$scope.form = {
		submit: function (form) {
			$validation.validate(form)
			.success(function(){
				authServiceHandler.registerUser({
					"data" : $scope.filterValues($scope.form)
				})
				.then(function(response) {
					$scope.groupAlertTitle = (typeof response.title != 'undefined') ? response.title : 'Welcome to scedge';
					$scope.groupAlertContent = response.msg_string;
					$scope.openModal('scedge-info-content');
				});

			})
			.error(function(){
				//console.log("invaid value");
			});
		}
	};

	$scope.gotoLogin = function(){
		$scope.closeModal('scedge-info-content');
		$state.go('login');
	}


	// Handle Login functionality
	$scope.handleLoginRequest = function(response) {
		if(response.error_code != 200) {
			if(response.error_code == 500) {
				// modalService.openModal({
				// 	templateUrl : 'scedge-info.html',
				// 	content : {
				// 		'modalMessage' : response.msg_string,
				// 		'modalTitle' : response.title,
				// 		'modalAction' : function(){
				// 			//$scope.handleRegistrationRequest(response);
				// 		}
				// 	}
				// });

				  $scope.groupAlertTitle = response.title;
				  $scope.groupAlertContent = response.msg_string;
					$scope.groupAlertAction = $scope.gotoJoin;
          $scope.openModal('scedge-logininfo-content');


			} else {
				$scope.loginError = response.msg_string;
			}
		} else {
			if(typeof response.token != 'undefined') {
				storageServiceHandler.setValue('token', 'Bearer ' + response.token);
				$state.go('myItem');
			} else {
				response.registration_stage = "" + response.registration_stage;
				switch(response.registration_stage) {
					case "1"  : $state.go('joinStep1', { verificationCode: response.verification_code, data: response.result}); break;
					case "2"  : $state.go('joinStep2', { verificationCode: response.verification_code, data: response.result}); break;
					case "3"  : $state.go('payment', { verificationCode: response.verification_code, data: response.result}); break;
					default   : $state.go('myItem'); break;
				}
			}
		}
	}

	$scope.gotoJoin = function(){
		$scope.closeModal('scedge-logininfo-content');
		$state.go('join');
	}


	// Handle Registration functionality
	$scope.handleRegistrationRequest = function(response) {
		if(response.error_code != 200) {
			if(response.error_code == 500) {
				/*modalService.openModal({
					templateUrl : 'scedge-info.html',
					content : {
							'modalMessage' : response.msg_string,
							'modalTitle' : response.title,
							'modalAction' : function(){
						}
					}
				});*/

				$scope.groupAlertTitle = response.title;
				$scope.groupAlertContent = response.msg_string;
				$scope.groupAlertAction = function(){};
				$scope.openModal('scedge-logininfo-content');

			} else {
				$scope.loginError = response.msg_string;
			}

		} else {
			if(typeof response.token != 'undefined') {
				storageServiceHandler.setValue('token', 'Bearer ' + response.token);
				$state.go('myItem');
			} else {
				response.registration_stage = "" + response.registration_stage;
				switch(response.registration_stage) {
					case "1"  : $state.go('joinStep1', { verificationCode: response.verification_code, data: []}); break;
					case "2"  : $state.go('joinStep2', { verificationCode: response.verification_code, data: []}); break;
					case "3"  : $state.go('payment', { verificationCode: response.verification_code, data: []}); break;
				}
			}
		}
	}

	// Reset password functionality
	$scope.resetPassword = function(){
		if($scope.resetPasswordEmail) {
			authServiceHandler.resetPassword({
				'data' : {
					'email' : $scope.resetPasswordEmail
				}
			}).then(function(response){
				if(response.error_code == 200) {
					$scope.resetPasswordEmail = "";
					/*modalService.openModal({
						templateUrl : 'scedge-info.html',
						size : 'lg',
						content : {
							'modalMessage' : response.msg_string,
							'modalTitle' : 'Successfully Processed',
							'modalAction' : function(){
								$modalInstance.close()
							}
						}
					});*/

					$scope.groupAlertTitle = response.title;
				  $scope.groupAlertContent = response.msg_string;
				  $scope.groupAlertAction =  function(){
							$scope.closeModal('scedge-logininfo-content');
							$state.go('login', { nextState: '', nextParams: ''}, {reload : true});
					};

					$scope.openModal('scedge-logininfo-content');


				} else {
					/*modalService.openModal({
						templateUrl : 'scedge-info.html',
						size : 'lg',
						content : {
							'modalMessage' : response.msg_string,
							'modalTitle' : response.title,
							'modalAction' : function($modalInstance){
								$modalInstance.close()
							}
						}
					});*/
					$scope.groupAlertTitle = response.title;
				  $scope.groupAlertContent = response.msg_string;
				  $scope.groupAlertAction =  function(){
							$scope.closeModal('scedge-logininfo-content');
							$state.go('login', { nextState: '', nextParams: ''}, {reload : true});
					};

					$scope.openModal('scedge-logininfo-content');
				}
			});
		}
	}


	if(typeof $rootScope.$$listeners['event:social-sign-in-success'] == 'undefined') {
		$rootScope.$on('event:social-sign-in-success', function(event, userDetails){
			if($state.current.name == 'login') {
				authServiceHandler.login({
					'data' : userDetails
				}).then(function(response){
					$scope.handleLoginRequest(response);
				});
			} else {
				authServiceHandler.socialRegisterUser({
					'data' : userDetails
				}).then(function(response){
					$scope.handleRegistrationRequest(response);
				});
			}
		});
	}

	if(typeof $rootScope.$$listeners['event:social-sign-out-success'] == 'undefined') {
		var deregisterSignOut = $rootScope.$on('event:social-sign-out-success', function(event, logoutStatus){

			socialLoginService.logout();
			authServiceHandler.logout({
				'data' : userDetails
			}).then(function(response){
				alert(response);
			});
		})
	}

	$scope.getScedgeSubscription();
}


function RProfileController($scope, $state, $stateParams, $validation, modalService, authServiceHandler, storageServiceHandler){
	if(typeof $stateParams.verificationCode != undefined){
		$scope.verification_code = $stateParams.verificationCode;
	} else {
		return $state.go('join');
	}

	$scope.genderList = [{
		'id' : 'Male',
		'value' : 'Male'
	},{
		'id' : 'Female',
		'value' : 'Female'
	}];

    var a = moment().format('YYYY');
    a = moment().subtract(15, 'years').format('YYYY');
    date = '12/31/'+a;
    $scope.currentDate = date;
    $scope.nowDate = new Date();
	$scope.nowDate = moment().subtract(15, 'years').calendar();
	$scope.activeProvince = false;
	$scope.activeCity = false;


	$scope.filterValues = function(formData) {
		$formData = {
			'subscription_id' :  $scope.user.subscription_id
		};

		angular.forEach(formData, function(value, index){
			if(typeof value != 'function') {
				$formData[index] = value;
			}
		});

		angular.forEach(['registration_stage', 'user_id'], function(value, index){
			$formData[value] = (typeof $scope.user[value] != 'undefined') ? $scope.user[value] : 0;
		});

		if(typeof  $scope.user['id'] != 'undefined') {
			$formData['user_id'] = $scope.user['id'];
		}

		return $formData;
	}

	$scope.getProvince = function(id){
		if(id){
			authServiceHandler.getProvince({
						'data' : {
							'country_id' : id
						}
					})
					.then(function(response){
						$scope.provinceList = (response.result.province.length) ? response.result.province : null;
						if(response.result.province.length){
							$scope.activeProvince = true;
						}
					});
		}
	}

	$scope.getCity = function(id){
		if(id){
			authServiceHandler.getCity({
						'data' : {
							'province_id' : id
						}
					})
					.then(function(response){
						$scope.cityList = (response.result.cities.length) ? response.result.cities : null;
						if(response.result.cities.length){
							$scope.activeCity = true;
						}
					});
		}
	}

	$scope.form = {
		submit: function (form) {
			$validation.validate(form)
			.success(function() {
				authServiceHandler.registerUser({
					"data" : $scope.filterValues($scope.form)
				})
				.then(function(response) {
					if(response.error_code == 200) {
						if(typeof response.token  != 'undefined') {
							storageServiceHandler.setValue('token', 'Bearer ' + response.token);
							return $state.go('myItem');
						} else {
							switch(response.registration_stage) {
								case 1  :  $state.go('joinStep1', { verificationCode: $scope.verification_code, data:''}); break;
								case 2  :  $state.go('joinStep2', { verificationCode: $scope.verification_code, data: ''}); break;
								case 3  :  $state.go('payment', { verificationCode: $scope.verification_code, data: ''}); break;
							}
						}
					} else {
						$scope.errorMessage = response.msg_string;
					}
				});

			})
			.error(function(){
				$scope.errorMessage = "Please fill the required field";
			});
		},
		skip: function(form) {
			authServiceHandler.registerUser({
				"data" : {
					'skip' : 1,
					'user_id' : $scope.user.id,
					'registration_stage' : $scope.user.registration_stage,
					'subscription_id' : $scope.user.subscription_id
				}
			})
			.then(function(response) {
				if(typeof response.token  != 'undefined') {
					storageServiceHandler.setValue('token', 'Bearer ' + response.token);
					return $state.go('myItem');
				} else {
					switch(Number(response.registration_stage)) {
						case 3  : response.registration_stage = 3;  $state.go('payment', {verificationCode: $scope.verification_code, data: ''}); break;
					}
				}
			});
		}
	};

	authServiceHandler.getVerificationDetail({
		'data' : {
			'verification_code' : $scope.verification_code
		}
	})
	.then(function(response){
		if(response.error_code == 200){
			$scope.user = response.result;
			$scope.countryList = response.others.countries;
		} else {
			$state.go('join');
		}
	});

	// Get subscription
	authServiceHandler.getSubscription()
	.then(function(response){
		if(response.error_code == 200){
			$scope.subscriptionList = response.result;
		}
	});
}

function VerificationController($scope, $rootScope, $state,
			$validation, modalFactory, modalService,
			authServiceHandler, storageServiceHandler){
	$scope.showPassword = false;
	if(authServiceHandler.isAuthenticated()) {
		return $state.go('myItem');
	}

	$scope.closeModal = function(modelId) {
		modalFactory.Close(modelId);
	}

	$scope.openModal = function(modelId) {
		modalFactory.Open(modelId);
	}

	$scope.gotoJoin = function(){
		$scope.closeModal('scedge-logininfo-content');
		$state.go('join');
	}

	$scope.gotoLogin = function(){
		$scope.closeModal('scedge-logininfo-content');
		$state.go('login');
	}

	$scope.resendActivationCode = function() {
			if($scope.email) {
					authServiceHandler.resendActivationCode({
						'data' : {
								'email' : $scope.email
						}
					})
					.then(function(response){
						switch(response.error_code){
							case 500 :   $scope.groupAlertTitle = response.title;
														$scope.groupAlertContent = response.msg_string;
														$scope.groupAlertAction = $scope.gotoJoin;
														$scope.openModal('scedge-logininfo-content');
														break;
							case 200 : 	$scope.groupAlertTitle = response.title;
														$scope.groupAlertContent = response.msg_string;
														$scope.groupAlertAction = $scope.gotoLogin;
														$scope.openModal('scedge-logininfo-content');
														break;
							default	 :	$state.go('login');
						}

					})
			}
	}


}


function ResetPasswordController($scope, $rootScope, $state,
			$validation, modalFactory, modalService,
			authServiceHandler, storageServiceHandler){

	$scope.validVerificationCode = false;
	if(authServiceHandler.isAuthenticated()) {
		return $state.go('myItem');
	}

	$scope.form = {};

	if(typeof $state.params != undefined && ($state.params.verificationCode != 'undefined' && $state.params.verificationCode)){
		$scope.verificationCode = $state.params.verificationCode;
	}


	$scope.closeModal = function(modelId) {
		modalFactory.Close(modelId);
	}

	$scope.openModal = function(modelId) {
		modalFactory.Open(modelId);
	}

	$scope.gotoJoin = function(){
		$scope.closeModal('scedge-logininfo-content');
		$state.go('join');
	}

	$scope.gotoLogin = function(){
		$scope.closeModal('scedge-logininfo-content');
		$state.go('login');
	}

	$scope.checkVerificationCode = function() {
			if($scope.verificationCode) {
					authServiceHandler.checkVerificationCode({
						'data' : {
								'verificationCode' : $scope.verificationCode
						}
					})
					.then(function(response){
						switch(response.error_code){
							case 500 :   $scope.groupAlertTitle = response.title;
														$scope.groupAlertContent = response.msg_string;
														$scope.groupAlertAction = $scope.gotoJoin;
														$scope.openModal('scedge-logininfo-content');
														break;
							case 200 :
													$scope.validVerificationCode = true;
													break;
							default	 :	$state.go('login');
						}

					})
			}
	}

	$scope.resetPassword = function() {
		  $scope.message = "";
			console.log($scope.form.newPassword);
			if(!$scope.form.newPassword) {
					$scope.message = "Please fill a valid password";
					return;
			}

			if(!$scope.form.repassword) {
					$scope.message = "Please retype your password";
					return;
			}

			if($scope.form.newPassword) {
					if($scope.form.newPassword != $scope.form.repassword) {
							$scope.message = "Password not matching";
							return
					}

					authServiceHandler.updateAccountPassword({
						'data' : {
								'activationCode' : $scope.verificationCode,
								'password' : $scope.form.newPassword
						}
					})
					.then(function(response){
						switch(response.error_code){
							case 500 :   $scope.groupAlertTitle = response.title;
														$scope.groupAlertContent = response.msg_string;
														$scope.groupAlertAction = $scope.gotoJoin;
														$scope.openModal('scedge-logininfo-content');
														break;

							case 200 :	storageServiceHandler.setValue('token', 'Bearer ' + response.token);
													$state.go('myItem');
													break;
							default	 :	$state.go('login');
						}

					})
			}
	}

	$scope.checkVerificationCode();


}

scedgeControllers
.controller('AuthController', ['$scope', '$rootScope', '$state', '$validation','ModalFactory', 'ModalService', 'AuthServiceHandler', 'StorageServiceHandler', AuthController])
.controller('VerificationController', ['$scope', '$rootScope', '$state', '$validation','ModalFactory', 'ModalService', 'AuthServiceHandler', 'StorageServiceHandler', VerificationController])
.controller('ResetPasswordController', ['$scope', '$rootScope', '$state', '$validation','ModalFactory', 'ModalService', 'AuthServiceHandler', 'StorageServiceHandler', ResetPasswordController])
.controller('RProfileController', ['$scope', '$state', '$stateParams', '$validation', 'ModalService', 'AuthServiceHandler', 'StorageServiceHandler', RProfileController])
