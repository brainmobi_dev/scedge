function HomeController($scope, $state, modalFactory, authServiceHandler, storageServiceHandler, socialshare) {
	$scope.subscriptionType = false;
	$scope.currentTab = 'tab1';

	$scope.closeModal = function(modelId) {
		modalFactory.Close(modelId);
	}

	$scope.openModal = function(modelId) {
		modalFactory.Open(modelId);
	}

	$scope.facebookShare = function() {
		socialshare.share({
				'provider': 'facebook',
				'attrs': {
					'socialshareUrl': 'http://scedge.apphosthub.com'
				}
			});
	}

	$scope.twitterShare = function() {
		socialshare.share({
				'provider': 'twitter',
				'attrs': {
					'socialshareUrl': 'http://scedge.apphosthub.com'
				}
			});
	}

	$scope.linkedinShare = function() {
		socialshare.share({
				'provider': 'linkedin',
				'attrs': {
					'socialshareUrl': 'http://scedge.apphosthub.com'
				}
			});
	}

	$scope.pinterestShare = function() {
		socialshare.share({
				'provider': 'pinterest',
				'attrs': {
					'socialshareUrl': 'http://scedge.apphosthub.com'
				}
			});
	}
	$scope.contentType = 'pricing';
	$scope.changeContentType = function(contentType) {
		$scope.contentType = contentType
	}

	$scope.currentSection = function(section) {
		$scope.currentTab = section;
	}

	$scope.getSubscriptionSaving = function($subscriptionsType, $scubscriptionPeriod){
		$subscriptionType = ($scubscriptionPeriod) ? 'ANNUAL' : 'MONTHLY'
		if($subscriptionType == 'ANNUAL') {
			switch ($subscriptionsType) {
				case 'PERSONAL': return "(Save $10)";
				case 'PROFESSIONAL': return "(Save $30)";
			}
		}

		return "";
	}

	$scope.getSubscriptionCharges = function($subscriptionsType, $scubscriptionPeriod){

		var index = _.findLastIndex($scope.subscriptionList, {
			subscription_name : $subscriptionsType,
			subscription_type : ($scubscriptionPeriod) ? 'ANNUAL' : 'MONTHLY'
		});

		$amount = ((index != -1) ? $scope.subscriptionList[index].subscription_amount : "0");

		switch($subscriptionsType) {
			case "PROFESSIONAL" : $scope.professionalAmount = $amount; break;
			case "PERSONAL" 	: $scope.personalAmount = $amount; break;
		}

		return "$" + $amount;
	}

	authServiceHandler.getSubscription()
	.then(function(response){
		if(response.error_code == 200){
			$scope.subscriptionList = response.result;
		}
	});
}

function ActivateController($scope, $state, $stateParams, modalService,modalFactory, authService){

	$scope.closeModal = function(modelId) {
		modalFactory.Close(modelId);
	}

	$scope.openModal = function(modelId) {
		modalFactory.Open(modelId);
	}

	$scope.activateUser = function() {
		authService.activeUser({
			"data" : {
				'verification_code' : $stateParams.verificationCode
			}
		})
		.then(function(response) {
			var modalContent = {
				'modalMessage' : response.msg_string,
				'modalTitle'   : 'Welcome to Scedge',
				'modalAction' : ''
			};

			if(response.error_code == 200) {
				$scope.successResult = response.result;
				$scope.showAlertTitle = "Successfully Verified";
				$scope.showAlertContent = response.msg_string;
				$scope.openModal('activate-content');
				$scope.checkSuccess = true;
			} else {
				$scope.showAlertTitle = "Invalid Request";
				$scope.showAlertContent = response.msg_string;
				$scope.openModal('activate-content');
				$scope.checkSuccess = false;
			}
		});
	}

	$scope.gotoStep1 = function(){
		if($scope.checkSuccess){
			$state.go('joinStep1', {verificationCode: $stateParams.verificationCode, data: $scope.successResult});
		}else{
			$state.go('join', { nextState: '', data: []});
		}
		$scope.closeModal('activate-content');
	}

	if(typeof $stateParams.verificationCode != 'undefined') {
		$scope.activateUser();
	}
}


scedgeControllers.controller('ModalInstanceCtrl', function ($uibModalInstance,$state, ModalService) {
	var $ctrl = this;
	$ctrl.modalData =  ModalService.getModal();

	$ctrl.message = "";

	$ctrl.ok = function (data) {
		$uibModalInstance.close();
		$ctrl.modalData.modalAction();
		if(data == 'login'){
			$state.go('join');
		}
	};

	$ctrl.close = function () {
		$uibModalInstance.close();
	}

	$ctrl.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};

});



scedgeControllers
.controller('HomeController', ['$scope', '$state', 'ModalFactory', 'AuthServiceHandler', 'StorageServiceHandler', 'Socialshare', HomeController])
.controller('ActivateController', ['$scope', '$state', '$stateParams', 'ModalService','ModalFactory', 'AuthServiceHandler', ActivateController]);
