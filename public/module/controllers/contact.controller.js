function ContactController($scope, $rootScope, $timeout, $state, $timeout, modalFactory, modalService, itemServiceHandler, contactServiceHandler, profileServiceHandler){
	$scope.group = "";
	if(typeof $state.params != undefined){
	 	if(typeof $state.params.groupId != 'undefined' && $state.params.groupId) {
			$scope.group = Number($state.params.groupId) ? Number($state.params.groupId) : "";
			$scope.selectedItem = $scope.group;
		}
  }

	$rootScope.notifications = profileServiceHandler.notifications();

			$timeout(function(){
				profileServiceHandler.removeUserNotifications({'data' : {'type' : 'contact'}})
								.then(function(response){
										$rootScope.notifications = response.result;
							})
			}, 1000);

	$scope.contactItems = {};
	$scope.contactGroup = {};
	$scope.filterName = "";

	$scope.currentPage = 1;
	$scope.totalPage = 1;

	$scope.selectedSorting = "";
	$scope.sortingType = "asc"

	$scope.openModal = function(id) {
			modalFactory.Open(id);
	}

	$scope.openMenu = 0
	$scope.showHideMenu = function(id){
		  if($scope.openMenu != id) {
				$scope.openMenu = id;
				return;
			}

			$scope.openMenu = 0;
	}

	$scope.closeModal = function(id) {

		    if(id == 'contact-alert-content'){
		    	$state.reload();
		    }

		    if(id == 'group-alert-content'){
		    	$state.reload();
		    }

			modalFactory.Close(id);
	}

	$scope.setPagination = function(result) {
			$scope.currentPage = result.current_page;
			$scope.totalPage = result.total;

	}

	$scope.isEmpty = function(data){
		if(data) {
			if(typeof data == 'object') {
				return isEmpty(data);
			} else {
				return data.length;
			}
		}
		return true;
	}

	$scope.closeAlertPopup = function() {
		$scope.closeModal('group-alert-content');
	}

	$scope.closeAlertPopupAndReload = function() {
		$state.go('myContact', null, {reload: true, inherit: false});
		$scope.closeModal('group-alert-content');
	}

	$scope.deleteContactGroup = function(group) {
		$scope.closeModal('block-alert-content');
		$params = {
			'data' : {
				'group' : $scope.deleteGroupId
			}
		};

		 contactServiceHandler.removeContactGroup($params)
						.then(function(response){

								if(response.error_code == 200) {
											$scope.groupAlertTitle = 'Delete Group Request';
											$scope.groupAlertContent = 'Contact group has been deleted Successfully.';
											$scope.groupAlertAction = $scope.closeAlertPopupAndReload;
											$scope.openModal('group-alert-content');
								} else {
										$scope.groupAlertTitle = 'Problem Occured';
										$scope.groupAlertContent = 'Some problem occured, Please try after some time';
										$scope.groupAlertAction = $scope.closeAlertPopup;
										$scope.openModal('group-alert-content');
								}
						});
	}


	$scope.removeContactGroup = function(group){
		$scope.deleteGroupId = group;
		$scope.groupAlertTitle = 'Delete Group Request';
		$scope.groupAlertContent = 'Are you sure to delete your contact group?';
		$scope.groupAlertAction = $scope.deleteContactGroup;
		$scope.openModal('block-alert-content');
 }


	$scope.getContactGroup = function($params) {
		contactServiceHandler.getContactGroup($params)
						.then(function(response){
							$scope.contactGroup = (response.result.length) ? response.result : null;
						});
	}

	$scope.getContacts = function($params) {
		contactServiceHandler.getContacts($params)
						.then(function(response){
								$scope.setPagination(response.result);
								$scope.contactItems = (response.result.data.length) ? response.result.data : null;
						});
	}

	$scope.getPendingRequest = function(){
		contactServiceHandler.pendingRequest($params)
						.then(function(response){
							$scope.pendingData = response.result;
							// console.log($scope.pendingData);
						});
	}



    $scope.approveConnection = function(id){
    	// alert(id)
    	contactServiceHandler.approveConnection({
					'data' : {
						'userID' : id
					}
			})
			.then(function(response){
				$scope.groupAlertTitle = 'Approval Request';
				$scope.groupAlertContent = 'Contact request has been approved Successfully';
				$scope.groupAlertAction = $scope.closeAlertPopup;
				$scope.openModal('group-alert-content');
				$scope.getPendingRequest();
			});
    }

		$scope.rejectConnection = function(id){
    	// alert(id)
    	contactServiceHandler.rejectConnection({
											'data' : {
												'userID' : id
											}
										})
			.then(function(response){
				$scope.groupAlertTitle = 'Rejection Request';
				$scope.groupAlertContent = 'Contact request has been rejected Successfully';
				$scope.groupAlertAction = $scope.closeAlertPopup;
				$scope.openModal('group-alert-content');
				$scope.getPendingRequest();
			});
    }

	$scope.openDeleteContact = function(contact_group_id,id){

		 $scope.groupDeleteId = contact_group_id;
		 $scope.contactDeleteId = id;
		 $scope.openModal('delete-alert-content');
	}

	$scope.deleteContact = function(){
            contactServiceHandler.deleteContact({
			'data' : {
				'contact_group_id':$scope.groupDeleteId,
				id : $scope.contactDeleteId
			}
		    })
		    .then(function(response){
					$scope.closeModal('delete-alert-content');
					openModal('delete-success-content');
					$scope.getContacts(null);
					$state.reload();
		    })
	}


	$scope.openContactPopup = function() {
			$scope.mygroup = {
					'group_image' : '',
					'title' : '',
					'description': ''
			};
			$scope.openModal('contact-add-content');
	}

	$scope.addContactGroup = function() {
				contactServiceHandler.addContactGroup({
					'data' : $scope.mygroup
				})
				.then(function(response) {
						if(response.error_code == 200) {
								//$scope.contactGroup = (response.result.length) ? response.result : null;
								$scope.closeModal('contact-add-content');
								$scope.alertTitle = "Success";
								$scope.alertContent = "Group has been created";
								modalFactory.Open('contact-alert-content');
								// $state.go('myContact', { nextState: '', data: []});

						} else {
							$scope.createGroupError = response.msg_string;
						}
				})
	}

	$scope.filterContactItems = function(newPageNumber, oldPageNumber){
			$params = {
					'data' : {
							'filter' : {
								'group' : $scope.group,
								'name'	: $scope.filterName,
								'page' 	: newPageNumber
							}
					}
			}
			$scope.getContacts($params);
	}

	$scope.changeSortType = function() {
		$scope.sortingType = ($scope.sortingType  == 'asc') ? 'desc' : 'asc';

		if($scope.selectedSorting)
			$scope.sortContactItems($scope.selectedSorting)
	}


	$scope.filterContact = function(filterBy){
			if($scope.group) {
				filterBy.data.filter['group'] = $scope.group;
			}
			$scope.getContacts(filterBy);
	}

	$scope.sortContactItems = function(sort) {
		var sortBy = {};
		$scope.selectedSorting = sort;

		switch(sort) {
			case 'name' : sortBy.sortByName = $scope.sortingType;
		}

		var myJsonString = JSON.stringify(sortBy);
		$scope.filterContact({'data' : {'filter' : {'sortBy' : sortBy}}});
	}

	$scope.filterResultByGroup = function(group) {
		  if(group) {
				$scope.group = group;
				$scope.selectedItem = group;
				$scope.filterResult();
			} else {
				$scope.getContacts(null);
			}
	}

	$scope.filterGroupByType = function(filterType){
			contactServiceHandler.getContactGroup((filterType) ? {'data' : {'group' : filterType}} : null)
						.then(function(response){
								$statePar = null;
								if(response.result && response.result.length) {
									$statePar = { 'groupId' : response.result[0].parent_group_id };
								}

								$state.go($state.current, $statePar, {reload: true, inherit: false});
						});
	};

	$scope.filterResultByName = function(searchData){
		  alert(searchData);
	}

	$scope.filterResult = function() {
			$params = {
					'data' : {
							'filter' : {
									'group' : $scope.group,
									'name'	: $scope.filterName
							}
					}
			}
			$scope.getContacts($params)
	}

	$params = null;
	if($scope.group) {
		$params = {
				'data' : {
						'filter' : {
								'group' : $scope.group
						}
				}
		};

	}

	$scope.getContacts($params);
	$scope.getContactGroup({
			'data' : {
					'group' : $scope.group
			}
	});



	$scope.sendInvitation = function() {
			if($scope.friendEmail) {
					contactServiceHandler.sendInvitation(
								{'data' :
									{'email' : $scope.friendEmail}
								})
								.then(function(response){

									 if(response.error_code == 200) {
										 $scope.friendEmail = "";

										  $scope.groupAlertTitle = 'Invitation Send';
							 				$scope.groupAlertContent = response.msg_string;
											$scope.groupAlertAction = $scope.closeAlertPopup;
							 				$scope.openModal('group-alert-content');
									 } else {
										  $scope.groupAlertTitle = 'Inviation Error';
											$scope.groupAlertContent =  response.msg_string;
											$scope.groupAlertAction = $scope.closeAlertPopup;
											$scope.openModal('group-alert-content');
									 }
								});
			} else {
				$scope.groupAlertTitle = 'Inviation Error';
				$scope.groupAlertContent =  'Please enter a valid email';
				$scope.groupAlertAction = $scope.closeAlertPopup;
				$scope.openModal('group-alert-content');
			}
 }

	 $scope.reportAbuse = function(userId) {
		 $scope.reportedUserId = userId;
		 itemServiceHandler
		 	 .getReports()
			 .then(function(response){
				 $scope.listAbuse = response.result;
				 $scope.openModal('contact-abuse-content');
			 })
		}

		$scope.cancelReportAbuse = function(userId) {
				$scope.closeModal('contact-abuse-content');
		}

	  $scope.sendReportAbuse = function(userId) {
			 var abuseList = [];
			 for(i=0;i<$scope.listAbuse.length;i++){
				 if(document.getElementById($scope.listAbuse[i].id).checked == true){
					 abuseList.push($scope.listAbuse[i].id);
				 }
			 }

			 if(!abuseList.length) {
				 $scope.reportAbuseError = "Please select a abuse type";
				 return;
			 }

			 contactServiceHandler.reportAbuse({
					 'data' : {
						 'user_id' : $scope.reportedUserId,
						 'report_id' : abuseList
					 }
				})
			 	.then(function(response){
					$scope.closeModal('contact-abuse-content');
					$scope.groupAlertTitle = 'Report Abuse Request';
					$scope.groupAlertContent = 'User has been reported to scedge admin';
					$scope.groupAlertAction = $scope.closeAlertPopup;
					$scope.openModal('group-alert-content');
				});
		}


   $scope.processBlockRequest = function() {
				 	contactServiceHandler.blockUser({
						'data' : {
							'user_id' : $scope.blockUserId
						}
					})
					.then(function(response){
						  $scope.closeModal('block-alert-content');
							$scope.groupAlertTitle = 'Block User Request';
							$scope.groupAlertContent = response.msg_string;
							$scope.groupAlertAction = $scope.closeAlertPopup;
							$scope.openModal('group-alert-content');
					});
		}


	 $scope.blockUser = function(userId) {
		 $scope.blockUserId = userId;
		 $scope.groupAlertTitle = 'Block User Request';
		 $scope.groupAlertContent = 'Are you sure to block your contact?';
		 $scope.groupAlertAction = $scope.processBlockRequest;
		 $scope.openModal('block-alert-content');
   }

	$scope.getPendingRequest(null);
}

scedgeControllers
	.controller('ContactController', ['$scope', '$rootScope', '$timeout', '$state', '$timeout', 'ModalFactory', 'ModalService', 'ItemServiceHandler', 'ContactServiceHandler', 'ProfileServiceHandler', ContactController]);
