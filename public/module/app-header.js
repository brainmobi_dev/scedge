var scedgeConstants     = angular.module('scedgeConstants', []);
var scedgeServices      = angular.module('scedgeServices', []);
var scedgeFilters       = angular.module('scedgeFilters', []);
var scedgeControllers   = angular.module('scedgeControllers', ['scedgeServices', 'validation', 'validation.rule']);
var scedgeDirectives    = angular.module('scedgeDirectives', []);
