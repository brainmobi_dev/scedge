scedgeDirectives.directive('modal', ['ModalFactory', Directive]);

function Directive(ModalService) {
    return {
        link: function (scope, element, attrs) {
            // ensure id attribute exists
            if (!attrs.id) {
                // console.error('modal must have an id');
                return;
            }

            // move element to bottom of page (just before </body>) so it can be displayed above everything else
            element.appendTo('body');

            // close modal on background click
            element.on('click', function (e) {

                var target = $(e.target);
                if (!target.closest('.modal-body').length) {
                    console.log(target);

                    if(target.attr('type') == 'file')
                      return;

                    if(target.hasClass('edit-profile-user-header'))
                        return;

                    if(target.hasClass('edit-profile-user-image'))
                            return;

                    if(target.hasClass('alert-popup'))
                      return;

                    if(target.hasClass('cropper-view-popup'))
                      return;

                    scope.$evalAsync(Close);
                }
            });

            // add self (this modal instance) to the modal service so it's accessible from controllers
            var modal = {
                id: attrs.id,
                open: Open,
                close: Close
            };
            ModalService.Add(modal);

            // remove self from modal service when directive is destroyed
            scope.$on('$destroy', function() {
                ModalService.Remove(attrs.id);
                element.remove();
            });

            // open modal
            function Open() {

                element.show("slow", function(){
                    $(this).slideDown();
                    $('body').addClass('modal-open');
                });

            }

            // close modal
            function Close() {
                element.hide("slow", function(){
                    $(this).slideUp();
                    $('body').removeClass('modal-open');
                });
            }
        }
    };
}
