scedgeDirectives
    .directive('footerSection', ["LINK", function (link) {
        return {
            restrict: 'E',
            templateUrl : link.public + 'pages/footer.html'
        }
    }])
    .directive('bannerSection', ["LINK", function (link) {
        return {
            restrict: 'E',
            templateUrl : link.public + 'pages/banner.html'
        }
     }])
    .directive('nevbarSection', ["LINK", function (link) {
        return {
            restrict: 'E',
            templateUrl : link.public + 'pages/nevbar.html'
        }
     }])
    .directive('nevbarMainSection', ["LINK", function (link) {
        return {
            restrict: 'E',
            templateUrl : link.public + 'pages/nevmainbar.html'
        }
     }])
     .directive('sharedCard', ["LINK", "FILTERDATA", function (link, filterData) {
         return {
             restrict: 'A',
             scope: {
                 data : '=data',
                 rejectItem: '&',
                 acceptItem: '&'
             },
             templateUrl : link.public + 'pages/item/shared-card.html',
             link: function (scope, element, attrs) {
               scope.dataToDisplay = {};

               scope.itemsFieldList = filterData.FIELDS;

               scope.filterDisplayData = function(){
                 var counter = 0;
                 angular.forEach(scope.itemsFieldList, function(value, index){
                     if( (!($.trim(scope.data[value]) == '')) && (counter < 3)) {
                         scope.dataToDisplay[value] = scope.data[value];
                         counter++;
                     }
                 })

                 if(!counter) {
                   scope.dataToDisplay = "";
                 }
               }
               scope.filterDisplayData();
             }
         }
      }])
    .directive('itemCard', ["LINK", "FILTERDATA", function (link, filterData)  {
        return {
            restrict: 'A',
            scope: {
                data : '=data',
                selectItem: '&',
                editItem: '&',
                openDeleteItem: '&',
                viewItem: '&',
                updateItemNotification: '&',
                shareItem: '&',
                checkAndMove : '&',
                sortableOptions: '=',
                getView: '&'
            },
            templateUrl : link.public + 'pages/item/item-card.html',
            link: function (scope, element, attrs) {
              scope.dataToDisplay = {};

              scope.itemsFieldList =  filterData.FIELDS;

              scope.filterDisplayData = function(){
                var counter = 0;
                angular.forEach(scope.itemsFieldList, function(value, index){
                    if($.trim(scope.data[value]) && (counter < 3)) {
                        scope.dataToDisplay[value] = scope.data[value];
                        counter++;
                    }
                })

                if(!counter) {
                  scope.dataToDisplay = "";
                }

                //console.log(scope.dataToDisplay, scope.data);
              }
              scope.filterDisplayData();
            }
        }
     }])

     .directive('itemCardChild', ["LINK", "FILTERDATA", function (link, filterData)  {
         return {
             restrict: 'A',
             scope: {
                 data : '=data',
                 selectItem: '&',
                 editItem: '&',
                 openDeleteItem: '&',
                 updateItemNotification: '&',
                 viewItem: '&',
                 shareItem: '&',
                 checkAndMove : '&',
                 sortableOptions: '=',
                 getView: '&'
             },
             templateUrl : link.public + 'pages/item/item-card-child.html',
             link: function (scope, element, attrs) {
               scope.dataToDisplay = {};

               scope.itemsFieldList =  filterData.FIELDS;
               scope.filterDisplayData = function(){
                 var counter = 0;
                 angular.forEach(scope.itemsFieldList, function(value, index){
                     if($.trim(scope.data[value]) && (counter < 3)) {
                         scope.dataToDisplay[value] = scope.data[value];
                         counter++;
                     }
                 })

                 if(!counter) {
                   scope.dataToDisplay = "";
                 }
               }
               scope.filterDisplayData();
             }
         }
      }])

     .directive('itemSelectedCard', ["LINK", "FILTERDATA", function (link, filterData)  {
        return {
            restrict: 'A',
            scope: {
                data : '=data',
                selectItem: '&',
                editItem: '&',
                openDeleteItem: '&',
                viewItem: '&',
                shareItem: '&',
                checkAndMove : '&'
            },
            templateUrl : link.public + 'pages/item/item-selected-card.html',
            link: function (scope, element, attrs) {
              scope.dataToDisplay = {};

              scope.itemsFieldList =  filterData.FIELDS;
              scope.filterDisplayData = function(){
                var counter = 0;
                angular.forEach(scope.itemsFieldList, function(value, index){
                    if($.trim(scope.data[value]) && (counter < 3)) {
                        scope.dataToDisplay[value] = scope.data[value];
                        counter++;
                    }
                })

                if(!counter) {
                  scope.dataToDisplay = "";
                }
              }
              scope.filterDisplayData();
            }
        }
     }])
    .directive('itemCardMain', ["LINK", function (link) {
        return {
            restrict: 'A',
            scope: {
                data : '=data',
                selectItem: '&',
                editItem: '&',
                openDeleteItem: '&',
                viewItem: '&',
                shareItem: '&',
                refreshMainItem : '&'
            },
            templateUrl : link.public + 'pages/item/item-card-main.html',
            link: function (scope, element, attrs) {

            }
        }
     }])
    .directive('browseCard', ["LINK", "FILTERDATA", function (link, filterData) {
        return {
            restrict: 'A',
            scope: {
                data : '=data',
                selectitem: '=selectitem'
            },
            templateUrl : link.public + 'pages/browse/browse-item.html',
            link: function (scope, element, attrs) {
              scope.dataToDisplay = {};

              scope.itemsFieldList =  filterData.FIELDS;

              scope.filterDisplayData = function(){
                var counter = 1;
                angular.forEach(scope.itemsFieldList, function(value, index){
                    if( (!($.trim(scope.data[value]) == '')) && (counter < 3)) {
                        scope.dataToDisplay[value] = scope.data[value];
                        counter++;
                    }
                });

                if(!counter) {
                  scope.dataToDisplay = "";
                }
              }
              scope.filterDisplayData();
            }
        }
    }])
    .directive('mainBrowseItem', ["LINK", function (link) {
        return {
            restrict: 'EA',
            scope: {
              browseItem : '=',
              blockCard : '&'
            },
            templateUrl : link.public + 'pages/browse/main-browse-item.html',
            link: function (scope, element, attrs) {

            }
        }
    }])
    .directive('listBrowseItem', ["LINK", function (link) {
        return {
            restrict: 'EA',
            scope: {
                browseItem : '=',
                blockCard : '&'
            },
            templateUrl : link.public + 'pages/browse/list-browse-item.html',
            link: function (scope, element, attrs) {

            }
        }
    }])
    .directive('contactCard', ["LINK", function (link) {
        return {
            restrict: 'A',
            scope: {
                data : '=data',
                openMenu : '=openMenu',
                selectitem: '=selectitem',
                openDeleteContact : '&',
                blockUser : '&',
                reportAbuse : '&',
                showHideMenu : '&'
            },
            templateUrl : link.public + 'pages/contact/contact-card.html',
            link: function (scope, element, attrs) {

            }
        }
     }])
    .directive('profileCard', ["LINK", function (link) {
        return {
            restrict: 'A',
            scope: {
                data : '=data',
                selectitem: '=selectitem',
                otherUser: '=otherUser'
            },
            templateUrl : link.public + 'pages/profile/profile-card.html',
            link: function (scope, element, attrs) {
                scope.otherUser = Number(scope.otherUser);
            }
        }
     }])
    .directive('profileDetail', ["LINK", function (link) {
        return {
            restrict: 'E',
            scope: {
                profileDetail : '=',
                selectitem: '=selectitem',
                authUserDetail: '=',
                uploadCoverImage : '&',
                openProfile: '&',
                openSetting : '&',
                uploader : '=uploader'
            },
            templateUrl : link.public + 'pages/profile/profile-detail.html',
            link: function (scope, element, attrs) {
                scope.uploadImage = function(){
                  angular.element('#upload-profile-photo-button').trigger('click');
                }
            }
        }
    }])
    .directive('otherProfileDetail', ["LINK", function (link) {
        return {
            restrict: 'E',
            scope: {
                profileDetail : '=',
                selectitem: '=selectitem',
                openPopup: '&',
                openProfile: '&',
                openSetting : '&'
            },
            templateUrl : link.public + 'pages/profile/other-profile-detail.html',
            link: function (scope, element, attrs) {

            }
        }
     }])
    .directive('itemFilterSection', ["LINK", "$timeout", function (link, $timeout) {
        return {
            restrict: 'E',
            templateUrl : link.public + 'pages/item/item-filter.html',
            link: function( $scope, elem, attrs ) {
                  elem.ready(function(){
                        $(".feature-slide").owlCarousel({
                            items: 3,
                            navigation: true,
 								responsive:{
							0:{  items:1 },
							1024:{ items:2 },
							1199:{ items:3 }
							}

                        });

                        $timeout(function () { $('#quick-add-input').focus(); });
                  })
           }
        }
    }])

    .directive('pendingContactSection', ["$timeout",  "LINK", function ($timeout, link) {
        return {
            restrict: 'E',
            scope: {
                 groupItem : '=',
                 selectedItem: '=',
                 hasGroup: '=',
                 filterResult: '&',
                 filterGroup: '&',
                 approveConnection: '&',
                 rejectConnection: '&'
             },
            templateUrl : link.public + 'pages/pending-contact-slider.html',
            link: function( $scope, elem, attrs ) {
                  elem.ready(function(){
                    $timeout(function(){
                      if($(".contact-slider")) {
                           $(".contact-slider").owlCarousel({
                               items: 4,
                               margin:30,
                               navigation: true,
                               responsive:{
                              0:{  items:1 },
                             600:{  items:2 },
                             1024:{ items:2 },
                             1199:{ items:4 }
                           },
                           });
                         }
                   }, 500);
                  })
           }
        }
    }])

    .directive('contactFilterSection', ["LINK", function (link) {
        return {
            restrict: 'E',
            templateUrl : link.public + 'pages/contact/contact-filter-section.html',
        }
     }])

     .directive('groupSlider', ["$timeout", "LINK", function ($timeout, link) {
         return {
             restrict: 'E',
             replace: true,
             scope: {
                 groupItem : '=',
                 selectedItem: '=',
                 hasGroup: '=',
                 filterResult: '&',
                 filterGroup: '&'
             },
             templateUrl : link.public + 'pages/group-slider.html',

             link: function( scope, elem, attrs ) {
               function initOwlCarousel(){
                 $timeout(function(){
                     if($(".feature-slide")) {
                        var totalItems = $('.item').length;
                        var isNav = true;
                        if (totalItems <= 3) {
                           isNav = false;
                        }

                       var owl = $(".feature-slide").owlCarousel({
                           items: 3,
                           navigation:false,
                           responsive:{
                           0:{  items:1 },
                           1024:{ items:2 },
                           1199:{ items:3 }
                         },
                           callbacks: true,
                           onInitialized: function(event){
                             totalItems = event.item.count;
                             $timeout(function(){
                               if(Number(totalItems)) {
                                 $(".feature-slide").trigger('to.owl.carousel', Number(totalItems));
                               }
                             }, 1000);
                           }
                       });
                       $(".item").show();
                     }
                   }, 1000);
               }

               /*function renderOwlCarousel(){
                 var items = [];
                 var counter = 0;
                 $(".item").each(function(){
                    items.push($(this).html());
                    $(this).remove();
                 });

                 $(".owl-item").each(function(){
                   if(!$(this).html()) {
                      $(this).html(items[counter++])
                   }
                 });

                 $(".item").show();
               }*/

               elem.ready(function(){
                  initOwlCarousel();
               });

               scope.$watch('groupItem', function(oldvalue, newvalue) {
                  /*if(oldvalue != newvalue) {
                    renderOwlCarousel();
                  }*/
               }, true);
             }
         }
    }])
    .directive('contactSlider', ["$timeout", "LINK", function ($timeout, link) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                groupItem : '=',
                selectedItem: '=',
                hasGroup: '=',
                filterResult: '&',
                filterGroup: '&',
                removeGroup: '&'
            },
            templateUrl : link.public + 'pages/contact-slider.html',

            link: function( scope, elem, attrs ) {
              function initOwlCarousel(){
                $timeout(function(){
                    if($(".feature-slide")) {
                       var totalItems = $('.item').length;
                       var isNav = true;
                       if (totalItems <= 3) {
                          isNav = false;
                       }

                      var owl = $(".feature-slide").owlCarousel({
                          items: 3,
                          navigation:false,
                          responsive:{
                          0:{  items:1 },
                          1024:{ items:2 },
                          1199:{ items:3 }
                        },
                          callbacks: true,
                      });
                      $(".item").show();
                    }
                  }, 1000);
              }

              /*function renderOwlCarousel(){
                var items = [];
                var counter = 0;
                $(".item").each(function(){
                   items.push($(this).html());
                   $(this).remove();
                });

                $(".owl-item").each(function(){
                  if(!$(this).html()) {
                     $(this).html(items[counter++])
                  }
                });

                $(".item").show();
              }*/

              elem.ready(function(){
                 initOwlCarousel();
              });

              scope.$watch('groupItem', function(oldvalue, newvalue) {
                 /*if(oldvalue != newvalue) {
                   renderOwlCarousel();
                 }*/
              }, true);
            }
        }
   }])


    .directive('noResultFound', ["LINK", function (link) {
        return {
            restrict: 'E',
            scope: {
              message : '@'
            },
            templateUrl : link.public + 'pages/no-result-found.html',
        }
     }])

     .directive('noContactFound', ["LINK", function (link) {
        return {
            restrict: 'E',
            templateUrl : link.public + 'pages/no-contact-found.html',
        }
     }])

    .directive('noItemFound', ["LINK", function (link) {
        return {
            restrict: 'E',
            templateUrl : link.public + 'pages/no-item-found.html',
        }
     }])

    .directive('noShowcaseFound', ["LINK", function (link) {
        return {
            restrict: 'E',
            templateUrl : link.public + 'pages/no-showcase-found.html',
        }
     }])

    .directive('subscriptionCheckbox', ["LINK", function (link) {
        return {
            restrict: 'A',
            link: function( $scope, elem, attrs ) {
                  elem.ready(function(){
                        $('.check-input').on('change', function () {
                            $('.check-input').not(this).prop('checked', false);
                        });
                  })
            }
        }
     }])

    .directive('periodCheckbox', ["LINK", function (link) {
        return {
            restrict: 'A',
            link: function( $scope, elem, attrs ) {
                  elem.ready(function(){
                        $('.check-input2').on('change', function () {
                            $('.check-input2').not(this).prop('checked', false);
                        });
                  })
            }
        }
    }])
    .directive('popupTab', ["LINK", function (link) {
        return {
            restrict: 'A',
            link: function( $scope, elem, attrs ) {
                  elem.ready(function(){
                      $(".pop-tab span").click(function(){
                        $(this).parent().addClass('active').siblings().removeClass('active');
                        var tab = $(this).attr('t-href');
                        $('.tab_content').not(tab).hide();
                        $(tab).show();
                      });
                  })
            }
        }
    }])
    .directive('keyEnter', function () {
        return function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if(event.which === 13) {
                    scope.$apply(function (){
                        scope.$eval(attrs.keyEnter);
                    });

                    event.preventDefault();
                }
            });
        };
    })
    .directive('appFilereader', function($q) {
        var slice = Array.prototype.slice;

        return {
            restrict: 'A',
            require: '?ngModel',
            link: function(scope, element, attrs, ngModel) {
                    if (!ngModel) return;

                    ngModel.$render = function() {};

                    element.bind('change', function(e) {
                        var element = e.target;

                        $q.all(slice.call(element.files, 0).map(readFile))
                            .then(function(values) {
                                if (element.multiple) ngModel.$setViewValue(values);
                                else ngModel.$setViewValue(values.length ? values[0] : null);
                            });

                        function readFile(file) {
                            var deferred = $q.defer();

                            var reader = new FileReader();
                            reader.onload = function(e) {
                                deferred.resolve(e.target.result);
                            };
                            reader.onerror = function(e) {
                                deferred.reject(e);
                            };
                            reader.readAsDataURL(file);

                            return deferred.promise;
                        }

                    }); //change

                } //link
        }; //return
    })
    .directive("leftmsgDirective",  ["LINK", function (link) {
    	return{
    		restrict: 'EA',
        replace:true,
    		templateUrl: link.public + 'pages/chat/left-chat.html',
    		scope: {
    			userList: '=',
          authContactList: '=',
    			email: '=',
          validUsers: "=",
    			click: '&',
    			getLastMessage: '&',
    			getLastMessagePostedBy: '&',
    			getLastMessageTime: '&',
    			unReadMessageCount: '&',
          isContactEmail: '&'
    		}
    	};
    }])
    .directive("rightmsgDirective",  ["LINK", "$timeout", function (link, $timeout) {
    	return{
    		restrict: 'EA',
        replace:true,
    		templateUrl: link.public + 'pages/chat/right-chat.html',
    		scope: {
    			chatMessages: '=',
    			email: '=',
          authContactList: '='
    		},
    		link: function($scope, elem, attrs){
          elem.ready(function() {
            $timeout(function(){
        			if($(".content-m")){
        				$(".content-m").mCustomScrollbar({
        					theme: "minimal"
        				});
                $(".content-m").mCustomScrollbar("scrollTo", "bottom");
        			}

              if($(".content-x")){
                $(".content-x")
           			 .mCustomScrollbar({theme:"minimal"});
              };

            }, 3000);

			  $(".menu-chat").click(function(){
				  $("body").toggleClass("user-hide");
			  });
          });

          $scope.$watch('chatMessages', function(oldvalue, newvalue) {
             $timeout(function(){
               $(".content-m").mCustomScrollbar("scrollTo", "bottom");
             }, 1000);
          }, true);
    		}
    	};
    }])
    .directive("emojiSpan", function(){
      return {
        restrict: 'EA',
        template: '<span class="select-symbol" ng-right-click="" menu-items="menuItems"><i class="fa fa-smile-o" aria-hidden="true" smilies-selector="message" smilies-placement="right" smilies-title="Smilies"></i></span>',
        link: function($scope, elem, attrs) {
           elem.ready(function() {
               wdtEmojiBundle.init('.chat-input');
               elem.bind("click", function(){
                 $('.wdt-emoji-picker').trigger('click');
               });
            });
        }
      };
    })
    .directive("clockPicker", function(){
      return {
        restrict: 'EA',
        link: function($scope, elem, attrs) {
           elem.ready(function() {
             elem.clockpicker({
                placement: 'top',
                align: 'left',
                donetext: 'Done'
              });
            });
        }
      };
    })

    .directive('imageDimensions', function() {
        return {
            restrict: 'A',
            link: function(scope, element, attrs, model) {
                element.on('load', function(){
                    //console.log(element[0].offsetHeight, element[0].naturalWidth);
                    //if(element[0].offsetHeight < 93){
                      //  element.css({'margin-top' : ((93-element[0].offsetHeight)/2)});
                    //}
                });
            }
        };
    })

    .directive('imageDimensionsMain', function() {
        return {
            restrict: 'EA',
            link: function(scope, element, attrs, model) {
                /*element.on('load', function(){
                    console.log(element[0].offsetHeight, element[0].naturalWidth);
                    if(element[0].offsetHeight < 218){
                        element.css({'margin-top' : ((218-element[0].offsetHeight)/2)});
                    }
                });*/
            }
        };
    })
    .directive('sizeCalculator', function(){
        return {
            restrict: 'EA',
            link: function(scope, element, attrs, model) {
                /*element.on('load', function(){
                    var offsetHeight = element[0].offsetHeight;
                    var offsetWidth = element[0].offsetWidth;
                    var parentElement =  element.parent();

                    if(element[0].naturalWidth < 142) {
                        element[0].style.width = element[0].naturalWidth + "px !important";
                    }

                    parentElement[0].style.marginTop = "-" + ( (offsetHeight / 2) + 36 ) + "px";

                });*/
            }
        };
    })
    .directive("ngFileSelect", function(){

      return {
        link: function($scope,el){

          el.bind("change", function(e){

            $scope.file = (e.srcElement || e.target).files[0];
            $scope.getFile();
          })

        }

      }
    });
