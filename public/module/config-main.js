function randomIdentifier(){
    return (new Date().getTime());
}

function config($stateProvider, $urlRouterProvider, $locationProvider, $validationProvider, $qProvider, localStorageServiceProvider, paginationTemplateProvider, socialProvider, link){
    $urlRouterProvider.otherwise("/home");

    $stateProvider
        .state('home',{
            url : "/home",
            templateUrl : link.server + "public/pages/home.html?refresh=" + randomIdentifier(),
            authenticate: false,
            controller : 'HomeController',
            authenticate : false
        })
        .state('login',{
            url : "/login",
            templateUrl : link.server + "public/pages/auth/login.html?refresh=" + randomIdentifier(),
            controller: 'AuthController',
            authenticate: false,
            params: {
                'data': ''
            }
        })
        .state('verification',{
            url : "/resend-verification",
            templateUrl : link.server + "pages/auth/verification.html?refresh=" + randomIdentifier(),
            controller: 'VerificationController',
            authenticate: false,
            params: {
                'data': ''
            }
        })
        .state('resetPassword',{
            url : "/reset-password/:verificationCode",
            templateUrl : link.server + "pages/auth/reset-password.html?refresh=" + randomIdentifier(),
            controller: 'ResetPasswordController',
            authenticate: false,
            params: {
                'data': '',
                'verificationCode' : ''
            }
        })
        .state('activate',{
            url : "/activate/:verificationCode",
            templateUrl : link.server + "public/pages/auth/activate.html?refresh=" + randomIdentifier(),
            controller: 'ActivateController',
            authenticate: false,
            params: {
                'data': ''
            }
        })
        .state('join',{
            url : "/join",
            templateUrl : link.server + "public/pages/auth/join.html?refresh=" + randomIdentifier(),
            controller: 'AuthController',
            authenticate: false,
            params: {
                'data': ''
            }
        })
        .state('joinStep1',{
            url : "/join-step-1/:verificationCode",
            templateUrl : link.server + "public/pages/auth/join-step-1.html?refresh=" + randomIdentifier(),
            controller: 'RProfileController',
            authenticate: false,
            params: {
                'data': '',
                'verificationCode' : ''
            }
        })
        .state('joinStep2',{
            url : "/join-step-2/:verificationCode",
            templateUrl : link.server + "public/pages/auth/join-step-2.html?refresh=" + randomIdentifier(),
            controller: 'RProfileController',
            authenticate: false,
            params: {
                'data': '',
                'verificationCode' : ''
            }
        })
        .state('payment',{
            url : "/payment/:verificationCode",
            templateUrl : link.server + "public/pages/auth/payment.html?refresh=" + randomIdentifier(),
            controller: 'PaymentController',
            authenticate: false,
            params: {
                'data': '',
                'verificationCode' : ''
            }
        })
        .state('about',{
            url : "/about",
            templateUrl : link.server + "public/pages/about.html?refresh=" + randomIdentifier(),
            authenticate: false
        })
        .state('contact',{
            url : "/contact",
            templateUrl : link.server + "public/pages/contact.html?refresh=" + randomIdentifier(),
            authenticate: false

        })

        .state('myItem',{
            url : "/my-item/:groupId?",
            templateUrl : link.server + "public/pages/item/my-item.html?refresh=" + randomIdentifier(),
            controller: 'ItemController',
            authenticate: true,
            params: {
                'groupId' : ''
            }

        })
        .state('profile',{
            url : "/profile",
            templateUrl : link.server + "public/pages/profile/profile.html?refresh=" + randomIdentifier(),
            authenticate: true,
            controller: 'ProfileController'

        })
        .state('userProfile',{
            url : "/user-profile/:profileId",
            templateUrl : link.server + "public/pages/profile/user-profile.html?refresh=" + randomIdentifier(),
            authenticate: true,
            controller: 'OtherProfileController',
            params: {
                'data': '',
                'profileId' : ''
            }

        })
        .state('myMessage',{
            url : "/my-message",
            templateUrl : link.server + "public/pages/chat/my-message.html?refresh=" + randomIdentifier(),
            authenticate: true,
            controller: 'ChatController',
            params: {
                'data': ''
            }

        })
        .state('messageInbox',{
            url : "/message-inbox/:chatroomId",
            templateUrl : link.server + "public/pages/chat/my-message.html?refresh=" + randomIdentifier(),
            authenticate: true,
            controller: 'ChatController',
            params: {
                'data': '',
                'chatroomId' : ''
            }

        })
        .state('myContact',{
            url : "/my-contact/:groupId?",
            templateUrl : link.server + "public/pages/contact/my-contact.html?refresh=" + randomIdentifier(),
            authenticate: true,
            controller: 'ContactController',
            params: {
                'groupId' : ''
            }

        })
        .state('browse',{
            url : "/browse",
            templateUrl : link.server + "public/pages/browse/browse.html?refresh=" + randomIdentifier(),
            authenticate: true,
            controller: 'BrowseController'

        })
        .state('browseDetail',{
              url : "/browse-detail/:cardId",
              templateUrl : link.server + "public/pages/browse/browse-detail.html?refresh=" + randomIdentifier(),
              authenticate: true,
              controller: 'BrowseDetailController',
              params: {
                  'data': '',
                  'cardId' : ''
              }
          });

        $urlRouterProvider.otherwise("/home");

        localStorageServiceProvider
            .setPrefix('scedge-web')
            .setStorageType('localStorage');

        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });

        angular.extend($validationProvider, {
            validCallback: function (element){
                $(element).parents('.form-group:first').removeClass('has-error');
            },
            invalidCallback: function (element) {
                $(element).parents('.form-group:first').addClass('has-error');
            }
        });

        paginationTemplateProvider.setPath('../pages/pagination.html');

        socialProvider.setGoogleKey("458613000830-jt0uuijmmad1c1p7ioblbbs6gdj2c85r.apps.googleusercontent.com");
        socialProvider.setLinkedInKey("86a5bi0s0jeyxw");
        socialProvider.setFbKey({appId: "1948768705335398", apiVersion: "v2.10"});

        $qProvider.errorOnUnhandledRejections(false);
}

angular
    .module('scedgeApp')
    .config(["$stateProvider", "$urlRouterProvider", "$locationProvider", "$validationProvider", "$qProvider", "localStorageServiceProvider", "paginationTemplateProvider",
        "socialProvider", "LINK", config])
    .run(['$rootScope', '$state', '$stateParams', '$transitions', 'AuthServiceHandler', 'StorageServiceHandler', 'ProfileServiceHandler',
        function($rootScope, $state, $stateParams, $transitions, authServiceHandler, storageServiceHandler, profileServiceHandler){
            $transitions.onEnter( {}, function($transition$) {
                 var toState = $transition$.to();
                 if ( authServiceHandler.isAuthenticated() ) {
                    profileServiceHandler.getUserNotifications()
                              .then(function(response){
                                  $rootScope.notifications = response.result;
                                  storageServiceHandler.setValue('notifications', response.result);
                              });

                 }
            });

            $transitions.onStart( {}, function($transition$) {
                 var toState = $transition$.to();
                 var nextParams = $transition$.params();
                 $rootScope.authUserDetail = null;
                 if(authServiceHandler.isAuthenticated()) {
                   authServiceHandler.getUserDetail()
                     .then(function(response){
                        $rootScope.authUserDetail = response.result;
                        storageServiceHandler.setValue('userDetail', $rootScope.authUserDetail);
                      });
                 }

                 if (toState.authenticate && !authServiceHandler.isAuthenticated()){
                      return $state.go('login', { nextState: toState.name, nextParams: nextParams});
                 }

                 if ( !toState.authenticate && authServiceHandler.isAuthenticated()) {
                     return $state.go('myItem', { nextState: toState.name, nextParams: nextParams});
                 }
            });
     }]);
