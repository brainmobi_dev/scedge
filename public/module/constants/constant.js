scedgeConstants
	.constant('LINK', {
		'local' :  'http://scedge.dev/',
		'server' : 'https://www.scedge.com/', //'http://scedge.apphosthub.com/',
		'public' : 'http://scedge.dev/',
	})
	.constant('MONTHS', {
		'NAME' : [
				{"JAN":1},
				{"FEB":2},
				{"MAR":3},
				{"APR":4},
				{"MAY":5},
				{"JUN":6},
				{"JUL":7},
				{"AUG":8},
				{"SEP":9},
				{"OCT":10},
				{"NOV":11},
				{"DEC":12}
		]
	})
	.constant('FILTERDATA', {
		'FIELDS' : [
			'location_address', 'location_city', 'location_province', 'location_country', 'cityName', 'provinceName',
			'countryName', 'price', 'quantity', 'discount', 'spacs_size', 'spacs_brand',
			'spacs_model', 'spacs_color', 'for_gender', 'for_age'
		]
	})
	.constant('API', (function(){
	    	return {
	    		'login' 	: 'api/v1/auth/login',
	    		'register'  : 'api/v1/auth/register',
	    		'socialRegister' : 'api/v1/auth/social-register',
	    		'subscription' : 'api/v1/auth/subscription',
	    		'activateUser' : 'api/v1/auth/activateUser',
	    		'processPayment' : 'api/v1/auth/process-payment',
	    		'getVerification' : 'api/v1/auth/get-verification',
	    		'resetPassword' :  'api/v1/auth/reset-password',
	    		'getProvinceAuth' : 'api/v1/auth/getProvince',
					'getCityAuth' : 'api/v1/auth/getCity',
					'resendActivationCode' : 'api/v1/auth/resend-activation-code',
					'checkVerificationCode' : 'api/v1/auth/check-verification-code',
					'updateAccountPassword' : 'api/v1/auth/update-account-password',

	    		'getItem'  : 'api/v1/item/get',
					'getShowcase'  : 'api/v1/profile/getShowcase',
					'createItem' : 'api/v1/item/create',
					'deleteItem' : 'api/v1/item/delete',
					'getCardGroup' : 'api/v1/item/group',
					'updateItem' : 'api/v1/item/update',
					'deleteAll'  : 'api/v1/item/delete-all',
					'getProvince' : 'api/v1/item/getProvince',
					'getCity' : 'api/v1/item/getCity',
					'getContacts' : 'api/v1/item/getContacts',
					'shareCard' : 'api/v1/item/share',
					'addItemGroup' 			: 'api/v1/item/addGroup',
					'updateItemOrder' 	: 'api/v1/item/updateOrder',
					'updateShowcaseOrder' : 'api/v1/item/updateShowcaseOrder',
					'updateSharedItem'  : 'api/v1/item/updateSharedItem',
					'getSharedItems' 		: 'api/v1/item/getSharedItems',
					'updateItemNotification' : 'api/v1/item/updateItemNotification',
					'shareItemViaEmail' : 'api/v1/item/shareItemViaEmail',

					'reportAbuseUser' : 'api/v1/contact/report-abuse',
					'blockUser' : 'api/v1/contact/block-user',

	    		'browse'  : 'api/v1/browse/get',
	    		'getReports' : 'api/v1/browse/getReports',
	    		'reportAbuse' : 'api/v1/browse/reportAbuse',

					'getContact'  : 'api/v1/contact/get',
					'getContactGroup' : 'api/v1/contact/get-group',
					'addContactGroup' : 'api/v1/contact/add-group',
					'addContact' : 'api/v1/contact/add-contact',
					'getUserContactGroup' : 'api/v1/contact/get-user-group',
					'deleteContact' : 'api/v1/contact/delete',
					'pendingRequest' : 'api/v1/contact/pendingRequest',
					'approveConnection' : 'api/v1/contact/approveConnection',
					'rejectConnection' : 'api/v1/contact/rejectConnection',
					'sendInvitation' : 'api/v1/contact/sendInvitation',

					'removeContactGroup' : 'api/v1/contact/removeContactGroup',

	    		'getProfile' : 'api/v1/profile/get',
					'getUserNotifications' : 'api/v1/profile/getUserNotifications',
					'removeUserNotifications' : 'api/v1/profile/removeUserNotifications',



	    		'updatePassword' : 'api/v1/profile/updatePassword',
	    		'updateSetting' : 'api/v1/profile/updateSetting',
	    		'updateCard' : 'api/v1/profile/updateCard',
					'deleteCard' : 'api/v1/profile/deleteCard',
	    		'updateProfile'  : 'api/v1/profile/updateProfile',
	    		'updateSubscription' : 'api/v1/profile/updateSubscription',
	    		'uploadCoverImage' : 'api/v1/profile/saveCoverImage'

	    	};
	})());
