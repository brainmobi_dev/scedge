'use strict';
/* App Module */

if(typeof String.prototype.trim !== 'function') {
  String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g, '');
  }
}

if(typeof String.prototype.toProperCase !== 'function') {
  String.prototype.toProperCase = function () {
      return this.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
  };
}

function isEmpty(obj) {

    // null and undefined are "empty"
    if (obj == null) return true;

    // Assume if it has a length property with a non-zero value
    // that that property is correct.
    if (obj.length > 0)    return false;
    if (obj.length === 0)  return true;

    // Otherwise, does it have any properties of its own?
    // Note that this doesn't handle
    // toString and valueOf enumeration bugs in IE < 9
    for (var key in obj) {
        if (hasOwnProperty.call(obj, key)) return false;
    }

    return true;
}

(function () {

      angular.module('tg.dynamicDirective', [])
        .directive('tgDynamicDirective', ['$compile',
            function($compile) {
                'use strict';

                function templateUrlProvider(getView, ngModelItem) {
                    if (getView) {
                        if (typeof getView === 'function') {
                            var templateUrl = getView(ngModelItem) || '';
                            if (templateUrl) {
                                return templateUrl;
                            }
                        } else if (typeof getView === 'string' && getView.length) {
                            return getView;
                        }
                    }
                    return '';
                }

                return {
                    restrict: 'E',
                    require: '^ngModel',
                    scope: true,
                    template: '<div ng-include="templateUrl"></div>',
                    link: function(scope, element, attrs, ngModel) {

                        scope.$watch(function() {
                            var ngModelItem = scope.$eval(attrs.ngModel);
                            var getView = scope.$eval(attrs.tgDynamicDirectiveView);
                            scope.ngModelItem = ngModelItem;
                            return templateUrlProvider(getView, ngModelItem);
                        }, function(newValue, oldValue) {
                            scope.templateUrl = newValue;
                        });
                    }
                };
            }
        ]);



    angular.module('scedgeApp', [
        'ui.router',
        'ui.bootstrap',
        'ui.sortable',
        'ngRoute',
        'ngResource',
        'ngAnimate',
        'ngCookies',
        'ngMessages',
        'validation',
        'validation.rule',
        '720kb.datepicker',
        '720kb.socialshare',
        'socialLogin',
        'angularUtils.directives.dirPagination',
        'firebase',
        'ngRightClick',
        'luegg.directives',
        'google.places',
        /*'ngAnimate',
        'toastr',*/

        'angular-loading-bar',
        'LocalStorageModule',
        'angucomplete-alt',

        // Scedge Local Modules
        'scedgeConstants',
        'scedgeFilters',
        'scedgeServices',
        'scedgeControllers',
        'scedgeDirectives',
        'duScroll',

        'angularFileUpload',
        'ngImgCrop',
        'infinite-scroll',
        'tg.dynamicDirective',
        'toastr',
        'angularjs-dropdown-multiselect'
        // '720kb.tooltips'
    ]);
})();
