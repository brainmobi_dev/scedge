<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('users');
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email', 100)->unique();
            $table->string('password');

            $table->string('api_token')->nullable();

            $table->string('facebook_token')->nullable();
            $table->string('google_token')->nullable();
            $table->string('linkedin_token')->nullable();

            $table->integer('subscription_id')->unsigned();
            $table->foreign('subscription_id')->references('id')->on('user_subscription')->onDelete('cascade');
            
            $table->boolean('is_verified')->default(0);

            $table->string('registration_stage')->nullable();

            $table->rememberToken();
            $table->timestamps();
            $table->boolean('is_active')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_subscription');
    }
}
