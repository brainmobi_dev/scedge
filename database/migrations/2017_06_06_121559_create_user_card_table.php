<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserCardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_cards', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('owner_id')->unsigned();
            $table->foreign('owner_id')->references('id')->on('users')->onDelete('cascade');

            $table->string('title');
            $table->text('body');
            $table->string('card_image')->nullable();

            $table->date('card_date');
            $table->time('card_time');

            $table->string('priority');

            $table->string('location_address');
            $table->string('location_city');
            $table->string('location_province');
            $table->string('location_country');

            $table->date('start_date')->nullable();
            $table->time('start_time')->nullable();

            $table->date('end_date')->nullable();
            $table->time('end_time')->nullable();

            $table->string('price')->nullable();
            $table->string('quantity')->nullable();
            $table->string('discount')->nullable();

            $table->string('spacs_size')->nullable();
            $table->string('spacs_brand')->nullable();
            $table->string('spacs_model')->nullable();
            $table->string('spacs_color')->nullable();

            $table->string('for_gender')->nullable();
            $table->string('for_age')->nullable();

            $table->boolean('share_on_showcase')->default(1);
            $table->boolean('two_way_sync')->default(0);
            $table->boolean('live_item')->default(0);
            $table->enum('card_type', ['FREE', 'PERSONAL', 'PROFESSIONAL'])->default('FREE');
            $table->boolean('is_active')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_cards');
    }
}
