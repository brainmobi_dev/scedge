<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->string('gender');
            $table->string('dob');
            $table->string('city');
            $table->string('province');
            $table->string('country');

            $table->string('profile_image')->nullable();
            $table->string('cover_image')->nullable();

            $table->string('company_name')->nullable();
            $table->string('website')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('business_hour')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile');
    }
}
