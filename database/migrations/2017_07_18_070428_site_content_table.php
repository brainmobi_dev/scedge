<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SiteContentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('site_contents', function (Blueprint $table) {
            $table->increments('id');

            $table->enum('content_type', ['ABOUT', 'CONTACT-US', 
                'TEAM-COMMUNITY', 'FAMILY-FRIENDS', 
                'PROFESSIONAL-BUSINESS', 'PERSONAL'])->default('ABOUT');

            $table->string('content_title');
            $table->text('content_text');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_contents');
    }
}
