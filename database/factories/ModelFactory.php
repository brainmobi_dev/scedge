<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

function fillOnlyFields($rowSet){
    $resultRowSet = [];
    foreach($rowSet as $key => $value){
        foreach($value as $subvalue){
            $resultRowSet[$key] = $subvalue;
        }
    }
    return $resultRowSet;
}


function createWebsite($companyName){
    return "www." . str_replace(" ", "-", $companyName) . ".com";
}

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    $subscription = fillOnlyFields(App\UserSubscription::select('id')->get()->toArray());

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('password'),
        'remember_token' => str_random(10),
        'is_verified' => 1,
        'subscription_id' => $faker->randomElement($subscription)
    ];
});

$factory->define(App\Profile::class, function (Faker\Generator $faker, $userDetail) {
        
    $profileData = [
        'gender' => $faker->randomElement(['Male', 'Female']),
        'dob' => $faker->date($format = 'Y-m-d', $max = '-30 years'),
        'city' => $faker->city,
        'province' => $faker->state,                
        'country' => $faker->country,                 
        'profile_image' => $faker->imageUrl(200, 200, 'people'),          
        'cover_image' => $faker->imageUrl(500, 300, 'nature')   
    ];

    $subscription = App\User::where('id', $userDetail['user_id'])->select('subscription_id')->first();

    if($subscription->subscription_id == 3) {
        $companyName = $faker->company;
        $professionalDetail = [
            'company_name' => $companyName,           
            'website' => createWebsite($companyName),                
            'phone_number' => $faker->tollFreePhoneNumber,           
            'business_hour' => $faker->randomElement(['7-4', '8-5', '9-6', '10-7', '11-8', '12-9', 'Other'])   
        ];

        return array_merge($profileData, $professionalDetail);
    } else {
        return $profileData;
    }
    
});

$factory->define(App\Contact::class, function (Faker\Generator $faker) {
    $owner = fillOnlyFields(App\User::select('id')->get()->toArray());

    $ownerId = $faker->randomElement($owner);

    $contact = fillOnlyFields(App\User::where('id', '<>', $ownerId)
                                        ->select('id')
                                        ->get()
                                        ->toArray());
    $validContact = false;
    $counter = 1;
    while( !$validContact) {
        $contactId = $faker->randomElement($contact);
        $validContact = !(App\Contact::where('owner_id', $ownerId)->where('contact_id', $contactId)->count());

        $counter++;

        if($counter > 10)
            break;
    }

    $contactGroup = fillOnlyFields(App\ContactGroup::select('id')->get()->toArray());
    return [
        'contact_group_id' => $faker->randomElement($contactGroup),
        'owner_id' => $ownerId,
        'contact_id' => $contactId
    ];
});


$factory->define(App\UserCard::class, function (Faker\Generator $faker) {
    $owner = fillOnlyFields(App\User::select('id')->get()->toArray());
    $cardTypes =  fillOnlyFields(App\CardType::select('id')->get()->toArray());

    $ownerId = $faker->randomElement($owner);
    $ownerSubscriptionType = App\User::where('id', $ownerId)->select('subscription_id')->first();


    $freeData = [
        'owner_id'      => $faker->randomElement($owner),
        'card_type'     => $faker->randomElement($cardTypes),                         
        'title'         => $faker->sentence(6, true),                       
        'body'          => $faker->text(200),                        
        'card_image'    => $faker->imageUrl(500, 500, 'nature'),                  
        'card_date'     => $faker->date('Y-m-d','now'),                   
        'card_time'     => $faker->time('H:i:s', 'now'),                   
        'priority'      => $faker->randomElement([0,1,2,3,4,5,6,7,8,9,10]),                    
        'share_on_showcase' => $faker->randomElement([0,1])
    ];

    $locationData = [
        'location_address'  => $faker->address,            
        'location_city'     => $faker->city,               
        'location_province' => $faker->state,           
        'location_country'  => $faker->country, 
    ];

    $cardData = array_merge($freeData, $locationData);

    if($ownerSubscriptionType->subscription_id == 2) {
        $timeData = [           
            'start_date'=> $faker->date('Y-m-d','now'),                  
            'start_time'=> $faker->time('H:i:s', 'now'),                  
            'end_date'  => $faker->date('Y-m-d', 'now'),                    
            'end_time'  => $faker->time('H:i:s', 'now')                  
        ];

        $cardData = array_merge($cardData, $timeData);
    }


    if($ownerSubscriptionType->subscription_id == 3) {
        $priceData = [   
            'price'     => $faker->randomNumber(2),                  
            'discount'  => $faker->randomNumber(2) 
        ];

        $spacsData = [                   
            'spacs_size'    => $faker->randomNumber(2),                  
            'spacs_brand'   => $faker->domainWord,                 
            'spacs_model'   => $faker->domainWord,                 
            'spacs_color'   => $faker->colorName  
        ];

        $audienceData = [               
            'for_gender'    => $faker->randomElement(['male', 'female']),                  
            'for_age'       => $faker->randomNumber(2)                     
        ];

        $visibilityData = [
            'two_way_sync' => $faker->randomElement([0,1]),
            'live_item' => $faker->randomElement([0,1])
        ];

        $cardData = array_merge($cardData, $priceData, $spacsData, $audienceData, $visibilityData);
    }

    return $cardData;
});

