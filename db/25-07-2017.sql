insert  into `user_subscription`(`id`,`subscription_name`,`subscription_amount`,`is_active`,`created_at`,`updated_at`) values (1,'FREE','0',1,NULL,NULL),(2,'PERSONAL','100',1,NULL,NULL),(3,'PROFESSIONAL','200',1,NULL,NULL);

insert  into `contact_group`(`id`,`name`,`is_active`) values (1,'PERSONAL',1),(2,'FAMILY',1),(3,'FRIEND',1),(4,'OFFICE',1);

insert  into `card_types`(`id`,`type`,`image`,`is_active`) values (1,'PERSONAL',NULL,1),(2,'FAMILY',NULL,1),(3,'OFFICE',NULL,1);
