INSERT INTO `user_subscription` (`id`, `subscription_code`, `subscription_name`, `subscription_amount`, `subscription_type`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'FREE', 'FREE', '0', 'ALL', 1, NULL, NULL),
(2, 'PER_MONT', 'PERSONAL', '10', 'MONTHLY', 1, NULL, NULL),
(3, 'PRO_MONT', 'PROFESSIONAL', '20', 'MONTHLY', 1, NULL, NULL),
(4, 'PER_ANUL', 'PERSONAL', '100', 'ANNUAL', 1, NULL, NULL),
(5, 'PRO_ANUL', 'PROFESSIONAL', '200', 'ANNUAL', 1, NULL, NULL);


INSERT INTO `card_types` (`id`, `type`, `image`, `is_active`) VALUES
(1, 'PERSONAL', NULL, 1),
(2, 'FAMILY', NULL, 1),
(3, 'OFFICE', NULL, 1);

INSERT INTO `contact_group` (`id`, `name`, `is_active`) VALUES
(1, 'PERSONAL', 1),
(2, 'FAMILY', 1),
(3, 'FRIEND', 1),
(4, 'OFFICE', 1);
