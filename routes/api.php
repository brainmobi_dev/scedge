<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('google-issue', 'Website\UserController@googleIssue');
Route::get('facebook-issue', 'Website\UserController@facebookIssue');
Route::get('linkdin-issue', 'Website\UserController@linkdinIssue');

Route::group(['prefix' => 'v1'], function() {
	Route::group(['prefix' => 'auth'], function(){
		Route::post('register', 'Website\UserController@register');
		Route::post('social-register', 'Website\UserController@socialRegistration');
		Route::post('login', 'Website\UserController@login');
		Route::post('verify', 'Website\UserController@verifyEmail');
		Route::post('forgot-password', 'Website\UserController@forgotPassword');
		Route::post('upload-image', 'Website\UserController@uploadImage');
		Route::post('validate-token', 'Website\UserController@isValidToken');
		Route::get('subscription', 'Website\UserController@subscription');
		Route::get('detail', 'Website\UserController@detail');
		Route::post('activateUser', 'Website\UserController@activateUser');
		Route::get('create-subscription', 'Website\UserController@createSubscription');
		Route::post('process-payment', 'Website\UserController@processPayment');
		Route::post('get-verification', 'Website\UserController@getVerification');
		Route::post('reset-password', 'Website\UserController@resetPassword');
		Route::post('getProvince', 'Website\UserController@getProvince')->name('getProvince');
		Route::post('getCity', 'Website\UserController@getCity')->name('getCity');
		Route::post('resend-activation-code', 'Website\UserController@resendActivationCode')->name('resendActivationCode');
		Route::post('check-verification-code', 'Website\UserController@checkVerificationCode')->name('checkVerificationCode');
		Route::post('update-account-password', 'Website\UserController@updateAccountPassword')->name('updateAccountPassword');
	});

	Route::group(['middleware' => 'jwt.auth'], function () {
	    Route::post('user', 'Website\UserController@getAuthUser')->name('userDetail');
	    Route::post('skills', 'ProfileController@getSkills')->name('skills');

	    Route::group(['prefix' => 'profile'], function(){
				Route::post('getShowcase', 'Website\CardController@getShowcase')->name('getShowcase');
	    	Route::post('get', 'Website\ProfileController@getProfile')->name('getProfile');
	    	Route::post('save', 'Website\ProfileController@saveProfile')->name('saveProfile');
	    	//Route::post('setting', 'ProfileController@getSetting')->name('getSetting');
	    	//Route::post('saveSetting', 'ProfileController@saveSetting')->name('saveSetting');
	    	Route::post('reviews', 'Website\ProfileController@getReviews')->name('getReviews');
	    	Route::post('updatePassword', 'Website\ProfileController@updatePassword')->name('updatePassword');
	    	Route::post('updateProfile', 'Website\ProfileController@updateProfile')->name('updateProfile');
	    	Route::post('updateCard', 'Website\ProfileController@updateCard')->name('updateCard');
	    	Route::post('updateSetting', 'Website\ProfileController@updateSetting')->name('updateSetting');
				Route::post('updateSubscription', 'Website\ProfileController@updateSubscription')->name('updateSubscription');
				Route::post('deleteCard', 'Website\ProfileController@deleteCard')->name('deleteCard');
				Route::post('disableAccount', 'Website\ProfileController@disableAccount')->name('disableAccount');
				Route::post('saveCoverImage', 'Website\ProfileController@saveCoverImage')->name('saveCoverImage');
				Route::post('getUserNotifications', 'Website\ProfileController@getUserNotifications')->name('getUserNotifications');
				Route::post('removeUserNotifications', 'Website\ProfileController@removeUserNotifications')->name('removeUserNotifications');
	    });

	    Route::group(['prefix' => 'payment'], function() {
	    	Route::get('create-stripe-Customer', 'Website\PaymentController@createStripeCustomer')->name('createStripeCustomer');
				Route::get('create-subscription', 'Website\PaymentController@createSubscription')->name('createSubscription');
				Route::get('update-subscription', 'Website\PaymentController@updateSubscription')->name('updateSubscription');
				Route::get('create-card', 'Website\PaymentController@createCard')->name('createCard');

				Route::get('get-plan', 'Website\PaymentController@getPlan')->name('getPlan');
				Route::get('get-card', 'Website\PaymentController@getCards')->name('getCards');
				Route::get('update-card', 'Website\PaymentController@updateCard')->name('updateCard');
	    });

	    Route::group(['prefix' => 'setting'], function(){
	    	Route::post('get', 'Website\ProfileController@getSetting')->name('getSetting');
	    	Route::post('save', 'Website\ProfileController@saveSetting')->name('saveSetting');
	    	Route::post('update-account', 'Website\ProfileController@updateAccount')->name('updateAccount');
	    	Route::post('terms-of-service', 'Website\ProfileController@termsOfService')->name('termsOfService');
	    	Route::post('privacy-policy', 'Website\ProfileController@privacyPolicy')->name('privacyPolicy');
	    });

	    Route::group(['prefix' => 'rating'], function(){
	    	Route::post('get', 'Website\RatingController@getRating')->name('getRating');
	    	Route::post('save', 'Website\RatingController@saveRating')->name('saveRating');
	    });

	    Route::group(['prefix' => 'item'], function() {
	    	Route::post('get', 'Website\CardController@getCard')->name('getItem');
				Route::post('create', 'Website\CardController@createCard')->name('createCard');
				Route::post('update', 'Website\CardController@updateCard')->name('updateCard');
				Route::post('delete', 'Website\CardController@deleteCard')->name('deleteCard');
				Route::post('delete-all', 'Website\CardController@deleteAllCards')->name('deleteAllCards');
				Route::post('share', 'Website\CardController@shareCard')->name('shareCard');
				Route::post('group', 'Website\CardController@cardGroup')->name('cardGroup');
				Route::post('getProvince', 'Website\CardController@getProvince')->name('getProvince');
				Route::post('getCity', 'Website\CardController@getCity')->name('getCity');
				Route::post('getContacts', 'Website\CardController@getContacts')->name('getContacts');
				Route::post('addGroup', 'Website\CardController@addCardGroup')->name('addCardGroup');
				Route::post('uploadImage', 'Website\CardController@uploadImage')->name('uploadImage');
				Route::post('updateOrder', 'Website\CardController@updateOrder')->name('updateOrder');
				Route::post('updateShowcaseOrder', 'Website\CardController@updateShowcaseOrder')->name('updateShowcaseOrder');

				Route::post('getSharedItems', 'Website\CardController@getSharedCards')->name('getSharedItems ');
				Route::post('updateSharedItem', 'Website\CardController@updateSharedCard')->name('updateSharedCard');
				Route::post('updateItemNotification', 'Website\CardController@updateItemNotification')->name('updateItemNotification');
				Route::post('shareItemViaEmail', 'Website\CardController@shareItemViaEmail')->name('shareItemViaEmail');

	    });

	    Route::group(['prefix' => 'contact'], function() {
	    	Route::post('get', 'Website\ContactController@getContact')->name('getContact');
				Route::post('get-group', 'Website\ContactController@getContactGroup')->name('getContactGroup');
				Route::post('add-group', 'Website\ContactController@addContactGroup')->name('addContactGroup');
				Route::post('add-contact', 'Website\ContactController@addContact')->name('addContact');
				Route::post('get-user-group', 'Website\ContactController@getUserGroup')->name('getUserGroup');
				Route::post('delete', 'Website\ContactController@deleteContact')->name('deleteContact');
				Route::post('pendingRequest', 'Website\ContactController@pendingRequest')->name('pendingRequest');

				Route::post('approveConnection', 'Website\ContactController@approveConnection')->name('approveConnection');
				Route::post('rejectConnection', 'Website\ContactController@rejectConnection')->name('rejectConnection');

				Route::post('sendInvitation', 'Website\ContactController@sendInvitation')->name('sendInvitation');

				Route::post('block-user', 'Website\ContactController@blockUser')->name('blockUser');
				Route::post('unblock-user', 'Website\ContactController@unblockUser')->name('unblockUser');
				Route::post('report-abuse', 'Website\ContactController@reportAbuse')->name('reportAbuse');
				Route::post('removeContactGroup', 'Website\ContactController@removeContactGroup')->name('removeContactGroup');
	    });

	    Route::group(['prefix' => 'browse'], function() {
	    	Route::post('get', 'Website\CardController@browseCard')->name('browseCard');
	    	Route::post('getReports', 'Website\CardController@getReports')->name('getReports');
	    	Route::post('reportAbuse', 'Website\CardController@reportAbuse')->name('reportAbuse');
	    });

	});
});
