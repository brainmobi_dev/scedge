<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//use Auth;
use Illuminate\Http\Request;

Route::get('google-issue', 'Website\UserController@googleIssue');
Route::get('facebook-issue', 'Website\UserController@facebookIssue');
Route::get('linkdin-issue', 'Website\UserController@linkdinIssue');
//Child Card


Route::get('/', function () {
    return view('welcome');
})->name('website');

Route::get('/login', function() {
    return view('welcome');
});

Route::get('/resend-verification', function() {
    return view('welcome');
});

Route::get('/reset-password/{verificationCode}', function() {
    return view('welcome');
});

Route::get('/about', function() {
    return view('welcome');
});

Route::get('/contact', function() {
    return view('welcome');
});

Route::get('/about', function() {
    return view('welcome');
});

Route::get('/activate/{verificationCode?}', function($verificationCode) {
    return view('welcome');
});

Route::get('/join-step-1/{verificationCode?}', function($verificationCode) {
	 return view('welcome');
});

Route::get('/join-step-2/{verificationCode?}', function($verificationCode) {
	 return view('welcome');
});

Route::get('/payment/{verificationCode?}', function($verificationCode) {
	 return view('welcome');
});

Route::get('/my-contact/{groupId?}', function() {
	 return view('welcome');
});

Route::get('/browse', function() {
	 return view('welcome');
});

Route::get('/browse-detail/{cardId?}', function($cardId) {
	 return view('welcome');
});

Route::get('/my-message', function() {
	 return view('welcome');
});

Route::get('/message-inbox/{chatroomId?}', function($chatroomId) {
	 return view('welcome');
});


Route::get('/user-profile/{profileId?}', function($profileId) {
	 return view('welcome');
});

Route::get('/profile', function() {
	 return view('welcome');
});

Route::get('/my-item/{verificationCode?}', function() {
	 return view('welcome');
});


Route::get('/join', function() {
	 return view('welcome');
});

Route::get('/home', function() {
	 return view('welcome');
});


Route::get('/admin', 'Admin\HomeController@index')->name('admin');

Route::get('/expireCards', 'Admin\UserController@expireCards')->name('expirecards');

Route::group(['prefix' => 'admin'], function() {
    Route::group(['prefix' => 'home'], function(){
        Route::get('/', 'Admin\HomeController@index')->name('admin-home');
    });

    //Route::get('/log-out', 'Auth\AdminLoginController@logout')->name('admin.logout');
    //Route::post('/log-out', 'Auth\AdminLoginController@logout')->name('admin.logout.submit');
    Route::get('/logout', function(Request $request){
      Auth::guard('admin')->logout();
      $request->session()->flush();
      $request->session()->regenerate();
      return redirect()->intended(route('admin.login'));
    })->name('admin.logout');

    Route::post('/logout', function(Request $request){
      Auth::guard('admin')->logout();
      $request->session()->flush();
      $request->session()->regenerate();
      return redirect()->intended(route('admin.login'));
    })->name('admin.logout.submit');

    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');

    Route::group(['prefix' => 'childcard'], function(){
        Route::match(['get','post'], '/', 'Admin\ChildcardController@index')->name('childcard');
        Route::match(['get','post'], 'view', 'Admin\ChildcardController@view')->name('viewchildcard');
        Route::match(['get','post'], 'edit', 'Admin\ChildcardController@edit')->name('editchildcard');
    });

    //User
    Route::group(['prefix' => 'user'], function(){
      Route::match(['get','post'], '/','Admin\UserController@index')->name('user');
      Route::post('delete','Admin\UserController@destroy')->name('user/delete');
      Route::post('changestatus','Admin\UserController@changeStatus')->name('user/changestatus');
      Route::match(['get','post'],'edit','Admin\UserController@edit')->name('user/edit');
      Route::match(['get','post'],'resetPassword','Admin\UserController@updatePassword')->name('adminResetPassword');
    });

    //Content Management Routes
    Route::group(['prefix' => 'content'], function(){
            Route::get('/', 'Admin\ContentController@index')->name('contentList');
            Route::match(['get','post'],'/edit', 'Admin\ContentController@edit')->name('contentUpdate');
            Route::post('/changestatus', 'Admin\ContentController@changeStatus')->name('adminContentStatus');
    });

    //Contact management routes
    Route::group(['prefix' => 'contact'], function(){
            Route::get('/', 'Admin\ContactController@index')->name('adminContactlists');
            Route::get('/groupMembersList','Admin\ContactController@group_members_list')->name('groupMembersList');
            Route::post('/group_edit','Admin\ContactController@groupEdit')->name('group_edit');
            Route::post('/deleteGroup', 'Admin\ContactController@deleteGroup')->name('deleteGroup');
            Route::post('/deleteContact', 'Admin\ContactController@deleteContact')->name('deleteContact');

    });

    Route::group(['prefix' => 'ajax'], function(){
            Route::get('/getStates', 'Admin\AjaxController@getStates')->name('getStates');
            Route::get('/getCities','Admin\AjaxController@getCities')->name('getCities');

    });

    //Subscription Management
    Route::group(['prefix' => 'subscription'], function(){
            Route::get('/', 'Admin\SubscriptionController@index')->name('subscriptionList');
            Route::match(['get','post'],'/edit','Admin\SubscriptionController@edit')->name('subscriptionUpdate');
            Route::post('/changestatus', 'Admin\SubscriptionController@changeStatus')->name('subscriptionStatus');
    });

    //Item Groups(Item type)
    Route::group(['prefix' => 'itemtype'], function(){
            Route::get('/','Admin\ItemtypeController@index')->name('itemTypeList');
            Route::match(['get','post'],'/add','Admin\ItemtypeController@add')->name('addItemType');
            Route::match(['get','post'],'/edit','Admin\ItemtypeController@edit')->name('itemTypeUpdate');
            Route::post('/changestatus', 'Admin\ItemtypeController@changeStatus')->name('itemTypeStatus');
            Route::post('/delete', 'Admin\ItemtypeController@delete')->name('deleteItemType');
    });

    //Item(Cards)
    Route::group(['prefix' => 'item'], function(){
            Route::get('/','Admin\ItemController@index')->name('item');
            Route::get('/view','Admin\ItemController@view')->name('itemView');
            Route::match(['get','post'],'/edit','Admin\ItemController@edit')->name('itemUpdate');
            Route::post('/changestatus', 'Admin\ItemController@changeStatus')->name('itemStatus');
            Route::post('/delete', 'Admin\ItemController@destroy')->name('itemDelete');
    });

//    //User
//    Route::group(['prefix' => 'user'], function(){
//            Route::match(['get','post'],'/','Admin\UserController@index')->name('user');
//            Route::post('/delete','Admin\UserController@destroy')->name('user/delete');
//            Route::post('/changestatus','Admin\UserController@changeStatus')->name('user/changestatus');
//            Route::match(['get','post'],'/edit','Admin\UserController@edit')->name('user/edit');
//    });

    //Chat Management
    Route::group(['prefix' => 'chat'], function(){
            Route::get('/', 'Admin\ChatController@index')->name('chatList');

    });
    
    //Abuse Management
    Route::group(['prefix' => 'abuse'], function(){
            Route::get('/cardAbuse', 'Admin\AbuseController@card_abuse')->name('cardAbuse');
            Route::get('/userAbuse', 'Admin\AbuseController@userAbuse')->name('userAbuse');
            Route::get('/blockUser', 'Admin\AbuseController@blockUser')->name('blockUser');
            Route::post('/deleteReport', 'Admin\AbuseController@deleteReport')->name('deleteReport');
            Route::get('/abuseContent', 'Admin\AbuseController@abuseContent')->name('abuseContent');
            Route::get('/abuseContentInsert', 'Admin\AbuseController@abuseContentInsert')->name('abuseContentInsert');
            Route::post('/abuseContentAdd', 'Admin\AbuseController@abuseContentAdd')->name('abuseContentAdd');
            Route::get('/abuseContentEdit', 'Admin\AbuseController@abuseContentEdit')->name('abuseContentEdit');
            Route::post('/abuseContentUpdate', 'Admin\AbuseController@abuseContentUpdate')->name('abuseContentUpdate');
            Route::post('/abuseContentDelete', 'Admin\AbuseController@abuseContentDelete')->name('abuseContentDelete');
            
            Route::get('/blockedUserLists', 'Admin\AbuseController@blockedUserLists')->name('blockedUserLists');
            Route::post('/deleteRecord', 'Admin\AbuseController@deleteRecord')->name('deleteRecord');
            Route::post('/unblockUser', 'Admin\AbuseController@unblockUser')->name('unblockUser');

    });

  //  Auth::routes();
});
