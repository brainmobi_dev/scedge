<!doctype html>
<html lang="<?php echo config('app.locale'); ?>" ng-app="scedgeApp">
    <head>
        <base href="/">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="<?php echo secure_asset('bower_components/angular-loading-bar/src/loading-bar.css'); ?>" type="text/css" href="">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>

        <div class="flex-center position-ref full-height">
           

            <div class="content">
                <div class="title m-b-md">
                    <ui-view></ui-view>
                </div>
            </div>
        </div>

        <script src="<?php echo secure_asset('bower_components/angular/angular.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo secure_asset('bower_components/angular-accordion/angular-accordion.js'); ?>"  type="text/javascript"></script>
        <script src="<?php echo secure_asset('bower_components/angular-animate/angular-animate.js'); ?>"  type="text/javascript"></script>
        <script src="<?php echo secure_asset('bower_components/angular-cookies/angular-cookies.js'); ?>"  type="text/javascript"></script>
        <script src="<?php echo secure_asset('bower_components/angular-loader/angular-loader.js'); ?>"  type="text/javascript"></script>
        <script src="<?php echo secure_asset('bower_components/angular-pagination/angular-pagination.js'); ?>"  type="text/javascript"></script>
        <script src="<?php echo secure_asset('bower_components/angular-resource/angular-resource.js'); ?>"  type="text/javascript"></script>
        <script src="<?php echo secure_asset('bower_components/angular-messages/angular-messages.js'); ?>"  type="text/javascript"></script>
        <script src="<?php echo secure_asset('bower_components/angular-loading-bar/src/loading-bar.js'); ?>"  type="text/javascript"></script>

        <script src="<?php echo secure_asset('bower_components/angular-route/angular-route.js'); ?>"  type="text/javascript"></script>
        <script src="<?php echo secure_asset('bower_components/angular-ui-router/release/angular-ui-router.js'); ?>"  type="text/javascript"></script>
        
        <script src="<?php echo secure_asset('bower_components/jquery/dist/jquery.js'); ?>"  type="text/javascript"></script>
        <script src="<?php echo secure_asset('bower_components/underscore/underscore.js'); ?>"  type="text/javascript"></script>        

        <script src="<?php echo secure_asset('build/module.js'); ?>"  type="text/javascript"></script>     
    </body>
</html>
