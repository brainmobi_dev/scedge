<!doctype html>
<html lang="{{ config('app.locale') }}" ng-app="scedgeApp">
    <head>
        <base href="/">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Scedge</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Oswald:200,300,400,500,600,700&subset=cyrillic,latin-ext,vietnamese" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i&subset=latin-ext,vietnamese" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="{{ secure_asset('bower_components/angular-loading-bar/src/loading-bar.css') }}" type="text/css" href="">
        <link href="{{ secure_asset('css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ secure_asset('css/style.css') }}" rel="stylesheet">
        <link href="{{ secure_asset('css/responsive.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="{{ secure_asset('css/font-awesome.min.css') }}">
        <link href="{{ secure_asset('bower_components/angularjs-datepicker/dist/angular-datepicker.css') }}" rel="stylesheet" type="text/css" />
        <!-- Styles -->
    </head>
    <body>
        <div id="wrapper">
        	<div align="center" style="margin-top: 20%;font-size: 32px !important;font-weight: bold;">
            	Page not found
            </div>
        </div>  

        <!-- Include all compiled plugins (below), or include individual files as needed --> 
       
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="{{ secure_asset('bower_components/underscore/underscore.js') }}"  type="text/javascript"></script> 
         <script src="{{ secure_asset('js/bootstrap.min.js') }}"></script> 
        <script src="{{ secure_asset('js/owl.carousel.js') }}"></script> 
        <script src="{{ secure_asset('js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
        <script src="{{ secure_asset('js/custom.js') }}"></script>  
    </body>
</html>
