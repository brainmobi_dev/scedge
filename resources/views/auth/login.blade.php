<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Sign In | Schedge</title>
     <!--Favicon-->
    <link rel="icon" href="../../favicon.ico" type="image/x-icon">

     <!--Google Fonts-->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

     <!--Bootstrap Core Css -->
    <link href="{{secure_asset("admin-theme/plugins/bootstrap/css/bootstrap.css")}}" rel="stylesheet">

     <!--Waves Effect Css-->
    <link href="{{secure_asset("admin-theme/plugins/node-waves/waves.css")}}" rel="stylesheet" />

     <!--Animation Css-->
    <link href="{{secure_asset("admin-theme/plugins/animate-css/animate.css")}}" rel="stylesheet" />

     <!--Custom Css-->
    <link href="{{secure_asset("admin-theme/css/style.css")}}" rel="stylesheet">
</head>

<body class="login-page">
    <div class="login-box">
        <div class="logo">
            <a href="javascript:void(0);">Admin</a>
        </div>
        <div class="card">
            <div class="body">
                <form id="sign_in" method="POST" action="{{ route('admin.login.submit') }}">
                     {{ csrf_field() }}
                    <div class="msg">Sign in to start your session</div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line{{ $errors->has('email') ? ' has-error' : '' }}" >
                            <input type="text" class="form-control" name="email" placeholder="Username" required autofocus>
                            @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line{{ $errors->has('email') ? ' has-error' : '' }}">
                            <input type="password" class="form-control" name="password" placeholder="Password" required>
                            @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-8 p-t-5">
                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }} id="rememberme" class="filled-in chk-col-pink">
                            <label for="rememberme">Remember Me</label>
                        </div>
                        <div class="col-xs-4">
                            <button class="btn btn-block bg-pink waves-effect" type="submit">SIGN IN</button>
                        </div>
                    </div>
                    <div class="row m-t-15 m-b--20">
                        <div class="col-xs-6 align-right">

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<!--     Jquery Core Js -->
    <script src="{{secure_asset("admin-theme/plugins/jquery/jquery.min.js")}}"></script>

<!--     Bootstrap Core Js -->
    <script src="{{secure_asset("admin-theme/plugins/bootstrap/js/bootstrap.js")}}"></script>

<!--     Waves Effect Plugin Js -->
    <script src="{{secure_asset("admin-theme/plugins/node-waves/waves.js")}}"></script>

<!--     Validation Plugin Js -->
    <script src="{{secure_asset("admin-theme/plugins/jquery-validation/jquery.validate.js")}}"></script>

<!--     Custom Js -->
    <script src="{{secure_asset("admin-theme/js/admin.js")}}"></script>
    <script src="{{secure_asset("admin-theme/js/pages/examples/sign-in.js")}}"></script>
</body>

</html>
