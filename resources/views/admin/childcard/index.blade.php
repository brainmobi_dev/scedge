@extends('admin.layout.admin')

@section('content')
<div class="card">
        <div class="header">
            <h2>
               ITEM LIST
            </h2>
        </div>

        <div class="body">
            <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline dt-bootstrap"><div class="row"><div class="col-sm-6"><div class="dataTables_length" id="DataTables_Table_0_length"></div></div><div class="col-sm-6"><div id="DataTables_Table_0_filter" class="dataTables_filter"><label>Search:<input type="search" class="form-control input-sm" placeholder="" aria-controls="DataTables_Table_0"></label></div></div></div><div class="row"><div class="col-sm-12"><table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
                <thead>
                    <tr role="row">
                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 223px;">Owner name</th>
                    <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 134px;">Title</th>
                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 223px;">Body</th>
                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 223px;">Item Image</th>
                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 223px;">Action</th>
                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 223px;">Status</th></tr>
                </thead>

                <tbody>
                @if(count($childCards)>0)
                    @foreach ($childCards as $card)
                    <tr role="row" class="odd" id="row_{{ $card->id}}">
                            <td><input type="hidden" id="item_id" name="_token" value="{{ csrf_token() }}">{{$card->name}}
                            <td>{{$card->title}}</td>
                            <td>
                                {{str_limit($card->body,40)}}
                            </td>
                            <td>
                              @if(!($card->card_image))
                                <img src="{{ secure_asset('public/image/project-img2.jpg') }}" style="width:80px; height: 80px;" />
                              @elseif(strpos($card->card_image, 'http') === false)
                                <img src="{{ secure_asset($card->card_image) }}" style="width:80px; height: 80px;" />
                              @else
                                <img src="{{ $card->card_image }}" style="width:80px; height: 80px;" />
                              @endif
                            </td>
                            <td>
                                <a href="{{URL::to('/').'/admin/childcard/edit?item_id='.$card->id}}" style="height: 60px;width:60px"><i class="material-icons">edit</i></a>
                                <a href="#" onclick="deleteItem({{$card->id}})"><i class="material-icons ">delete</i>
                                </a>
                                <a href="{{URL::to('/').'/admin/childcard/view?item_id='.$card->id}}"><i class="material-icons ">visibility</i>
                                </a>
                            </td>

                            <td>
                                 @if($card->is_active===1)
                                  <a id="item_{{ $card->id}}" href="javascript:changeStatus({{ $card->id}}, 0);" ><span class="btn btn-danger">Deactivate Card</span></a>
                                 @else
                                   <a id="item_{{ $card->id}}" href="javascript:changeStatus({{ $card->id}}, 1);" ><span class="btn btn-success">Activate Card</span></a>
                                 @endif
                            </td>

                    </tr>
                    @endforeach
                @else
                    <tr>
                      <td  colspan="6"><div align='center'><b>No result founds</b></div></td>
                    </tr>
                @endif

                </tbody>
            </table>
            <div>
                {{ $childCards->links() }}
            </div>
          </div>
        </div>
    </div>

    <script>
    function deleteItem(item_id) {
        var host = "{{URL::to('/admin')}}";
            bootbox.confirm("Are you sure you want to delete this data?", function(result) {
            if (result) {
                    $.ajax({
                        url: host+'/item/delete',
                        type: "post",
                        data: {"item_id": item_id,'_token':"{{ csrf_token()}}" },
                        success: function(response) {
                            $("#row_" + item_id).remove();
                        }
                    });
            }
        });
    }

    function changeStatus(item_id, status){
        var host = "{{URL::to('/admin')}}";
            $.ajax({
                url: host+'/item/changestatus',
                type: "post",
                data: {"item_id": item_id, "status":status,'_token':"{{ csrf_token()}}"},
                success: function(response) {
                var newStatus= (status)?0:1;
                if(response.error_code == 200){
                    if(status == "1"){
                        $("#item_"+item_id+ " span").removeClass("btn btn-success");
                        $("#item_"+item_id+ " span").addClass("btn btn-danger");
                        $("#item_"+item_id+ " span").text("Deactivate Card");
                        $("#item_"+item_id).attr("href", "javascript:changeStatus("+item_id+", "+newStatus+")");

                    } else {

                        $("#item_"+item_id+ " span").removeClass("btn btn-danger");
                        $("#item_"+item_id+ " span").addClass("btn btn-success");
                        $("#item_"+item_id+ " span").text("Activate Card");
                        $("#item_"+item_id).attr("href", "javascript:changeStatus("+item_id+", "+newStatus+")");
                    }
                }
                }
                });

    }
    </script>

@endsection
