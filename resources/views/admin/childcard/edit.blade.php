@extends('admin.layout.admin')

@section('content')
    
<!-- #END# Basic Validation -->
<script src="{{secure_asset("admin-theme/js/jquery-ui.js")}}"></script>
<!--<script src="{{asset("admin-theme/js/moment.min.js")}}"></script>
<link href="{{asset("admin-theme/css/bootstrap-datetimepicker.min.css")}}" rel="stylesheet"/>-->
<script src="{{secure_asset("admin-theme/js/combodate.js")}}"></script>
<script type="text/javascript">
    $(document).ready(function () {
    $('#card_time').datetimepicker({
        console.log('scsd');
        //defaultDate: new Date(),
        //format: 'hh:mm:ss A',
        //sideBySide: true
    });
    });
    $(document).ready(function () {
        $("#card_date").datepicker({
            dateFormat: "yy-M-dd",
            minDate: 0,
            onSelect: function (date) {
                var startDate = $(this).datepicker('getDate');
                var minDate = $(this).datepicker('getDate');
                startDate.setDate(startDate.getDate() + 30);
            }
    });
        
    $("#card_start_date").datepicker({
            dateFormat: "dd-M-yy",
            minDate: 0,
            onSelect: function (date) {
                var endDate = $('#card_end_date');
                var startDate = $(this).datepicker('getDate');
                var minDate = $(this).datepicker('getDate');
                endDate.datepicker('setDate', minDate);
                startDate.setDate(startDate.getDate() + 30);
                //sets dt2 maxDate to the last day of 30 days window
                endDate.datepicker('option', 'maxDate', startDate);
                endDate.datepicker('option', 'minDate', minDate);
                $(this).datepicker('option', 'minDate', minDate);
            }
        });
    $('#card_end_date').datepicker({
        dateFormat: "dd-M-yy",
    });

    });
    
    </script>

<!-- Basic Validation -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Edit Item Details</h2>
                </div>
                <div class="body">
                    <form id="form_validation" action="#" method="POST" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="user_id" value="{{ $card->id }}">
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="card_title">Item Title</label>
                            </div>

                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="card_title" name="card_title" value="{{ !empty($card->title)? $card->title:''}}" class="form-control" placeholder="Enter card title">
                                        </div>
                                    </div>
                            <span class="error">{{$errors->first('card_title')}}</span>        
                            </div>
                        </div>    

                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                    <label for="card_body">Item Body</label>
                            </div>

                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="card_body" name="card_body" value="{{ !empty($card->body)? $card->body:''}}" class="form-control" placeholder="Enter card body">
                                        </div>
                                    </div>
                            <span class="error">{{$errors->first('card_body')}}</span>        
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3 form-control-label">
                                    <label for="card_date">Card Date</label>
                            </div>

                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-3">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name ="card_date" id="card_date" class="form-control" value= "{{$card->card_date}}"placeholder="Select card date">
                                        </div>
                                    </div>
                            <span class="error">{{$errors->first('card_date')}}</span>    
                            </div>

                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3 form-control-label">
                                    <label for="card_time">Card Time</label>
                            </div>

                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-3">
                                    <div class="form-group">
                                        <div class="form-line">
                                           <input type="text" id="card_time" name="card_time" data-format="HH : mm : ss" data-template="HH : mm : ss " name="datetime" value="21-12-2012 20:30">
                                        </div>
                                    </div>
                            <span class="error">{{$errors->first('card_time')}}</span>    
                            </div>
                        </div>
                        
                        
                         <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3 form-control-label">
                                    <label for="priority">Priority</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="priority" name="priority" value="{{ !empty($card->priority)? $card->priority:''}}" class="form-control" placeholder="Enter card title">
                                    </div>
                                </div>
                                <span class="error">{{$errors->first('priority')}}</span>        
                            </div>
                         </div> 
                        
                        <!----  For professional subscription  ----->
                         <div class="professional-subscription row clearfix" id="professional-subscription" {{$professional}} >
                            <div class="col-lg-offset-1 col-md-offset-1 col-sm-offset-1  col-lg-11 col-md-11 col-sm-12 col-xs-12">
                                <!-- Nav tabs -->
                                <ul class=" nav nav-tabs tab-nav-right" role="tablist">
                                    <li role="presentation" class="active form-control-label" ><a href="#specs" data-toggle="tab">SPECS</a></li>
                                    <li role="presentation" class="form-control-label"><a href="#location" data-toggle="tab">LOCATION</a></li>
                                    <li role="presentation" class="form-control-label"><a href="#price" data-toggle="tab">PRICE</a></li>
                                    <li role="presentation" class="form-control-label"><a href="#for" data-toggle="tab">FOR</a></li>
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content" style="margin-left:15px">
                                    <div role="tabpanel" class="tab-pane fade in active" id="specs">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-line">   
                                                      <input type="text" name="spacs_size" id="spacs_size" class="form-control" placeholder="Enter size">    
                                                    </div>
                                                 </div>

                                                <div class="col-md-6">
                                                  <div class="form-line">  
                                                      <input type="text" name="spacs_brand" id="spacs_brand" class="form-control" placeholder="Enter brand">   
                                                  </div>
                                                 </div>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <div class="row">
                                                    <div class="col-md-6">
                                                      <div class="form-line">
                                                           <input type="text" name="spacs_modal" id="spacs_modal" class="form-control" placeholder="Enter modal">     
                                                      </div>
                                                    </div>
                                                    <div class="col-md-6">
  
                                                    <div class="form-line">
                                                      <input type="text" name="spacs_color" id="spacs_color" class="form-control" placeholder="Enter color"> 
                                                    </div>
                                                  </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="switch">
                                                      <label>Show on Showcase<input type="checkbox" checked=""><span class="lever"></span></label>
                                                </div> 
                                            </div>

                                            <div class="col-md-6">
                                                <div class="switch">
                                                      <label>Live Item<input type="checkbox" checked=""><span class="lever"></span></label>
                                                </div> 
                                            </div>
                                        </div>
                                        </div>
                                    </div>

                                    <!-- Location Tab-->
                                    <div role="tabpanel" class="tab-pane fade" id="location">
                                           <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                          <select class="form-control"  id="prof-country-list" name="prof_loc_country" onchange="getStates(this.value)">
                                                              <option>Select Country</option>
                                                                @foreach($countries as $country)
                                                                  {{$selected = ""}}
                                                                @if($card->location_country_id == $country['id'])
                                                                  {{$selected = "selected"}}
                                                                @endif
                                                               <option {{$selected}} value="{{$country['id']}}">{{$country['name']}}</option>
                                                                @endforeach
                                                          </select>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <select class="form-control" id="prof-state-list" name="prof_loc_state" onchange="getCities(this.value)" >
                                                                @foreach($stateList as $stateRow)
                                                                    {{$selected = ""}}
                                                                @if($card->location_province_id == $stateRow['id'])
                                                                    {{$selected = "selected"}}
                                                                @endif
                                                                <option {{$selected}} value="{{$stateRow['id']}}">{{$stateRow['name']}}</option>
                                                                @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                              <div class="row">
                                                    <div class="col-md-6">
                                                        <input type="text" name="prof_loc_address" value="{{$card->location_address}}" class="form-control" placeholder="Enter address"> 
                                                    </div>
                                                    <div class="col-md-6">
                                                      <select class="form-control" id="prof-city-list" name="prof_loc_city">
                                                            @foreach($cityList as $cityRow)
                                                                    {{$selected = ""}}
                                                            @if($card->location_city_id == $cityRow['id'])
                                                                {{$selected = "selected"}}
                                                            @endif
                                                            <option {{$selected}} value="{{$cityRow['id']}}">{{$cityRow['name']}}</option>
                                                            @endforeach  
                                                      </select>
                                                    </div>
                                              </div>
                                            </div>
                                    </div>
                                    <!-- Location Tab End -->

                                    <!-- Price Tab-->
                                    <div role="tabpanel" class="tab-pane fade" id="price">
                                        <div class="form-group">
                                              <div class="row">
                                                    <div class="col-md-6">
                                                      <input type="text" class="form-control" name="prof_price" value="{{ !empty($card->price)? $card->price:''}}" placeholder="Price">
                                                    </div>
                                                    
                                                    <div class="col-md-6">
                                                      <input type="text" class="form-control" name="discount" value="{{ !empty($card->discount)? $card->discount:''}}" placeholder="Discount">
                                                    </div>
                                              </div>
                                        </div>
                                    </div>
                                    <!-- Price Tab End-->

                                    <!-- For Tab  -->
                                    <div role="tabpanel" class="tab-pane fade" id="for">
                                        <div class="form-group">
                                            <div class="row">
                                                    <div class="col-md-6">
                                                      <select class="form-control" name="for_gender">
                                                            <option>For Gender</option>
                                                            <option value="Male">Male</option>
                                                            <option value="Female">Female</option>
                                                      </select>
                                                    </div>

                                                    <div class="col-md-6">
                                                      <select class="form-control" name ="for_age">
                                                            <option>For ages</option>
                                                            <option value="8-12">8-12 years </option>
                                                            <option value="12-18">12-18 years </option>
                                                      </select>
                                                    </div>
                                            </div>
                                        </div>
    <!--                        </div>-->
                                  <!-- For Tab End -->
                                    </div>
                                  
                                </div>
                                <div class="col-sm-offset-9">
                                    <a class="btn btn-danger waves-effect " href="{{route("item")}}">CANCEL</a>
                                    <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                                </div>
                                
                            </div>
                        </div>
                        
                        <!----  for personal and free subscription ----->
                        <div class="personal-subscription row clearfix" id="personal-subscription" {{ $personal}} >
                            <div class="col-lg-offset-1 col-md-offset-1 col-sm-offset-1  col-lg-11 col-md-11 col-sm-12 col-xs-12">
                                <!-- Nav tabs -->
                                <ul class=" nav nav-tabs tab-nav-right" role="tablist">
                                    <li role="presentation" class="active form-control-label" ><a href="#time" data-toggle="tab">TIME</a></li>
                                    <li role="presentation" class="form-control-label"><a href="#location-personal" data-toggle="tab">LOCATION</a></li>
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content" style="margin-left:15px">
                                    <div role="tabpanel" class="tab-pane fade in active" id="time">
                                           
                                           <div class="row clearfix">
                                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3 form-control-label">
                                                        <label for="card_date">Card Start Date</label>
                                                </div>

                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-3">
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <input type="text" name ="card_start_date" id="card_start_date" class="form-control" value= "{{$card->start_date}}"placeholder="Select card start date">
                                                            </div>
                                                        </div>
                                                <span class="error">{{$errors->first('card_start_date')}}</span>    
                                                </div>

                                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3 form-control-label">
                                                        <label for="card_start_time">Card Start Time</label>
                                                </div>

                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-3">
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <input type="text" name ="card_start_time" id="card_start_time" class="form-control" value= "{{$card->start_time}}" placeholder="Select card start time">
                                                            </div>
                                                        </div>
                                                <span class="error">{{$errors->first('card_start_time')}}</span>    
                                                </div>
                                            </div>
                                           
                                            <div class="row clearfix">
                                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3 form-control-label">
                                                        <label for="card_date">Card End Date</label>
                                                </div>

                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-3">
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <input type="text" name ="card_end_date" id="card_end_date" class="form-control" value= "{{$card->end_date}}"placeholder="Select card end date">
                                                            </div>
                                                        </div>
                                                <span class="error">{{$errors->first('card_end_date')}}</span>    
                                                </div>

                                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3 form-control-label">
                                                        <label for="card_end_time">Card End Time</label>
                                                </div>

                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-3">
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <input type="text" name ="card_end_time" id="card_end_time" class="form-control" value= "{{$card->end_time}}" placeholder="Select card end time">
                                                            </div>
                                                        </div>
                                                <span class="error">{{$errors->first('card_end_time')}}</span>    
                                                </div>
                                            </div>
                                            
                                            <div class="row clearfix">
                                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3 form-control-label">
                                                        <label for="price">Price</label>
                                                </div>

                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-3">
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <input type="text" name ="price" class="form-control" value= "{{$card->price}}"placeholder="Enter price">
                                                            </div>
                                                        </div>
                                                <span class="error">{{$errors->first('price')}}</span>    
                                                </div>

                                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3 form-control-label">
                                                        <label for="quanitity">Quantity</label>
                                                </div>

                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-3">
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <select  class="form-control" name="quantity">
                                                                    <option value="">Select quantity </option>
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                <span class="error">{{$errors->first('quantity')}}</span>    
                                                </div>
                                                
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="switch">
                                                                {{$checked = ""}}
                                                                @if(!empty($card->two_way_sync))
                                                                  {{$checked = "checked"}}
                                                                @endif
                                                                <label>Sync item 2 way<input type="checkbox" value="{{($checked == 'checked')? 1 : 0}}"  name= "two_way_sync" checked="{{$checked}}"><span class="lever"></span></label>
                                                            </div> 
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="switch">
                                                                {{$checked = ""}}
                                                                @if(!empty($card->share_on_showcase))
                                                                  {{$checked = "checked"}}
                                                                @endif
                                                                <label>Share on showcase<input type="checkbox" name= "share_on_showcase" value="{{($checked == 'checked')? 1 : 0}}" checked="{{$checked}}"><span class="lever"></span></label>
                                                            </div> 
                                                        </div>
                                                    </div>
                                                </div>
                                          </div>
                                    </div>
                                    <!-- Location Tab-->
                                    <div role="tabpanel" class="tab-pane fade" id="location-personal">
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <select class="form-control"  id="country-list" name="location_country" onchange="getStates(this.value)">
                                                            <option>Select Country</option>
                                                            @foreach($countries as $country)
                                                                  {{$selected = ""}}
                                                              @if($card->location_country_id == $country['id'])
                                                                  {{$selected = "selected"}}
                                                                @endif
                                                              <option {{$selected}} value="{{$country['id']}}">{{$country['name']}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <select class="form-control" id="state-list" name="location_province" onchange="getCities(this.value)">
                                                            @foreach($stateList as $stateRow)
                                                                  {{$selected = ""}}
                                                              @if($card->location_province_id == $stateRow['id'])
                                                                  {{$selected = "selected"}}
                                                              @endif
                                                              <option {{$selected}} value="{{$stateRow['id']}}">{{$stateRow['name']}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                              <div class="row">
                                                    <div class="col-md-6">
                                                        <input type="text" name="location_address" value="{{$card->location_address}}" class="form-control" placeholder="Enter address"> 
                                                    </div>
                                                    <div class="col-md-6">
                                                      <select class="form-control" id="city-list" name="location_city">
                                                            <option>Select City</option>
                                                            @foreach($cityList as $cityRow)
                                                                  {{$selected = ""}}
                                                                @if($card->location_city_id == $cityRow['id'])
                                                                    {{$selected = "selected"}}
                                                                @endif
                                                              <option {{$selected}} value="{{$cityRow['id']}}">{{$cityRow['name']}}</option>
                                                            @endforeach
                                                      </select>
                                                    </div>
                                              </div>    
                                            </div>
                                    </div>
                                    <!-- Location Tab End -->
                                </div>
                                <div class="col-sm-offset-9">
                                    <button class="btn btn-primary waves-effect" type="submit">CANCEL</button>
                                    <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                                </div>
                            </div>
                        </div>
                        
                        </div>  
                            
                    </form>
                </div>
            </div>
        </div>    
    </div>
    <script>
    function getStates(val) {
       var host = "{{URL::to('/admin')}}";
     
         $.ajax({
            type: "GET",
            url: host+"/admin/ajax/getStates",
            data: {"country_id": val},
            success: function(data){
                $("#state-list").html(data);
                $("#prof-state-list").html(data);
              
            }
        }); 
    }
    
    /**
     * Function to get cities on the basis of state
     * 
     */
    
    function getCities(val) {
       var host = "{{URL::to('/admin')}}";
     
         $.ajax({
            type: "GET",
            url: host+"/admin/ajax/getCities",
            data: {"state_id": val},
            success: function(data){
                $("#city-list").html(data);
                $("#prof-state-list").html(data);
            }
        }); 
    }
    
    $(function(){
         $('#card_time').combodate();  
    });
    

  </script>    
        
@endsection
