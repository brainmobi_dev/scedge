
@extends('admin.layout.admin')

@section('content')

    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASHBOARD</h2>
            </div>

            <!-- Widgets -->
            <div class="row clearfix">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <a class="undecorated-anchor" href="{{route('user')}}">
                      <div class="info-box bg-orange hover-expand-effect">
                          <div class="icon">
                              <i class="material-icons">person_add</i>
                          </div>
                          <div class="content">
                              <div class="text">Users</div>
                              <div class="number count-to" data-from="0" data-to="1000" data-speed="1000" data-fresh-interval="20">{{$userCount}}</div>
                          </div>
                      </div>
                    </a>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <a class="undecorated-anchor" href="{{route('item')}}">
                      <div class="info-box bg-cyan hover-expand-effect">
                          <div class="icon">
                              <i class="material-icons">event</i>
                          </div>
                          <div class="content">
                              <div class="text">Items</div>
                              <div class="number count-to" data-from="0" data-to="257" data-speed="1000" data-fresh-interval="20">{{$itemCount}}</div>
                          </div>
                      </div>
                    </a>
                </div>
<!--                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-light-green hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">forum</i>
                        </div>
                        <div class="content">
                            <div class="text">NEW COMMENTS</div>
                            <div class="number count-to" data-from="0" data-to="243" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>-->

            </div>


        </div>
    </section>
@endsection
