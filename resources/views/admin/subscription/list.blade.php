@extends('admin.layout.admin')

@section('content')
<div class="card">
        <div class="header">
            <h2>
               SUBSCRIPTION TYPES
            </h2>
        </div>
    
        <div class="body">
            <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                    <div class="col-sm-offset-6 col-sm-6">
                        <div id="DataTables_Table_0_filter" class="dataTables_filter">
                            <form id="user-form"  method="get">
                                <input type='search' name='search_text'>
                                <input type="hidden"  id="sorting_field"  name="sorting_field" value="{{ !empty( $sortData['sorting_field'] )? $sortData['sorting_field'] : '' }}"/>
				<input type="hidden"  id="sorting_order"  name="sorting_order" value="{{ !empty( $sortData['sorting_order'] )? $sortData['sorting_order'] : '' }}"/>	
                                <button class="btn btn-primary" type='submit'>Search</button>
                            </form>                        
                        </div>
                    </div>
                </div>
                <div class="row">
                <div class="col-sm-12">
                    <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
                <thead>
                    <tr role="row">
                     <th class="{{ $getSortingClass('user_subscription.subscription_code') }}"  
                        rowspan="1" 
                        colspan="1" 
                        onclick="setSortingField('user_subscription.subscription_code', '{{ $getSortingOrder('user_subscription.subscription_code') }}' )">Name</th>    
                    
                    <th class="{{$getSortingClass('user_subscription.subscription_name')}}"
                         rowspan="1"
                         colspan="1"
                         onclick="setSortingField('user_subscription.subscription_name', '{{ $getSortingOrder('user_subscription.subscription_name') }}' )">Subscription Name</th>
                    <th class="{{$getSortingClass('user_subscription.subscription_name')}}"
                        colspan="1"
                        rowspan="1" 
                        onclick="setSortingField('user_subscription.subscription_amount', '{{ $getSortingOrder('user_subscription.subscription_amount') }}' )">Subscription Amount</th>
                    <th class="{{$getSortingClass('user_subscription.subscription_amount')}}"
                        rowspan="1"
                        colspan="1"
                        onclick="setSortingField('user_subscription.subscription_type', '{{ $getSortingOrder('user_subscription.subscription_type') }}' )">Subscription Type</th>
                    <th rowspan="1" colspan="1">Status</th>
                    <th rowspan="1" colspan="1">Action</th>
                    </tr>
                </thead>

                <tbody>
                @if(count($subscriptions)>0)
                @foreach ($subscriptions as $subscription)
                <input type="hidden" id="industry_id" name="_token" value="{{ csrf_token() }}">
                <tr role="row" class="odd" id="row_{{ $subscription->id}}">
                        <td>{{$subscription->subscription_code}}</td>
                        <td>{{$subscription->subscription_name}}</td>
                        <td>{{$subscription->subscription_amount}}
                        <td>{{$subscription->subscription_type}}</td>
                        <td>
                            @if($subscription->is_active===1)
                              <a id="user_{{ $subscription->id}}" href="javascript:changeStatus({{ $subscription->id}}, 0);" ><span class="btn btn-danger">Inactive</span></a>
                            @else
                              <a id="user_{{ $subscription->id}}" href="javascript:changeStatus({{ $subscription->id}}, 1);" ><span class="btn btn-success">Active</span></a>
                            @endif
                        </td>
                        
                        <td>
                           <a href="{{URL::to('/').'/admin/subscription/edit?id='.$subscription->id}}" <i class="material-icons">edit</i></a>
                        </td>
                         
                        
                </tr>
                @endforeach
                @else
                    <tr><td colspan="6" style="text-align: center;"><b>No result founds</b></td></tr>
                @endif
                </tbody>
                </table>
               
                </div> 
                </div>       

        </div>
    </div>
   <script>
    /**
     * Apply sorting on field 
     * 
     */    
    
    function setSortingField(field, order) {
        $("#sorting_field").val(field);
        $("#sorting_order").val(order);
        $("#user-form").submit();
    }
    
    function changeStatus(subscription_id, status){
        var host = "{{URL::to('/admin')}}";
        bootbox.confirm("Are you sure you want to change status?",function(result){
            if(result){
            $.ajax({
                url: host+'/admin/subscription/changestatus',
                type: "post",
                data: {"subscription_id": subscription_id, "status":status,'_token':"{{ csrf_token()}}"},
                success: function(response) {
                    
                var newStatus= (status)?0:1;
                if(response.error_code == 200){
                    if(status == "1"){
                        $("#user_"+subscription_id+ " span").removeClass("btn btn-success"); 
                        $("#user_"+subscription_id+ " span").addClass("btn btn-danger"); 
                        $("#user_"+subscription_id+ " span").text("Inactive"); 
                        $("#user_"+subscription_id).attr("href", "javascript:changeStatus("+subscription_id+", "+newStatus+")");
                    } else {
                        $("#user_"+subscription_id+ " span").removeClass("btn btn-danger"); 
                        $("#user_"+subscription_id+ " span").addClass("btn btn-success"); 
                        $("#user_"+subscription_id+ " span").text("Active"); 
                        $("#user_"+subscription_id).attr("href", "javascript:changeStatus("+subscription_id+", "+newStatus+")");
                    }
                }
                }
                });
                
            }
        });
           
    
    }
    </script>

 
@endsection