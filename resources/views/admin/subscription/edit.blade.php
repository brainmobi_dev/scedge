
@extends('admin.layout.admin')

@section('content')
 
           <!-- Basic Validation -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>Edit Subscription Details</h2>
                        </div>
                        <div class="body">
                            <form id="form_validation" action="#" method="POST">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="subscription_ammount">Subscription Amount</label>
                                    </div>
                                    
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" name="subscription_amount" value="{{ $subscription->subscription_amount}}" class="form-control" placeholder="Enter subscription ammount">
                                            </div>
                                        </div>
                                    <span class="error">{{$errors->first('subscription_amount')}}</span>        
                                    </div>
                                </div>    
                                <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Validation -->
@endsection
