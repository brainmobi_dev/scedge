@extends('admin.layout.admin')

@section('content')

<!-- Basic Validation -->
 <div class="row clearfix">
     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <div class="card">
             <div class="header">
                 <h2>Reset Password</h2>
             </div>
            @if (session('message'))
                <p style="text-align: center; color: #ff0000; padding-top: 10px;">{{ session('message') }}</p>
            @endif 
             <div class="body">
                 <form id="form_validation" action="{{route('adminResetPassword')}}" onsubmit="return validateForm()" method="POST">
                     <input type="hidden" name="_token" value="{{ csrf_token() }}">

                     <div class="row clearfix">
                         <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                 <label for="new_password">New Password</label>
                         </div>

                         <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                 <div class="form-group">
                                     <div class="form-line">
                                         <input type="password" id="new_password" name="new_password" onfocus="hideErr();" value="" class="form-control" placeholder="Enter New Password">
                                     </div>
                                 </div>
                         <span class="error"></span>
                         </div>
                     </div>

                     <div class="row clearfix">
                         <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                 <label for="confirm_password">Confirm Password</label>
                         </div>

                         <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                 <div class="form-group">
                                     <div class="form-line">
                                         <input type="password" id="confirm_password" name="confirm_password" onfocus="hideErr();" class="form-control" placeholder="Confirm Password">
                                     </div>
                                 </div>
                             <span id="err" class="error"></span>
                         </div>
                     </div>

                     <div class="col-sm-offset-9">
                         <a class="btn btn-danger waves-effect" href="{{route("adminResetPassword")}}">CANCEL</a>
                         <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                     </div>
                 </form>
             </div>
         </div>
     </div>
 </div>

<!-- #END# Basic Vali-- Slimscroll Plugin Js -->

<script type="text/javascript">
//validate password
function validateForm() {
    var newPassword = $("#new_password").val();
    var confirmPassword = $("#confirm_password").val();
    
    //check
    if(newPassword != confirmPassword) 
    {
        $("#new_password").val("");
        $("#confirm_password").val("");
        $("#err").html("Password did not match.");
        return false;
    }
    //alert('succ');
}

//hide error
function hideErr() {
    $("#err").html("");
}
</script>

@endsection
