
@extends('admin.layout.admin')

@section('content')

<!-- #END# Basic Validation -->
<script src="{{secure_asset("admin-theme/js/jquery-ui.js")}}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#dob").datepicker({
            dateFormat: "dd-M-yy",
            onSelect: function (date) {
                var startDate = $(this).datepicker('getDate');
                var minDate = $(this).datepicker('getDate');
                startDate.setDate(startDate.getDate() + 30);
            }
    });
    });
</script>
           <!-- Basic Validation -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>Edit User Details</h2>
                        </div>
                        <div class="body">
                            <form id="form_validation" action="#" method="POST" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="user_name">User Name</label>
                                    </div>

                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" id="user_name" name="user_name" value="{{ $userData->name}}" class="form-control" placeholder="Enter user name">
                                                </div>
                                            </div>
                                    <span class="error">{{$errors->first('user_name')}}</span>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="email">User Email</label>
                                    </div>

                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" name="email" value="{{  $userData->email}}" class="form-control" placeholder="Enter user email">
                                                </div>
                                            </div>
                                    <span class="error">{{$errors->first('email')}}</span>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="subscription_name">Subscription Type</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <select name="subscription_name" class="col-sm-12 form-control">
                                        @foreach( $subscriptions as $subscription )

                                        @if( $subscription->subscription_name == $userData->subscription_name)
                                                {{ $select = "selected" }}
                                                <option value="{{ $subscription->id }}">{{ $subscription->subscription_name }}({{ $subscription->subscription_type}})</option>
                                        @else
                                                <option value="{{ $subscription->id }}">{{ $subscription->subscription_name }}({{ $subscription->subscription_type}})</option>
                                        @endif

                                        @endforeach
                                        </select>
                                        <span class="error">{{$errors->first('subscription_name')}}</span>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="Gender">Gender</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <select name="gender" class="col-sm-12 form-control">
                                            <option @if($userData->gender == 'Male') selected @endif value="Male">Male</option>
                                            <option  @if($userData->gender == 'Female') selected @endif value="Female">Female</option>
                                        </select>
                                    <span class="error">{{$errors->first('gender')}}</span>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="dob">Date of Birth</label>
                                    </div>

                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <input type="text" name ="dob" id="dob" class="form-control" value= "{{$userData->dob}}" placeholder="Select date of birth">
                                        <span class="error">{{$errors->first('dob')}}</span>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="country">Country</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <select class="form-control"  id="prof-country-list" name="country" onchange="getStates(this.value)">
                                             <option>Select Country</option>
                                               @foreach($countries as $country)
                                                 {{$selected = ""}}
                                               @if($userData->country == $country['id'])
                                                 {{$selected = "selected"}}
                                               @endif
                                              <option {{$selected}} value="{{$country['id']}}">{{$country['name']}}</option>
                                               @endforeach
                                         </select>
                                        <span class="error">{{$errors->first('country')}}</span>
                                    </div>

                                </div>

                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="city">Province</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                         <select class="form-control" id="state-list" name="province" onchange="getCities(this.value)" >
                                            @foreach($stateList as $stateRow)
                                                {{$selected = ""}}
                                            @if($userData->province == $stateRow['id'])
                                                {{$selected = "selected"}}
                                            @endif
                                            <option {{$selected}} value="{{$stateRow['id']}}">{{$stateRow['name']}}</option>
                                            @endforeach
                                        </select>
                                        <span class="error">{{$errors->first('province')}}</span>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="city">City</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <select class="form-control" id="city-list" name="city">
                                            @foreach($cityList as $cityRow)
                                                    {{$selected = ""}}
                                            @if($userData->city == $cityRow['id'])
                                                    {{$selected = "selected"}}
                                            @endif
                                            <option {{$selected}} value="{{$cityRow['id']}}">{{$cityRow['name']}}</option>
                                            @endforeach
                                        </select>

                                    <span class="error">{{$errors->first('city')}}</span>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="company_name">Company Name </label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" name="company_name" value="{{  $userData->company_name}}" class="form-control" placeholder="Enter company name">
                                                </div>
                                        <span class="error">{{$errors->first('company_name')}}</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="website">Website</label>
                                    </div>

                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text"  name="website" value="{{  $userData->website}}" class="form-control" placeholder="Enter website name">
                                                </div>
                                        </div>
                                        <span class="error">{{$errors->first('website')}}</span>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="phone_no">Phone No</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" name="phone_no" value="{{  $userData->phone_number}}" class="form-control" placeholder="Enter user phone no">
                                                </div>
                                        </div>
                                        <span class="error">{{$errors->first('phone_no')}}</span>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="bussiness_hour">Bussiness Hour</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" name="business_hour" value="{{  $userData->business_hour}}" class="form-control" placeholder="Enter business hour">
                                                </div>
                                        </div>
                                        <span class="error">{{$errors->first('business_hour')}}</span>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="profile_image">Profile Image</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-group">
                                            <span class="btn btn-info btn-primary btn-file">
                                                Browse <input type="file" name="profile_image" accept=".jpg,.jpeg,.png, .gif" onchange="uploadImage(this,'profile-image-preview');">
                                            </span>
                                            @if(!empty($userData->profile_image))
                                                <img id="profile-image-preview" src="{{URL::to('/'). '/public/' . $userData->profile_image}}" alt="Profile Image"/>
                                            @else
                                               <img id="profile-image-preview" src="{{URL::to('/'). '/public/' . '/admin-theme/images/upload.png'}}" alt="Card image"/>
                                            @endif
                                            </div>
                                        <span class="error">{{$errors->first('profile_image')}}</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="cover_image">Cover Image</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                        <span class="btn btn-info btn-primary btn-file">
                                            Browse <input type="file" name="cover_image" accept=".jpg,.jpeg,.png, .gif" onchange="uploadImage(this,'cover-image-preview');">
                                        </span>
					                    @if(!empty($userData->cover_image))
                                            <img id="cover-image-preview" src="{{URL::to('/'). $userData->cover_image}}" alt="Cover Image"/>
                                        @else
                                            <img id="cover-image-preview" src="{{URL::to('/').'/admin-theme/images/upload.png'}}" alt="Card cover image"/>
                                        @endif
                                        </div>
                                        <span class="error">{{$errors->first('cover_image')}}</span>
                                    </div>
                                </div>

                                <div class="col-sm-offset-9">
                                    <a class="btn btn-danger waves-effect " href="{{route("user")}}">CANCEL</a>
                                    <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

<!-- #END# Basic Vali-- Slimscroll Plugin Js -->
<script src="{{secure_asset("admin-theme/js/myscript.js")}}"></script>
<script>
    function getStates(val) {
       var host = "{{URL::to('/admin')}}";
        $.ajax({
            type: "GET",
            url: host+"/ajax/getStates",
            data: {"country_id": val},
            success: function(data){
                $("#state-list").html(data);
                @if($userData->province)
                  setTimeout(function(){
                      $("#state-list").val({{$userData->province}})
                  }, 500)
                @endif
            }
        });
    }

    /**
     * Function to get cities on the basis of state
     *
     */

    function getCities(val) {
       var host = "{{URL::to('/admin')}}";

        $.ajax({
            type: "GET",
            url: host+"/ajax/getCities",
            data: {"state_id": val},
            success: function(data){
                $("#city-list").html(data);

                @if($userData->city)
                    setTimeout(function(){
                        $("#city-list").val({{$userData->city}})
                    }, 500)
                @endif
            }
        });
    }

    @if($userData->country)
        getStates({{$userData->country}})
    @endif

    @if($userData->province)
        getCities({{$userData->province}})
    @endif




  </script>
@endsection
