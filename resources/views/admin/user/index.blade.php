@extends('admin.layout.admin')

@section('content')
<div class="card">
        <div class="header">
            <h2>
               USER LIST
            </h2>
        </div>

        <div class="body">
            <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                    <div class="col-sm-offset-6 col-sm-6">
                        <div id="DataTables_Table_0_filter" class="dataTables_filter">
                            <form id="user-form"  method="get">
                                <input type='search' name='search_text'>
                                <input type="hidden"  id="sorting_field"  name="sorting_field" value="{{ !empty( $sortData['sorting_field'] )? $sortData['sorting_field'] : '' }}"/>
				<input type="hidden"  id="sorting_order"  name="sorting_order" value="{{ !empty( $sortData['sorting_order'] )? $sortData['sorting_order'] : '' }}"/>
                                <button class="btn btn-primary" type='submit'>Search</button>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="row">
                <div class="col-sm-12" id="content">
                <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
                <thead>
                    <tr role="row">
                    <th class="{{ $getSortingClass('users.name') }}"
                        rowspan="1"
                        colspan="1"
                        onclick="setSortingField('users.name', '{{ $getSortingOrder('users.name') }}' )">Name</th>

                    <th class="{{ $getSortingClass('users.email') }}"
                        rowspan="1"
                        colspan="1"
                        onclick="setSortingField('users.email', '{{ $getSortingOrder('users.email') }}' )">Email</th>

                    <th aria-controls="DataTables_Table_0" rowspan="1" colspan="1" >No of Items</th>
                    <th aria-controls="DataTables_Table_0" rowspan="1" colspan="1" >Child Card Items</th>
                    <th aria-controls="DataTables_Table_0" rowspan="1" colspan="1" >Subscription Name</th>
                     <th class="{{ $getSortingClass('users.email') }}"
                        rowspan="1"
                        colspan="1"
                        onclick="setSortingField('users.email', '{{ $getSortingOrder('users.created_at') }}' )">Created At</th>
                    <th aria-controls="DataTables_Table_0" rowspan="1" colspan="1" >Status</th>
                    <th aria-controls="DataTables_Table_0" rowspan="1" colspan="1" >Action</th>
                    </tr>
                </thead>

                <tbody>
              @if(count($allUsers)>0)
              @foreach ($allUsers as $user)
                <input type="hidden" id="industry_id" name="_token" value="{{ csrf_token() }}">
                <tr role="row" class="odd" id="row_{{ $user->id}}">
                       <td>{{$user->name}}</td>
                       <td>{{$user->email}}</td>
                       <td>{{$user->user_cards}}
                       <td>{{$user->user_child_cards}}
                       <td>{{$user->subscription_name}}</td>
                       <td>{{$user->created_at}}</td>
                       <td>
                            @if($user->is_active===1)
                              <a id="user_{{ $user->id}}" href="javascript:changeStatus({{ $user->id}}, 0);" ><span class="btn btn-danger">Deactivate User</span></a>
                            @else
                              <a id="user_{{ $user->id}}" href="javascript:changeStatus({{ $user->id}}, 1);" ><span class="btn btn-success">Activate User</span></a>
                            @endif
                       </td>
                       <td>
                           <a href="{{URL::to('/').'/admin/user/edit?user_id='.$user->id}}" <i class="material-icons">edit</i></a>
                           <a href="#" onclick="deleteUser({{$user->id}})"><i class="material-icons">delete</i>
                           </a>
                     <!--    <a href="#" onclick="deleteUser({{$user->id}})"><i class="material-icons">person</i>
                           </a> -->

                       </td>

                </tr>
                @endforeach
                @else
                    <tr><td colspan="8" style="text-align: center;"><b>No result founds</b></td></tr>
                @endif
                </tbody>
                </table>
                  <div class="row pull-right">
                    {{$allUsers->links()}}
                  </div>
                </div>
                </div>

        </div>
    </div>

   <script>
    /**
     * Apply sorting on field
     *
     */

//    function setSortingField(field, order) {
//        $("#sorting_field").val(field);
//        $("#sorting_order").val(order);
//        $("#user-form").submit();
//    }
    $('#content table thead a').live('click', function(e)
    {
        e.preventDefault();
        $.ajax(
        {
            url : $(this).attr('href'),
            success : function(resp)
            {
                $('#content').html($(resp).html());
            },
            error : function()
            {
                alert('There was a problem sorting your table...');
            }
        });
    });
    /**
     * Function to delete user
     *
     */
    function deleteUser(user_id) {
        var host = "{{URL::to('/admin')}}";
            bootbox.confirm("Are you sure you want to delete this data?", function(result) {
            if (result) {
                $.ajax({
                    url: host+'/user/delete',
                    type: "post",
                    data: {"user_id": user_id,'_token':"{{ csrf_token()}}" },
                    success: function(response) {
                        $("#row_" + user_id).remove();
                    }
                });
            }
        });
    }
    /**
     * Function to change user status
     *
     */

    function changeStatus(user_id, status){
        var host = "{{URL::to('/admin')}}";
        bootbox.confirm("Are you sure you want to change status?", function(result) {

        if (result) {
            $.ajax({
                url: host+'/user/changestatus',
                type: "post",
                data: {"user_id": user_id, "status":status,'_token':"{{ csrf_token()}}"},
                success: function(response) {

                var newStatus = (status)?0:1;

                if(response.error_code == 200){
                    if(status == "1"){
                        $("#user_"+user_id+ " span").removeClass("btn btn-success");
                        $("#user_"+user_id+ " span").addClass("btn btn-danger");
                        $("#user_"+user_id+ " span").text("Deactivate User");
                        $("#user_"+user_id).attr("href", "javascript:changeStatus("+user_id+", "+newStatus+")");
                    } else {
                        $("#user_"+user_id+ " span").removeClass("btn btn-danger");
                        $("#user_"+user_id+ " span").addClass("btn btn-success");
                        $("#user_"+user_id+ " span").text("Activate User");
                        $("#user_"+user_id).attr("href", "javascript:changeStatus("+user_id+", "+newStatus+")");
                    }
                }
                }
                });
            }
        });

    }


    </script>
<!--    <s

@endsection
