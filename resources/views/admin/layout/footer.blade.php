    <!-- Jquery Core Js -->
    <script src="{{secure_asset("admin-theme/plugins/jquery/jquery.min.js")}}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{secure_asset("admin-theme/plugins/bootstrap/js/bootstrap.js")}}"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="{{secure_asset("admin-theme/plugins/jquery-slimscroll/jquery.slimscroll.js")}}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{secure_asset("admin-theme/plugins/node-waves/waves.js")}}"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="{{secure_asset("admin-theme/plugins/jquery-countto/jquery.countTo.js")}}"></script>

    <!-- Morris Plugin Js -->
    <script src="{{secure_asset("admin-theme/plugins/raphael/raphael.min.js")}}"></script>
    <script src="{{secure_asset("admin-theme/plugins/morrisjs/morris.js")}}"></script>

    <!-- Custom Js -->
    <script src="{{secure_asset("admin-theme/js/admin.js")}}"></script>

    <!-- Bootbox js -->
    <script src="{{secure_asset("admin-theme/js/bootbox.js")}}"></script>




</body>
