<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Scedge</title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="{{secure_asset("admin-theme/plugins/bootstrap/css/bootstrap.css")}}" rel="stylesheet">

    <link href="{{secure_asset("admin-theme/css/jquery-ui.css")}}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{secure_asset("admin-theme/plugins/node-waves/waves.css")}}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{secure_asset("admin-theme/plugins/animate-css/animate.css")}}" rel="stylesheet" />

    <!-- Morris Chart Css-->
    <link href="{{secure_asset("admin-theme/plugins/morrisjs/morris.css")}}" rel="stylesheet" />

    <!-- Bootstrap core css-->
    <link href="{{secure_asset("admin-theme/css/style.css")}}" rel="stylesheet">

    <!-- Custom css-->
    <link href="{{secure_asset("admin-theme/css/custom.css")}}" rel="stylesheet">

    <!-- select box css-->
    <link href="{{secure_asset("admin-theme/css/themes/all-themes.css")}}" rel="stylesheet" />

     <!-- JQuery DataTable Css -->
    <link href="{{secure_asset("admin-theme/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css")}}" rel="stylesheet">


   <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="{{secure_asset("admin-theme/plugins/bootstrap-select/css/bootstrap-select.css" )}}" />

</head>
<body class="theme-blue">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="{{route('admin-home')}}">SCEDGE-ADMIN</a>

            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
<!--                <ul class="nav navbar-nav navbar-right">
                     Call Search
                    <li><a href="javascript:void(0);" class="js-search" data-close="true"><i class="material-icons">search</i></a></li>
                     #END# Call Search


                </ul>-->
              <div class="info-container pull-right">

                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
<!--                    <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>-->
                            <li><a href="{{ route('admin.logout') }}"
                                onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ route('admin.logout.submit') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                            </li>
                        </ul>
                    </div>
               </div>
            </div>
        </div>
    </nav>
