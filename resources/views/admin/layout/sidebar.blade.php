
<section>
    <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="{{Customhelper::setActive('admin/home', $request)}}">
                        <a href="{{route('admin-home')}}">
                            <i class="material-icons">home</i>
                            <span>Dashboard</span>
                        </a>
                    </li>

                    <li class="{{($request->is('admin/user')||$request->is('user/edit'))?'active' : ''}}">
                        <a href="{{route('user')}}">
                            <i class="material-icons">person</i>
                            <span>User Management</span>
                        </a>
                    </li>

                    <li class="{{($request->is('admin/item')||$request->is('admin/itemtype')||$request->is('admin/childcard'))?'active' : ''}}">
                        <a href="javascript:void(0);" class="menu-toggle {{($request->is('admin/item')||$request->is('admin/itemtype'))? 'active' : ''}}">
                            <i class="material-icons">event</i>
                            <span>Item management</span>
                        </a>

                        <ul class="ml-menu">
                            <li class="{{($request->is('admin/item')||$request->is('admin/item/view'))?'active' : ''}}">
                                <a href="{{route('item')}}">
                                    <span>Item list</span>
                                </a>
                            </li>

                            <li class="{{($request->is('admin/itemtype')||$request->is('itemtype/edit'))?'active' : ''}}">
                                <a href="{{route('itemTypeList')}}">
                                    <span>Item Type list</span>
                                </a>
                            </li>

                            <li class="{{$request->is('admin/childcard') ? 'active' : ''}}">
                                <a href="{{route('childcard')}}">
                                    <span>Child Item</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                  <!--  <li class="{{($request->is('admin/content')||$request->is('content/edit'))?'active' : ''}}">
                        <a href="{{route('contentList')}}">
                            <i class="material-icons">event</i>
                            <span>Content Management</span>
                        </a>
                    </li> -->

                    <li class="{{Customhelper::setActive('admin/subscription', $request)}}">
                        <a href="{{route('subscriptionList')}}">
                            <i class="material-icons">subscriptions</i>
                            <span>Subscription Management</span>
                        </a>
                    </li>

<!--                    <li class="{{Customhelper::setActive('admin/contact', $request)}}">
                        <a href="{{route('adminContactlists')}}">
                            <i class="material-icons">contacts</i>
                            <span>Contact Management</span>
                        </a>
                    </li>-->
                    
                    <li class="{{($request->is('admin/contact')||$request->is('admin/contact/groupMembersList'))?'active' : ''}}">
                        <a href="javascript:void(0);" class="menu-toggle {{($request->is('admin/contact')||$request->is('admin/contact/groupMembersList'))? 'active' : ''}}">
                            <i class="material-icons">contacts</i>
                            <span>Contact management</span>
                        </a>

                        <ul class="ml-menu">
                            <li class="{{($request->is('admin/contact')||$request->is('admin/contact/groupMembersList'))?'active' : ''}}">
                                <a href="{{route('adminContactlists')}}">
                                    <span>Contact Groups</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    
                    
                    <li class="{{($request->is('admin/abuse/cardAbuse') || $request->is('admin/abuse/userAbuse') || $request->is('admin/abuse/abuseContent')) ? 'active' : ''}}">
                        <a href="javascript:void(0);" class="menu-toggle {{($request->is('admin/abuse/cardAbuse') || $request->is('admin/abuse/userAbuse') || $request->is('admin/abuse/abuseContent'))?'active' : ''}}">
                            <i class="material-icons">report</i>
                            <span>Abuse management</span>
                        </a>
                        <ul class="ml-menu">
                            <li class="{{($request->is('admin/abuse/cardAbuse')) ? 'active' : ''}}">
                                <a href="{{route('cardAbuse')}}">
                                    <span>Card Abuse</span>
                                </a>
                            </li>
                            <li class="{{($request->is('admin/abuse/userAbuse')) ? 'active' : ''}}">
                                <a href="{{route('userAbuse')}}">
                                    <span>User Abuse</span>
                                </a>
                            </li>
                            <li class="{{($request->is('admin/abuse/abuseContent')) ? 'active' : ''}}">
                                <a href="{{route('abuseContent')}}">
                                    <span>Abuse Content</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    
                    <li class="{{($request->is('admin/abuse/blockedUserLists')) ? 'active' : ''}}">
                        <a href="{{route('blockedUserLists')}}">
                            <i class="material-icons">block</i>
                            <span>Blocked Users</span>
                        </a>
                    </li>
                    
                    <li class="{{($request->is('admin/user/resetPassword')) ? 'active' : ''}}">
                        <a href="{{route('adminResetPassword')}}">
                            <i class="material-icons">devices</i>
                            <span>Reset Password</span>
                        </a>
                    </li>

                  <!--  <li class="{{Customhelper::setActive('admin/chat', $request)}}">
                        <a href="{{route('chatList')}}">
                            <i class="material-icons">message</i>
                            <span>Chat History</span>
                        </a>
                    </li>

                    <li >
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">payment</i>
                            <span>Payment History</span>
                        </a>
                    </li>

                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">warning</i>
                            <span>Analysis and Report</span>
                        </a>
                    </li> -->
                  
                    <li>
                        <a href="{{ route('admin.logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <i class="material-icons">exit_to_app</i>
                            <span>Logout</span>
                        </a>
                    </li>

                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2017 <a href="javascript:void(0);">Admin-Scedge</a>.
                </div>
                <div class="version">
                    <b>Version: </b> 1.0.4
                </div>
            </div>
            <!-- #Footer -->
        </aside>

        <!-- #END# Left Sidebar -->
