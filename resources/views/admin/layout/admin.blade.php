    <!-- Header -->
    @include('admin.layout.header')
    <!-- Sidebar -->
    @include('admin.layout.sidebar')
    <!-- Footer -->
    @include('admin.layout.footer')
   
     <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Main content -->
        <section class="content">
            <!-- Your Page Content Here -->
             @yield('content')
        </section><!-- /.content -->
    
    </div><!-- /.content-wrapper -->
