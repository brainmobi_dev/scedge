@extends('admin.layout.admin')

@section('content')
<div class="card">
        <div class="header">
            <h2>
               CARD ABUSE
            </h2>
        </div>

        <div class="body">
           <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                    <div class="col-sm-offset-6 col-sm-6">
                        <div id="DataTables_Table_0_filter" class="dataTables_filter">
                            <form id="user-form"  method="get">
                                <input type='search' name='search_text'>
                                <input type="hidden"  id="sorting_field"  name="sorting_field" value="{{ !empty( $sortData['sorting_field'] )? $sortData['sorting_field'] : '' }}"/>
				<input type="hidden"  id="sorting_order"  name="sorting_order" value="{{ !empty( $sortData['sorting_order'] )? $sortData['sorting_order'] : '' }}"/>
                                <button class="btn btn-primary" type='submit'>Search</button>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="row">
                <div class="col-sm-12">
                @if($errors->any())
                    <p>{{$errors->first()}}</p>
                @endif
                <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
                <thead>
                    <tr role="row">
                    <th class="{{ $getSortingClass('users.name') }}"
                        rowspan="1"
                        colspan="1"
                        onclick="setSortingField('users.name', '{{ $getSortinOrder('users.name') }}' )">Reported By</th>
                    <th class="{{ $getSortingClass('user_cards.title') }}"
                        rowspan="1" colspan="1" 
                        onclick="setSortingField('user_cards.title', '{{ $getSortinOrder('user_cards.title') }}' )">Item</th>
                    <th class="{{ $getSortingClass('reports.report_description') }}"
                        rowspan="1" colspan="1" 
                        onclick="setSortingField('reports.report_description', '{{ $getSortinOrder('reports.report_description') }}' )">Report Description</th>
                    <th rowspan="1" colspan="1" >Action</th>
                    </tr>
                </thead>

                <tbody>
                @if(count($abuseCardLists) > 0)
                    @foreach ($abuseCardLists as $abuseCardList)
                    <input type="hidden" id="item_id" name="_token" value="{{ csrf_token() }}">
                    <tr role="row" class="odd" id="row_{{ $abuseCardList->id}}">

                            <td>{{ucfirst($abuseCardList->name)}}
                            <td>{{$abuseCardList->title}}</td>
                            <td>
                                {{$abuseCardList->report_description}}
                            </td>
                            <td>
<!--                                <a href="{{URL::to('/').'/admin/abuse/blockUser?block_user_id='.$abuseCardList->user_id}}"><i class="material-icons">block</i></a>-->
                                <a href="javascript:void(0);" onclick="deleteItem({{$abuseCardList->id}});"><i class="material-icons ">delete</i></a>
                            </td>

                    </tr>
                    @endforeach
                @else
                    <tr><td colspan="4" style="text-align: center;"><b>No result founds</b></td></tr>
                @endif

                </tbody>
                </table>
                  <div class="row pull-right">
                 {{$abuseCardLists->links()}}
                  </div>
                </div>
                </div>

        </div>
    </div>
    <script>
    /**
     * Apply sorting on field
     *
     */

    function setSortingField(field, order) {
        $("#sorting_field").val(field);
        $("#sorting_order").val(order);
        $("#user-form").submit();
    }
    /**
     * Delete item
     *
     */
    function deleteItem(id) {
        var host = "{{URL::to('/admin')}}";
            bootbox.confirm("Are you sure you want to delete this data?", function(result) {
            if (result) {
                    $.ajax({
                        url: host+'/abuse/deleteReport',
                        type: "post",
                        data: {"id": id,'_token':"{{ csrf_token()}}" },
                        success: function(response) {
                            $("#row_" + id).remove();
                        }
                    });
            }
        });
    }

    </script>
@endsection
