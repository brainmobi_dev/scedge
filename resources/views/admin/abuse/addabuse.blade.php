@extends('admin.layout.admin')

@section('content')
    
<!-- Basic Validation -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Add Abuse Content</h2>
                </div>
                <div class="body">
                    @if($errors->any())
                        <p class="succ-msg">{{$errors->first()}}</p>
                    @endif
                    <form id="form_validation" action="{{route("abuseContentAdd")}}" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="row clearfix">
                            <div class="col-lg-1 col-md-1 col-sm-4 col-xs-5 form-control-label">
                                <label for="card_title">Description</label>
                            </div>

                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="abuse_desc" name="abuse_desc" placeholder="Enter Description" class="form-control" required="">
                                    </div>
                                </div>
                            </div>
                        </div>    

                        <div class="col-sm-offset-9">
                            <a class="btn btn-danger waves-effect " href="{{route("abuseContent")}}">CANCEL</a>
                            <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                        </div>
                      
                        </div>  
                            
                    </form>
                </div>
            </div>
        </div>    
    </div>
        
@endsection
