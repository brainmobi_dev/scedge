@extends('admin.layout.admin')

@section('content')
<div class="card">
        <div class="header">
            <h2>
               ABUSE CONTENT
            </h2>
        </div>

        <div class="body">
           <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                    <div class="col-sm-6">
                        <form id="user-form"  method="get">
                            <input type='search' name='search_text'>
                            <input type="hidden"  id="sorting_field"  name="sorting_field" value="{{ !empty( $sortData['sorting_field'] )? $sortData['sorting_field'] : '' }}"/>
                            <input type="hidden"  id="sorting_order"  name="sorting_order" value="{{ !empty( $sortData['sorting_order'] )? $sortData['sorting_order'] : '' }}"/>
                            <button class="btn btn-primary" type='submit'>Search</button>
                        </form>
                    </div>

                    <div class="col-sm-6">
                        <div class="dataTables_length" id="DataTables_Table_0_length">
                        <a class="btn btn-success pull-right" href="{{route('abuseContentInsert')}}">ADD CONTENT</a>
                        </div>
                    </div>
                </div>

                <div class="row">
                <div class="col-sm-12">
                @if($errors->any())
                    <p>{{$errors->first()}}</p>
                @endif
                <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
                <thead>
                    <tr role="row">
                    <th class="{{ $getSortingClass('report_description') }}"
                        rowspan="1"
                        colspan="1"
                        onclick="setSortingField('report_description', '{{ $getSortinOrder('report_description') }}' )">Description</th>
                    <th rowspan="1" colspan="1" >Action</th>
                    </tr>
                </thead>

                <tbody>
                @if(count($abuseContents) > 0)
                    @foreach ($abuseContents as $abuseContent)
                    <input type="hidden" id="item_id" name="_token" value="{{ csrf_token() }}">
                    <tr role="row" class="odd" id="row_{{ $abuseContent->id}}">

                        <td>{{ucfirst($abuseContent->report_description)}}</td>
                        <td>
                            <a href="{{URL::to('/').'/admin/abuse/abuseContentEdit?id='.$abuseContent->id}}"><i class="material-icons">edit</i></a>
                            <a href="javascript:void(0);" onclick="deleteAbuseContent({{$abuseContent->id}});"><i class="material-icons ">delete</i></a>
                        </td>

                    </tr>
                    @endforeach
                @else
                    <tr><td colspan="2" style="text-align: center;">Data not found.</td></tr>
                @endif
                </tbody>
                </table>
                  <div class="row pull-right">
                 {{$abuseContents->links()}}
                  </div>
                </div>
                </div>

        </div>
    </div>
    <script>
    /**
     * Apply sorting on field
     *
     */

    function setSortingField(field, order) {
        $("#sorting_field").val(field);
        $("#sorting_order").val(order);
        $("#user-form").submit();
    }
    /**
     * Delete item
     *
     */
    function deleteAbuseContent(id) {
        var host = "{{URL::to('/admin')}}";
            bootbox.confirm("Are you sure you want to delete this data?", function(result) {
            if (result) {
                    $.ajax({
                        url: host+'/abuse/abuseContentDelete',
                        type: "post",
                        data: {"id": id,'_token':"{{ csrf_token()}}" },
                        success: function(response) {
                            $("#row_" + id).remove();
                        }
                    });
            }
        });
    }

    </script>
@endsection
