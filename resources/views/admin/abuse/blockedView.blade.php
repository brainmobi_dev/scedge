@extends('admin.layout.admin')

@section('content')
<div class="card">
        <div class="header">
            <h2>
               BLOCKED USERS
            </h2>
        </div>

        <div class="body">
           <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                    <div class="col-sm-offset-6 col-sm-6">
                        <div id="DataTables_Table_0_filter" class="dataTables_filter">
                            <form id="user-form"  method="get">
                                <input type='search' name='search_text'>
                                <input type="hidden"  id="sorting_field"  name="sorting_field" value="{{ !empty( $sortData['sorting_field'] )? $sortData['sorting_field'] : '' }}"/>
				<input type="hidden"  id="sorting_order"  name="sorting_order" value="{{ !empty( $sortData['sorting_order'] )? $sortData['sorting_order'] : '' }}"/>
                                <button class="btn btn-primary" type='submit'>Search</button>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="row">
                <div class="col-sm-12">
                @if($errors->any())
                    <p>{{$errors->first()}}</p>
                @endif
                <p class="message" style="color: #ff0000;"></p>
                <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
                <thead>
                    <tr role="row">
                    <th class=""
                        rowspan="1"
                        colspan="1">Blocked</th>
                    <th rowspan="1" colspan="1" >Blocked By</th>
                    <th rowspan="1" colspan="1" >Action</th>
                    </tr>
                </thead>

                <tbody>
                @if(count($blockedContactLists) > 0)
                    @foreach ($blockedContactLists as $blockedContactList)
                    <input type="hidden" id="item_id" name="_token" value="{{ csrf_token() }}">
                    <tr role="row" class="odd" id="row_{{ $blockedContactList->user_id}}">

                        <td>{{ucfirst($blockedContactList->blockUserName)}}
                        <td>{{$blockedContactList->blockedByUserName}}</td>
                        <td>
                            <a href="javascript:void(0);" onclick="unblockRecord({{$blockedContactList->user_id}});"><i class="material-icons">block</i></a>
                            <a href="javascript:void(0);" onclick="deleteRecord({{$blockedContactList->user_id}});"><i class="material-icons ">delete</i></a>
                        </td>

                    </tr>
                    @endforeach
                @else
                <tr><td colspan="3" style="text-align: center;"><b>No result founds</b></td></tr>
                @endif

                </tbody>
                </table>
                  <div class="row pull-right">
                  </div>
                </div>
                </div>

        </div>
    </div>
    <script>
    /**
     * Apply sorting on field
     *
     */

    function setSortingField(field, order) {
        $("#sorting_field").val(field);
        $("#sorting_order").val(order);
        $("#user-form").submit();
    }
    /**
     * Delete item
     *
     */
    function deleteRecord(user_id) {
        var host = "{{URL::to('/admin')}}";
            bootbox.confirm("Are you sure you want to delete all records of user?", function(result) {
            if (result) {
                    $.ajax({
                        url: host+'/abuse/deleteRecord',
                        type: "post",
                         data: {"id": user_id,'_token':"{{ csrf_token()}}" },
                        success: function(response) {
                            if(response.error_code == 200) {
                                $("#row_" + user_id).remove();
                                $(".message").html(response.msg_string);
                            }
                            setTimeout(function(){
                                $(".message").html("");
                            }, 3000);
                        }
                    });
            }
        });
    }
    
    /**
     * Delete item
     *
     */
    function unblockRecord(id) {
        var host = "{{URL::to('/admin')}}";
            bootbox.confirm("Are you sure you want to unblock user?", function(result) {
            if (result) {
                    $.ajax({
                        url: host+'/abuse/unblockUser',
                        type: "post",
                         data: {"id": id,'_token':"{{ csrf_token()}}" },
                        success: function(response) {
                            if(response.error_code == 200) {
                                $("#row_" + id).remove();
                                $(".message").html(response.msg_string);
                            }
                            setTimeout(function(){
                                $(".message").html("");
                            }, 3000);
                        }
                    });
            }
        });
    }
    
    </script>
@endsection
