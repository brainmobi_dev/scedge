@extends('admin.layout.admin')

@section('content')
<link href="{{secure_asset("admin-theme/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css")}}" rel="stylesheet">
<div class="card">
        <div class="header">
            <h2>
               User LIST
            </h2>
        </div>
    
        <div class="body">
            <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                    <div class="col-sm-offset-6 col-sm-6">
                        <div id="DataTables_Table_0_filter" class="dataTables_filter">
                            <form id="user-form"  method="get">
                                <input type='search' name='search_text'>
                                <input type="hidden"  id="sorting_field"  name="sorting_field" value="{{ !empty( $sortData['sorting_field'] )? $sortData['sorting_field'] : '' }}"/>
				<input type="hidden"  id="sorting_order"  name="sorting_order" value="{{ !empty( $sortData['sorting_order'] )? $sortData['sorting_order'] : '' }}"/>	
                                <button class="btn btn-primary" type='submit'>Search</button>
                            </form>   
                        </div>
                    </div>
                </div>
                
                <div class="row">
                <div class="col-sm-12">
                <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
                
                <thead>
                    <tr role="row">
                     <th class="{{ $getSortingClass('users.name') }}"  
                        rowspan="1" 
                        colspan="1" 
                        onclick="setSortingField('users.name', '{{ $getSortingOrder('users.name') }}' )">Sender Name</th>
                    
                    <th class="{{ $getSortingClass('users.name') }}"  
                        rowspan="1" 
                        colspan="1" 
                        onclick="setSortingField('users.name', '{{ $getSortingOrder('users.name') }}' )">Receiver Name</th>
                    
                    <th rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 134px;">Chat history</th>
                    </tr>
                </thead>

                <tbody>
                @if(count($chatData)>0)
                    @foreach ($chatData as $chatRow)
                    <input type="hidden" id="industry_id" name="_token" value="{{ csrf_token() }}">
                    <tr role="row" class="odd" id="row_{{ $chatRow->id}}">
                           <td>{{$chatRow->sender_name}}</td>
                           <td>{{$chatRow->receiver_name}}</td>
                           <td><i class="material-icons icon-blue">message</i></td>
                    </tr>
                    @endforeach
                @else
                    <tr><td colspan="3" style="text-align: center;"><b>No result founds</b></td></tr>
                @endif
                </tbody>
                </table>
                  <div class="row pull-right">      
                      {{$chatData->links()}}
                  </div>
                </div> 
                </div>       

        </div>
    </div>
   
@endsection
<script>
    /**
     * Apply sorting on field 
     * 
     */    
    function setSortingField(field, order) {
        $("#sorting_field").val(field);
        $("#sorting_order").val(order);
        $("#user-form").submit();
    }
    
</script>