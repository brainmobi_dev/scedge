@extends('admin.layout.admin')

@section('content')
    
<link href="{{secure_asset("admin-theme/bootstrap-material-datetimepicker-gh-pages/css/bootstrap-material-datetimepicker.css")}}" rel="stylesheet">
<!-- #END# Basic Validation -->
<script src="{{secure_asset("admin-theme/js/jquery-ui.js")}}"></script>
<!--<script src="{{secure_asset("admin-theme/js/moment.min.js")}}"></script>
<link href="{{secure_asset("admin-theme/css/bootstrap-datetimepicker.min.css")}}" rel="stylesheet"/>-->
<!--<script src="{{secure_asset("admin-theme/js/combodate.js")}}"></script>-->
<script src="{{secure_asset("admin-theme/plugins/momentjs/moment.js")}}"></script>
<script src="{{secure_asset("admin-theme/bootstrap-material-datetimepicker-gh-pages/js/bootstrap-material-datetimepicker.js")}}"></script>
<script type="text/javascript">
    $(document).ready(function () {
    $('#card_time').datetimepicker({
        console.log('scsd');
        //defaultDate: new Date(),
        //format: 'hh:mm:ss A',
        //sideBySide: true
    });
    });
    $(document).ready(function () {
        $("#card_date").datepicker({
            dateFormat: "yy-M-dd",
            minDate: 0,
            onSelect: function (date) {
                var startDate = $(this).datepicker('getDate');
                var minDate = $(this).datepicker('getDate');
                startDate.setDate(startDate.getDate() + 30);
            }
    });
        
    $("#card_start_date").datepicker({
            dateFormat: "dd-M-yy",
            minDate: 0,
            onSelect: function (date) {
                var endDate = $('#card_end_date');
                var startDate = $(this).datepicker('getDate');
                var minDate = $(this).datepicker('getDate');
                endDate.datepicker('setDate', minDate);
                startDate.setDate(startDate.getDate() + 30);
                //sets dt2 maxDate to the last day of 30 days window
                endDate.datepicker('option', 'maxDate', startDate);
                endDate.datepicker('option', 'minDate', minDate);
                $(this).datepicker('option', 'minDate', minDate);
            }
        });
    $('#card_end_date').datepicker({
        dateFormat: "dd-M-yy",
    });

    });
    
    </script>

<!-- Basic Validation -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Edit Item Details</h2>
                </div>
                <div class="body">
                    @if($errors->any())
                        <h4>{{$errors->first()}}</h4>
                    @endif
                    <form id="form_validation" action="{{route("itemUpdate")}}" method="POST" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="item_id" value="{{ $card->id }}">
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="card_title">Item Title</label>
                            </div>

                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="card_title" name="title" value="{{ !empty($card->title)? $card->title:''}}" class="form-control" placeholder="Enter card title">
                                        </div>
                                    </div>
                            <span class="error">{{$errors->first('title')}}</span>        
                            </div>
                        </div>    

                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                    <label for="card_body">Item Body</label>
                            </div>

                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="card_body" name="body" value="{{ !empty($card->body)? $card->body:''}}" class="form-control" placeholder="Enter card body">
                                        </div>
                                    </div>
                            <span class="error">{{$errors->first('body')}}</span>        
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3 form-control-label">
                                    <label for="card_date">Card Date</label>
                            </div>
                            <?php
                            if(!empty($card->card_date)) {
                                $card_date_format = strtr(trim($card->card_date),"-","/");
                                $card_date = date("d/m/Y", strtotime($card_date_format));
                                //time
                                $card_time = $card->card_time;
                                //card date time
                                $card->card_date_time = $card_date.' - '.$card_time;
                            }
                            ?>

                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-3">
                                <div class="form-group label-floating is-empty">
                                    <div class="form-line">
                                        <input type="text" id="date-format" name ="card_date_time" value="{{ !empty($card->card_date_time) ? $card->card_date_time:''}}" class="form-control" data-dtp="dtp_8A1LI" >
                                    </div>
                                </div>
<!--                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name ="card_date" id="card_date" class="form-control" value= "{{$card->card_date}}" placeholder="Select card date">
                                    </div>
                                </div>-->
                                <span class="error">{{$errors->first('card_date')}}</span>
                            </div>

<!--                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3 form-control-label">
                                    <label for="card_time">Card Time</label>
                            </div>

                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-3">
                                    <div class="form-group">
                                        <div class="form-line">
                                           <input type="text" id="card_time" name="card_time" data-format="HH : mm : ss" data-template="HH : mm : ss " name="datetime" value="{{$card->card_time}}">
                                        </div>
                                    </div>
                            <span class="error">{{$errors->first('card_time')}}</span>    
                            </div>-->
                        </div>
                        
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3 form-control-label">
                                    <label for="priority">Priority</label>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-3">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="priority" name="priority" value="{{ !empty($card->priority)? $card->priority:''}}" class="form-control" placeholder="Enter card title">
                                    </div>
                                </div>
                                <span class="error">{{$errors->first('priority')}}</span>        
                            </div>
                             
                             <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3 form-control-label">
                                    <label for="priority">Card Status</label>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-3">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="priority" name="card_status" value="{{ !empty($card->card_status)? $card->card_status:''}}" class="form-control" placeholder="Enter card status">
                                    </div>
                                </div>
                                <span class="error">{{$errors->first('card_status')}}</span>        
                            </div>
                        </div> 
                        
                        <!----  for personal and free subscription ----->
                        <div class="personal-subscription row clearfix" id="personal-subscription">
                            <div class="col-lg-offset-1 col-md-offset-1 col-sm-offset-1  col-lg-11 col-md-11 col-sm-12 col-xs-12">
                                <!-- Nav tabs -->
                                <ul class=" nav nav-tabs tab-nav-right" role="tablist">
                                    <li role="presentation" class="active form-control-label" ><a href="#time" data-toggle="tab">TIME</a></li>
                                    <li role="presentation" class="form-control-label"><a href="#location-personal" data-toggle="tab">LOCATION</a></li>
                                    <li role="presentation" class="form-control-label"><a href="#price-personal" data-toggle="tab">PRICE</a></li>
                                    <li role="presentation" class="form-control-label"><a href="#other-personal" data-toggle="tab">OTHER</a></li>
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content" style="margin-left:15px">
                                    <div role="tabpanel" class="tab-pane fade in active" id="time">
                                        
                                        <?php
                                        if(!empty($card->start_date)) {
                                            $start_date_format = strtr(trim($card->start_date),"-","/");
                                            $start_date = date("d/m/Y", strtotime($start_date_format));
                                            //time
                                            $start_time = $card->start_time;
                                            //card start date time
                                            $card->card_start_date_time = $start_date.' - '.$start_time;
                                        }
                                        if(!empty($card->end_date)) {
                                            $end_date_format = strtr(trim($card->end_date),"-","/");
                                            $end_date = date("d/m/Y", strtotime($end_date_format));
                                            //time
                                            $end_time = $card->end_time;
                                            //card end date time
                                            $card->card_end_date_time = $end_date.' - '.$end_time;
                                        }
                                        ?>
                                           
                                        <div class="row clearfix">
                                             <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 form-control-label">
                                                     <label for="card_date">Card Start Date</label>
                                             </div>

                                             <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                     <div class="form-group label-floating is-empty">
                                                         <div class="form-line">
                                                             <input type="text" id="date-format1" name ="card_start_date_time" value="{{ !empty($card->card_start_date_time) ? $card->card_start_date_time:''}}" class="form-control" data-dtp="dtp_8A1LI" >
<!--                                                                <input type="text" name ="start_date" id="card_start_date" class="form-control" value= "{{$card->start_date}}"placeholder="Select card start date">-->
                                                         </div>
                                                     </div>
                                                     <span class="error">{{$errors->first('start_date')}}</span>    
                                             </div>

                                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 form-control-label">
                                                     <label for="card_date">Card End Date</label>
                                             </div>

                                             <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                     <div class="form-group  label-floating is-empty">
                                                         <div class="form-line">
                                                             <input type="text" id="date-format2" name ="card_end_date_time" value="{{ !empty($card->card_end_date_time) ? $card->card_end_date_time:''}}" class="form-control" data-dtp="dtp_8A1LI" >
<!--                                                             <input type="text" name ="end_date" id="card_end_date" class="form-control" value= "{{$card->end_date}}"placeholder="Select card end date">-->
                                                         </div>
                                                     </div>
                                                    <span class="error">{{$errors->first('end_date')}}</span>    
                                             </div>
                                            
                                         </div>

<!--                                         <div class="row clearfix">
                                             <div class="col-lg-1 col-md-1 col-sm-1 col-xs-2 form-control-label">
                                                     <label for="card_date">Card End Date</label>
                                             </div>

                                             <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                     <div class="form-group  label-floating is-empty">
                                                         <div class="form-line">
                                                             <input type="text" id="date-format2" name ="card_end_date_time" value="{{ !empty($card->card_end_date_time) ? $card->card_end_date_time:''}}" class="form-control" data-dtp="dtp_8A1LI" >
                                                             <input type="text" name ="end_date" id="card_end_date" class="form-control" value= "{{$card->end_date}}"placeholder="Select card end date">
                                                         </div>
                                                     </div>
                                                    <span class="error">{{$errors->first('end_date')}}</span>    
                                             </div>

                                             <div class="col-lg-1 col-md-1 col-sm-1 col-xs-3 form-control-label">
                                                     <label for="card_end_time">Card End Time</label>
                                             </div>

                                             <div class="col-lg-4 col-md-4 col-sm-4 col-xs-3">
                                                     <div class="form-group">
                                                         <div class="form-line">
                                                             <input type="text" name ="end_time" id="card_end_time" class="form-control" value= "{{$card->end_time}}" placeholder="Select card end time">
                                                         </div>
                                                     </div>
                                             <span class="error">{{$errors->first('end_time')}}</span>    
                                             </div>
                                         </div>-->
                                            
                                    </div>
                                    <!-- Location Tab-->
                                    <div role="tabpanel" class="tab-pane fade" id="location-personal">
                                        <label for="category">Location address </label>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <input type="text" id="category" name="location_address" value="{{ !empty($card->location_address )? $card->location_address:''}}" class="form-control" placeholder="Enter location address">
                                                        </div>
                                                    </div>
                                                    <span class="error">{{$errors->first('location_address')}}</span>        
                                                </div>
                                            </div>
                                         </div>
                                        <label for="category">Street address</label>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <input type="text" id="street_address" name="street_address" value="{{ !empty($card->street_address)? $card->street_address:''}}" class="form-control" placeholder="Enter street address">
                                                        </div>
                                                    </div>
                                                    <span class="error">{{$errors->first('street_address')}}</span>        
                                                </div>
                                            </div>
                                         </div>
                                        <label for="category">Location city </label>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <input type="text" id="location_city " name="location_city" value="{{ !empty($card->location_city )? $card->location_city :''}}" class="form-control" placeholder="Enter location city ">
                                                        </div>
                                                    </div>
                                                    <span class="error">{{$errors->first('location_city')}}</span>        
                                                </div>
                                            </div>
                                         </div>
                                        <label for="category">Location province</label>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <input type="text" id="location_province" name="location_province" value="{{ !empty($card->location_province )? $card->location_province :''}}" class="form-control" placeholder="Enter location province">
                                                        </div>
                                                    </div>
                                                    <span class="error">{{$errors->first('location_province')}}</span>        
                                                </div>
                                            </div>
                                         </div>
                                        <label for="category">Location country</label>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <input type="text" id="location_country" name="location_country" value="{{ !empty($card->location_country )? $card->location_country :''}}" class="form-control" placeholder="Enter location country">
                                                        </div>
                                                    </div>
                                                    <span class="error">{{$errors->first('location_country')}}</span>        
                                                </div>
                                            </div>
                                         </div> 
                                    </div>
                                    <!-- Location Tab End -->
                                    <!-- Price Start -->
                                    <div role="tabpanel" class="tab-pane fade" id="price-personal">
                                        <div class="row clearfix">
                                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-3 form-control-label">
                                                        <label for="price">Price</label>
                                                </div>

                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-3">
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <input type="text" name ="price" class="form-control" value= "{{$card->price}}"placeholder="Enter price">
                                                            </div>
                                                        </div>
                                                <span class="error">{{$errors->first('price')}}</span>    
                                                </div>
                                            
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-3">
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <input type="text" name ="currency" class="form-control" value= "{{$card->currency}}"placeholder="Enter currency">
                                                            </div>
                                                        </div>
                                                <span class="error">{{$errors->first('currency')}}</span>    
                                                </div>
                                            
                                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3">
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <input type="text" name ="discount" class="form-control" value= "{{$card->discount}}"placeholder="Enter discount">
                                                            </div>
                                                        </div>
                                                <span class="error">{{$errors->first('discount')}}</span>    
                                                </div>
                                          </div>
                                    </div>
                                    <!-- Price End ---->
                                    <!-- Other Start -->
                                    <div role="tabpanel" class="tab-pane fade" id="other-personal">
                                        <label for="category">Category</label>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <input type="text" id="category" name="category" value="{{ !empty($card->category)? $card->category:''}}" class="form-control" placeholder="Enter category">
                                                        </div>
                                                    </div>
                                                    <span class="error">{{$errors->first('category')}}</span>        
                                                </div>
                                            </div>
                                         </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-6">
                                                    <label for="spacs_brand">Brand</label>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <input type="text" id="spacs_brand " name="spacs_brand" value="{{ !empty($card->spacs_brand )? $card->spacs_brand :''}}" class="form-control" placeholder="Enter Brand">
                                                        </div>
                                                    </div>
                                                    <span class="error">{{$errors->first('spacs_brand')}}</span>        
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-6">
                                                    <label for="spacs_model">Model</label>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <input type="text" id="spacs_model" name="spacs_model" value="{{ !empty($card->spacs_model )? $card->spacs_model :''}}" class="form-control" placeholder="Enter Modal">
                                                        </div>
                                                    </div>
                                                    <span class="error">{{$errors->first('spacs_model')}}</span>        
                                                </div>
                                            </div>
                                         </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                    <label for="quantity">quantity</label>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <input type="text" id="quantity" name="quantity" value="{{ !empty($card->quantity)? $card->quantity:''}}" class="form-control" placeholder="Enter quantity">
                                                        </div>
                                                    </div>
                                                    <span class="error">{{$errors->first('quantity')}}</span>        
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                    <label for="spacs_size">Size</label>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <input type="text" id="spacs_size" name="spacs_size" value="{{ !empty($card->spacs_size)? $card->spacs_size:''}}" class="form-control" placeholder="Enter size">
                                                        </div>
                                                    </div>
                                                    <span class="error">{{$errors->first('spacs_size')}}</span>        
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                    <label for="spacs_color">Color</label>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <input type="text" id="spacs_color" name="spacs_color" value="{{ !empty($card->spacs_color)? $card->spacs_color:''}}" class="form-control" placeholder="Enter color">
                                                        </div>
                                                    </div>
                                                    <span class="error">{{$errors->first('spacs_color')}}</span>        
                                                </div>
                                            </div>
                                         </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-6 col-md-106 col-sm-6 col-xs-12">
                                                     <label for="for_age">For Age</label>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <input type="text" id="for_age" name="for_age" value="{{ !empty($card->for_age)? $card->for_age:''}}" class="form-control" placeholder="Enter age">
                                                        </div>
                                                    </div>
                                                    <span class="error">{{$errors->first('for_age')}}</span>        
                                                </div>
                                                
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                    <label for="for_gender">Gender</label>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <select name="for_gender" id="" class="form-control">
                                                                <option value="male" {{$card->for_gender=='male' ? 'selected':''}}>MALE</option>
                                                                <option value="female" {{$card->for_gender=='female' ? 'selected':''}}>FEMALE</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <span class="error">{{$errors->first('for_age')}}</span>        
                                                </div>
                                            </div>
                                         </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                                    <label for="contact_info">Contact Info</label>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <input type="text" id="contact_info" name="contact_info" value="{{ !empty($card->contact_info)? $card->contact_info:''}}" class="form-control" placeholder="Enter contact info">
                                                        </div>
                                                    </div>
                                                    <span class="error">{{$errors->first('contact_info')}}</span>        
                                                </div>
                                            </div>
                                         </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                                    <label for="url">URL</label>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <input type="text" id="url" name="url" value="{{ !empty($card->url)? $card->url:''}}" class="form-control" placeholder="Enter url">
                                                        </div>
                                                    </div>
                                                    <span class="error">{{$errors->first('url')}}</span>        
                                                </div>
                                            </div>
                                         </div>
                                    </div>
                                    <!-- Other End ---->
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="switch">
                                                {{$checked = ""}}
                                                @if(!empty($card->two_way_sync))
                                                  {{$checked = "checked"}}
                                                @endif
                                                <label>Sync item 2 way<input type="checkbox" value="{{($checked == 'checked')? 1 : 0}}"  name= "two_way_sync" checked="{{$checked}}"><span class="lever"></span></label>
                                            </div> 
                                        </div>
                                        <div class="col-md-4">
                                            <div class="switch">
                                                {{$checked = ""}}
                                                @if(!empty($card->live_item))
                                                  {{$checked = "checked"}}
                                                @endif
                                                <label>Live Item<input type="checkbox" value="{{($checked == 'checked')? 1 : 0}}"  name= "live_item" checked="{{$checked}}"><span class="lever"></span></label>
                                            </div> 
                                        </div>
                                        <div class="col-md-4">
                                            <div class="switch">
                                                {{$checked = ""}}
                                                @if(!empty($card->share_on_showcase))
                                                  {{$checked = "checked"}}
                                                @endif
                                                <label>Share on showcase<input type="checkbox" name= "share_on_showcase" value="{{($checked == 'checked')? 1 : 0}}" checked="{{$checked}}"><span class="lever"></span></label>
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-1 col-md-1 col-sm-4 col-xs-5 form-control-label">
                                            <label for="profile_image">Item Image</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-group">
                                            <span class="btn btn-info btn-primary btn-file">
                                                Browse <input type="file" name="card_image" accept=".jpg,.jpeg,.png, .gif" onchange="uploadImage(this,'profile-image-preview');">
                                            </span>
                                            @if(!empty($card->card_image))
                                                <img id="profile-image-preview" src="{{URL::to('/'). '/public/' . $card->card_image}}" alt="Item Image"/>
                                            @else
                                               <img id="profile-image-preview" src="{{URL::to('/'). '/public/' . '/admin-theme/images/upload.png'}}" alt="Item image"/>
                                            @endif
                                            </div>
                                        <span class="error">{{$errors->first('card_image')}}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-offset-9">
                                    <button class="btn btn-primary waves-effect" type="submit">CANCEL</button>
                                    <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                                </div>
                            </div>
                        </div>
                        
                        </div>  
                            
                    </form>
                </div>
            </div>
        </div>    
    </div>
    <script src="{{secure_asset("admin-theme/js/myscript.js")}}"></script>
    <script>
    function getStates(val) {
       var host = "{{URL::to('/admin')}}";
     
         $.ajax({
            type: "GET",
            url: host+"/admin/ajax/getStates",
            data: {"country_id": val},
            success: function(data){
                $("#state-list").html(data);
                $("#prof-state-list").html(data);
              
            }
        }); 
    }
    
    /**
     * Function to get cities on the basis of state
     * 
     */
    
    function getCities(val) {
       var host = "{{URL::to('/admin')}}";
     
         $.ajax({
            type: "GET",
            url: host+"/admin/ajax/getCities",
            data: {"state_id": val},
            success: function(data){
                $("#city-list").html(data);
                $("#prof-state-list").html(data);
            }
        }); 
    }
    
    $(function(){
         $('#card_time').combodate();  
    });
    
    //datepicker
    $( function() {
      $( "#datepicker" ).datepicker();
    } );
    
    $('#date-format, #date-format1, #date-format2').bootstrapMaterialDatePicker({ format : 'DD/MM/YYYY - HH:mm:ss' });

  </script>    
        
@endsection
