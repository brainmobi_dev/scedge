@extends('admin.layout.admin')

@section('content')
<div class="card">
        <div class="header">
            <h2>
               ITEM LIST
            </h2>
        </div>

        <div class="body">
           <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                    <div class="col-sm-offset-6 col-sm-6">
                        <div id="DataTables_Table_0_filter" class="dataTables_filter">
                            <form id="user-form"  method="get">
                                <input type='search' name='search_text'>
                                <input type="hidden"  id="sorting_field"  name="sorting_field" value="{{ !empty( $sortData['sorting_field'] )? $sortData['sorting_field'] : '' }}"/>
				<input type="hidden"  id="sorting_order"  name="sorting_order" value="{{ !empty( $sortData['sorting_order'] )? $sortData['sorting_order'] : '' }}"/>
                                <button class="btn btn-primary" type='submit'>Search</button>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="row">
                <div class="col-sm-12">
                <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
                <thead>
                    <tr role="row">
                    <th class="{{ $getSortingClass('users.name') }}"
                        rowspan="1"
                        colspan="1"
                        onclick="setSortingField('users.name', '{{ $getSortinOrder('users.name') }}' )">Owner Name</th>
                    <th rowspan="1" colspan="1" >Title</th>
                    <th rowspan="1" colspan="1" >Body</th>
                    <th rowspan="1" colspan="1" >Card Image</th>
                    <th rowspan="1" colspan="1" >Status</th>
                    <th rowspan="1" colspan="1" >Action</th>
                    </tr>
                </thead>

                <tbody>
                @if(count($userCards)>0)
                @foreach ($userCards as $card)
                <input type="hidden" id="item_id" name="_token" value="{{ csrf_token() }}">
                <tr role="row" class="odd" id="row_{{ $card->id}}">
                        <td>{{$card->name}}
                        <td>{{$card->title}}</td>
                        <td>
                            {{str_limit($card->body,40)}}
                        </td>
                        <td>
                            @if(!($card->card_image))
                              <img src="{{ secure_asset('public/image/project-img2.jpg') }}" style="width:80px; height: 80px;" />
                            @elseif(strpos($card->card_image, 'http') === false)
                              <img src="{{ secure_asset($card->card_image) }}" style="width:80px; height: 80px;" />
                            @else
                              <img src="{{ $card->card_image }}" style="width:80px; height: 80px;" />
                            @endif

                        </td>

                        <td>
                             @if($card->is_active===1)
                              <a id="item_{{ $card->id}}" href="javascript:changeStatus({{ $card->id}}, 0);" ><span class="btn btn-danger">Deactivate Card</span></a>
                             @else
                               <a id="item_{{ $card->id}}" href="javascript:changeStatus({{ $card->id}}, 1);" ><span class="btn btn-success">Activate Card</span></a>
                             @endif
                        </td>

                        <td>
                            <a href="{{URL::to('/').'/admin/item/edit?item_id='.$card->id}}"><i class="material-icons">edit</i></a>
                            <a href="#" onclick="deleteItem({{$card->id}})"><i class="material-icons ">delete</i>
                            </a>
                            <a href="{{URL::to('/').'/admin/item/view?item_id='.$card->id}}"><i class="material-icons ">visibility</i>
                            </a>
                        </td>

                </tr>
                @endforeach
                @else
                    <tr><td colspan="6" style="text-align: center;"><b>No result founds</b></td></tr>
                @endif

                </tbody>
                </table>
                  <div class="row pull-right">
                 {{$userCards->links()}}
                  </div>
                </div>
                </div>

        </div>
    </div>
    <script>
    /**
     * Apply sorting on field
     *
     */

    function setSortingField(field, order) {
        $("#sorting_field").val(field);
        $("#sorting_order").val(order);
        $("#user-form").submit();
    }
    /**
     * Delete item
     *
     */
    function deleteItem(item_id) {
        var host = "{{URL::to('/admin')}}";
            bootbox.confirm("Are you sure you want to delete this data?", function(result) {
            if (result) {
                    $.ajax({
                        url: host+'/item/delete',
                        type: "post",
                        data: {"item_id": item_id,'_token':"{{ csrf_token()}}" },
                        success: function(response) {
                            $("#row_" + item_id).remove();
                        }
                    });
            }
        });
    }

    function changeStatus(item_id, status){
        var host = "{{URL::to('/admin')}}";
         bootbox.confirm("Are you sure you want to change status?", function(result) {
            if (result) {
            $.ajax({
                url: host+'/item/changestatus',
                type: "post",
                data: {"item_id": item_id, "status":status,'_token':"{{ csrf_token()}}"},
                success: function(response) {
                var newStatus= (status)?0:1;
                if(response.error_code == 200){
                    if(status == "1"){
                        $("#item_"+item_id+ " span").removeClass("btn btn-success");
                        $("#item_"+item_id+ " span").addClass("btn btn-danger");
                        $("#item_"+item_id+ " span").text("Deactivate Card");
                        $("#item_"+item_id).attr("href", "javascript:changeStatus("+item_id+", "+newStatus+")");

                    } else {

                        $("#item_"+item_id+ " span").removeClass("btn btn-danger");
                        $("#item_"+item_id+ " span").addClass("btn btn-success");
                        $("#item_"+item_id+ " span").text("Activate Card");
                        $("#item_"+item_id).attr("href", "javascript:changeStatus("+item_id+", "+newStatus+")");
                    }
                }
                }
                });
            }
        });

    }
    </script>
@endsection
