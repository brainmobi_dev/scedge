@extends('admin.layout.admin')

@section('content')
<div class="card">
    <div class="header bg-orange">
        <h2>
            Card Title : {{$card->title}}
        </h2>
        <ul class="header-dropdown m-r--5">
            <li class="dropdown">
                <a href="{{URL::to('/').'/admin/item/edit?item_id='.$card->id}}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                    <i class="material-icons">edit</i>
                </a>
            </li>
        </ul>
    </div>
    <div class="body">
        <div class="row clearfix">
            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                <label class="pull-left">Owner Name</label>
            </div>

            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                <span>{{$card->name}}</span>    
            </div>  
        </div>

        <hr>
        <div class="row clearfix">
            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                <label class="pull-left">Body</label>
            </div>

            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                <span>{{$card->body}}</span>    
            </div>  
        </div>
        
        <hr>
        <div class="row clearfix">
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 form-control-label">
                <label class="pull-left">Card Date</label>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <span>{{$card->card_date}}</span>    
            </div> 
            
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 form-control-label">
                <label class="pull-left">Card Time</label>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <span>{{$card->card_time}}</span>    
            </div>  
        </div>
        
        <hr>
        <div class="row clearfix">
            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                <label class="pull-left">Priority</label>
            </div>

            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                <span>{{$card->priority}}</span>    
            </div>  
        </div>
        
        <hr>
        <div class="row clearfix">
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 form-control-label">
                <label class="pull-left">Location Address</label>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <span>{{$card->location_address}}</span>    
            </div>
            
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 form-control-label">
                <label class="pull-left">Location City</label>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <span>{{$card->location_city}}</span>    
            </div>
        </div>
      
        <hr>
        <div class="row clearfix">
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 form-control-label">
                <label class="pull-left">Location Province</label>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <span>{{$card->location_province}}</span>    
            </div>
            
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 form-control-label">
                <label class="pull-left">Location Country</label>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <span>{{$card->location_country}}</span>    
            </div>
            
        </div>
        
        <hr>
        <div class="row clearfix">
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 form-control-label">
                <label class="pull-left">Start Date</label>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <span>{{$card->start_date}}</span>    
            </div>
            
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 form-control-label">
                <label class="pull-left">Start Time</label>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <span>{{$card->start_time}}</span>    
            </div>
        </div>
        
       
        <hr>
        <div class="row clearfix">
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 form-control-label">
                <label class="pull-left">End Date</label>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <span>{{$card->end_date}}</span>    
            </div>
            
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 form-control-label">
                <label class="pull-left">End Time</label>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <span>{{$card->end_time}}</span>    
            </div>
        </div>
        
        <hr>
        <div class="row clearfix">
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 form-control-label">
                <label class="pull-left">Price</label>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <span>{{$card->price}}</span>    
            </div>
            
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 form-control-label">
                <label class="pull-left">Quantity</label>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <span>{{$card->quantity}}</span>    
            </div>
            
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 form-control-label" {{$professional}}>
                <label class="pull-left">Discount</label>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" {{$professional}}>
                <span>{{$card->discount}}</span>    
            </div>
        </div>
 
        <hr {{$professional}}>
        <div class="row clearfix" {{$professional}}>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 form-control-label">
                <label class="pull-left">Spacs Size</label>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <span>{{$card->spacs_size}}</span>    
            </div>
            
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 form-control-label">
                <label class="pull-left">Spacs Brand</label>
            </div>

             <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <span>{{$card->spacs_brand}}</span>    
            </div>
        </div>
        
     
        <hr {{$professional}}>
        <div class="row clearfix"  {{$professional}}>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 form-control-label">
                <label class="pull-left">Spacs Model</label>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <span>{{$card->spacs_model}}</span>    
            </div>
            
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 form-control-label">
                <label class="pull-left">Spacs Color</label>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <span>{{$card->spacs_color}}</span>    
            </div>
        </div>
        
      
        <hr {{$professional}}>
        <div class="row clearfix"  {{$professional}}>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 form-control-label">
                <label class="pull-left">For Gender</label>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <span>{{$card->for_gender}}</span>    
            </div>
            
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 form-control-label">
                <label class="pull-left">For Age</label>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <span>{{$card->for_age}}</span>    
            </div>
            
        </div>
        
        <hr>
        <div class="row clearfix">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 form-control-label">
                <label class="pull-left">Share On showcase</label>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <span>{{$card->share_on_showcase}}</span>    
            </div>
            
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 form-control-label">
                <label class="pull-left">Two way Sync</label>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <span>{{$card->two_way_sync}}</span>    
            </div>
            
<!--        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 form-control-label"  {{$professional}}>
                <label class="pull-left">Live Item</label>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"  {{$professional}}>
                <span>{{$card->live_item}}</span>    
            </div>-->
        </div>
     
        <hr>
        <div class="row clearfix">
            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                <label class="pull-left">Card Type</label>
            </div>

            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                <span>{{$card->card_type}}</span>    
            </div>
        </div>
        
        
    </div>
</div>
@endsection