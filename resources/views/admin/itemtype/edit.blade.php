
@extends('admin.layout.admin')

@section('content')
 
           <!-- Basic Validation -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>Edit Item Type Details</h2>
                        </div>
                        <div class="body">
                            <form id="form_validation" action="#" method="POST" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{ csrf_token()}}">
                                
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="card_type">Item Type</label>
                                    </div>
                                    
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text"  name="card_type" value="{{$cardTypeData->name}}" class="form-control" placeholder="Enter card  type name">
                                                </div>
                                            </div>
                                            <span class="error">{{$errors->first('card_type')}}</span>        
                                    </div>
                                </div>    

                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="card_image">Item Type Image</label>
                                    </div>
                                    
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <span class="btn btn-info btn-primary btn-file">
                                            Browse <input type="file" name="card_image" accept=".jpg,.jpeg,.png, .gif" onchange="uploadImage(this,'image-preview');">
                                        </span>  
					<img id="image-preview" src="{{URL::to('/').'/admin/uploads/card_items/'.$cardTypeData->image}}" alt="Card image"/>
                                    </div>
                                        
                                    <span class="error">{{$errors->first('card_image')}}</span>        
                                    </div>
                                </div>
                                
                                <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Validation -->
@endsection
<script type="text/javascript">
/*
 * Load the preview of image on browsing the file
 * 
 */
function uploadImage(input,id) {
      //alert(input.files);
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#'+id)
                    .attr('src', e.target.result)
                    .width(80)
                    .height(80);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>