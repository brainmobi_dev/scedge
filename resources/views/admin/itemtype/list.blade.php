@extends('admin.layout.admin')

@section('content')
<div class="card">
        <div class="header">
            <h2>
              ITEM TYPES
            </h2>
        </div>

        <div class="body">
                <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                    <div class="row">
                        <div class="col-sm-6">
                            <form id="user-form"  method="get">
                                <input type='search' name='search_text'>
                                <input type="hidden"  id="sorting_field"  name="sorting_field" value="{{ !empty( $sortData['sorting_field'] )? $sortData['sorting_field'] : '' }}"/>
                                <input type="hidden"  id="sorting_order"  name="sorting_order" value="{{ !empty( $sortData['sorting_order'] )? $sortData['sorting_order'] : '' }}"/>
                                <button class="btn btn-primary" type='submit'>Search</button>
                            </form>
                        </div>

                        <div class="col-sm-6">
                            <div class="dataTables_length" id="DataTables_Table_0_length">
                            <a class="btn btn-success pull-right" href="{{route('addItemType')}}">ADD ITEM</a>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                    <div class="col-sm-12">
                    <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">

                <thead>
                    <tr role="row">
                    <th rowspan="1" colspan="1" >Item Type</th>
                    <th rowspan="1" colspan="1" >Item Image</th>
                    <th rowspan="1" colspan="1" >Status</th>
                    <th rowspan="1" colspan="1" >Action</th>
                    </tr>
                </thead>

                <tbody>
                @if(count($cardTypes)>0)
                @foreach ($cardTypes as $cardType)
                <input type="hidden" id="industry_id" name="_token" value="{{ csrf_token() }}">
                <tr role="row" class="odd" id="row_{{ $cardType->id}}">
                        <td>{{$cardType->name}}</td>
                        <td>
                          @if(!($cardType->image))
                            <img src="{{ secure_asset('/image/project-img2.jpg') }}" style="width:80px; height: 80px;" />
                          @elseif(strpos($cardType->image, 'http') === false)
                            <img src="{{ secure_asset('/admin/uploads/card_items/'.$cardType->image) }}" style="width:80px; height: 80px;" />
                          @endif
                        </td>
                        <td>
                            @if($cardType->is_active===1)
                              <a id="user_{{ $cardType->id}}" href="javascript:changeStatus({{ $cardType->id}}, 0);" ><span class="btn btn-danger">Inactive</span></a>
                            @else
                              <a id="user_{{ $cardType->id}}" href="javascript:changeStatus({{ $cardType->id}}, 1);" ><span class="btn btn-success">Active</span></a>
                            @endif
                        </td>
                        <td>
                           <a href="{{URL::to('/').'/admin/itemtype/edit?id='.$cardType->id}}" <i class="material-icons">edit</i></a>
                           <a href="#" onclick="deleteItem({{$cardType->id}})"><i class="material-icons ">delete</i>
                        </td>

                </tr>
                @endforeach
                @else
                    <tr><td colspan="4" style="text-align: center;"><b>No result founds</b></td></tr>
                @endif
                </tbody>
                </table>

                </div>
                </div>

        </div>
    </div>
</div>
   <script>
    function deleteItem(card_type_id) {
        var host = "{{URL::to('/admin')}}";
            bootbox.confirm("Are you sure you want to delete this data?", function(result) {
            if (result) {
                    $.ajax({
                        url: host+'/admin/itemtype/delete',
                        type: "post",
                        data: {"card_type_id": card_type_id,'_token':"{{ csrf_token()}}" },
                        success: function(response) {
                            $("#row_"+card_type_id).remove();
                        }
                    });
            }
        });
    }

    function changeStatus(card_type_id, status){
        var host = "{{URL::to('/admin')}}";
            bootbox.confirm("Are you sure you want to change status?", function(result) {
            if (result) {
            $.ajax({
                url: host+'/admin/itemtype/changestatus',
                type: "post",
                data: {"card_type_id": card_type_id, "status":status,'_token':"{{ csrf_token()}}"},
                success: function(response) {

                var newStatus= (status)?0:1;
                if(response.error_code == 200){
                    if(status == "1"){
                        $("#user_"+card_type_id+ " span").removeClass("btn btn-success");
                        $("#user_"+card_type_id+ " span").addClass("btn btn-danger");
                        $("#user_"+card_type_id+ " span").text("Inactive");
                        $("#user_"+card_type_id).attr("href", "javascript:changeStatus("+card_type_id+", "+newStatus+")");
                    } else {
                        $("#user_"+card_type_id+ " span").removeClass("btn btn-danger");
                        $("#user_"+card_type_id+ " span").addClass("btn btn-success");
                        $("#user_"+card_type_id+ " span").text("Active");
                        $("#user_"+card_type_id).attr("href", "javascript:changeStatus("+card_type_id+", "+newStatus+")");
                    }
                }
                }
                });
            }
        });

    }
    </script>


@endsection
