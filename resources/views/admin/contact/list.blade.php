@extends('admin.layout.admin')

@section('content')
<style>
/*th{
    padding: 15px;
    text-align: left;
}*/
/*tr tr,td {
    padding: 15px;
    text-align: left;
}    */
</style>
<div class="card">
        <div class="header">
            <h2>
               USER LIST
            </h2>
        </div>
    
        <div class="body">
            <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline dt-bootstrap"><div class="row"><div class="col-sm-6"><div class="dataTables_length" id="DataTables_Table_0_length"></div></div><div class="col-sm-6"><div id="DataTables_Table_0_filter" class="dataTables_filter"><label>Search:<input type="search" class="form-control input-sm" placeholder="" aria-controls="DataTables_Table_0"></label></div></div></div><div class="row"><div class="col-sm-12"><table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
                <thead>
                    <tr role="row">
                    <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 134px;">Owner Name</th>
                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 223px;">Group</th>
                </thead>
                
                <tbody>
                <input type="hidden" id="contact_group_id" name="_token" value="{{ csrf_token() }}">
                if(count($ownerData)>0)
                @foreach ($ownerData as $ownerRow)
                <tr role="contacts" class="odd" id="contacts">
                       <td>{{$ownerRow->name}}</td>
                       <td id="col_{{$ownerRow->id}}">
                        @foreach($userGroup as $key1=> $group)
                            @if($key1 == $ownerRow->id)
                                @foreach($group as $key2=>$value)
                                    @if($key2 == $value[0]['contact_group_id'])
                                        <a href="{{URL::to('/').'/admin/contact/groupMembersList?contact_group_id='.$key2.'&owner_id='.$ownerRow->id }}">{{$value[0]['contact_group_name']}}({{$value['count']}}) </a></br>
                                    @endif
                                @endforeach
                            @endif
                        @endforeach
                       </td>
                </tr>
                @endforeach
                @else
                    <tr><td colspan="4" style="text-align: center;"><b>No result founds</b></td></tr>
                @endif
                </tbody>
                
                </table>
                <div class="pull-right">
                    {{$ownerData->links()}}	
                </div>
        </div>
    </div>
   
@endsection

