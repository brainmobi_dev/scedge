@extends('admin.layout.admin')

@section('content')

<div class="card">
        <div class="header">
            <h2>
               GROUP MEMBER LIST
            </h2>
        </div>

        <div class="body">
            <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                    <div class="col-sm-offset-3 col-sm-6">
                        <p class="msgClass" style="color: green"></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
                            <thead>
                                <tr role="row">
                                <th rowspan="1" colspan="2">Group</th>
                                <th rowspan="1" colspan="3">Contacts</th>
<!--                                <th rowspan="1" colspan="1">Action</th>-->
                            </thead>
                            <tbody>
                            <input type="hidden" id="contact_group_id" name="_token" value="{{ csrf_token() }}">
                            
                            @foreach ($contactByGroup as $groupId => $contactLists)
                            <tr role="contacts" class="odd" id="group_{{$groupId}}">
                                <td>
                                    <a href="javascript:void(0);" class="groupname" id="name_{{$groupId}}">{{ $groupArray[$groupId]['name'] }}</a>
                                    <div id="edit_{{$groupId}}" class="inlineEdit" style="display: none"><input type="text" class="form-control" id="update_{{$groupId}}" value="{{ $groupArray[$groupId]['name'] }}"></div>
                                    <div class="" style="float: right;">
                                        <a href="javascript:void(0);" id="inlineUpdate_{{$groupId}}" style="display: none;" onclick="updateGroup({{$groupId}})"><i class="material-icons">check_circle</i></a>
                                        <a href="javascript:void(0);" id="edit_group{{$groupId}}" onclick="editGroup({{$groupId}})"><i class="material-icons">edit</i></a>
                                        <a href="javascript:void(0);" onclick="deleteGroup({{$groupId}})"><i class="material-icons">delete</i></a>
                                    </div>
                                </td>
                                <td colspan="3">
                                    @foreach ($contactLists as $key => $contact)
                                        <a href="javascript:void(0);" id="contact_{{$contact->contact_id}}_{{$groupId}}" class="btn btn-info waves-effect" onclick="deleteContact({{$contact->owner_id}},{{$contact->contact_id}},{{$groupId}});">{{$contact->contactname}}  <i class="material-icons">cancel</i></a>
                                    @endforeach
                                </td>
<!--                                <td>
                                    <a href="javascript:void(0);" onclick="editGroup({{$groupId}})"><i class="material-icons">edit</a>
                                    <a href="javascript:void(0);" onclick="deleteGroup({{$groupId}})"><i class="material-icons">delete</i></a>
                                </td>-->
                            </tr>
                            @endforeach
                            
                            </tbody>

                        </table>
                        <div class="pull-right">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection
<script>
    /**
     * Edit Group
     *
     */
    function editGroup(groupid) {
        $(".inlineEdit").hide();
        $("#edit_group"+groupid).hide();
        $("#inlineUpdate_"+groupid).show();
        $(".groupname").show();
        $("#name_"+groupid).hide();
        $("#edit_"+groupid).show();
    }
    /**
     * Update Group
     *
     */
    function updateGroup(groupid) {
        
        var name = $("#update_"+groupid).val();
        var host = "{{URL::to('/admin')}}";
           
        if(groupid && name) {
            $.ajax({
                url: host+'/contact/group_edit',
                type: "post",
                data: {"groupid": groupid,"name": name,'_token':"{{ csrf_token()}}" },
                success: function(response) {
                    if(response.error_code == 200) {
                        $(".msgClass").html(response.msg_string);
                        $("#name_"+groupid).html(name);
                        $("#edit_"+groupid).hide();
                        $("#inlineUpdate_"+groupid).hide();
                        $("#name_"+groupid).show();
                        $("#edit_group"+groupid).show();
                    }
                    setTimeout(function(){
                        $(".msgClass").html("");
                    },2000);
                }
            });
        }
    }
    /**
     * Delete Group
     *
     */
    function deleteGroup(groupid) {
        var host = "{{URL::to('/admin')}}";
            bootbox.confirm("Are you sure you want to delete this group?", function(result) {
            if (result) {
                    $.ajax({
                        url: host+'/contact/deleteGroup',
                        type: "post",
                        data: {"groupid": groupid,'_token':"{{ csrf_token()}}" },
                        success: function(response) {
                            if(response.error_code == 200) {
                                $(".msgClass").html(response.msg_string);
                                $("#group_" + groupid).remove();
                            }
                            setTimeout(function(){
                                $(".msgClass").html("");
                            },2000);
                        }
                    });
            }
        });
    }
    
    /**
     * Delete Contact
     *
     */
    function deleteContact(userid,contactid,groupid) {
        var host = "{{URL::to('/admin')}}";
            bootbox.confirm("Are you sure you want to delete this contact?", function(result) {
            if (result) {
                    $.ajax({
                        url: host+'/contact/deleteContact',
                        type: "post",
                        data: {"userid": userid,"contactid": contactid,"groupid": groupid,'_token':"{{ csrf_token()}}" },
                        success: function(response) {
                            console.log(response);
                            if(response.error_code == 200) {
                                $(".msgClass").html(response.msg_string);
                                $("#contact_" + contactid + "_" + groupid).remove();
                            }
                            setTimeout(function(){
                                $(".msgClass").html("");
                            },2000);                            
                        }
                    });
            }
        });
    }
</script>
