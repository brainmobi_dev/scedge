@extends('admin.layout.admin')

@section('content')

<div class="card">
        <div class="header">
            <h2>
               USER CONTACT GROUP LIST
            </h2>
        </div>

        <div class="body">
            <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                    <div class="col-sm-offset-6 col-sm-6">
                        <div id="DataTables_Table_0_filter" class="dataTables_filter">
                            <form id="user-form" method="get">
                                <input type='search' name='search_text'>
                                <input type="hidden"  id="sorting_field"  name="sorting_field" value="{{ !empty( $sortData['sorting_field'] )? $sortData['sorting_field'] : '' }}"/>
				<input type="hidden"  id="sorting_order"  name="sorting_order" value="{{ !empty( $sortData['sorting_order'] )? $sortData['sorting_order'] : '' }}"/>
                                <button class="btn btn-primary" type='submit'>Search</button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
                            <thead>
                                <tr role="row">
                                <th class="{{ $getSortingClass('users.name') }}"
                                    rowspan="1"
                                    colspan="1"
                                    onclick="setSortingField('users.name', '{{ $getSortinOrder('users.name') }}' )">Name</th>
                                <th rowspan="1" colspan="1">Number of Groups</th>
                                <th rowspan="1" colspan="1">Number of Contacts</th>

            <!--                    @foreach($groupTypes as $groupType)
                                    <th rowspan="1" colspan="1">{{$groupType->name}}</th>
                                @endforeach-->
                            </thead>
                            <tbody>
                            <input type="hidden" id="contact_group_id" name="_token" value="{{ csrf_token() }}">
                            
                            @if(count($ownerRowset)>0)
                            
                            @foreach ($ownerRowset as $ownerRow)
                            <!--<tr><td>{{$ownerRow}}<td></tr>-->
                              <tr role="contacts" class="odd" id="contacts">
                                    <td>{{$ownerRow->name}}</td>
                                    <td><a class="btn bg-cyan waves-effect" href="{{empty($ownerRow->userContactGroupCount) ? 'javascript:void(0);' : URL::to('/').'/admin/contact/groupMembersList?id='.$ownerRow->id }}"><i class="material-icons">supervisor_account</i>&nbsp;&nbsp; <b>{{$ownerRow->userContactGroupCount}}</b> Groups&nbsp;</a></td>
                                    <td><a class="btn bg-light-green waves-effect" href="{{empty($ownerRow->userContactsCount) ? 'javascript:void(0);' : URL::to('/').'/admin/contact/groupMembersList?id='.$ownerRow->id }}"><i class="material-icons">contact_mail</i>&nbsp;&nbsp; <b>{{$ownerRow->userContactsCount}}</b> Contacts&nbsp;</a></td>
                              </tr>
                            @endforeach
                            @else
                                <tr><td colspan="3" style="text-align: center;"><b>No result founds</b></td></tr>
                            @endif
                            </tbody>

                        </table>
                        <div class="pull-right">
                            {{$ownerRowset->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection
<script>
   /**
     * Apply sorting on field
     *
     */
    function setSortingField(field, order) {
        $("#sorting_field").val(field);
        $("#sorting_order").val(order);
        $("#user-form").submit();
    }
</script>
