@extends('admin.layout.admin')

@section('content')
  <!-- Ckeditor -->
<script src="{{secure_asset("admin-theme/plugins/ckeditor/ckeditor.js")}}"></script>

<script src="{{secure_asset("admin-theme/js/pages/forms/editors.js")}}"></script>

<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Edit Content Details 
                </h2>
            </div>

            <div class="body">
                <form id="form_validation" action="#" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row clearfix">
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="content_type">Content Type</label>
                        </div>

                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
<!--                                <input type="text" name="content_type" value="{{ $contentRow->content_type}}" class="form-control" placeholder="Enter content title">-->
                                        <select name="content_type" class="form-control">
                                            <option> ABOUT</option>
                                            <option>CONTACT-US</option>
                                        </select>    
                                    </div>
                                </div>
                        <span class="error">{{$errors->first('content_type')}}</span>        
                        </div>
                </div>    
                
                <div class="row clearfix">
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="content_title">Content Title</label>
                        </div>

                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="content_title" value="{{ $contentRow->content_title}}" class="form-control" placeholder="Enter content text">
                                    </div>
                                </div>
                        <span class="error">{{$errors->first('content_title')}}</span>        
                        </div>
                </div>    
                <div class="row clearfix">
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="content_text">Content Text</label>
                        </div>

                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                               <textarea id="ckeditor" name="content_text">
                                {{ $contentRow->content_text}}
                               </textarea>
                        <span class="error">{{$errors->first('content_text')}}</span>        
                        </div>
                </div>    
                <div class="col-sm-offset-9">
                    <a class="btn btn-danger waves-effect " href="{{route("contentList")}}">CANCEL</a>
                    <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- #END# CKEditor -->
@endsection

