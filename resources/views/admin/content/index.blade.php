@extends('admin.layout.admin')

@section('content')
<div class="card">
        <div class="header">
            <h2>
               Content List
            </h2>
        </div>

        <div class="body">
            <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
<!--                <div class="row">
                    <div class="col-sm-offset-6 col-sm-6">
                        <div id="DataTables_Table_0_filter" class="dataTables_filter">
                            <label>Search:<input type="search" class="form-control input-sm" placeholder="" aria-controls="DataTables_Table_0"></label>
                        </div>
                    </div>
                </div>-->
                <div class="row">
                    <div class="col-sm-12">
                        <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
                <thead>
                    <tr role="row">
                    <th rowspan="1" colspan="1">Content Type</th>
                    <th rowspan="1" colspan="1">Content Title</th>
                    <th rowspan="1" colspan="1">Content Text</th>
                    <th rowspan="1" colspan="1">Status</th>
                    <th rowspan="1" colspan="1">Action</th>
                    </tr>
                </thead>

                <tbody>
                @if($contentRowset->count())
                  @foreach ($contentRowset as $contentRow)
                  <input type="hidden" id="item_id" name="_token" value="{{ csrf_token() }}">
                  <tr role="row" class="odd" id="row_{{ $contentRow->id}}">
                          <td>{{$contentRow->content_type}}</td>
                          <td>
                              {{str_limit($contentRow->content_title,40)}}
                          </td>
                          <td>
                              {{str_limit($contentRow->content_text,40)}}
                          </td>
                          <td>
                               @if($contentRow->is_active===1)
                                <a id="item_{{ $contentRow->id}}" href="javascript:changeStatus({{ $contentRow->id}}, 0);" ><span class="btn btn-danger">Inactive</span></a>
                               @else
                                 <a id="item_{{ $contentRow->id}}" href="javascript:changeStatus({{ $contentRow->id}}, 1);" ><span class="btn btn-success">Active</span></a>
                               @endif
                          </td>
                          <td>
                              <a href="{{URL::to('/').'/admin/content/edit?id='.$contentRow->id}}" style="height: 60px;width:60px"><i class="material-icons">edit</i></a>
                              <a href="#" onclick="deleteContent({{$contentRow->id}})"><i class="material-icons ">delete</i>
                              </a>
                          </td>


                  </tr>
                  @endforeach
                @else
                    <tr>
                      <td  colspan="5"><div align='center'><b>No result founds</b></div></td>
                    </tr>
                @endif

                </tbody>
            </table>
            <div class="row pull-right">
                 {{$contentRowset->links()}}
            </div>
        </div>
    </div>

    <script>
    function deleteContent(content_id) {
        var host = "{{URL::to('/admin')}}";
            bootbox.confirm("Are you sure you want to delete this data?", function(result) {
            if (result) {
                    $.ajax({
                        url: host+'/admin/content/delete',
                        type: "post",
                        data: {"content_id": content_id,'_token':"{{ csrf_token()}}" },
                        success: function(response) {
                            $("#row_" + content_id).remove();
                        }
                    });
            }
        });
    }

    function changeStatus(content_id, status){
        var host = "{{URL::to('/admin')}}";
        bootbox.confirm("Are you sure you want to change the status?", function(result) {
            if (result) {
            $.ajax({
                url: host+'/admin/content/changestatus',
                type: "post",
                data: {"content_id": content_id, "status":status,'_token':"{{ csrf_token()}}"},
                success: function(response) {
                var newStatus= (status)?0:1;
                if(response.error_code == 200){
                    if(status == "1"){
                        $("#item_"+content_id+ " span").removeClass("btn btn-success");
                        $("#item_"+content_id+ " span").addClass("btn btn-danger");
                        $("#item_"+content_id+ " span").text("Inactive");
                        $("#item_"+content_id).attr("href", "javascript:changeStatus("+content_id+", "+newStatus+")");

                    } else {

                        $("#item_"+content_id+ " span").removeClass("btn btn-danger");
                        $("#item_"+content_id+ " span").addClass("btn btn-success");
                        $("#item_"+content_id+ " span").text("Active");
                        $("#item_"+content_id).attr("href", "javascript:changeStatus("+content_id+", "+newStatus+")");
                    }
                }
                }
                });
            }
        });

    }
    </script>

@endsection
