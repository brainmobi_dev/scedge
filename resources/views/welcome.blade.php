<!doctype html>
<html lang="{{ config('app.locale') }}" ng-app="scedgeApp">
    <head>
        <base href="/">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Scedge</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Oswald:200,300,400,500,600,700&subset=cyrillic,latin-ext,vietnamese" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i&subset=latin-ext,vietnamese" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="{{ asset('public/bower_components/angular-loading-bar/src/loading-bar.css') }}" type="text/css" href="">
        <link href="{{ asset('public/css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('public/css/style.css?version=' . time()) }}" rel="stylesheet" />
        <link href="{{ asset('public/css/responsive.css') }}" rel="stylesheet" />
        <link href="{{ asset('public/bower_components/ng-img-crop/compile/unminified/ng-img-crop.css') }}" rel="stylesheet" />
        <link rel="stylesheet" href="{{ asset('public/css/animate.css') }}" />
        <link rel="stylesheet" href="{{ asset('public/css/font-awesome.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('public/css/jquery.mCustomScrollbar.css') }}" />
        <link href="{{ asset('public/bower_components/angularjs-datepicker/dist/angular-datepicker.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('public/bower_components/wdt-emoji-bundle/wdt-emoji-bundle.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('public/bower_components/angucomplete-alt/angucomplete-alt.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('public/js/clockpicker.css') }}" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="{{ asset('public/bower_components/angular-google-places-autocomplete/src/autocomplete.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('public/bower_components/angular-toastr/dist/angular-toastr.css') }}" />
     <!--    <link rel="stylesheet" href="{{ asset('public//bower_components/angular-tooltips/dist/angular-tooltips.min.css') }}"> -->
        <!-- Styles -->


        <!-- <script src="{{ asset('public/bower_components/echojs/dist/echo.js') }}"></script>
        <script type="text/javascript">
            echo.channel('messages')
                .listen('NewMessage', (e) => {
                    console.log(e.order.name);
                });
        </script> -->

    </head>
    <body>
        <div id="wrapper">
            <ui-view class="chat-wrap"></ui-view>
        </div>

        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?&libraries=places&language=en&v=3&key=AIzaSyCiWFB-_4euHGpHIuSruaFwM7cosRL_5Dk"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="{{ asset('public/bower_components/underscore/underscore.js') }}"  type="text/javascript"></script>
         <script src="{{ asset('public/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('public/js/owl.carousel.js') }}"></script>
        <script src="{{ asset('public/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
        <script src="{{ asset('public/js/custom.js') }}"></script>
        <script src="{{ asset('public/js/clockpicker.js') }}"></script>

        <script src="{{ asset('public/bower_components/jquery-ui/jquery-ui.js') }}" type="text/javascript"></script>
        <script src="{{ asset('public/bower_components/angular/angular.js') }}" type="text/javascript"></script>
        <script src="{{ asset('public/bower_components/angular-accordion/angular-accordion.js') }}"  type="text/javascript"></script>
        <script src="{{ asset('public/bower_components/angular-animate/angular-animate.js') }}"  type="text/javascript"></script>
        <script src="{{ asset('public/bower_components/angular-cookies/angular-cookies.js') }}"  type="text/javascript"></script>
        <script src="{{ asset('public/bower_components/angular-loader/angular-loader.js') }}"  type="text/javascript"></script>
        <script src="{{ asset('public/bower_components/angular-pagination/angular-pagination.js') }}"  type="text/javascript"></script>
        <script src="{{ asset('public/bower_components/angular-resource/angular-resource.js') }}"  type="text/javascript"></script>
        <script src="{{ asset('public/bower_components/angular-messages/angular-messages.js') }}"  type="text/javascript"></script>
        <script src="{{ asset('public/bower_components/angular-loading-bar/src/loading-bar.js') }}"  type="text/javascript"></script>
        <script src="{{ asset('public/bower_components/angular-local-storage/dist/angular-local-storage.js') }}"  type="text/javascript"></script>
        <script src="{{ asset('public/bower_components/angularjs-datepicker/dist/angular-datepicker.js') }}"></script>
        <script src="{{ asset('public/bower_components/angularUtils-pagination/dirPagination.js') }}"></script>
        <!-- AngularFire -->
       <script src="https://cdn.firebase.com/libs/angularfire/1.1.3/angularfire.min.js"></script>
       <!-- Firebase -->
       <script src="https://static.firebase.com/v0/firebase.js"></script>
       <script src="{{ asset('public/bower_components/angular-scroll-glue/src/scrollglue.js') }}"></script>
   		 <script src="{{ asset('public/bower_components/angular-right-click/src/ng-right-click.js') }}"></script>

        <script src="{{ asset('public/bower_components/angular-route/angular-route.js') }}"  type="text/javascript"></script>
        <script src="{{ asset('public/bower_components/angular-ui-router/release/angular-ui-router.js') }}"  type="text/javascript"></script>
         <script src="{{ asset('public/bower_components/angular-bootstrap/ui-bootstrap.js') }}"  type="text/javascript"></script>
        <script src="{{ asset('public/bower_components/angular-bootstrap/ui-bootstrap-tpls.js') }}"  type="text/javascript"></script>
        <script src="{{ asset('public/bower_components/angular-validation/dist/angular-validation.js') }}"></script>
        <script src="{{ asset('public/bower_components/angular-validation/dist/angular-validation-rule.js') }}"></script>
        <script src="{{ asset('public/bower_components/angularjs-datepicker/dist/angular-datepicker.js') }}"></script>
        <script src="{{ asset('public/bower_components/angularjs-social-login/angularjs-social-login.js') }}"></script>
        <script src="{{ asset('public/bower_components/moment/moment.js') }}"></script>
        <script src="{{ asset('public/bower_components/wdt-emoji-bundle/emoji.js') }}"></script>
        <script src="{{ asset('public/bower_components/wdt-emoji-bundle/wdt-emoji-bundle.min.js') }}"></script>
        <script src="{{ asset('public/bower_components/angucomplete-alt/angucomplete-alt.js') }}"></script>

        <script src="{{ asset('public/bower_components/angular-ui-sortable/sortable.js') }}"></script>
        <script src="{{ asset('public/bower_components/angular-socialshare/dist/angular-socialshare.js') }}"></script>
        <script src="{{ asset('public/bower_components/angular-google-places-autocomplete/src/autocomplete.js') }}"></script>
        <script src="{{ asset('public/bower_components/angular-scroll/angular-scroll.js') }}"></script>
        <script src="{{ asset('public/bower_components/ng-img-crop/compile/unminified/ng-img-crop.js') }}"></script>
        <script src="{{ asset('public/bower_components/angular-file-upload/dist/angular-file-upload.js') }}"></script>
        <script src="{{ asset('public/bower_components/ngInfiniteScroll/build/ng-infinite-scroll.js') }}"></script>
         <script src="https://rawgithub.com/thgreasi/tg-dynamic-directive/master/src/tg.dynamic.directive.js"></script>

         <script type="text/javascript" src="{{ asset('public/bower_components/angular-toastr/dist/angular-toastr.tpls.js') }}"></script>
         <script type="text/javascript" src="{{ asset('public/bower_components/angularjs-dropdown-multiselect/src/angularjs-dropdown-multiselect.js') }}"></script>
      <!--   <script src="{{ asset('public//bower_components/angular-tooltips/dist/angular-tooltips.min.js') }}"></script> -->

        <!--<link rel="stylesheet" href="{{ asset('public//bower_components/ng-dialog/css/ngDialog.min.css') }}">
        <link rel="stylesheet" href="{{ asset('public//bower_components/ng-dialog/css/ngDialog-theme-default.min.css') }}">
        <script src="{{ asset('public//bower_components/ng-dialog/js/ngDialog.min.js') }}"></script>-->

        <!--<script src="{{ asset('public//bower_components/jquery/dist/jquery.js') }}"  type="text/javascript"></script>-->


        <script src="{{ asset('public/build/module.js?version=' . time()) }}"  type="text/javascript"></script>
    </body>
</html>
