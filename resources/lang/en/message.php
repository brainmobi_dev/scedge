<?php

return [
	'VALID_TOKEN' 					=> 'Provided token is valid',
	'INVALID_TOKEN' 				=> 'Provided token is invalid',
	'FAILED_TOKEN'					=> 'Provided token is invalid',

	'PROCESS_SUCCESS' 				=> 'Your request has been processed',
	'SUCCESS_APPLIED'				=> 'You have successfully applied',
	'PROBLEM_OCCURED'				=> 'Some Problem Occurred',
	'INVALID_REQUEST'				=> 'Invalid Request',
	'VALIDATION_FAIL'				=> 'Required fields has not been provided',
	'INVALID_EMAIL_PASSWORD'    	=> 'Please enter a valid email and password',
	'USER_NOT_EXIST'				=> 'You are not registered with scedge, Please select a membership and complete your registration.',
	'ACTIVATE_USER'					=> 'We have sent you a verification email. Please click verification link to proceed.',
	'SUCCESS_TOKEN'					=> 'Please login with your credential',

	'FACEBOOK_USER'					=> 'Please login with your facebook account.',

	'SETTING_UPDATED'				=> 'Setting updated successfully',
	'SETTING_CREATED'				=>	'Setting created successfully',

	'ALREADY_REGISTERED'			=> 'Email already registered with scedge',
	'EMAIL_AVAILABLE'				=> 'Email not registred',
	'ACCOUNT_VERIFIED_SUCCESSFULLY'	=> 'You have successfully verified your account. Please click the button below to continue.',
	'INVALID_VERIFICATION_CODE'	=> 'You have provided a wrong / expired verification code.',
	'PASSWORD_CHANGED' => 'Your password has been changed, Please check your email for latest password'
];
