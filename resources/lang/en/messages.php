<?php

return [
	'VALID_TOKEN' 				=> 'Provided token is valid',
	'INVALID_TOKEN' 			=> 'Provided token is invalid',
	'FAILED_TOKEN'				=> 'Provided token is invalid',

	'PROCESS_SUCCESS' 			=> 'Your request has been processed',
	'PROBLEM_OCCURED'			=> 'Some Problem Occurred',
	'DUPLICATE_GROUP'			=> 'Group name allready exist',
	'INVALID_REQUEST'			=> 'Invalid Request',
	'VALIDATION_FAIL'			=> 'Required fields has not been provided',
	'INVALID_EMAIL_PASSWORD'    => 'Please enter a valid email and password',
	'USER_NOT_EXIST'			=> 'You are not registered with scedge, Please select a membership and complete your registration.',
	'ACTIVATE_USER'				=> 'We have sent you a verification email. Please click verification link to proceed.',
	'SUCCESS_TOKEN'				=> 'Please login with your credential',
	'ALREADY_ACTIVE'			=> 'Email is already active state, Please login with your credentials.',
	'RESET_VERIFICATION'  => 'Please check your email for verificaiton link.',
	'EMAIL_NOT_EXIST'  		=> 'Email is not registered with Scedge, Please signup to enjoy our services.',
	'VALID_ACTIVATION_CODE' => 'Valid activation code',
	'NOT_ACTIVE' 				=> 'You cannot use this feature as account is not active.',
	'NOT_VERIFIED'						=> 'You cannot use this feature as account is not verified.',
	'PASSWORD_CHANGED_EMAIL' 	=> 'Password change request has been processed, Please check your email',
	'INVALID_ACTIVATION_CODE' => 'You have provided invalid verification code. Please try again with valid code.',

	'FACEBOOK_USER'							=> 'Please login with your facebook account.',

	'SETTING_UPDATED'						=> 'Setting updated successfully',
	'SETTING_CREATED'						=>	'Setting created successfully',

	'ALLREDAY_REGISTERED'				=> 'Email already registered',
	'EMAIL_AVAILABLE'						=> 'Email not registred',
	'PASSWORD_NOT_MATCHING' 		=> 'Password not matching',
	'GROUP_ALLREDAY_EXIST'			=> 'Contact group allready exist',
	'RESET_PASSWORD_SUCCESS'		=> 'Your password has been reset successfully',
	'DUPLICATE_CARD'						=> 'Your password has been reset successfully',

	'UNAUTHRIZED_ACCESS' 				=> 'You cannot update this card',
	'USER_ALREADY_EXIST' 				=> 'User with the email already exist, Please use a different email',
	'USER_BLOCKED' 							=> 'User with the email has been blocked by you, Please contact scedge admin',
	'INVITATION_ALREADY_EXIST' 	=> 'User has already been requested by you',
	'INVITATION_SEND' 					=> 'Invitation request has been send to your friend',

	'CARD_NOTIFICATION_ACTIVE' => 'Item notification has been switched on',
	'CARD_NOTIFICATION_DEACTIVE' => 'Item notification has been switched off',

	'ALREADY_REPORTED_CARD' => 'Card has already been reported by you',
	'ALREADY_REPORTED_USER' => 'User has already been reported by you',

	'BLOCKED_USER' 					=> 'User has been blocked successfully',
	'UNBLOCKED_USER'				=> 'User has been unblocked successfully',

	'SUCCESSFULLY_SHARED'   => 'Card has been shared with provided email',
];
