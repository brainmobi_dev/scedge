## Dependen	cies

1. Laravel - 5.4
2. Angular - 1.6
3. Stripe
4. jwt auth
5. bower
6. gulp

---------------------------------------------

## Installation

1. composer install or composer update
2. .env.example > .env
3. change database and other credentials
4. npm install on parent folder
5. bower install on public folder
6. install all dependencies of <gulp>
7. gulp build on public folder (for local build)
8. gulp build-main on public folder (for server build)

----------------------------------------------

## Database

1. php artisan migrate
2. run sql file from sql_dump folder
3. php artisan db:seed

----------------------------------------------

## Architecture

1. Website (Laravel)
2. Admin (Laravel)

-----------------------------------------------

1. module (Angular)
2. admin-module(Angular)

-----------------------------------------------
## Libraries

1. vendor (Laravel)
2. public/bower_components (Angular)

