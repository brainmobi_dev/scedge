ALTER TABLE `card_types` ADD COLUMN `parent_group_id` INT(10) DEFAULT 0 NOT NULL AFTER `image`;
ALTER TABLE `card_types` ADD COLUMN `owner_id` INT(10) DEFAULT 0 NOT NULL AFTER `image`;
