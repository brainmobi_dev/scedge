insert  into `user_subscription`(`id`,`subscription_name`,`subscription_amount`,`is_active`,`created_at`,`updated_at`) values (1,'FREE','0',1,NULL,NULL),(2,'PERSONAL','100',1,NULL,NULL),(3,'PROFESSIONAL','200',1,NULL,NULL);

insert  into `card_types`(`id`,`type`,`image`,`is_active`) values (1,'Home',NULL,1),(2,'Family',NULL,1),(3,'Office',NULL,1);

insert  into `contact_group`(`id`,`name`,`is_active`) values (1,'Family',1),(2,'Friend',1),(3,'Office',1);