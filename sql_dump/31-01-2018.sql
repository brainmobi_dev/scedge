ALTER TABLE `report_abuse` CHANGE `card_id` `item_id` INT(11) UNSIGNED NOT NULL, ADD COLUMN `item_type` ENUM('card','user') CHARSET utf8 COLLATE utf8_bin DEFAULT 'card' NULL;
ALTER TABLE `user_cards` ADD COLUMN `allow_notification` TINYINT(1) DEFAULT 1 NULL AFTER `is_active`;
CREATE TABLE `blocked_contact`( `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT, `user_id` INT(10) NOT NULL, `blocked_by` INT(10) NOT NULL, `created_at` TIMESTAMP DEFAULT NOW(), `updated_at` TIMESTAMP DEFAULT NOW(), PRIMARY KEY (`id`) ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_bin;
ALTER TABLE `report_abuse` CHANGE `created_at` `created_at` TIMESTAMP DEFAULT NOW() NOT NULL, CHANGE `updated_at` `updated_at` TIMESTAMP DEFAULT NOW() NOT NULL;
ALTER TABLE `user_cards` ADD COLUMN `showcase_order` INT(10) DEFAULT 0 NULL AFTER `card_order`; 
