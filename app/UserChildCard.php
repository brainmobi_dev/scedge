<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class UserchildCard extends Authenticatable
{
    use Notifiable;

    protected $table = 'user_child_cards';

    public $timestamps = true;

}
