<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class ChatHistory extends Authenticatable
{
    use Notifiable;

    protected $table = 'chat_history';
}
