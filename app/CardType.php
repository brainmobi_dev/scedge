<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class CardType extends Authenticatable
{
    use Notifiable;

    protected $table = 'card_types';
    public $timestamps = false;
}
