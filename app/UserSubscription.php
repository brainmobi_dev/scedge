<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class UserSubscription extends Authenticatable
{
    use Notifiable;

    protected $table = 'user_subscription';

    protected $fillable = ['subscription_code', 'subscription_name', 'subscription_amount', 'subscription_type','is_active'];
}
