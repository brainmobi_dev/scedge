<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class ContactGroup extends Authenticatable
{
    use Notifiable;

    protected $table = 'contact_group';
    
    protected $fillable = [ 'user_id', 'name', 'description','image', 'is_active'];
    public $timestamps = false;
}
