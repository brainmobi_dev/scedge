<?php

namespace App;

use DB;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class ReportAbuse extends Model
{
    use Notifiable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'report_abuse';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'user_id', 'item_id', 'item_type', 'report_id' ];

    public $timestamps = true;
}
