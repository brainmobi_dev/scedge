<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Profile extends Authenticatable
{
    use Notifiable;

    protected $table = 'profile';

    protected $fillable = ['user_id', 'gender', 'dob', 'city', 'province', 'country', 'profile_image', 
    						'cover_image', 'company_name', 'website', 'phone_number', 'business_hour'];

    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public $timestamps = true; 

}
