<?php

namespace App\Custom;

use App\Contact;

class Customhelper {
    /**
     * @param string path
     * 
     * @return string
     */
  public static function setActive($path, $request, $active = 'active')
    {
      $uri = $request->path();
      
      if($path == $uri){
          return $active;
      }
      
    }
    
    /**
     * @param string contactGroupId
     * @param string userId
     * 
     * @return string
     */
    public static function getGroupCount($contactGroupId , $userId ,$data = false){
       
        $contact = Contact::leftJoin('contact_group','contact_group.id','=','contacts.contact_group_id')
                            ->select('contacts.*','contact_group.*')
                            ->Where('contacts.owner_id',$userId)
                            ->Where('contact_group_id',$contactGroupId);
                                            
        if($data == true){
           $allMembers = $contact->get();
           
        } else {
           $allMembers = $contact->count(); 
        }
        return $allMembers;
    }
    
  
}