<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class ContactInvitation extends Authenticatable
{
    use Notifiable;

    protected $table = 'contact_invitation';
    public $timestamps = false;
}
