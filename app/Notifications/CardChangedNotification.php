<?php

namespace App\Notifications;

use App\UserNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class CardChangedNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        UserNotification::insert([
            'user_id' =>  $notifiable->id,
            'section' => 'item',
            'message' => 'Card with title ' . $notifiable->title . ' has been changed by ' . $notifiable->user_name
        ]);

        return (new MailMessage)
                    ->greeting('Hi, ' . title_case($notifiable->name))
                    ->line('Your card with title ' . $notifiable->title . ' has been changed by ' . $notifiable->user_name)
                    ->line('Thank you for using Scedge!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
