<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class UserNotification extends Authenticatable
{
    use Notifiable;

    protected $table = 'user_notifications';
    public $timestamps = true;

    public static function removeNotification($userId, $section = 'item'){
        return static::where('user_id', $userId)->where('section', $section)->update('is_active', 0);
    }
}
