<?php

namespace App;

use DB;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'subscription_id', 'registration_stage', 'verification_code', 'activation_code', 'facebook_token', 'google_token', 'linkedin_token', 'is_active', 'firebase_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'facebook_token', 'google_token', 'card_id', 'stripe_id', 'password', 'remember_token', 'is_active', 'is_verified', 'linkedin_token'
    ];


    public static function getUserDetail($condition = []) {
        $userQuery = User::join('user_subscription', 'user_subscription.id', '=', 'users.subscription_id')
                                    ->select(DB::raw('users.id as user_id'), 'users.name', 'users.email', 'users.registration_stage', 'users.is_verified', 'users.subscription_id', 'user_subscription.subscription_name', 'user_subscription.subscription_amount', 'user_subscription.subscription_type',
                                        'users.verification_code');

        foreach($condition as $field => $value) {
            $userQuery = $userQuery->where($field, $value);
        }

        return $userQuery->first();
    }
}
