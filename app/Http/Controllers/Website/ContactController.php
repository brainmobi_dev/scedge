<?php

namespace App\Http\Controllers\Website;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;
use JWTAuthException;

use DB;
use App\User;
use App\UserCard;
use App\Contact;
use App\ContactGroup;
use App\ContactInvitation;
use App\ReportAbuse;
use App\BlockedContact;
use J42\LaravelFirebase\LaravelFirebaseFacade as Firebase;

use App\Notifications\ContactRequestNotification;
use App\Notifications\ContactRejectNotification;
use App\Notifications\ContactAcceptNotification;
use App\Notifications\InvitationNotification;

class ContactController extends Controller
{
    public function getCurrentUser($request) {
      return JWTAuth::toUser($request->token);
    }

    public function getContactGroup(Request $request){
      $authUser = $this->getCurrentUser($request);

      $result = ContactGroup::where( 'user_id', $authUser->id )->orWhere( 'user_id', 0 );


      if($groupId = $request->get('group', '')) {
          $result = $result->where('id', $groupId);
      }
      $result = $result->get();

      return response()->json([
        'error_code'=>200,
        'msg_string'=> __('messages.PROCESS_SUCCESS'),
        'result' => $result
      ]);
    }


    public function getUserGroup(Request $request){
        $authUser = $this->getCurrentUser($request);
        if($userId = $request->get('userId', '')) {
            $userContacts = Contact::join('contact_group', 'contact_group.id', '=', 'contacts.contact_group_id')
                      ->where('owner_id', $authUser->id)
                      ->where('contact_id', $userId)
                      ->select('contact_group.id', 'contact_group.name');

            $userContacts = $userContacts->get();

            return response()->json([
              'error_code'=>200,
              'msg_string'=> __('messages.PROCESS_SUCCESS'),
              'result' => $userContacts
            ]);
        }

        return response()->json([
          'error_code'=>500,
          'msg_string'=> __('messages.INVALID_REQUEST'),
          'result' => []
        ]);
    }

    public function uploadImage($imageData){
      if($imageData) {
        $pos = strpos($imageData, 'http');

        if($pos === false) {
          $extenstion = preg_replace('#^data:image/\w+;base64,#i', '', $imageData);
          $extenstion = str_replace(['data:', 'image/', ';base64,', $extenstion], ['', '', '', ''], $imageData);

          $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $imageData));
          $photoName= uniqid('contactGroupImg_') . '.' . $extenstion;
          $photoPath = env('APP_UPLOAD_FOLDER', '/public/images/uploads/') . $photoName;
          file_put_contents( $_SERVER['DOCUMENT_ROOT'] . $photoPath, $data);
          $imageData = env('APP_URL') . $photoPath;
        }
      }
     return $imageData;
    }

    public function removeContactGroup(Request $request) {
        $authUser = $this->getCurrentUser($request);
        $groupId = $request->get('group', null);

        $contactQuery = ContactGroup::where( 'id', $groupId)->where('user_id', $authUser->id);

        if($contactQuery->count()) {
            Contact::where('contact_group_id', $groupId)->delete();
            $contactQuery->delete();

            return response()->json([
              'error_code'=>200,
              'msg_string'=> __('messages.PROCESS_SUCCESS'),
              'result' => ContactGroup::get()
            ]);
        }

        return response()->json([
              'error_code'=>500,
              'msg_string'=> __('messages.INVALID_REQUEST'),
              'result' => []
        ]);

    }

    public function addContactGroup(Request $request){
        $authUser = $this->getCurrentUser($request);

        $groupTitle = $request->get('title', null);

        $groupTitle = strtoupper($groupTitle);

        $result = ContactGroup::where( 'name', $groupTitle )
                                ->where( function($query) use($authUser){
                                    return $query->where('user_id', $authUser->id)
                                                ->orWhere('user_id', 0);
                                })
                                ->count();

        if($result){

            return response()->json([
                  'error_code'=>500,
                  'msg_string'=> __('messages.GROUP_ALLREDAY_EXIST'),
                  'result' => []
                ]);

        }



        $userGroup = [
            'user_id'     => $authUser->id,
            'name'        => $groupTitle,
            'description' => $request->get('description', ''),
            'image'       => $this->uploadImage($request->get('group_image', ''))
        ];

        ContactGroup::insert($userGroup);

        return response()->json([
          'error_code'=>200,
          'msg_string'=> __('messages.PROCESS_SUCCESS'),
          'result' => ContactGroup::get()
        ]);
    }

    public function approveConnection(Request $request){
      $authUser = $this->getCurrentUser($request);

      $userID = $request->only('userID');

      Contact::where( 'owner_id', $userID )
              ->where( 'contact_id', $authUser->id )
              ->update( [ 'is_approve' => 1 ] );

      $contactDetail = User::where('id', $userID)->first();

      if($contactDetail) {
        $contactDetail->user_name = $authUser->name;
        $contactDetail->notify(new ContactAcceptNotification());
      }

      return $this->pendingRequest($request);
    }

    public function rejectConnection(Request $request){
      $authUser = $this->getCurrentUser($request);

      $userID = $request->only('userID');

      $contactRequest = Contact::where( 'owner_id', $userID )
              ->where( 'contact_id', $authUser->id )
              ->first();

      if($contactRequest) {
        $contactRequest->delete();
      }

      return $this->pendingRequest($request);

    }

    public function pendingRequest(Request $request){

      $authUser = $this->getCurrentUser($request);

      $contactResult = Contact::join('users', 'users.id', '=', 'contacts.owner_id')
                              ->join('profile', 'profile.user_id', '=', 'users.id')
                              ->where( 'contacts.contact_id', $authUser->id )
                              ->where( 'is_approve', 0 )
                              ->select( 'contacts.*', 'users.name', 'profile.profile_image', 'profile.cover_image' )->paginate(12);

      $result = $this->unique_multidim_array( $contactResult, 'owner_id' );

      return response()->json([
        'error_code'=>200,
        'msg_string'=> __('messages.PROCESS_SUCCESS'),
        'result' => $result
      ]);

    }

    public function getContact(Request $request){
      $authUser = $this->getCurrentUser($request);

      $blockedUsers = BlockedContact::where('blocked_by', $authUser->id)
                      ->select('user_id')
                      ->get();
      $blockedUserList = (array_column($blockedUsers->toArray(), 'user_id'));

      $query = User::join('profile', 'profile.user_id', '=', 'users.id')
                    ->where('users.id', '<>', $authUser->id)
                    ->where('users.is_active', '=', 1)
                    ->select('users.id', 'users.name', 'users.email', 'profile.profile_image', 'profile.cover_image');

      if( count($blockedUserList) ){
          $query = $query->whereNotIn('users.id', $blockedUserList);
      }

      if($filter = $request->get('filter')) {
          if(isset($filter['group'])) {
              if( !empty( $filter['group'] ) ) {

                $query = Contact::join('users', 'users.id', '=', 'contacts.contact_id')
                                ->join('profile', 'profile.user_id', '=', 'users.id')
                                ->join('contact_group', 'contact_group.id', '=', 'contacts.contact_group_id')
                                ->where(function($query) use($authUser){
                                    return $query->where('contacts.owner_id', '=', $authUser->id)
                                                  ->orWhere('contacts.contact_id', '=', $authUser->id);
                                })
                                ->where('users.id', '<>', $authUser->id)
                                ->where('contacts.is_active', '=', 1)
                                ->where('contacts.is_approve', '=', 1)
                                ->select('users.id', 'users.name', 'users.email', 'profile.profile_image', 'profile.cover_image', 'contacts.contact_group_id');

              } else {
                  unset($filter['group']);
              }
          }

          if(isset($filter['sortBy'])) {
              foreach($filter['sortBy'] as $field => $value) {
                switch($field) {
                   case "sortByName" : $query = $query->orderBy('users.name', $value); break;
                }
              }

              unset($filter['sortBy']);

          }

          foreach($filter as $field => $value) {
            if(trim($value)) {
              switch($field) {
                case "name" : $query = $query->where('users.name', 'like', '%' . $value . '%'); break;
                case "group" : $query = $query->where('contacts.contact_group_id', '=', $value); break;
              }
            }
          }
      }

      $result = $query->paginate(12);

      foreach ($result as $key => $value) {

          $tempResult = Contact::where(function($query) use($authUser, $value){
                                      return $query->where('owner_id', $authUser->id )
                                                   ->where( 'contact_id', $value['id']);
                                  })
                                 ->orWhere(function($query)  use($authUser, $value){
                                      return $query->where('contact_id', $authUser->id )
                                                   ->where( 'owner_id', $value['id']);
                                  })
                                 ->where( 'is_approve', 1 )
                                 ->select( 'id' )->first();

          if( !empty( $tempResult['id'] ) ){
            $result[$key]['isDelete'] = '1';
          }else{
            $result[$key]['isDelete'] = '0';
          }

          if( empty($value['contact_group_id']) ){

            $result[$key]['contact_group_id'] = '';

          }

      }

      $authFirebaseUser = User::join('profile', 'profile.user_id', '=', 'users.id')
                                ->where('users.id', $authUser->id)
                                ->select('profile.profile_image', 'users.name', 'users.email', 'users.id', 'users.firebase_id')
                                ->first();


      $authUserFirebaseIdentifier = $this->getFirebaseIdentifier($authFirebaseUser);
      $authUserFirebaseIdentifier = ($authUserFirebaseIdentifier) ? $authUserFirebaseIdentifier : $this->createFirebaseIdentifier($authFirebaseUser);
      $result = $this->getFirebaseChatRoomID($authUserFirebaseIdentifier, $result);

    	return response()->json([
    		'error_code'=>200,
    		'msg_string'=> __('messages.PROCESS_SUCCESS'),
        'result' => $result
    	]);
    }

    public function getFirebaseChatRoomID($currentUser, $users) {
      $userList = Firebase::get('/userList/' . $currentUser . "/chatRoomMate");
      $userList = (json_decode($userList->getContents()));

      foreach ($users as $key => $value) {

        $users[$key]['chatRoomId'] = '';

        if( isset( $value->email ) && !empty( $value->email ) ){

          if( isset( $userList ) && !empty( $userList ) ){

            foreach($userList as $firebaseId => $user) {

              if( property_exists( $user, 'chatMate' ) ){

                if($value->email == $user->chatMate) {

                  $users[$key]['chatRoomId'] = $user->chatRoom;
                }

              }

            }

          }

        }

      }

      return $users;
    }

    public function getFirebaseIdentifier($currentUser) {
       $userList = Firebase::get('/userList');

       $userList = (json_decode($userList->getContents()));
       if($userList) {
         foreach($userList as $firebaseId => $user) {
            if( property_exists( $user, 'email' ) ){
              if($currentUser->email == $user->email) {
                if($firebaseId != $currentUser->firebase_id) {
                    User::where('id', $currentUser->id)->update(['firebase_id' => $firebaseId]);
                }
                return $firebaseId ;
              }
            }
         }
       }

       return null;
    }

    public function createFirebaseIdentifier($currentUser) {
           $userDetail =  [
                 'name' => $currentUser->name,
                 'email' => $currentUser->email,
                 'photoURL' => $currentUser->profile_image
           ];
           $response = Firebase::push('/userList', $userDetail);

           $firebaseIdentifier = $this->getFirebaseIdentifier($currentUser);

           User::where('id', $currentUser->id)->update(['firebase_id' => $firebaseIdentifier]);
           return $firebaseIdentifier;
    }

    public function isUserContact($currentUser, $contactUser) {
       $userList = Firebase::get('/userList/' . $currentUser . "/chatRoomMate");

       $userList = (json_decode($userList->getContents()));
       if( !empty($userList) ) {
         foreach($userList as $firebaseId => $user) {
          if( isset($user->chatMate) ){
            if($user->chatMate == $contactUser['email']) {
              return true ;
            }
          }
         }
       }
       return false;
    }

    public function addContact(Request $request){
        $authUser = $this->getCurrentUser($request);

        $groups  = $request->get('group', null);
        $contact = $request->get('contactId', null);

        if($groups && $contact) {
            $previousContactRowset = Contact::where('owner_id', $authUser->id)
                     ->where('contact_id', $contact)
                     ->get();

            foreach($previousContactRowset as $previousContactRow) {
                  $previousContactRow->delete();
            }

            foreach($groups as $group) {
                if( ! Contact::where('contact_group_id', $group)
                       ->where('owner_id', $authUser->id)
                       ->where('contact_id', $contact)
                       ->count()) {

                    Contact::insert([
                        'contact_group_id' => $group,
                        'owner_id' => $authUser->id,
                        'contact_id' => $contact
                    ]);
                }

            }

            $contactDetail  = User::where('id', $contact)
                                      ->first();

            if($contactDetail) {
              $contactDetail->user_name = $authUser->name;
              $contactDetail->notify(new ContactRequestNotification());
            }

            $authFirebaseUser = User::join('profile', 'profile.user_id', '=', 'users.id')
                                      ->where('users.id', $authUser->id)
                                      ->select('profile.profile_image', 'users.name', 'users.email', 'users.id', 'users.firebase_id')
                                      ->first();
            $contactFirebaseDetail  = User::join('profile', 'profile.user_id', '=', 'users.id')
                                      ->where('users.id', $contact)
                                      ->select('profile.profile_image', 'users.name', 'users.email', 'users.id', 'users.firebase_id')
                                      ->first();

            $authUserFirebaseIdentifier     =  $this->getFirebaseIdentifier($authFirebaseUser);

            $authUserFirebaseIdentifier     =  ($authUserFirebaseIdentifier) ? $authUserFirebaseIdentifier : $this->createFirebaseIdentifier($authFirebaseUser);
            $contactUserFirebaseIdentifier  =  $this->getFirebaseIdentifier($contactFirebaseDetail);
            $contactUserFirebaseIdentifier  =  ($contactUserFirebaseIdentifier) ? $contactUserFirebaseIdentifier : $this->createFirebaseIdentifier($contactFirebaseDetail);;

            if( $authUserFirebaseIdentifier && $contactUserFirebaseIdentifier ) {
                if( !$this->isUserContact($authUserFirebaseIdentifier, $contactDetail)) {
                  Firebase::push('/userList/' . $authUserFirebaseIdentifier . '/chatRoomMate', [
                       'chatMate' => $contactDetail->email,
                       'chatRoom' => md5($authUser->email) . "-" . md5($contactDetail->email)
                  ]);

                  Firebase::push('/userList/' . $contactUserFirebaseIdentifier . '/chatRoomMate', [
                       'chatMate' => $authUser->email,
                       'chatRoom' => md5($authUser->email) . "-" . md5($contactDetail->email)
                  ]);
                }
            }

            $userList = Firebase::get('/userList/' . $authUserFirebaseIdentifier . "/chatRoomMate");
            $userList = (json_decode($userList->getContents()));

            $result['chat_room_id'] = '';

            if( isset( $contactDetail['email'] ) && !empty( $contactDetail['email'] ) ){

              if( isset( $userList ) && !empty( $userList ) ){

                foreach($userList as $firebaseId => $user) {

                  if( property_exists( $user, 'chatMate' ) ){

                    if($contactDetail['email'] == $user->chatMate) {

                      $result['chat_room_id'] = $user->chatRoom;

                    }

                  }

                }

              }

            }

            Contact::where(function($query) use($authUser, $contact){
                                      return $query->where('owner_id', $authUser->id )
                                                   ->where( 'contact_id', $contact);
                                  })
                                 ->orWhere(function($query)  use($authUser, $contact){
                                      return $query->where('contact_id', $authUser->id )
                                                   ->where( 'owner_id', $contact);
                                  })
                                 ->update($result);

            $userContacts = Contact::join('contact_group', 'contact_group.id', '=', 'contacts.contact_group_id')
                      ->where('owner_id', $authUser->id)
                      ->where('contact_id', $contact)
                      ->select('contact_group.id', 'contact_group.name')
                      ->get();

            return response()->json([
          		'error_code'=>200,
          		'msg_string'=> __('messages.PROCESS_SUCCESS'),
          		'result' => $userContacts
          	]);
        }

        return response()->json([
      		'error_code'=>500,
      		'msg_string'=> __('messages.INVALID_REQUEST'),
      		'result' => []
      	]);
    }

    public function updateContact(Request $request){}

    public function deleteContact(Request $request){

        $authUser = $this->getCurrentUser($request);

        if( $params = $request->only('id', 'contact_group_id') ) {

          $contactRow = Contact::where(function($query) use($authUser, $params){
                                      return $query->where('owner_id', $authUser->id )
                                                   ->where( 'contact_id', $params['id']);
                                  })
                                 ->orWhere(function($query)  use($authUser, $params){
                                      return $query->where('contact_id', $authUser->id )
                                                   ->where( 'owner_id', $params['id']);
                                  });


          if( !empty( $params['contact_group_id'] ) ){

              $contactRow = $contactRow->where('contact_group_id', $params['contact_group_id']);
          }

          $contactRow->delete();

          $userId   = $request->get('id');

          if(
            !BlockedContact::where('user_id', $userId)
                  ->where('blocked_by', $authUser->id)
                  ->count()
          ) {
            BlockedContact::create([
              'user_id'     => $userId,
              'blocked_by'  => $authUser->id
            ]);
          }

          return response()->json([
              'error_code'=>200,
              'msg_string'=> __('messages.PROCESS_SUCCESS'),
              'result' => []
          ]);

        }

        return response()->json([
          'error_code'=>500,
          'msg_string'=> __('messages.INVALID_REQUEST'),
          'result' => []
        ]);

    }

    public function unique_multidim_array($array, $key)
    {
        $temp_array = array();
        $i = 0;
        $key_array = array();

        foreach($array as $val) {

            if (!in_array($val[$key], $key_array)) {
                $key_array[$i] = $val[$key];
                $temp_array[$i] = $val;
            }
            $i++;
        }
        return $temp_array;
    }


        public function sendInvitation(Request $request) {
          $authUser = $this->getCurrentUser($request);
          $friendEmail = $request->get('email', '');

          if($friendEmail) {
            $isExistingUser = User::where('email', $friendEmail)->count();

            if(!$isExistingUser) {
                $isExistingUser = ContactInvitation::where('email', $friendEmail)
                                                    ->where('owner_id', $authUser->id)
                                                    ->count();
            } else {
              $friendUsers = User::where('email', $friendEmail)->first();

              if(
                BlockedContact::where('blocked_by', $authUser->id)
                              ->where('user_id', $friendUsers->id)
                              ->count()
              ) {
                  return response()->json([
                    'error_code'=>500,
                    'msg_string'=> __('messages.USER_BLOCKED'),
                    'result' => []
                  ]);
              } else {
                  return response()->json([
                    'error_code'=>500,
                    'msg_string'=> __('messages.USER_ALREADY_EXIST'),
                    'result' => []
                  ]);
              }
            }

            if(!$isExistingUser) {
              if(true) {

                ContactInvitation::insert([
                  'email' => $friendEmail,
                  'owner_id' => $authUser->id
                ]);

                $contactDetail = $authUser;

                $contactDetail->email = $friendEmail;
                if($contactDetail) {
                  $contactDetail->user_name = $authUser->name;
                  $contactDetail->name = $friendEmail;
                  $contactDetail->notify(new InvitationNotification());
                }

                return response()->json([
                  'error_code'=>200,
                  'msg_string'=> __('messages.INVITATION_SEND'),
                  'result' => []
                ]);
              }
            } else {
                return response()->json([
                  'error_code'=>500,
                  'msg_string'=> __('messages.INVITATION_ALREADY_EXIST'),
                  'result' => []
                ]);
            }
          }

          return response()->json([
            'error_code'=>500,
            'msg_string'=> __('messages.INVALID_REQUEST'),
            'result' => []
          ]);
        }

    public function blockUser(Request $request) {
        $authUser = $this->getCurrentUser($request);
        $userId   = $request->get('user_id');

        if(
          !BlockedContact::where('user_id', $userId)
                ->where('blocked_by', $authUser->id)
                ->count()
        ) {
          BlockedContact::create([
            'user_id'     => $userId,
            'blocked_by'  => $authUser->id
          ]);
        }

        return response()->json([
          'error_code'=> 200,
          'msg_string'=> __('messages.BLOCKED_USER'),
          'result' => []
        ]);
    }

    public function unblockUser(Request $request) {
      $authUser = $this->getCurrentUser($request);
      $userId   = $request->get('user_id');

      $blockedContactRow = BlockedContact::where('user_id', $userId)
            ->where('blocked_by', $authUser->id)
            ->first();

      if($blockedContactRow) {
        $blockedContactRow->delete();
      }

      return response()->json([
        'error_code'=> 500,
        'msg_string'=> __('messages.UNBLOCKED_USER'),
        'result' => []
      ]);
    }

    public function reportAbuse(Request $request) {
        $authUser = $this->getCurrentUser($request);

        $insertData = [
          'user_id' => $authUser->id,
          'item_id' => $request->get('user_id'),
          'item_type' => 'user'
        ];

        if(
          ReportAbuse::where('user_id', $insertData['user_id'])
                      ->where('item_id', $insertData['item_id'])
                      ->where('item_type', 'user')
                      ->count()
        ) {
          return response()->json([
            'error_code'=> 500,
            'msg_string'=> __('messages.ALREADY_REPORTED_USER'),
            'result' => []
          ]);
        }

        $reportList = $request->get('report_id');

        foreach($reportList as $reportId) {
              $insertData['report_id'] = $reportId;
              ReportAbuse::insert($insertData);
        }



        return response()->json([
          'error_code'=>200,
          'msg_string'=> __('messages.PROCESS_SUCCESS'),
          'result' => []
        ]);
    }
}
