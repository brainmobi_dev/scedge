<?php

namespace App\Http\Controllers\Website;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;
use JWTAuthException;

use App\User;
use App\Profile;
use App\UserSubscription;
use App\Country;
use App\City;
use App\Region;
use App\ContactInvitation;
use App\Contact;
use J42\LaravelFirebase\LaravelFirebaseFacade as Firebase;

use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Stripe\Error\Card;


use App\Notifications\RegistrationNotification;
use App\Notifications\ResetpasswordNotification;

class UserController extends Controller
{
    public function __construct() {
      $this->stripe = Stripe::make(env('STRIPE_SECRET'));
    }

    public function uploadImage(Request $request){
        $file = $request->file('file');

        $name = time().'.'.$file->getClientOriginalExtension();
        $file->move('images/uploads', $name);

        return 'images/uploads/' . $name;
    }

    public function googleIssue() { echo "Some Google issue occured"; }
    public function facebookIssue() { echo "Some Facebook issue occured"; }
    public function linkdinIssue() { echo "Some Linkdin issue occured"; }

    public function testing(Request $request){

          $userDetail =  [
                'name' => "aman",
                'email' => "aman@gmail.com",
                'photoURL' => ""
          ];

          $response = Firebase::push('/userList', $userDetail);

          $authUserFirebaseIdentifier = $this->getFirebaseIdentifier($userDetail);
          dd($authUserFirebaseIdentifier);
    }

    public function createSubscription() {
        $plans = $this->stripe->plans()->all();
        foreach($plans['data'] as $plan) {
            UserSubscription::create([
                'subscription_code' => $plan['id'],
                'subscription_name' => strtoupper(($plan['name'])),
                'subscription_amount' => $plan['amount'],
                'subscription_type' => (( $plan['id'] == 'FREE') ? 'ALL' : (($plan['interval'] == 'year') ? 'ANNUAL' : 'MONTHLY')),
                'is_active' => 1
            ]);
        }
    }

    public function resetPassword(Request $request) {
        $email = $request->get('email', '0');

        if($email) {
            $userDetail = User::where('email', $email)->first();
            if($userDetail) {
                if( !$userDetail->is_active ) {
                  return response()->json([
                    'error_code'  => 500,
                    'title'       => "Inactive User",
                    'msg_string'  => __('messages.NOT_ACTIVE')
                  ]);
                }

                if( !$userDetail->is_verified ) {
                  return response()->json([
                    'error_code'  => 500,
                    'title'       => "Unverified User",
                    'msg_string'  => __('messages.NOT_VERIFIED')
                  ]);
                }

                $userDetail->activation_code = md5($userDetail->email . "-" . time());
                $userDetail->save();
                $userDetail->notify(new ResetpasswordNotification($userDetail->activation_code));

                return response()->json([
                  'error_code'=> 200,
                  'title' => "Successfully Processed",
                  'msg_string'=> __('messages.PASSWORD_CHANGED_EMAIL'),
                  'result' => []
                ]);
            }
        }

         return response()->json([
              'error_code'=> 500,
              'title' => "Invalid Request",
              'msg_string'=> __('message.USER_NOT_EXIST'),
              'result' => []
            ]);
    }

    public function subscription() {
        return response()->json([
          'error_code'=>200,
          'msg_string'=> __('message.PROCESS_SUCCESS'),
          'result' => UserSubscription::get()
        ]);
    }

    public function createStripeCustomer($email) {
        $customer = $this->stripe->customers()->create([
              'email' => $email
        ]);
        return $customer['id'];
    }

    public function activateUser(Request $request) {
        $verificationToken = $request->get('verification_code', '0');

        $isUserRegistered = User::where('verification_code', $verificationToken)->first();
        if($isUserRegistered) {
          $userDetail = User::join('user_subscription', 'user_subscription.id', '=', 'users.subscription_id')
                              ->where('users.id', $isUserRegistered->id)
                              ->select('user_subscription.subscription_name', 'user_subscription.subscription_amount', 'user_subscription.subscription_type',
                                'user_subscription.subscription_code', 'users.*')
                              ->first();

          User::where('verification_code', $verificationToken)->update([
              'is_verified' => 1,
              'registration_stage' => 1
          ]);

          $contactInvitationRequests = ContactInvitation::where('email', $isUserRegistered->email)->get();
          if($contactInvitationRequests) {
            foreach($contactInvitationRequests as $invitationRequest) {

                if(!Contact::where('owner_id', $invitationRequest->owner_id)
                            ->where('contact_id', $isUserRegistered->id)
                            ->count()) {
                      Contact::insert([
                          'contact_group_id' => 1,
                          'owner_id' => $invitationRequest->owner_id,
                          'contact_id' => $isUserRegistered->id
                      ]);
                }


                $invitationRequest->delete();
            }
          }

          $userDetail->is_verified = 1;
          $userDetail->registration_stage = 1;


          return response()->json([
              'error_code'=>200,
              'title' => "Successfully Verified",
              'msg_string'=> __('message.ACCOUNT_VERIFIED_SUCCESSFULLY'),
              'result' => $userDetail
          ]);
       } else {
          return response()->json([
              'error_code'=>500,
              'title' => "Wrong Verification Code",
              'msg_string'=> __('message.INVALID_VERIFICATION_CODE'),
              'result' => []
          ]);
       }
    }


    public function verifyAccount(Request $request, $verificationToken) {
       $isUserRegistered = User::where('verification_code', $verificationToken)->first();
       if($isUserRegistered) {
          User::where('verification_code', $verificationToken)
          ->update([
              'verification_code' => '',
              'is_verified' => 1
          ]);


          $contactInvitationRequests = ContactInvitation::where('email', $isUserRegistered->email)->get();
          if($contactInvitationRequests) {
            foreach($contactInvitationRequests as $invitationRequest) {

                if(!Contact::where('owner_id', $invitationRequest->owner_id)
                            ->where('contact_id', $isUserRegistered->id)
                            ->count()) {
                      Contact::insert([
                          'contact_group_id' => 1,
                          'owner_id' => $invitationRequest->owner_id,
                          'contact_id' => $isUserRegistered->id
                      ]);
                }

                $invitationRequest->delete();
            }
          }

          return  view('verify', ['title' => 'Successfully Verified' , 'message' => __('message.ACCOUNT_VERIFIED_SUCCESSFULLY')]);
       } else {
          return  view('verify', ['title' => 'Invalid Request' , 'message' => __('message.INVALID_VERIFICATION_CODE')]);
       }
    }


    public function register(Request $request) {
       $registrationStage = $request->get('registration_stage', 0);


       if ($registrationStage == 0) {
          return $this->initRegistration($request);
       }

       if ($registrationStage == 1) {
          return $this->stageOneRegistration($request);
       }

       if($registrationStage == 2) {
          return $this->stageTwoRegistration($request);
       }

       if($registrationStage == 3) {
          return $this->processPayment($request);
       }

    }

    public function socialRegistration(Request $request) {

        $socialData = $request->only('token', 'idtoken', 'provider', 'imageUrl');
        $registrationData = $request->only('name', 'email', 'uid');

        $isUserRegistered = User::where('email', $request->get('email', null))->count();
        $isSocialRegistered = User::where(function($query) use($request){
                                      return $query->where('google_token', $request->get('uid', null))
                                                  ->orWhere('facebook_token', $request->get('uid', null))
                                                  ->orWhere('linkedin_token', $request->get('uid', null));
                                    })->count();

        if(!$isUserRegistered && !$isSocialRegistered) {
            switch($socialData['provider']) {
              case "google"   :  $registrationData['google_token'] =  $registrationData['uid']; break;
              case "facebook" :  $registrationData['facebook_token'] =  $registrationData['uid']; break;
              case "linkedin" :  $registrationData['linkedin_token'] =  $registrationData['uid']; break;
            }

            $registrationData['name'] = ($registrationData['name']) ? $registrationData['name'] : "User";
            $registrationData['password'] =  bcrypt('scedgeweb');

            $registrationData['registration_stage'] =  $request->get('registration_stage', 1);
            $registrationData['subscription_id']    =  ( isset($registrationData['subscription_id']) ) ? $registrationData['subscription_id'] : 1;
            $registrationData['verification_code']  =  md5($request->get('email') . "-" . time());

            $registrationData['stripe_id'] = $this->createStripeCustomer($request->get('email'));

            User::create($registrationData);

            $user = User::where('email', $registrationData['email'])->first();


            Profile::create([
                'user_id' => $user->id,
                'profile_image' => $socialData['imageUrl']
            ]);

            $user->notify(new RegistrationNotification());

            return response()->json([
                'error_code' =>200,
                'title' => "Successfully Registered",
                'msg_string' => __('message.SUCCESS_TOKEN'),
                'registration_stage'  => $registrationData['registration_stage'],
                'verification_code' => $registrationData['verification_code']
            ]);


        } else {
            if($isSocialRegistered) {
              $currentUser = User::where(function($query) use($request){
                                            return $query->where('google_token', $request->get('uid', null))
                                                        ->orWhere('facebook_token', $request->get('uid', null))
                                                        ->orWhere('linkedin_token', $request->get('uid', null));
                                          })->first();

              $token = JWTAuth::fromUser($currentUser);

              $response = [
                  'error_code'=>200,
                  'title' => "Successfully Registered",
                  'msg_string'=> __('message.SUCCESS_TOKEN'),
                  'token' => $token,
                  'result' => $currentUser
              ];

            } else {
              $userDetail = User::where('email', $request->get('email', null))->first();

              $response = [
                  'error_code' => ($userDetail->is_verified && !$userDetail->verification_code) ? 200 : 500,
                  'msg_string' =>  ($userDetail->is_verified && !$userDetail->verification_code) ? __('message.SUCCESS_TOKEN') : __('message.ACTIVATE_USER')
              ];

              if($userDetail->is_verified && !$userDetail->verification_code) {
                  $response['token'] = JWTAuth::fromUser($userDetail);
              } else {
                  $response['registration_stage'] = $userDetail->registration_stage;
                  $response['verification_code'] = $userDetail->verification_code;
              }
            }
            return response()->json($response);

      }

    }

    public function initRegistration(Request $request) {
         $registrationData = $request->only('fullName', 'email', 'password', 'subscription_id');

         $isUserRegistered = User::where('email', $request->get('email', null))->count();
         if(!$isUserRegistered) {

            $registrationData['password']           =  bcrypt($registrationData['password']);
            $registrationData['name']               =  $registrationData['fullName'];
            $registrationData['registration_stage'] =  $request->get('registration_stage', 1);
            $registrationData['subscription_id']    =  ( isset($registrationData['subscription_id']) ) ? $registrationData['subscription_id'] : 1;
            $registrationData['verification_code']  =  md5($request->get('email') . "-" . time()) ;

            unset($registrationData['fullName']);

            User::create($registrationData);

            $user = User::where('email', $registrationData['email'])->first();
            $user->notify(new RegistrationNotification());

            return response()->json([
                'error_code'  => 200,
                'title'       => "Welcome to Scedge",
                'msg_string'  => __('message.ACTIVATE_USER'),
                'result'      => User::getUserDetail(['users.email' => $request->get('email', null)])
            ]);
         } else {
            $userDetail = User::where('users.email' , $request->get('email', ''))->select('is_verified', 'is_active')->first();

            return response()->json([
                'error_code' =>  ($userDetail->is_verified && $userDetail->is_active) ? 500 : 200,
                'title'      =>  ($userDetail->is_verified && $userDetail->is_active) ? "User already exist" : "Inactive User",
                'msg_string' =>  ($userDetail->is_verified && $userDetail->is_active) ? __('message.SUCCESS_TOKEN') : __('message.ACTIVATE_USER'),
                'result'     =>  ($userDetail->is_verified && $userDetail->is_active) ? [] : []
            ]);
         }
    }

    public function stageOneRegistration(Request $request) {
       $registrationData = $request->only( 'user_id', 'email', 'gender', 'dob', 'city', 'province', 'country', 'profile_image');

       if($userId = $request->get('user_id', null)) {
           $user = User::where('id', $userId)->first();

           if($user){

            if(isset($registrationData['profile_image']) && $registrationData['profile_image']) {

                $pos = strpos($registrationData['profile_image'], 'http');

                if($pos === false) {
                    $extenstion = preg_replace('#^data:image/\w+;base64,#i', '', $registrationData['profile_image']);
                    $extenstion = str_replace(['data:', 'image/', ';base64,', $extenstion], ['', '', '', ''], $registrationData['profile_image']);

                    $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $registrationData['profile_image']));
                    $photoName= uniqid('profileImg_') . '.' . $extenstion;
                    $photoPath = env('APP_UPLOAD_FOLDER', '/public/images/uploads/') . $photoName;
                    file_put_contents( $_SERVER['DOCUMENT_ROOT'] . $photoPath, $data);
                    $registrationData['profile_image'] = env('APP_URL') . $photoPath;
                }
            }

            // Check previous profile
            $profile = Profile::where('user_id', $user->id)->first();


            if($profile) {
                $profile->update($registrationData);
            } else {
                Profile::create($registrationData);
            }

            $userDetail = User::getUserDetail(['users.id' => $registrationData['user_id']]);


            $userDataset = [
                'registration_stage' => (($userDetail->subscription_name == 'FREE') ? 4 : (($userDetail->subscription_name == 'PROFESSIONAL') ? 2 : 3))
            ];

            if($userDetail->subscription_name == 'FREE') {
              $userDataset['verification_code'] = "";
              $userDataset['is_active'] = 1;

              $userDetail->verification_code = "";
              $userDetail->is_active = 1;
            }

            // Update registration stage

            $userDetail->registration_stage = $userDataset['registration_stage'];

             if(!$this->hasPreviousFirebase($userDetail)) {

               Firebase::push('/userList', [
                    'name' => $userDetail->name,
                    'email' => $userDetail->email,
                    'photoURL' => isset($profile->profile_image) ? $profile->profile_image : ""
               ]);

            }
            $authUserFirebaseIdentifier = $this->getFirebaseIdentifier($userDetail);

            $userDataset['firebase_id'] = $authUserFirebaseIdentifier;

            $user->update($userDataset);

            if($userDetail->registration_stage == 4) {
                 return response()->json([
                      'error_code' => 200,
                      'title' => 'Successfully Stored',
                      'msg_string' => "Registration Stage 2",
                      'token'     => JWTAuth::fromUser($user)
                  ]);
            } else {
                 return response()->json([
                      'error_code' => 200,
                      'title' => 'Successfully Stored',
                      'msg_string' => "Registration Stage 2",
                      'registration_stage'  => $userDetail->registration_stage
                  ]);
            }

          }
      }


      return response()->json([
        'error_code'  => 500,
        'title' => 'Invalid Request',
        'msg_string'  => __('message.INVALID_REQUEST'),
        'result'      => []
      ]);


    }

    public function stageTwoRegistration(Request $request) {
       if($request->get('skip', null)) {
            $user = User::where('id', $request->get('user_id', null))->first();
            $userDetail = User::getUserDetail(['users.id' => $request->get('user_id', null)]);

            $user->update(['registration_stage' => 3]);

            if($userDetail)
              $userDetail->registration_stage =  3;

            return response()->json([
                      'error_code' => 200,
                      'title' => 'Successfully Stored',
                      'msg_string' => __('message.PROCESS_SUCCESS'),
                      'registration_stage'  => $userDetail->registration_stage,
                      'verification_code'  => $userDetail->verification_code
                  ]);
       } else {
           $registrationData = $request->only('company_name', 'website', 'phone_number', 'business_hour');

           if($userId = $request->get('user_id', null)) {
               $user = User::where('id', $userId)->first();

               if($user){
                // Check previous profile
                $profile = Profile::where('user_id', $user->id)->first();

                if($profile) {
                    $profile->update($registrationData);
                }

                $userDetail = User::getUserDetail(['users.id' => $userId]);

                // Update registration stage
                $user->update(['registration_stage' => 3]);

                $userDetail->registration_stage =  3;

                return response()->json([
                          'error_code' => 200,
                          'title' => 'Successfully Stored',
                          'msg_string' => __('message.PROCESS_SUCCESS'),
                          'registration_stage' => $userDetail->registration_stage,
                      ]);
              }
          }
      }


      return response()->json([
        'error_code'  => 500,
        'title' => 'Invalid Request',
        'msg_string'  => __('message.INVALID_REQUEST'),
        'result'      => []
      ]);
    }

    public function processPayment(Request $request) {
        $userDetail = $request->only('user_id', 'subscription_id');

        if(isset($userDetail['user_id']) && $userDetail['user_id']){
              $user = User::where('id', $userDetail['user_id'])->first();
              if($user && !$user->stripe_id ) {
                $customer = $this->stripe->customers()->create([
                    'email' => $user->email
                ]);

                $user->stripe_id = $customer['id'];
                $user->save();
              }
        }

        if($user) {
            $subscriptionDetail = UserSubscription::where('id', $userDetail['subscription_id'])->first();
            if($subscriptionDetail) {
                // ADD CARD
                $paymentData = $request->only(
                              'name',
                              'number',
                              'exp_date', 'cvc',
                              'address_line1', 'address_line2', 'address_city',
                              'address_state', 'address_zip'
                );

                $expDate = explode("/", $paymentData['exp_date']);

                if(count($expDate) == 2) {
                    $paymentData['exp_month'] = $expDate[0];
                    $paymentData['exp_year'] = $expDate[1];

                    unset($paymentData['exp_date']);
                }

                $cardToken = $this->stripe->tokens()->create([
                    'card' => $paymentData
                ]);

                $card = $this->stripe->cards()->create($user->stripe_id, $cardToken['id']);

                if($card) {

                  // ADD SUBSCRIPTION
                  $subscription = $this->stripe->subscriptions()->create($user->stripe_id, [
                      'plan' => $subscriptionDetail->subscription_code,
                  ]);

                  $user->card_id = $card['id'];
                  $user->verification_code = '';
                  $user->is_active = 1;
                  $user->save();
                }

                //token creation
                $token = JWTAuth::fromUser($user);

                return response()->json([
                    'error_code'=>200,
                    'title' => 'Successfully Process',
                    'msg_string'=> __('message.SUCCESS_TOKEN'),
                    'token' => $token,
                    'result' => $user
                ]);
            }
        }

        return response()->json([
                    'error_code' => 500,
                    'title' => 'Some problem occured',
                    'msg_string'=> __('message.PROBLEM_OCCURED'),
                    'result' => []
                ]);

    }


    public function getProvince(Request $request) {

      $countryID = $request->get('country_id', null);

      $result['province'] = Region::where( 'regions.country_id', '=', $countryID )->select( 'id', 'name', 'code', 'country_id' )->get();

      return response()->json([
        'error_code'=>200,

        'msg_string'=> __('messages.PROCESS_SUCCESS'),
        'result' => $result
      ]);
    }


    public function getCity(Request $request) {

      $provinceID = $request->get('province_id', null);

      $result['cities']  = City::where( 'cities.region_id', '=', $provinceID )->select( 'id', 'region_id', 'country_id', 'name' )->get();


      return response()->json([
        'error_code'=>200,
        'msg_string'=> __('messages.PROCESS_SUCCESS'),
        'result' => $result
      ]);
    }


    public function getVerification(Request $request) {
      if( ($verificationCode = $request->get('verification_code', null)) && User::where('verification_code', $request->get('verification_code', null))->count()){

          $others['countries']  = Country::select( 'id', 'name', 'code' )->get();

          return response()->json([
            'error_code'=>200,
            'title' => 'Successfully Verified',
            'msg_string'=> __('message.PROCESS_SUCCESS'),
            'result' => User::join('user_subscription', 'user_subscription.id', '=', 'users.subscription_id')
                ->where('verification_code', $verificationCode)
                ->select('user_subscription.subscription_name', 'user_subscription.subscription_amount', 'user_subscription.subscription_type',
                  'user_subscription.subscription_code', 'users.*')
                ->first(),
            'others' => $others

          ]);

        }

        return response()->json([
          'error_code'=> 500,
          'title' => 'Invalid Request',
          'msg_string'=> 'Please provide a valid verification code',
          'result' => []
        ]);
    }

    public function hasPreviousFirebase($currentUser) {
       $userList = Firebase::get('/userList');
       $userList = (json_decode($userList->getContents()));

       $returnType = false;

       if( !empty($userList) ){
          foreach($userList as $firebaseId => $user) {
            if(property_exists($user, 'email')) {
              if($currentUser->email == $user->email) {
                if($firebaseId != $currentUser->firebase_id) {
                    User::where('id', $currentUser->id)->update(['firebase_id' => $firebaseId]);
                }
                $returnType = true;
                break;
              }
            }
          }
       }
       return $returnType;
    }


    public function login(Request $request){
        $registrationData = $request->only('provider', 'email', 'uid');

        if($request->get('uid', null)){
          $tokenType = ($registrationData['provider'] == 'facebook') ? 'facebook_token' :
                    (($registrationData['provider'] == 'google') ? 'google_token' : 'linkedin_token');

          $isUserExist = User::where( $tokenType, $request->get('uid') )
                               ->where( 'is_active', 1 )
                               ->count();

          if($isUserExist) {
              $currentUser =  User::where( $tokenType, $request->get('uid'))->first();

              try {
                 $token = JWTAuth::fromUser($currentUser);

                 if(!$this->hasPreviousFirebase($currentUser)) {
                   $profile = Profile::where('user_id', $currentUser->id)->first();

                   $profileImage = ($profile) ? $profile->profile_image : "";

                  Firebase::push('/userList', [
                        'name' => $currentUser->name,
                        'email' => $currentUser->email,
                        'photoURL' => $profileImage
                   ]);
                 }

                 User::where('id', $currentUser->id)
                      ->update(['api_token' => $token]);

              } catch (JWTAuthException $e) {
                  return response()->json([
                    'error_code'=>500,
                    'title' => 'Invalid social id provided',
                    'msg_string'=>__('message.FAILED_TOKEN'),
                    'token'=>[]
                  ]);
              }


              return response()->json(['error_code'=>200,
                  'msg_string'=> __('message.SUCCESS_TOKEN'),
                  'token' => $token,
                  'result' => $currentUser
              ]);

          } else {
              return response()->json(['error_code'=>500,
              'title' => 'User not registered',
              'msg_string'=> __('message.USER_NOT_EXIST'),'result'=>[]]);
          }
        } else {
          $credentials = $request->only('email', 'password');
        }


        $token = null;
        try {
           if (!$token = JWTAuth::attempt($credentials)) {
              return response()->json(['error_code'=>422,
              'title' => 'Some problem occured',
              'msg_string'=> __('message.INVALID_EMAIL_PASSWORD'),'result'=>[]]);
           } else {
               $user = JWTAuth::toUser($token);
               $currentUser = User::where('id', $user->id)->first();

               if( $currentUser->is_active ){

                   if($currentUser->is_verified) {
                       User::where('id', $user->id)
                            ->update(['api_token' => $token]);

                       if(!$this->hasPreviousFirebase($currentUser)) {

                         $profile = Profile::where('user_id', $currentUser->id)->first();

                         $profileImage = ($profile) ? $profile->profile_image : "";

                         Firebase::push('/userList', [
                              'name' => $currentUser->name,
                              'email' => $currentUser->email,
                              'photoURL' => $profileImage
                         ]);
                       }

                  } else {
                       return response()->json(['error_code'=>500, 'title' => 'Inactive User', 'msg_string'=>__('message.ACTIVATE_USER'), 'result'=>[]]);
                  }
               } else {

                  return response()->json(['error_code'=>500, 'title' => 'Inactive User',  'msg_string'=> __('message.ACTIVATE_USER'),'result'=>[]]);
              }
           }
        } catch (JWTAuthException $e) {
            return response()->json(['error_code'=>422, 'title' => 'Some problem occured',  'msg_string'=>__('message.FAILED_TOKEN'), 'result'=>[]]);
        }

        $userDetail = User::getUserDetail(['users.email' => $request->get('email', null)]);

        /*if(!$this->hasPreviousFirebase($userDetail)) {

           Firebase::push('/userList', [
                'name'     => $userDetail->name,
                'email'    => $userDetail->email,
                'photoURL' => isset($profile->profile_image) ? $profile->profile_image : ""
           ]);

        }*/
        $authUserFirebaseIdentifier = $this->getFirebaseIdentifier($userDetail);
        $userDataset['firebase_id'] = $authUserFirebaseIdentifier;
        User::where( 'id', $userDetail->user_id )->update($userDataset);

        return response()->json(['error_code'=>200, 'msg_string'=>__('message.SUCCESS_TOKEN'), 'token' => $token, 'result' => $userDetail]);
    }


    /**
    * Verify Email API
    */
    public function verifyEmail(Request $request){
         $email = $request->only('email');

         $userRow = User::where('email', $email)->get();

         if( count($userRow->toArray()) ) {
             return response()->json(['error_code'=>500, 'title' => 'User already exist', 'msg_string'=> __('message.ALREADY_REGISTERED'), 'result' => false]);
         } else {
             return response()->json(['error_code'=>200, 'msg_string'=> __('message.EMAIL_AVAILABLE'), 'result' => true]);
         }
    }

    /**
    * Forgot Password API
    */
    public function forgotPassword(Request $request) {
        $email = $request->only('email', 0);
        $userRowset = User::where('email', $email)->get();
        if( count($userRowset->toArray()) ) {
          foreach($userRowset as $userRow){
            $userRow->update(['remember_token' => md5(microtime())]);
            //$userRow->notify(new ForgotPasswordNotification());
          }
          return response()->json(['error_code'=>200, 'msg_string'=> __('messgaes.RESET_PASSWORD_LINK'), 'result' => true]);
        } else {
          return response()->json(['error_code'=>500, 'title' => 'Invalid Token', 'msg_string'=> __('message.FAILED_TOKEN'), 'result' => false]);
        }
    }

    /**
    * Validate Token API
    */
    public function isValidToken(Request $request) {

    	try {

	        if (! $user = JWTAuth::parseToken()->authenticate()) {
	            return response()->json(['error_code'=>444, 'msg_string'=> __('message.FAILED_TOKEN'), 'result' => 'User not found']);
	        }

	    } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

	        return response()->json(['error_code'=> 444,  'title' => 'Invalid Token', 'msg_string'=> 'Provided Token has been Expired', 'result' => false]);

	    } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

	        return response()->json(['error_code'=> 444,  'title' => 'Invalid Token', 'msg_string'=> 'You have provided a Invalid Token ', 'result' => false]);

	    } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

	        return response()->json(['error_code'=> 444, 'title' => 'Empty Token', 'msg_string'=> 'Please provide a Token', 'result' => false]);

	    }

	    // the token is valid and we have found the user via the sub claim
	    return response()->json(compact('user'));
    }

    public function detail(Request $request) {
        $user = JWTAuth::toUser($request->token);
        $userDetail = User::getUserDetail(['users.id' =>   $user->id]);
        return response()->json(['error_code'=>200, 'msg_string'=>__('message.SUCCESS_TOKEN'), 'result' => $userDetail]);
    }

    public function getAuthUser(Request $request){
        $user = JWTAuth::toUser($request->token);
        return response()->json(['result' => $user]);
    }

    public function getFirebaseIdentifier($currentUser) {
       $userList = Firebase::get('/userList');

       $userList = (json_decode($userList->getContents()));
       foreach($userList as $firebaseId => $user) {
          if( property_exists( $user, 'email' ) ){
            if($currentUser->email == $user->email) {
              return $firebaseId ;
            }
          }
       }
       return null;
    }

    public function updateAccountPassword(Request $request) {
        $activationCode = $request->get('activationCode', '');
        $password = $request->get('password', '');
        if($activationCode) {
            $userDetail = User::where('activation_code', $activationCode)->first();
            if($userDetail) {
                $userDetail->activation_code = "";
                $userDetail->password = bcrypt($password);
                $userDetail->save();
                $credentials = [
                  'email'     =>  $userDetail->email,
                  'password'  =>  $password
                ];

                if($token = JWTAuth::attempt($credentials)) {
                      $user = JWTAuth::toUser($token);

                      if(!$this->hasPreviousFirebase($userDetail)) {

                          $profile = Profile::where('user_id', $currentUser->id)->first();

                          $profileImage = ($profile) ? $profile->profile_image : "";


                          Firebase::push('/userList', [
                               'name' => $currentUser->name,
                               'email' => $currentUser->email,
                               'photoURL' => $profileImage
                          ]);
                      }


                      $userProfileDetail = User::getUserDetail(['users.email' => $userDetail->email]);
                      $authUserFirebaseIdentifier = $this->getFirebaseIdentifier($userProfileDetail);
                      $userDetail->firebase_id = $authUserFirebaseIdentifier;
                      $userDetail->update();


                      return response()->json(['error_code'=>200,
                        'msg_string'=>__('message.SUCCESS_TOKEN'),
                        'token' => $token,
                        'result' => $userProfileDetail]);
                }

            }

        }

        return response()->json([
            'error_code' => 500,
            'title' => "Invalid verification code",
            'msg_string'=> __('messages.INVALID_ACTIVATION_CODE')
        ]);
    }

    public function checkVerificationCode(Request $request) {
          $activationCode = $request->get('verificationCode', '');
          if($activationCode) {
              $userDetail = User::where('activation_code', $activationCode)->first();

              if($userDetail) {
                  return response()->json([
                      'error_code'  => 200,
                      'title'       => "Valid activation code",
                      'msg_string'  => __('messages.VALID_ACTIVATION_CODE')
                  ]);
              }
          }

          return response()->json([
              'error_code'  => 500,
              'title'       => "Invalid verification code",
              'msg_string'  => __('messages.INVALID_ACTIVATION_CODE')
          ]);
    }


    public function resendActivationCode(Request $request) {
        $email = $request->get('email', '');
        if($email) {
            $userDetail = User::where('email', $email)->first();

            if($userDetail) {
              if($userDetail->is_active) {
                return response()->json([
                  'error_code'  => 200,
                  'title'       => "Already Active",
                  'msg_string'  => __('messages.ALREADY_ACTIVE')
                ]);
              }

              $userDetail->is_active = 0;
              $userDetail->is_verified = 0;
              $userDetail->verification_code =  md5($request->get('email') . "-" . time());
              $userDetail->registration_stage = 1;
              $userDetail->save();

              $userDetail->notify(new RegistrationNotification());

              return response()->json([
                'error_code'  => 200,
                'title'       => "Successfully Send",
                'msg_string'  => __('messages.RESET_VERIFICATION')
              ]);
            } else {
              return response()->json([
                'error_code'  => 500,
                'title'       => "Email not registered",
                'msg_string'  => __('messages.EMAIL_NOT_EXIST')
              ]);
            }

        }

        return response()->json([
          'error_code' => 500,
          'title' => "Invalid Request",
          'msg_string'=> __('messages.INVALID_REQUEST')
        ]);
      }
}
