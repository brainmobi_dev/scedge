<?php

namespace App\Http\Controllers\Website;

use DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;
use JWTAuthException;

use App\UserCard;
use App\CardType;
use App\City;
use App\Region;
use App\Country;
use App\Contact;
use App\User;
use App\Report;
use App\ReportAbuse;
use App\UserChildCard;
use App\ContactInvitation;
use App\UserNotification;

use App\Notifications\CardChangedNotification;
use App\Notifications\CardSharingNotification;
use App\Notifications\ShareItemNotification;

class CardController extends Controller
{
    public function getCurrentUser($request) {
        return JWTAuth::toUser($request->token);
    }


    public function updateShowcaseOrder(Request $request) {
      $authUser = $this->getCurrentUser($request);

      $userCardOrder = $request->get('showcaseOrder', null);

      if($userCardOrder) {
        foreach($userCardOrder as $cardOrder) {
          $userCard = UserCard::where('id', $cardOrder['id'])->first();
          if($userCard) {
            $userCard->showcase_order = $cardOrder['showcase_order'];
            $userCard->save();
          }
        }
      }

      return response()->json([
        'error_code'=>200,
        'msg_string'=> __('messages.PROCESS_SUCCESS'),
        'result' => []
      ]);
    }

    public function updateOrder(Request $request) {
      $authUser = $this->getCurrentUser($request);

      $userCardOrder = $request->get('cardOrder', null);

      if($userCardOrder) {
        foreach($userCardOrder as $cardOrder) {
          $parentCardTypeId = $cardOrder['card_type_id'];
          $userCard = UserCard::where('id', $cardOrder['id'])->first();
          if($userCard) {
            CardType::where('id', $parentCardTypeId)
                      ->update([
                        'parent_group_id' => 0
                      ]);


            $userCard->card_order = $cardOrder['card_order'];
            $userCard->save();
          }

          if(isset($cardOrder['child_item'])) {
              foreach($cardOrder['child_item'] as $childItems) {
                  if(isset($childItems['card_type_id'])) {
                    CardType::where('id', $childItems['card_type_id'])
                              ->update([
                                'parent_group_id' => $parentCardTypeId
                              ]);
                  }
              }
          }
        }
      }

      return response()->json([
        'error_code'=>200,
        'msg_string'=> __('messages.PROCESS_SUCCESS'),
        'result' => []
      ]);
    }

    public function addCardGroup(Request $request) {
        $authUser = $this->getCurrentUser($request);
        $cardGroup = [
            'name' => $request->get('title', null),
            'description' => $request->get('body', null),
            'owner_id' => $authUser->id,
            'parent_group_id' => $request->get('groupId', 0),
            'image' => $request->get('group_image', null)
        ];

      $hasPreviousCard  = CardType::where('name', $request->get('title', null))
                  ->where(function($query) use($authUser){
                      return $query->where('owner_id', $authUser->id)
                                   ->orWhere('owner_id', 0);
                  })
                  ->where(function($query) use($cardGroup){
                      if($cardGroup['parent_group_id']) {
                          return $query->where('parent_group_id', $cardGroup['parent_group_id'])
                                      ->orWhere('parent_group_id', 0);
                      } else {
                          return $query->where('parent_group_id', 0);
                      }
                  })
                  ->count();

        if($hasPreviousCard) {
            return response()->json([
                'error_code'=> 500,
                'msg_string'=> __('messages.DUPLICATE_GROUP'),
                'result' => []
            ]);
        } else {

              if(isset($cardGroup['image']) && $cardGroup['image']) {
                  $pos = strpos($cardGroup['image'], 'http');

                  if($pos === false) {
                      $extenstion = preg_replace('#^data:image/\w+;base64,#i', '', $cardGroup['image']);
                      $extenstion = str_replace(['data:', 'image/', ';base64,', $extenstion], ['', '', '', ''], $cardGroup['image']);

                      $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $cardGroup['image']));
                      $photoName= uniqid('groupImg_') . '.' . $extenstion;
                      $photoPath = env('APP_UPLOAD_FOLDER', '/public/images/uploads/') . $photoName;
                      file_put_contents( $_SERVER['DOCUMENT_ROOT'] . $photoPath, $data);
                      $cardGroup['image'] = env('APP_URL') . $photoPath;
                  }
              }

              if($cardGroup['name']) {
                  CardType::insert($cardGroup);
                  return $this->cardGroup($request);
              }
        }

        return response()->json([
            'error_code'=> 500,
            'msg_string'=> __('messages.INVALID_REQUEST'),
            'result' => []
        ]);
    }

    public function addUserGroup(Request $request) {
        $authUser = $this->getCurrentUser($request);

        $formData = $request->get('item', null);
        $cardGroup = [
            'name' => (isset($formData['title']) ? $formData['title'] : ""),
            'description' => (isset($formData['body']) ? $formData['body'] : ""),
            'owner_id' => $authUser->id,
            'parent_group_id' => (isset($formData['card_type_id'])) ? $formData['card_type_id'] : 0,
            'image' => (isset($formData['card_image']) ? $formData['card_image'] : "")
        ];

      // $hasPreviousCard  = CardType::where('name', $formData['title'])
      //             ->where(function($query) use($authUser){
      //                 return $query->where('owner_id', $authUser->id)
      //                              ->orWhere('owner_id', 0);
      //             })
      //             ->where(function($query) use($cardGroup){
      //                 if($cardGroup['parent_group_id']) {
      //                     return $query->where('parent_group_id', $cardGroup['parent_group_id'])
      //                                 ->orWhere('parent_group_id', 0);
      //                 } else {
      //                     return $query->where('parent_group_id', 0);
      //                 }
      //             })
      //             ->count();

        // if($hasPreviousCard) {
        //     return 0;
        // } else {

              /*if(isset($cardGroup['image']) && $cardGroup['image']) {
                  $pos = strpos($cardGroup['image'], 'http');

                  if($pos === false) {
                      $extenstion = preg_replace('#^data:image/\w+;base64,#i', '', $cardGroup['image']);
                      $extenstion = str_replace(['data:', 'image/', ';base64,', $extenstion], ['', '', '', ''], $cardGroup['image']);

                      $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $cardGroup['image']));
                      $photoName= uniqid('groupImg_') . '.' . $extenstion;
                      $photoPath = env('APP_UPLOAD_FOLDER', '/public/images/uploads/') . $photoName;
                      file_put_contents( $_SERVER['DOCUMENT_ROOT'] . $photoPath, $data);
                      $cardGroup['image'] = env('APP_URL') . $photoPath;
                  }
              }*/

              if($cardGroup['name']) {
                  $cardDetail = CardType::insertGetId($cardGroup);
                  return $cardDetail;
              }
        // }

        // return 0;
    }

    public function getAllCardGroups($groupId) {
      $cardGroupArray = "";
      if($groupId) {
        $parentGroupId = CardType::where('id', $groupId)->select('parent_group_id')->first();
        if($parentGroupId) {
          $cardGroups = CardType::where('id', $parentGroupId->parent_group_id)->first();

          if($cardGroups){
            $cardGroupArray = [];
            $cardGroupArray[] = ($cardGroups);
            if($cardGroups->parent_group_id){
                 $tempGroup = ($this->getAllCardGroups($parentGroupId->parent_group_id));

                 foreach($tempGroup as $tempGroupData){
                    $cardGroupArray[] = $tempGroupData;
                 }

            }
          }
        }
      }
      return $cardGroupArray;
    }

    public function cardGroup(Request $request) {
      $authUser = $this->getCurrentUser($request);

      if($groupId = $request->get('groupId', null)) {
        /*$cardGroups =   CardType::where('id', $groupId)
                    ->orWhere(function($query) use($groupId, $authUser){
                      return $query->where('parent_group_id', $groupId)
                                  ->where('owner_id', $authUser->id);
                    })
                    ->get();

        $count = CardType::where( 'parent_group_id', $groupId )->count();

        if( $count < 1  ){
          $cardGroups = '';
        }*/
        $parentGroupId = CardType::where('id', $groupId)->select('parent_group_id')->first();
        if($parentGroupId) {
          $cardGroups = CardType::where('id', $parentGroupId->parent_group_id)->get();
        } else {
          $cardGroups = '';
        }


      } else {
        $cardGroups = [];
        /*CardType::where('parent_group_id', 0)
          ->where(function($query) use($authUser){
            return $query->where('owner_id', 0)
                        ->orWhere('owner_id', $authUser->id);
          })
          ->get();*/
      }


      $allCardGroups = [];
      $cardGroups = $this->getAllCardGroups($groupId);
      if($cardGroups) {
        foreach($cardGroups as $cardGroup) {
          $allCardGroups[$cardGroup->id] = $cardGroup;
        }
      }

      return response()->json([
        'error_code'=>200,
        'msg_string'=> __('messages.PROCESS_SUCCESS'),
        'result' => $allCardGroups
      ]);
    }

    public function createCard(Request $request) {

        $authUser = $this->getCurrentUser($request);
        $UserCardValidField = ["owner_id", "card_type_id", "title", "body", "card_image", "card_date", "card_time", "priority", "location_address", "location_city",
          "location_province", "location_country", "start_date", "start_time", "end_date",
          "end_time", "price", "quantity", "discount", "spacs_size", "spacs_brand", "spacs_model",
          "spacs_color", "for_gender", "for_age", "share_on_showcase", "two_way_sync", "live_item",
          "card_type", "allow_notification", "is_active", "card_order", "showcase_order", "currency",
          "url", "category", "contact_info", "card_status", "street_address"
        ];

        if( $formData = $request->get('item', null) ) {

              $validation = Validator::make($request->get('item', null), [
                'title' => 'required|max:255',
              ]);

            if($validation->fails()){

                $errors = $validation->errors();
                return response()->json([
                  'error_code'=>500,
                  'msg_string'=> __('messages.VALIDATION_FAIL'),
                  'result' => array('errors' => $errors),
                ]);

            }else{
                $userCardOrder = UserCard::where('owner_id', $authUser->id)
                                  ->select('card_order')
                                  ->orderBy('card_order', 'asc')
                                  ->first();

                if($userCardOrder){
                    $formData['card_order'] = $userCardOrder->card_order - 1;
                }

                $formData['card_type_id'] = $this->addUserGroup($request);
                if(!$formData['card_type_id']) {
                  return response()->json([
                      'error_code'=>500,
                      'msg_string'=> __('messages.DUPLICATE_GROUP'),
                      'result' => []
                  ]);
                }
                $formData['owner_id'] = $authUser->id;

                /* if(isset($formData['card_image']) && !empty($formData['card_image']) ) {
                  $pos = strpos($formData['card_image'], 'http');

                  if($pos === false) {
                      $extenstion = preg_replace('#^data:image/\w+;base64,#i', '', $formData['card_image']);
                      $extenstion = str_replace(['data:', 'image/', ';base64,', $extenstion], ['', '', '', ''], $formData['card_image']);

                      $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $formData['card_image']));
                      $photoName= uniqid('cardImg_') . '.' . $extenstion;
                      $photoPath = env('APP_UPLOAD_FOLDER', '/public/images/uploads/') . $photoName;
                      file_put_contents( $_SERVER['DOCUMENT_ROOT'] . $photoPath, $data);
                      $formData['card_image'] = env('APP_URL') . $photoPath;
                  }
                } */

                if(isset($formData['card_time']) && $formData['card_time']) {
                  $formData['card_time'] = date("h:i", strtotime($formData['card_time']));
                }

                if(isset($formData['start_time']) && $formData['start_time']) {
                  $formData['start_time'] = date("h:i", strtotime($formData['start_time']));
                }

                if(isset($formData['end_time']) && $formData['end_time']) {
                  $formData['end_time'] = date("h:i", strtotime($formData['end_time']));
                }

                if(isset($formData['two_way_sync'])) {
                    $formData['two_way_sync'] = ($formData['two_way_sync'] == 'true') ? 1 : 0;
                }

                if(isset($formData['share_on_showcase'])) {
                    $formData['share_on_showcase'] = ($formData['share_on_showcase'] == 'true') ? 1 : 0;
                }


                if(isset($formData['live_item'])) {
                    $formData['live_item'] = ($formData['live_item'] == 'true') ? 1 : 0;
                }

                $shareToContact = "";
                $shareToEmail = "";

                if(isset($formData['shareToContact'])) {
                    $shareToContact = $formData['shareToContact'];
                }

                if(isset($formData['shareToEmail'])) {
                    $shareToEmail = $formData['shareToEmail'];
                }

                foreach($formData as $field => $value){
                  if(!in_array($field, $UserCardValidField)) {
                    unset($formData[$field]);
                  }
                }

                $userCardId = UserCard::create($formData);

                if($shareToContact) {
                  foreach($shareToContact as $contact) {
                      UserChildCard::insert([
                        'card_id' => $userCardId->id,
                        'user_id' => $contact['id'],
                        'can_edit' => 1
                      ]);
                  }
                }

                if($shareToEmail) {
                    $contactDetail = $authUser;
                    $contactDetail->email = $shareToEmail;
                    $contactDetail->cardId = $userCardId->id;

                    $contactDetail->notify(new ShareItemNotification());
                }

                $result = UserCard::join('users', 'users.id', '=', 'user_cards.owner_id')
                                  ->join('profile', 'users.id', '=', 'profile.user_id')
                                  ->join('card_types', 'card_types.id', '=', 'user_cards.card_type_id')
                                  ->where('user_cards.is_active', '=', 1)
                                  ->where('users.id', '=', $authUser->id)
                                  ->orderBy('user_cards.id', 'desc')
                                  ->paginate(12);

                return response()->json([
                  'error_code'=>200,
                  'msg_string'=> __('messages.PROCESS_SUCCESS'),
                  'result' => $result
                ]);
            }

        }

        return response()->json([
      		'error_code'=>500,
      		'msg_string'=> __('messages.INVALID_REQUEST'),
      		'result' => $result
      	]);
    }

    public function updateSharedCard(Request $request) {
       $authUser = $this->getCurrentUser($request);
       $acceptRequest = $request->get('accept_request' , 0);
       $cardId = $request->get('card_id' , 0);

       if($cardId) {
         $userChildCard =  UserChildCard::where('card_id', $cardId)
                                          ->where('user_id', $authUser->id)
                                          ->first();

          if($userChildCard) {
            if($acceptRequest) {
              $userChildCard->is_accepted = 1;
              $userChildCard->save();
            } else {
              $userChildCard->delete();
            }

            return response()->json([
          		'error_code'=>200,
          		'msg_string'  => __('messages.PROCESS_SUCCESS'),
              'result'      => []

          	]);
          }

          return response()->json([
        		'error_code'=>500,
        		'msg_string'  => __('messages.UNAUTHRIZED_ACCESS'),
            'result'      => []
          ]);


       }


    }

    public function getSharedCards(Request $request) {
      $others = [];
      $authUser = $this->getCurrentUser($request);
      $result = UserChildCard::join('user_cards', 'user_cards.id', '=', 'user_child_cards.card_id')
                                      ->where('user_child_cards.user_id', $authUser->id)
                                      ->where('user_child_cards.is_accepted', 0)
                                      ->join('users', 'users.id', '=', 'user_cards.owner_id')
                                      ->join('profile', 'users.id', '=', 'profile.user_id')
                                      ->join('card_types', 'card_types.id', '=', 'user_cards.card_type_id')
                                      ->leftjoin('countries', 'countries.id', '=', 'user_cards.location_country')
                                      ->leftjoin('regions', 'regions.id', '=', 'user_cards.location_province')
                                      ->leftjoin('cities', 'cities.id', '=', 'user_cards.location_city')
                                      ->where('user_cards.is_active', '=', 1)
                                      ->select('user_cards.*', 'users.name as userName', 'card_types.name', 'countries.name as countryName', 'regions.name as provinceName', 'cities.name as cityName')
                                      ->paginate(12);


      $others['countries']  = Country::select( 'id', 'name', 'code' )->get();
      return response()->json([
    		'error_code'=>200,
    		'msg_string'  => __('messages.PROCESS_SUCCESS'),
        'result'      => $result,
        'others'      => $others
    	]);
    }

    public function getCard(Request $request) {
      $authUser = $this->getCurrentUser($request);

      $userId = $request->get('user_id', $authUser->id);
      $shareOnShowcase = $request->get('share_on_showcase', 0);

      $userChildCard = UserChildCard::where('user_child_cards.user_id', $userId)
                      ->join('user_cards', 'user_cards.id', '=', 'user_child_cards.card_id')
                      ->where('user_child_cards.is_accepted', 1)
                      ->select('user_cards.id as card_id')
                      ->get();

      $childCards = array();
      foreach ($userChildCard as $key => $value) {
        array_push($childCards,$value->card_id);
      }

      $query = UserCard::join('users', 'users.id', '=', 'user_cards.owner_id')
                        ->join('profile', 'users.id', '=', 'profile.user_id')
                        ->join('card_types', 'card_types.id', '=', 'user_cards.card_type_id')
                        ->leftjoin('countries', 'countries.id', '=', 'user_cards.location_country')
                        ->leftjoin('regions', 'regions.id', '=', 'user_cards.location_province')
                        ->leftjoin('cities', 'cities.id', '=', 'user_cards.location_city')
                        ->where('user_cards.is_active', '=', 1)
                        ->where(function($query) use($childCards, $userId){
                          return $query->whereIn('user_cards.id', $childCards)
                                       ->orwhere('users.id', '=', $userId);

                        });
      if($shareOnShowcase) {
          $query = $query->where('user_cards.share_on_showcase', 1);
      }

      if($filter = $request->get('filter', null)) {
        foreach($filter as $field => $value) {
          if($value) {
            switch($field) {
              //Searching
              case "type" : $query = $query->where(function($query) use($value){
                                              return $query->where('card_types.id', strtoupper($value))
                                                           ->orWhere('card_types.parent_group_id', strtoupper($value));
                                      });
                                      break;
              case "title" : $query = $query->where('user_cards.title', 'like', '%' . $value . '%');
                                  break;
              case "card_type_id" : $query = $query->where('card_types.id', strtoupper($value));
                                    break;

              case "share_on_showcase" : $query = $query->where('user_cards.share_on_showcase', $value);
                                          break;

              case "two_way_sync" : $query = $query->where('user_cards.two_way_sync', $value);
                                    break;

              case "live_item" : $query = $query->where('user_cards.live_item', $value);
                                  break;

              case "searchText" : $query = $query->where(function($query) use($value){
                                                return $query->where('user_cards.title', 'like', '%' . $value . '%')
                                                            ->orWhere('user_cards.body', 'like', '%' . $value . '%');
                                           }); break;
              case "group" : $query = $query->where('user_cards.card_type_id', '=', $value); break;
              case "country" : $query = $query->where('user_cards.location_country', 'like', '%' . $value . '%'); break;
              case "province" : $query = $query->where('user_cards.location_province', 'likes', '%' . $value . '%'); break;
              case "city" : $query = $query->where('user_cards.location_city', 'like', '%' . $value . '%'); break;
              case "date" : $query = $query->where('user_cards.card_date', '=', ($value == 'now') ? date('Y-m-d', time() ) : $value); break;

              //Sorting
              case "sortBy" : foreach ($value as $key => $val) {
                                if(trim($val)) {
                                  switch($key) {
                                      case "sortByCardTypeID" : $query = $query->orderBy('card_types.id', $val);
                                        break;

                                      case "sortByDate" : $query = $query->orderBy('user_cards.card_date', $val);
                                        break;

                                      case "sortByTitle" : $query = $query->orderBy('user_cards.title', $val);
                                        break;

                                      case "sortByDescription" : $query = $query->orderBy('user_cards.body', $val);
                                        break;

                                      case "sortByPriority" : $query = $query->orderBy('user_cards.priority', $val);
                                        break;
                                  }
                                }
                              }
                              break;


            }
          } else {
            switch($field) {
                case "share_on_showcase" : $query = $query->where('user_cards.share_on_showcase', $value);
                                            break;

                case "two_way_sync" : $query = $query->where('user_cards.two_way_sync', $value);
                                      break;

                case "live_item" : $query = $query->where('user_cards.live_item', $value);
                                    break;
            }
          }
        }
      }

      if(!isset($filter['type'])){
        $query = $query->where('card_types.parent_group_id', '=', 0);
      }

      if(! isset($filter['sortBy'])) {
        $query = $query->orderBy('user_cards.card_order', 'asc');
      }

      $result = $query->select('user_cards.*', DB::raw('users.name as created_by'), 'card_types.name', 'countries.name as countryName', 'regions.name as provinceName', 'cities.name as cityName')->paginate(12);

      $others['countries']  = Country::select( 'id', 'name', 'code' )->get();

      $itemGroups = [];
      if($group = $request->get('group', null)) {
          $parentGroupId = CardType::where('id', $group)->select('parent_group_id')->first();

          if($parentGroupId) {
                $itemGroups = CardType::where('id', $parentGroupId->parent_group_id)
                                          ->get();
          }
      }

    	return response()->json([
    		'error_code'=>200,
    		'msg_string'  => __('messages.PROCESS_SUCCESS'),
        'result'      => $result,
        'group'       => $itemGroups,
    		'others'      => $others
    	]);
    }


    public function getProvince(Request $request) {

      $countryID = $request->get('country_id', null);

      $result['province'] = Region::where( 'regions.country_id', '=', $countryID )->select( 'id', 'name', 'code', 'country_id' )->get();

      return response()->json([
        'error_code'=>200,
        'msg_string'=> __('messages.PROCESS_SUCCESS'),
        'result' => $result
      ]);
    }


    public function getCity(Request $request) {

      $provinceID = $request->get('province_id', null);

      $result['cities']  = City::where( 'cities.region_id', '=', $provinceID )->select( 'id', 'region_id', 'country_id', 'name' )->get();


      return response()->json([
        'error_code'=>200,
        'msg_string'=> __('messages.PROCESS_SUCCESS'),
        'result' => $result
      ]);
    }

    public function updateCard(Request $request){
        $authUser = $this->getCurrentUser($request);


          $UserCardValidField = ["owner_id", "card_type_id", "title", "body", "card_image", "card_date", "card_time", "priority", "location_address", "location_city",
            "location_province", "location_country", "start_date", "start_time", "end_date",
            "end_time", "price", "quantity", "discount", "spacs_size", "spacs_brand", "spacs_model",
            "spacs_color", "for_gender", "for_age", "share_on_showcase", "two_way_sync", "live_item",
            "card_type", "allow_notification", "is_active", "card_order", "showcase_order", "currency",
            "url", "category", "contact_info", "card_status", "street_address"
          ];


        if($itemId = $request->get('id', null)) {
          $itemRow = UserCard::where('id', $itemId)->first();

        if($itemRow) {
              $canUpdate = false;
              if($itemRow->owner_id == $authUser->id){
                  $canUpdate = true;
              } else {
                 $sharedUser = UserChildCard::where('user_id', $authUser->id)
                                    ->where('card_id', $itemId)
                                    ->where('is_accepted', 1)
                                    ->first();
                 if($sharedUser) {
                   if( $sharedUser->can_edit ) {
                      $canUpdate = true;
                   }
                 }
              }


              if($canUpdate) {
                $formData = $request->get('item');
                if( isset($formData['id']) )
                  unset($formData['id']);

                if( isset($formData['$$hashKey']) )
                  unset($formData['$$hashKey']);

                if( isset($formData['owner_id']) )
                  unset($formData['owner_id']);

                if(isset($formData['card_time'])) {
                  $formData['card_time'] = date("h:i", strtotime($formData['card_time']));
                }

                if(isset($formData['start_time'])) {
                  $formData['start_time'] = date("h:i", strtotime($formData['start_time']));
                }

                if(isset($formData['end_time'])) {
                  $formData['end_time'] = date("h:i", strtotime($formData['end_time']));
                }

                if(isset($formData['two_way_sync'])) {
                    $formData['two_way_sync'] = ($formData['two_way_sync'] == 'true') ? 1 : 0;
                }

                if(isset($formData['share_on_showcase'])) {
                    $formData['share_on_showcase'] = ( $formData['share_on_showcase'] == 'true' )? 1 : 0;
                }


                if(isset($formData['live_item'])) {
                    $formData['live_item'] = ( $formData['live_item'] == 'true' )? 1 : 0 ;
                }

                $shareToContact = "";
                $shareToEmail = "";

                if(isset($formData['shareToContact'])) {
                    $shareToContact = $formData['shareToContact'] ;
                }

                if(isset($formData['shareToEmail'])) {
                    $shareToEmail = $formData['shareToEmail'];
                }


                foreach($formData as $field => $value){
                  if(!in_array($field, $UserCardValidField)) {
                    unset($formData[$field]);
                  }
                }


                $itemRow->update($formData);


                if($shareToContact) {
                  foreach($shareToContact as $contact) {
                      if( !UserChildCard::where('card_id', $itemRow->id)->where('user_id', $contact['id'])->count()) {
                        UserChildCard::insert([
                          'card_id' => $itemRow->id,
                          'user_id' => $contact['id'],
                          'can_edit' => 1
                        ]);
                      }
                    }
                }

                if($shareToEmail) {
                    $contactDetail = $authUser;
                    $contactDetail->email = $shareToEmail;
                    $contactDetail->cardId = $itemRow->id;

                    $contactDetail->notify(new ShareItemNotification());
                }

                // Send Notificaiton to all Users;
                $sharedUserList = UserChildCard::where('card_id', $itemId)
                                          ->where('is_accepted', 1)
                                          ->get();
                if($sharedUserList) {
                  foreach($sharedUserList as $sharedUser) {
                      $sharedUser->title = $itemRow->title;
                      $sharedUser->user_name = $authUser->name;

                      if($itemRow->allow_notification)
                        $sharedUser->notify(new CardChangedNotification());
                  }
                }

                if($itemRow->owner_id != $authUser->id) {
                    $sharedUser = User::where('id', $itemRow->owner_id)->first();
                    $sharedUser->title = $itemRow->title;
                    $sharedUser->user_name = $authUser->name;

                    if($itemRow->allow_notification)
                      $sharedUser->notify(new CardChangedNotification());
                }


                return response()->json([
                    'error_code'=>200,
                    'msg_string'=> __('messages.PROCESS_SUCCESS'),
                    'result' => []
                ]);
              } else {

                  return response()->json([
                    'error_code'=>500,
                    'msg_string'=> __('messages.UNAUTHRIZED_ACCESS'),
                    'result' => []
                  ]);

              }

          }
        }

        return response()->json([
      		'error_code'=>500,
      		'msg_string'=> __('messages.INVALID_REQUEST'),
      		'result' => []
      	]);

    }

    public function deleteCard(Request $request){
      $authUser = $this->getCurrentUser($request);
      if($itemId = $request->get('id', null)) {
        UserCard::where('owner_id', $authUser->id)
                    ->where('id', $itemId)
                    ->delete();

          return response()->json([
              'error_code'=>200,
              'msg_string'=> __('messages.PROCESS_SUCCESS'),
              'result' => []
          ]);
      }

      return response()->json([
        'error_code'=>500,
        'msg_string'=> __('messages.INVALID_REQUEST'),
        'result' => []
      ]);
    }


    public function deleteAllCards(Request $request){

      $authUser = $this->getCurrentUser($request);

      if($cardIds = $request->get('cardIds', null)) {

        foreach ($cardIds as $key => $value) {

            $itemRow = UserCard::where('owner_id', $authUser->id)->where('id', $value)->first();
            if($itemRow) {
              $itemRow->delete();
            }
        }

        return response()->json([
            'error_code'=>200,
            'msg_string'=> __('messages.PROCESS_SUCCESS'),
            'result' => []
        ]);

      }

      return response()->json([
        'error_code'=>500,
        'msg_string'=> __('messages.INVALID_REQUEST'),
        'result' => []
      ]);

    }


    public function shareCard(Request $request){
      $authUser = $this->getCurrentUser($request);

      if($itemId = $request->get('id', null)) {

        $itemRow = UserCard::where('owner_id', $authUser->id)->where('id', $itemId)->first();

        if( !empty($itemRow) ) {

          $insertData['card_id'] = $itemId;

          if( $itemRow['live_item'] == 1 && $itemRow['two_way_sync'] == 0 ){

            $insertData['can_edit'] = 0;
          }else{

            $insertData['can_edit'] = 1;
          }

          $userList = $request->get('to_users', null);

          if($userList) {
            foreach($userList as $user) {

                $insertData['user_id'] = $user;
                UserChildCard::insert( $insertData );


                $contactDetail = User::where('id', $user)->first();

                if($contactDetail) {
                  $contactDetail->user_name = $authUser->name;

                  if($itemRow->allow_notification) {
                    $contactDetail->notify(new CardSharingNotification());
                  }
                }
            }

            return response()->json([
                'error_code'=>200,
                'msg_string'=> __('messages.PROCESS_SUCCESS'),
                'result' => []
            ]);
          }
        }
      }

      return response()->json([
        'error_code'=>500,
        'msg_string'=> __('messages.INVALID_REQUEST'),
        'result' => []
      ]);
    }



    public function browseCard(Request $request){

      $authUser = $this->getCurrentUser($request);
      $query = UserCard::join('users', 'users.id', '=', 'user_cards.owner_id')
                          ->join('profile', 'users.id', '=', 'profile.user_id')
                          ->join('card_types', 'card_types.id', '=', 'user_cards.card_type_id')
                          ->leftjoin('countries', 'countries.id', '=', 'user_cards.location_country')
                          ->leftjoin('regions', 'regions.id', '=', 'user_cards.location_province')
                          ->leftjoin('cities', 'cities.id', '=', 'user_cards.location_city')
                          ->where('user_cards.is_active', '=', 1)
                          ->where('user_cards.share_on_showcase', '=', 1)
                          ->select(DB::raw('users.id as userId'), 'profile.gender', 'users.name', 'user_cards.*', 'profile.profile_image', 'countries.name as countryName', 'regions.name as provinceName', 'cities.name as cityName');


      if($filter = $request->get('filter', null)) {

        foreach($filter as $field => $value) {
          if(trim($value)) {
            switch($field) {
              case "cardId" : $query = $query->where('user_cards.id', $value); break;
              case "notCardId" :  $cardTypeId = UserCard::where('id', $value)->select('card_type_id', 'owner_id')->first();

                                  /*if($cardTypeId) {

                                    $cardChildList = CardType::where('parent_group_id', $cardTypeId->card_type_id)->select('id')->get();

                                    if($cardChildList) {
                                      $cardChildList = $cardChildList->toArray();
                                        if(count($cardChildList)) {
                                          $cardChildList = array_column($cardChildList, 'id');

                                          if(count($cardChildList)) {
                                              $query = $query->where('user_cards.id', '<>', $value)
                                                             ->where('user_cards.owner_id', $cardTypeId->owner_id)
                                                             ->whereIn('user_cards.card_type_id', $cardChildList);
                                          }
                                      } else {
                                          $query = $query->where('user_cards.id', '<>', $value)
                                                         ->where('user_cards.owner_id', $cardTypeId->owner_id)
                                                         ->whereIn('user_cards.card_type_id', [$cardTypeId->card_type_id]);
                                      }
                                    }

                                  }*/

                                  $query = $query->where('user_cards.id', '<>', $value)
                                                 ->where('card_types.parent_group_id', $cardTypeId->card_type_id);

                                  break;
              case "searchText" : $query = $query->where(function($query) use($value){
                                                return $query->where('users.name', 'like', '%' . $value . '%')
                                                      ->orWhere('user_cards.title', 'like', '%' . $value . '%')
                                                      ->orWhere('user_cards.body', 'like', '%' . $value . '%');
                                           }); break;
              case "user" : $query = $query->where('users.name', 'like', '%' . $value . '%'); break;
              case "group" : $query = $query->where('user_cards.card_type_id', '=', $value); break;
              case "city" : $query = $query->where('user_cards.location_city', 'like', '%' . $value . '%'); break;
              case "province" : $query = $query->where('user_cards.location_province', 'likes', '%' . $value . '%'); break;
              case "date" : $query = $query->where('user_cards.card_date', '=', $value); break;

            }
          }
        }
      }

      $sortFilter = $request->get('sort', null);

      if($sortFilter) {
        foreach($sortFilter as $sortBy => $sortType) {
          switch($sortBy) {
            case "by_date" : $query = $query->orderBy('user_cards.card_date', $sortType); break;
          }
        }
      }

      if($filter && isset($filter['cardId'])){
          $result = $query->first();
      } else {
        $result = $query->paginate(12);
      }

      $others['countries']  = Country::select( 'id', 'name', 'code' )->get();
      $others['users']  = User::where( 'is_active', 1 )->select( 'id', 'name' )->get();

    	return response()->json([
      		'error_code'=>200,
      		'msg_string'=> __('messages.PROCESS_SUCCESS'),
      		'result' => $result,
          'others' => $others
    	]);
    }

    public function reportAbuse(Request $request){
      $authUser = $this->getCurrentUser($request);


      $insertData = [
        'user_id' => $authUser->id,
        'item_id' => $request->get('card_id'),
        'item_type' => 'card'
      ];

      if(
        ReportAbuse::where('user_id', $insertData['user_id'])
                    ->where('item_id', $insertData['item_id'])
                    ->where('item_type', 'card')
                    ->count()
      ) {
        return response()->json([
          'error_code'=> 500,
          'msg_string'=> __('messages.ALREADY_REPORTED_CARD'),
          'result' => []
        ]);
      }

      $reportID = $request->get('report_id');

      foreach ($reportID as $key => $value) {
        $insertData['report_id'] = $value;
        ReportAbuse::insert($insertData);
      }

      return response()->json([
        'error_code'=>200,
        'msg_string'=> __('messages.PROCESS_SUCCESS'),
        'result' => []
      ]);
    }

    public function getReports(Request $request){
      $authUser = $this->getCurrentUser($request);

      $result = Report::get();

      return response()->json([
        'error_code'=>200,
        'msg_string'=> __('messages.PROCESS_SUCCESS'),
        'result' => $result
      ]);
    }

    public function getContacts(Request $request){
      $authUser = $this->getCurrentUser($request);

      $params = $request->only('card_id');

      $result = Contact::join('users', 'users.id', '=', 'contacts.contact_id')
                    ->where('contacts.owner_id', $authUser->id)
                    ->where('contacts.is_active', 1)
                    ->select(DB::raw('distinct(users.id) as userId'), 'users.name')
                    ->get();

      foreach ($result as $key => $value) {

        $result[$key]['alreadyShared'] = 0;

        $alreadyShared = UserChildCard::where( 'user_id', $value['userId'] )
        ->where( 'card_id', $params['card_id'] )
        ->first();

        $result[$key]['alreadyShared'] = !empty($alreadyShared)? 1 : 0;
      }

      return response()->json([
        'error_code'=>200,
        'msg_string'=> __('messages.PROCESS_SUCCESS'),
        'result' => $result
      ]);

    }


    public function unique_multidim_array($array, $key)
    {
        $temp_array = array();
        $i = 0;
        $key_array = array();

        foreach($array as $val) {

            if (!in_array($val[$key], $key_array)) {
                $key_array[$i] = $val[$key];
                $temp_array[$i] = $val;
            }
            $i++;
        }
        return $temp_array;
    }


    public function getShowcase(Request $request) {
      $authUser = $this->getCurrentUser($request);

      $userId = $request->get('user_id', $authUser->id);
      $query = UserCard::join('users', 'users.id', '=', 'user_cards.owner_id')
                        ->join('profile', 'users.id', '=', 'profile.user_id')
                        ->join('card_types', 'card_types.id', '=', 'user_cards.card_type_id')
                        ->leftjoin('countries', 'countries.id', '=', 'user_cards.location_country')
                        ->leftjoin('regions', 'regions.id', '=', 'user_cards.location_province')
                        ->leftjoin('cities', 'cities.id', '=', 'user_cards.location_city')
                        ->where('user_cards.is_active', '=', 1)
                        ->where('users.id', '=', $userId)
                        ->where('user_cards.share_on_showcase', '=', 1);

      $result = $query->orderBy('user_cards.showcase_order', 'asc');
      $result = $query->select('user_cards.*', 'card_types.name', 'countries.name as countryName', 'regions.name as provinceName', 'cities.name as cityName')->paginate(8);

      $others['countries']  = Country::select( 'id', 'name', 'code' )->get();

      $itemGroups = [];
      if($group = $request->get('group', null)) {
          $itemGroups = CardType::where('id', $group)
                                  ->orWhere(function($query) use($group, $authUser){
                                      return $query->where('parent_group_id', $group)
                                            ->where('owner_id', $authUser->id);
                                  })
                                  ->orderBy('card_types.id')
                                  ->get();
      }

      return response()->json([
        'error_code'=>200,
        'msg_string'=> __('messages.PROCESS_SUCCESS'),
        'result' => $result,
        'group' => $itemGroups,
        'others' => $others
      ]);
    }


    public function uploadImage(Request $request){
      $formData = $request->get('file', []);

      if(isset($formData['card_image']) && !empty($formData['card_image']) ) {
        $pos = strpos($formData['card_image'], 'http');

        if($pos === false) {
            $extenstion = preg_replace('#^data:image/\w+;base64,#i', '', $formData['card_image']);
            $extenstion = str_replace(['data:', 'image/', ';base64,', $extenstion], ['', '', '', ''], $formData['card_image']);

            $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $formData['card_image']));
            $photoName= uniqid('cardImg_') . '.' . $extenstion;
            $photoPath = env('APP_UPLOAD_FOLDER', '/public/images/uploads/') . $photoName;
            file_put_contents( $_SERVER['DOCUMENT_ROOT'] . $photoPath, $data);
            $formData['card_image'] = env('APP_URL') . $photoPath;
        }
      }
    }

    public function updateItemNotification(Request $request) {
          $allowNotification = $request->get('notification', 0);
          $cardId = $request->get('itemId', 0);

          $authUser = $this->getCurrentUser($request);

          $cardDetail = UserCard::where('id', $cardId)
                            ->first();

          if($cardDetail) {
              $cardDetail->allow_notification = $allowNotification;
              $cardDetail->save();

              return response()->json([
                'error_code'=>200,
                'msg_string'=> (($allowNotification)
                                  ? __('messages.CARD_NOTIFICATION_ACTIVE')
                                  : __('messages.CARD_NOTIFICATION_DEACTIVE')),
                'result' => [],
              ]);
          }

          return response()->json([
            'error_code'=>500,
            'msg_string'=> __('messages.PROBLEM_OCCURED'),
            'result' => [],
          ]);
    }


    function shareItemViaEmail(Request $request){
        $authUser = $this->getCurrentUser($request);

        $userEmail = $request->get('email', '');
        $cardId = $request->get('card', '');

        if($userEmail && $cardId) {
          $authUser->email = $userEmail;
          $authUser->cardId = $cardId;
          $authUser->notify(new ShareItemNotification());
        }

        return response()->json([
          'error_code'=>200,
          'msg_string'=> __('messages.SUCCESSFULLY_SHARED'),
          'result' => [],
        ]);
    }
}
