<?php

namespace App\Http\Controllers\Website;

use DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use JWTAuth;
use JWTAuthException;

use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Stripe\Error\Card;

use App\Profile;
use App\User;
use App\Country;
use App\UserSubscription;
use App\UserNotification;

class ProfileController extends Controller
{
    public function __construct() {
      $this->stripe = Stripe::make(env('STRIPE_SECRET'));
    }

    public function getCurrentUser($request) {
        return JWTAuth::toUser($request->token);
    }

    public function getProfile(Request $request){
        $user = $this->getCurrentUser($request);


        if($user) {
            $userId = $request->get('id', $user->id);
            $profileDetail = User::join('profile', 'profile.user_id', '=', 'users.id')
                                    ->join('user_subscription', 'user_subscription.id', '=', 'users.subscription_id')
                                    ->where('users.id', $userId)
                                    ->first();

            if(!$request->get('id', null)) {

              if($profileDetail->stripe_id && $profileDetail->card_id) {

                  $cards = $this->stripe->cards()->all( $profileDetail->stripe_id );
                  foreach ($cards['data'] as $card) {
                      if($card['id'] ==  $profileDetail->card_id) {
                           $usercard = $this->stripe->cards()->find($profileDetail->stripe_id, $profileDetail->card_id);
                          if($usercard) {
                            $profileDetail->number = "XXXXXXXXXXXX" . $usercard['last4'];
                            $profileDetail->exp_date = $card['exp_month'] . "/" .$usercard['exp_year'];
                          }
                      }
                  }
              }
           }

          $others['countries']  = Country::select( 'id', 'name', 'code' )->get();

         	return response()->json([
        		'error_code'=>200,
        		'msg_string'=> __('messages.PROCESS_SUCCESS'),
        		'result' => $profileDetail,
            'others' => $others
      	  ]);
        }
    }

    public function updatePassword(Request $request) {
        $user = $this->getCurrentUser($request);

        if (Hash::check($request->get('oldPassword', null), $user->password)) {

          $user->password = bcrypt($request->get('newPassword', null));
          $user->save();

          return response()->json([
              'error_code'=>200,
              'msg_string'=> __('messages.RESET_PASSWORD_SUCCESS'),
              'result' => []]
          );

        }else{
          return response()->json([
                'error_code'=>500,
                'msg_string'=> __('messages.PASSWORD_NOT_MATCHING'),
                'result' => []]
            );
        }
    }

    public function saveCoverImage(Request $request) {

        $user = $this->getCurrentUser($request);
        $coverImage = $request->only('cover_image');

        if( !empty($coverImage['cover_image']) ) {

          $pos = strpos($coverImage['cover_image'], 'http');

          if($pos === false) {
              $extenstion = preg_replace('#^data:image/\w+;base64,#i', '', $coverImage['cover_image']);
              $extenstion = str_replace(['data:', 'image/', ';base64,', $extenstion], ['', '', '', ''], $coverImage['cover_image']);

              $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $coverImage['cover_image']));
              $photoName= uniqid('coverImg_') . '.' . $extenstion;
              $photoPath = env('APP_UPLOAD_FOLDER', '/public/images/uploads/') . $photoName;
              file_put_contents( $_SERVER['DOCUMENT_ROOT'] . $photoPath, $data);
              $coverImage['cover_image'] = env('APP_URL') . $photoPath;

              Profile::where('user_id', $user->id)->update($coverImage);

              return response()->json([
                'error_code'=>200,
                'msg_string'=> __('messages.PROCESS_SUCCESS'),
                'result' => []]
              );
          }

        }else{

          return response()->json([
            'error_code'=>500,
            'msg_string'=> __('messages.INVALID_REQUEST'),
            'result' => []]
          );

        }
    }

    public function updateProfile(Request $request) {

        $validation = Validator::make($request->all(), [
            'name'      => 'required|max:80',
            'email'     => 'required|email|max:100',
            'gender'    => 'required',
            'dob'       => 'required',
            'city'      => 'required',
            'province'  => 'required',
            'country'   => 'required'
          ]);

        if($validation->fails()){

            $errors = $validation->errors();
            return response()->json([
              'error_code'=>500,
              'msg_string'=> __('messages.VALIDATION_FAIL'),
              'result' => array('errors' => $errors),
            ]);

        }else{

          $user = $this->getCurrentUser($request);

          $params = $request->only('name', 'email', 'gender', 'password', 'dob', 'city', 'province', 'country', 'profile_image');

         if(isset($params['profile_image']) && $params['profile_image']) {
            $updateProfile['profile_image'] = $params['profile_image'];
          }


        if(isset($params['cover_image']) && $params['cover_image']) {
            $pos = strpos($params['cover_image'], 'http');

              if($pos === false) {
                  $extenstion = preg_replace('#^data:image/\w+;base64,#i', '', $params['cover_image']);
                  $extenstion = str_replace(['data:', 'image/', ';base64,', $extenstion], ['', '', '', ''], $params['cover_image']);

                  $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $params['cover_image']));
                  $photoName= uniqid('coverImg_') . '.' . $extenstion;
                  $photoPath = env('APP_UPLOAD_FOLDER', '/public/images/uploads/') . $photoName;
                  file_put_contents( $_SERVER['DOCUMENT_ROOT'] . $photoPath, $data);
                  $updateProfile['cover_image'] = env('APP_URL') . $photoPath;
              }
          }

          $updateUser['name']         = $params['name'];
          $updateUser['email']        = $params['email'];

          $updateProfile['gender']    = $params['name'];
          $updateProfile['dob']       = $params['dob'];
          $updateProfile['city']      = $params['city'];
          $updateProfile['province']  = $params['province'];
          $updateProfile['country']   = $params['country'];

          User::where('id', $user->id)->update($updateUser);
          Profile::where('user_id', $user->id)->update($updateProfile);

          return response()->json([
              'error_code'=>200,
              'msg_string'=> __('messages.PROCESS_SUCCESS'),
              'result' => []]
          );
        }

    }

    public function getSetting(Request $request){}

    public function updateSetting(Request $request){
        $authUser = $this->getCurrentUser($request);
        $params = $request->only( 'updateSubscription_name', 'updateSubscription_type' );

        if( !empty($params) ) {
            $userData = User::where( 'id', $authUser->id )->first();

            $response = UserSubscription::where( 'subscription_name', $params['updateSubscription_name'] )->where('subscription_type', $params['updateSubscription_type'])->where('is_active', '1' )->first();

            if( !empty($response) && !empty($userData) ){

              if( $userData->subscription_id != $response['id'] ){

                $updateUser['subscription_id'] = $response['id'];
                User::where( 'id', $authUser->id )->update($updateUser);

                return response()->json([
                  'error_code'=>200,
                  'msg_string'=> __('messages.PROCESS_SUCCESS'),
                  'result' => []]
                );

              }

            }

            return response()->json([
                'error_code'=>500,
                'msg_string'=> "Invalid request",
                'result' => []]
            );

        }

        return response()->json([
          'error_code'=>500,
          'msg_string'=> __('messages.INVALID_REQUEST'),
          'result' => []]
      );

    }


    public function updateCard(Request $request){
            $authUser = $this->getCurrentUser($request);

            if(!$authUser->stripe_id) {
                $user_stripe = $this->stripe->customers()->create([
                      'email' => $authUser->email
                ]);
                $authUser->stripe_id = $user_stripe['id'];
                $authUser->save();
            }


            $paymentData = $request->only(
                          'name',
                          'number',
                          'exp_date',
                          'cvc'
            );

            $expDate = explode("/", $paymentData['exp_date']);

            if(count($expDate) == 2) {
                $paymentData['exp_month'] = $expDate[0];
                $paymentData['exp_year'] = $expDate[1];

                unset($paymentData['exp_date']);
            } else {
                return response()->json([
                    'error_code'=>500,
                    'msg_string'=> "Invalid exprie date example - 05/2018",
                    'result' => []]
                );
            }


            try{
              $cardToken = $this->stripe->tokens()->create([
                  'card' => $paymentData
              ]);

              $card = $this->stripe->cards()->create($authUser->stripe_id, $cardToken['id']);

              $authUser->card_id = $card['id'];
              $authUser->save();

              return response()->json([
                  'error_code'=>200,
                  'msg_string'=> __('messages.PROCESS_SUCCESS'),
                  'result' => []]
              );
            } catch(Exceptions $e){
              return response()->json([
                  'error_code'=>500,
                  'msg_string'=> $e->getMessage(),
                  'result' => []]
              );
            };
    }

    public function deleteCard(Request $request){
      $authUser = $this->getCurrentUser($request);
      if($authUser->stripe_id && $authUser->card_id) {
          $this->stripe->cards()->delete($authUser->stripe_id, $authUser->card_id);

          return response()->json([
              'error_code'=>200,
              'msg_string'=> __('messages.PROCESS_SUCCESS'),
              'result' => []]
          );
      }

      return response()->json([
          'error_code'=>500,
          'msg_string'=> __('messages.INVALID_REQUEST'),
          'result' => []]
      );

    }

    public function disableAccount(Request $request) {
        $authUser = $this->getCurrentUser($request);
        $authUser->is_active = 0;
        $authUser->save();

        return response()->json([
            'error_code'=>200,
            'msg_string'=> __('messages.PROCESS_SUCCESS'),
            'result' => []]
        );
    }

    public function getUserNotifications(Request $request) {
        $authUser = $this->getCurrentUser($request);
        $result = [];
        $notificationCount = UserNotification::where('user_id', $authUser->id)
                                                ->where('is_active', 1)
                                                ->groupBy('section')
                                                ->select('section', DB::raw('COUNT(*) as total'))
                                                ->get();

        foreach($notificationCount as $notificationRow) {
            $result[$notificationRow->section] = $notificationRow->total;
        }
        return response()->json([
            'error_code'=>200,
            'msg_string'=> __('messages.PROCESS_SUCCESS'),
            'result' => $result
        ]);
    }

    public function removeUserNotifications(Request $request){
        $authUser = $this->getCurrentUser($request);
        $notificationCount = UserNotification::where('user_id', $authUser->id)
                                                ->where('section', $request->get('type', null))
                                                ->update(['is_active' =>  0]);

        return $this->getUserNotifications($request);
    }
}
