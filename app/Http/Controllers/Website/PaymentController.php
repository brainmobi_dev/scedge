<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Stripe\Error\Card;

use App\UserSubscription;

class PaymentController extends Controller
{
    public function __construct() {
        $this->stripe = Stripe::make(env('STRIPE_SECRET'));
    }
    public function getCurrentUser($request) {
        return JWTAuth::toUser($request->token);
    }
    
    public function getPlan() { 
        $plans = $this->stripe->plans()->all();
        foreach($plans['data'] as $plan) {
            UserSubscription::create([
                'subscription_code' => $plan->id,
                'subscription_name' => $plan->name,
                'subscription_amount' => $plan->amount,
                'subscription_type' => (( $plan->id == 'FREE') ? 'ALL' : (($plan->interval == 'year') ? 'ANNUAL' : 'MONTHLY')),
                'is_active' => 1
            ]);
        }
    }

    public function createSubscription(Request $request)
    {
        $AuthUser = $this->getCurrentUser($request);
        
        $this->stripeCustomerId = $AuthUser->stripe_id;
        if(!$this->stripeCustomerId ) {
            $this->stripeCustomerId  = $this->createStripeCustomer($request);
        }

        $subscription = $this->stripe->subscriptions()->create($this->stripeCustomerId, [
            'plan' => 'FREE',
        ]);

        $AuthUser->subscription_id = $subscription['id'];
        $AuthUser->save();
 
        return $subscription['id'];

    }

    public function updateSubscription() {
        $AuthUser = $this->getCurrentUser($request);
        
        $this->stripeCustomerId = $AuthUser->stripe_id;
        $subscripitionId =  $AuthUser->subscription_id;

        $subscription = $this->stripe->subscriptions()->update($this->stripeCustomerId, $subscripitionId, [
            'plan' => 'PER_MONT'
        ]);

        $AuthUser->subscription_id = $subscription['id'];
        $AuthUser->save();

        echo $subscription['id'];
    }

    public function createCard(Request $request) {
       try {
            $AuthUser = $this->getCurrentUser($request);
            $this->stripeCustomerId = $AuthUser->stripe_id;
            $token = $this->stripe->tokens()->create([
                'card' => [
                    'number'    => '424242424242424242',
                    'exp_month' => 6,
                    'exp_year'  => 2015,
                    'cvc'       => 314,
                ]
            ]);

            $card = $this->stripe->cards()->create($this->stripeCustomerId, $token['id']);
            return $card['id'];
        } catch (Exception $e) {
            echo $e->getMessage();
        }
       
    }

    public function updateCard(Request $request) {
        $AuthUser = $this->getCurrentUser($request);
        
        $this->stripeCustomerId = $AuthUser->stripe_id;
        $cards = $this->stripe->cards()->all($this->stripeCustomerId);

        $customerCardId = null;
        foreach ($cards['data'] as $card) {
            $customerCardId = $card['id'];
        }

        $this->createCard($request);
        
        if($customerCardId) {
            $this->deleteCard($this->stripeCustomerId, $customerCardId);
        }
    }

    public function deleteCard($customerId, $cardId){
        $AuthUser = $this->getCurrentUser($request);
        $this->stripe->cards()->delete($customerId, $cardId);
    }

    public function getCards(Request $request) {
        $AuthUser = $this->getCurrentUser($request);
        $this->stripeCustomerId = $AuthUser->stripe_id;
        $cards = $this->stripe->cards()->all($this->stripeCustomerId);

        foreach ($cards['data'] as $card) {
            var_dump($card['id']);
        }
    }

    public function createStripeCustomer(Request $request)
    {
        $AuthUser = $this->getCurrentUser($request);
        $customer = $this->stripe->customers()->create([
            'email' => $AuthUser->email
        ]);
        
        $AuthUser->stripe_id = $customer['id'];
        $AuthUser->save();
 
        return $customer['id'];
    }
}
