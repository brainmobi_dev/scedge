<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\UserCard;
use App\UserchildCard;
use App\CardType;
use Illuminate\Support\Facades\Validator;
use App\Country;
use App\Region;
use App\City;

use Illuminate\Support\Facades\DB;

class ItemController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
     public function __construct()
     {
         $this->middleware('auth:admin');
     }

    /**
     * Show the item info
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sortData = $request->only('sorting_order', 'sorting_field');
        $searchData = $request->search_text;

        if( !$sortData['sorting_order'] ) {
            $sortData['sorting_order'] = 'asc';
        }

        if( !$sortData['sorting_field'] ) {
            $sortData['sorting_field'] = 'users.name';
        }

        $userCards = UserCard::leftJoin( 'users', 'users.id', '=', 'user_cards.owner_id' )
                               ->Where('users.name','like','%'.$searchData.'%')
                               ->orderBy($sortData['sorting_field'], $sortData['sorting_order'])
                               ->select( 'user_cards.*', 'users.name' )
                               ->paginate(10);

        return view('admin.item.index',['userCards'=> $userCards ,
                                        'request'  => $request,
                                        'getSortingClass' => function($field) use($sortData) {

                                        if($sortData['sorting_field'] == $field) {
                                            if($sortData['sorting_order'] == 'desc') {
                                                return "sorting_desc";
                                            } else {
                                                return "sorting_asc";
                                            }
                                        } else {
                                            return "sorting";
                                        }
                                        },

                                        'getSortinOrder' => function($field) use($sortData) {
                                            if($sortData['sorting_field'] == $field) {
                                                if($sortData['sorting_order'] == 'desc') {
                                                    return "asc";
                                                } else {
                                                    return "desc";
                                                }
                                            } else {
                                                return "asc";
                                            }
                                        }
                                    ]);
    }

    /*
     * Show item details
     *
     */
    public function view(Request $request){

        $cardID = $request->item_id;

        $card = UserCard::leftJoin( 'users', 'users.id', '=', 'user_cards.owner_id' )
                        ->select( 'user_cards.*', 'users.name' )
                        ->where('user_cards.id',$cardID)
                        ->first();

        if( $card->card_type == 'PROFESSIONAL' ){
                $personal = 'style = display:none';
                $professional = 'style = display:block';

        }elseif($card->card_type == 'FREE' || $card->card_type == 'PERSONAL'){
                $professional = 'style=display:none';
                $personal = 'style=display:block';
        }


        return view('admin.item.view',['card'=> $card ,
                                       'request'=> $request,
                                       'personal'=> $personal,
                                       'professional'=> $professional]);
    }

    /*
     * View edit form of item
     *
     */

     public function edit(Request $request)
    {
        $cardID = $request->item_id;
        
        if($request->isMethod("post")) {
            //Iterate
            $userValues = $request->all();
            
            ///////get date and time
            $card_date_time = explode("-", $request->card_date_time);
            
            //card date
            $card_date = strtr(trim($card_date_time[0]), '/', '-');
            $userValues['card_date'] = date("Y-m-d", strtotime($card_date));
            //card time
            $card_time = $card_date_time[1];
            $userValues['card_time'] = $card_time;
            
            ///////get start date and time
            $card_start_date_time = explode("-", $request->card_start_date_time);
            
            //card start date
            $card_start_date = strtr(trim($card_start_date_time[0]), '/', '-');
            $userValues['start_date'] = date("Y-m-d", strtotime($card_start_date));
            //card start time
            $card_start_time = $card_start_date_time[1];
            $userValues['start_time'] = $card_start_time;
            
            //////get start date and time
            $card_end_date_time = explode("-", $request->card_end_date_time);
            
            //card start date
            $card_end_date = strtr(trim($card_end_date_time[0]), '/', '-');
            $userValues['end_date'] = date("Y-m-d", strtotime($card_end_date));
            //card start time
            $card_end_time = $card_end_date_time[1];
            $userValues['end_time'] = $card_end_time;
            
            
            
            //dd($userValues);
            $UserCardValidField = ["title", "body", "card_date", "card_time", "priority", "location_address", "location_city",
                   "location_province", "location_country", "start_date", "start_time", "end_date",
                   "end_time", "price", "quantity", "discount", "spacs_size", "spacs_brand", "spacs_model",
                   "spacs_color", "for_gender", "for_age", "share_on_showcase", "two_way_sync", "live_item",
                   "card_type", "allow_notification", "is_active", "card_order", "showcase_order", "currency",
                   "url", "category", "contact_info", "card_status", "street_address"
                 ];

             $dataToStore = [];
             foreach($UserCardValidField as $fields) {
                 if(isset($userValues[$fields]))
                    $dataToStore[$fields] = $userValues[$fields];
             }
             
              $profileImage   = $request->file('card_image');

                    if(!empty( $profileImage)){

                        $profileImageName = 'card_'.time().'.'.$profileImage->getClientOriginalExtension();
                        $destinationPath = public_path('admin/uploads/card/');

                        $profileImage->move($destinationPath , $profileImageName );

                         $dataToStore['card_image'] =  'admin/uploads/card/' . $profileImageName;
                    }

          
              UserCard::where('id', $cardID)->update( $dataToStore );
              
              return redirect()->back()->withErrors(['message','User card detail updated successfully.']);
        }
              
        $card = UserCard::leftJoin( 'users', 'users.id', '=', 'user_cards.owner_id' )
                        ->select( 'user_cards.*', 'users.name' )
                        ->where('user_cards.id',$cardID)
                        ->first();
        
        return view('admin.item.edit',[ 'card'      => $card,
                                       'request'    => $request,
                                       ]);
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $itemId = $request->item_id;
        //if request method is post
        if( $request->isMethod('post') ){
            
            UserchildCard::Where('card_id',$itemId)->delete();
            CardType::Where('id',$itemId)->delete();
            $user = UserCard::where('id',$itemId);
            $user->delete();
            
            return response()->json([
    		'error_code' => 200,
    		'msg_string'=> __('messages.PROCESS_SUCCESS'),

            ]);
        }

    }

    /*
     * Change user status
     *
     */
    public function changeStatus(Request $request){
        $itemId = $request->item_id;
        $itemData = array('is_active'=> $request->status);
        //if request method is post
        if( $request->isMethod('post') ){
           UserCard::where('id', $itemId)->update($itemData);
           return response()->json([
    		'error_code' => 200,
    		'msg_string'=> __('messages.PROCESS_SUCCESS'),

            ]);
        }

    }

}
