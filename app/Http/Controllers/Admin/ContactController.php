<?php

namespace App\Http\Controllers\Admin;

use DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use App\Contact;
use App\ContactGroup;
use App\User;
use App\Custom\Customhelper;

class ContactController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
     public function __construct()
     {
         $this->middleware('auth:admin');
     }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $searchData = $request->search_text;
        $sortData = $request->only('sorting_order', 'sorting_field');

        $ownerData = User::paginate(10);

        if( !$sortData['sorting_order'] ) {
            $sortData['sorting_order'] = 'asc';
        }

        if( !$sortData['sorting_field'] ) {
            $sortData['sorting_field'] = 'users.name';
        }

        $ownerRowset = User::Where('name','like','%'.$searchData.'%')
                            ->orderBy($sortData['sorting_field'], $sortData['sorting_order'])
                            ->paginate(10);
        $groupType   = ContactGroup::get();


        foreach($ownerRowset as $ownerRow){
            $userId = $ownerRow->id;
            
            $ownerRow->userContactGroup = User::join('contact_group','users.id','=','contact_group.user_id')
                                                ->select('users.*', 'contact_group.name AS contact_group_name')
                                                ->Where([
                                                    ['contact_group.user_id',$userId],
                                                    ['contact_group.is_active',1]
                                                ])
                                                ->get();
            
            $ownerRow->userContactGroupCount = count($ownerRow->userContactGroup);
            
            $ownerRow->userContacts = User::join('contacts','users.id','=','contacts.owner_id')
                                                ->select('users.*', 'contacts.contact_group_id AS contact_id')
                                                ->Where([
                                                    ['contacts.owner_id',$userId],
                                                    ['contacts.is_active',1]
                                                ])
                                                ->get();
            
            $ownerRow->userContactsCount = count($ownerRow->userContacts);

//            $ownerRow->personalMembers = Contact::join('contact_group','contact_group.id','=','contacts.contact_group_id')
//                                                ->join('users','users.id','=','contacts.contact_id')
//                                                ->select('contacts.*','contact_group.name AS contact_group_name','users.*')
//                                                ->Where('contacts.owner_id',$userId)
//                                                ->Where('contact_group_id','1')
//                                                ->get();
//
//            $ownerRow->personalMembersCount = count($ownerRow->personalMembers);

        }
        //dd($ownerRowset);
        return view('admin.contact.index',['request'    => $request ,
                                           'ownerRowset' => $ownerRowset ,
                                           'ownerData'   => $ownerData,
                                           'groupTypes'  => $groupType,
                                           'getSortingClass' => function($field) use($sortData) {
                                            if($sortData['sorting_field'] == $field) {
                                                if($sortData['sorting_order'] == 'desc') {
                                                    return "sorting_desc";
                                                } else {
                                                    return "sorting_asc";
                                                }
                                            } else {
                                                return "sorting";
                                            }
                                            },

                                            'getSortinOrder' => function($field) use($sortData) {
                                                if($sortData['sorting_field'] == $field) {
                                                    if($sortData['sorting_order'] == 'desc') {
                                                        return "asc";
                                                    } else {
                                                        return "desc";
                                                    }
                                                } else {
                                                    return "asc";
                                                }
                                            }

                                         ]);
    }


    /*
     * Function to get list of all numbers
     *
     */
    public function group_members_list(Request $request){
        //$contactGroupId = $request->contact_group_id;
                
        $userId = $request->id;
        
        //Group lists
        $groupQuery = ContactGroup::Where('user_id',$userId)
                                ->orderBy('name');
        
        
        $groups = $groupQuery->get();
        $groupArrayList = $groups->toArray();
        
        $groupArray = [];
        foreach($groupArrayList as $groupList){
                $groupArray[$groupList['id']] = $groupList;
        }
        
        //contact group member
        $groupMembers = DB::table('contact_group')->select('contact_group.*', 'contacts.contact_group_id','contacts.contact_id','contacts.owner_id','users.name AS contactname')
                                        ->join('contacts','contacts.contact_group_id','contact_group.id')
                                        ->join('users','users.id','contacts.contact_id')
                                        ->where('contact_group.user_id', $userId)
                                        ->orderBy('users.name')
                                        ->get();
     
        $groupMembers = ($groupMembers->toArray());
        
        
        //if not empty?
        $contactByGroup = [];
        if($groupMembers) {
            foreach($groups as $group) {
                $groupId = $group->id;
                $result = (array_filter($groupMembers, function($var) use($groupId){
                        return ($var->id == $groupId);
                }));
                
                $contactByGroup[$groupId]  = array_values($result);
            }
        }
       
        return view('admin.contact.group_member_list',[
                                                'contactByGroup' => $contactByGroup,
                                                'groups' => $groups,
                                                'groupArray' => $groupArray,
                                                'request' =>  $request
                                               ]);


    }
    
    /*
     * Function to update group
     *
     */
    public function groupEdit(Request $request){
        
        //collect request data
        $groupId = $request->groupid;
        $value = $request->name;
        
        //if request method is post
        
        if( $request->isMethod('post') ){
            
            //update group
            $updateGroup = [
                'name' =>  $value
            ];
            ContactGroup::where('id', $request->groupid)->update( $updateGroup );
            
            //return response
            return response()->json([
    		'error_code' => 200,
    		'msg_string'=> __('Group updated successfully.'),

            ]);
        }
    }


    /*
     * Function to delete group
     *
     */
    public function deleteGroup(Request $request){
        $groupId = $request->groupid;
        
        //if request method is post and groupid is not empty
        if( $request->isMethod('post') && !empty($groupId) ){
            
            //delete group
            $deleteGroup = ContactGroup::where('id',$groupId);
            $deleteGroup->delete();
            
            //delete group contact
            $contactGroup = Contact::where('contact_group_id',$groupId);
            $contactGroup->delete();
            
            return response()->json([
    		'error_code' => 200,
    		'msg_string'=> __('Group deleted successfully.'),

            ]);
        }
    }
    
    /*
     * Function to delete members of group
     *
     */
    public function deleteContact(Request $request){
        
        //collect request data
        $ownerId = $request->userid;
        $contactId = $request->contactid;
        $groupId = $request->groupid;
        
        //if request method is post and groupid is not empty
        if( $request->isMethod('post') && !empty($ownerId) && !empty($contactId) && !empty($groupId) ){
            
            //delete contact
            $deleteContact = Contact::where([
                                        ['owner_id', $ownerId],
                                        ['contact_id', $contactId],
                                        ['contact_group_id', $groupId]
                                    ])
                                    ->delete();
            
            //return response
            return response()->json([
    		'error_code' => 200,
    		'msg_string'=> __('Contact deleted successfully.'),

            ]);
        }
    }

}
