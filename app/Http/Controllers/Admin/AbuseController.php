<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\UserController;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\BlockedContact;
use App\ReportAbuse;
use App\UserCard;
use App\Report;
use App\User;
use DB;


class AbuseController extends Controller
{
    protected $deleteUserRecords;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
     public function __construct()
     {
         $this->middleware('auth:admin');
         $this->deleteUserRecords = new UserController();
     }

    /**
     * Show card abuse
     *
     * @return \Illuminate\Http\Response
     */
    public function card_abuse(Request $request)
    {
        //search and sort
        $sortData = $request->only('sorting_order', 'sorting_field');
        $searchData = $request->search_text;
        
        if( !$sortData['sorting_order'] ) {
            $sortData['sorting_order'] = 'asc';
        }

        if( !$sortData['sorting_field'] ) {
            $sortData['sorting_field'] = 'users.name';
        }
        
        //Fetch data
        $abuseCardLists = ReportAbuse::select('report_abuse.*','reports.report_description','users.name','user_cards.title')
                                    ->join('reports', 'reports.id','report_abuse.report_id')
                                    ->join('users', 'users.id','report_abuse.user_id')
                                    ->join('user_cards', 'user_cards.id','report_abuse.item_id')
                                    ->Where('report_abuse.item_type','card')
                                    ->orderBy($sortData['sorting_field'], $sortData['sorting_order']);                                    
        
        if($searchData) {
              $abuseCardLists = $abuseCardLists->Where(function($query) use($searchData){
                                        return $query->where('users.name','like','%'.$searchData.'%')
                                                        ->orWhere('user_cards.title','like','%'.$searchData.'%')
                                                        ->orWhere('reports.report_description','like','%'.$searchData.'%');
                                    });
        }
        
        $abuseCardLists = $abuseCardLists->paginate(10);
        
        //return response
        return view('admin.abuse.card_abuse',['request'    => $request ,
                                            'abuseCardLists' => $abuseCardLists,
                                            'getSortingClass' => function($field) use($sortData) {

                                            if($sortData['sorting_field'] == $field) {
                                                if($sortData['sorting_order'] == 'desc') {
                                                    return "sorting_desc";
                                                } else {
                                                    return "sorting_asc";
                                                }
                                            } else {
                                                return "sorting";
                                            }
                                            },

                                            'getSortinOrder' => function($field) use($sortData) {
                                                if($sortData['sorting_field'] == $field) {
                                                    if($sortData['sorting_order'] == 'desc') {
                                                        return "asc";
                                                    } else {
                                                        return "desc";
                                                    }
                                                } else {
                                                    return "asc";
                                                }
                                            }
                                        ]);
    }
    
    /**
     * Show user abuse.
     *
     * @return \Illuminate\Http\Response
     */
    public function userAbuse(Request $request)
    {
        //search and sort
        $sortData = $request->only('sorting_order', 'sorting_field');
        $searchData = $request->search_text;

        if( !$sortData['sorting_order'] ) {
            $sortData['sorting_order'] = 'asc';
        }

        if( !$sortData['sorting_field'] ) {
            $sortData['sorting_field'] = 'users.name';
        }
        
        //Fetch data
        $abuseUserLists = ReportAbuse::select('report_abuse.*','reports.report_description','users.name','user_cards.title')
                                    ->join('reports', 'reports.id','report_abuse.report_id')
                                    ->join('users', 'users.id','report_abuse.user_id')
                                    ->join('user_cards', 'user_cards.id','report_abuse.item_id')
                                    ->Where('report_abuse.item_type','user')
                                    ->orderBy($sortData['sorting_field'], $sortData['sorting_order']);
      
        if($searchData) {
            $abuseUserLists = $abuseUserLists->Where(function($query) use ($searchData){
                                    return $query->Where('users.name','like','%'.$searchData.'%')
                                                    ->orWhere('user_cards.title','like','%'.$searchData.'%')
                                                    ->orWhere('reports.report_description','like','%'.$searchData.'%');
            });
        }
        
        $abuseUserLists = $abuseUserLists->paginate(10);
        
        //return response
        return view('admin.abuse.user_abuse',['request'    => $request ,
                                                'abuseUserLists' => $abuseUserLists,
                                                'getSortingClass' => function($field) use($sortData) {

                                                if($sortData['sorting_field'] == $field) {
                                                    if($sortData['sorting_order'] == 'desc') {
                                                        return "sorting_desc";
                                                    } else {
                                                        return "sorting_asc";
                                                    }
                                                } else {
                                                    return "sorting";
                                                }
                                                },

                                                'getSortinOrder' => function($field) use($sortData) {
                                                    if($sortData['sorting_field'] == $field) {
                                                        if($sortData['sorting_order'] == 'desc') {
                                                            return "asc";
                                                        } else {
                                                            return "desc";
                                                        }
                                                    } else {
                                                        return "asc";
                                                    }
                                                }
                                            ]);
    }


    /*
     * Function to block user
     *
     */
    public function blockUser(Request $request){
        //$id = Auth::user()->id;
        $userid = 7;
        $blockUserId = $request->block_user_id;
       
        //Check request data
        if($blockUserId) {
            //block user
            $data['user_id'] = $blockUserId;
            $data['blocked_by'] = $userid;
            $blockUser = BlockedContact::insert($data);
            
            //If it's successful
            if($blockUser) {
                //return response
                return redirect()->back()->withErrors(['User blocked successfully.']);
            }
        }
    }
    
    /*
     * Function to delete report abuse
     *
     */
    public function deleteReport(Request $request){
        
        //collect data
        $id = $request->id;
       
        //Check request data
        if($id) {
            
            //delete records
            $this->deleteUserRecords->deleteFirebaseUser($id);
            $this->deleteUserRecords->deleteUserFromDB($id);           
            
            //If it's successful
            return response()->json([
    		'error_code' => 200,
    		'msg_string'=> __('Record deleted successfully.'),
            ]);
        }
    }
    
    
    /*
     * Function to get list of all abused content
     *
     */
    public function abuseContent(Request $request){
        
        $sortData = $request->only('sorting_order', 'sorting_field');
        $searchData = $request->search_text;

        if( !$sortData['sorting_order'] ) {
            $sortData['sorting_order'] = 'asc';
        }

        if( !$sortData['sorting_field'] ) {
            $sortData['sorting_field'] = 'report_description';
        }
        
        //fetch data
        $abuseContents = Report::Where('report_description','like','%'.$searchData.'%')
                                ->orderBy($sortData['sorting_field'], $sortData['sorting_order'])
                                ->paginate(10);
       
        //Check request data
        if($abuseContents) {
            
            //return response
            return view('admin.abuse.viewabuse',['request'    => $request ,
                                                'abuseContents' => $abuseContents,
                                                'getSortingClass' => function($field) use($sortData) {

                                                    if($sortData['sorting_field'] == $field) {
                                                        if($sortData['sorting_order'] == 'desc') {
                                                            return "sorting_desc";
                                                        } else {
                                                            return "sorting_asc";
                                                        }
                                                    } else {
                                                        return "sorting";
                                                    }
                                                    },

                                                    'getSortinOrder' => function($field) use($sortData) {
                                                        if($sortData['sorting_field'] == $field) {
                                                            if($sortData['sorting_order'] == 'desc') {
                                                                return "asc";
                                                            } else {
                                                                return "desc";
                                                            }
                                                        } else {
                                                            return "asc";
                                                        }
                                                    }
                                                ]);
        }
    }
    
    /*
     * 
     * Add Form View
     * 
     */
    public function abuseContentInsert(Request $request) {
        return view('admin.abuse.addabuse',['request' => $request]);
    }
    
    /*
     * 
     * Edit Abuse Management
     * 
     */
    public function abuseContentAdd(Request $request) {
        
        //request data
        $value = $request->abuse_desc;
        
        if($value) {
            $insertAbuse = [
                'report_description' =>  $value
            ];
            
            Report::insert( $insertAbuse );
            
            //return response
            return redirect()->back()->withErrors(['Content added successfully.']);
           
        }
        
    }
    
    /*
     * 
     * Edit Abuse Management
     * 
     */
    public function abuseContentEdit(Request $request) {
        
        //request data
        $id = $request->id;
        
        //fetch data
        $abuseContent = Report::Where('id',$id)->first();
       
        //Check request data
        if($abuseContent) {
            
            //return response
            return view('admin.abuse.editabuse',['request'    => $request ,
                                                'abuseContent' => $abuseContent,
                                            ]);
        }
        
    }
    
    /*
     * 
     * Edit Abuse Management
     * 
     */
    public function abuseContentUpdate(Request $request) {
        
        //request data
        $id = $request->abuse_id;
        $value = $request->abuse_desc;
        
        if($id) {
            $updateAbuse = [
                'report_description' =>  $value
            ];
            
            Report::where('id', $id)->update( $updateAbuse );
            
            //return response
            return redirect()->back()->withErrors(['Content updated successfully.']);
        }
        
    }
    
    /*
     * delete abuse content
     *
     */
    public function abuseContentDelete(Request $request){
        
        //collect data
        $id = $request->id;
       
        //Check request data
        if($id) {
            
            //delete records
            $this->deleteUserRecords->deleteFirebaseUser($id);
            $this->deleteUserRecords->deleteUserFromDB($id);
            
            //If it's successful
            return response()->json([
    		'error_code' => 200,
    		'msg_string'=> __('Record deleted successfully.'),
            ]);
        }
    }
    
    /*
     * Blocked User Lists
     *
     */
    public function blockedUserLists(Request $request){
        
        //fetch data
        $blockedContactLists = BlockedContact::select('blocked_contact.*', 'u1.name AS blockUserName', 'u2.name AS blockedByUserName')
                                                ->join('users as u1', 'u1.id','blocked_contact.user_id')
                                                ->join('users as u2', 'u2.id','blocked_contact.blocked_by')
                                                ->get();
        
        //Check request data
        if($blockedContactLists) {
            
            //If it's successful
            return view('admin.abuse.blockedView',[
                                            'blockedContactLists'   => $blockedContactLists,
                                            'request'               => $request,
                                        ]);
        }
    }
    
    /*
     * 
     * Delete Record From Blocked Contact
     * 
     */
    public function deleteRecord(Request $request) {
        //collect data
        $id = $request->id;
        
        if($id) {
            //delete record
//            $deleteRecord = BlockedContact::where('id', $id);
//            $deleteRecord->delete();
            $this->deleteUserRecords->deleteFirebaseUser($id);
            $this->deleteUserRecords->deleteUserFromDB($id);
            
            //If it's successful
            return response()->json([
    		'error_code' => 200,
    		'msg_string'=> __('All records of user deleted successfully.'),
            ]);
        }
    }
    
    /*
     * Unblock User
     * 
     */
    public function unblockUser(Request $request) {
        $id = $request->id;
        
        //unblock
        $unblockUser = BlockedContact::Where('user_id',$id)->delete();
        
        //If it's success
        if($unblockUser) {
            return response()->json([
    		'error_code' => 200,
    		'msg_string'=> __('User Unblocked successfully.'),
            ]);
        }
    }
}
