<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Content;

class ContentController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
     public function __construct()
     {
         $this->middleware('auth:admin');
     }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $contentRowset = Content::paginate(10);
        return view('admin.content.index',['contentRowset'=> $contentRowset ,'request'=> $request]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $contentId = $request->id;
        $contentRow = Content::where('id',$contentId)
                              ->first();

        if($request->isMethod('post')){

            $validation = Validator::make($request->all(), [
                    'section_name'     => 'required',
                    'section_content'  => 'required'
                 ]);

                $errors = $validation->errors();

                if( $validation->fails() ){

                   return view('admin.content.edit',['contentRow'=> $contentRow ,'request'=> $request,'errors' => $errors]);

                } else {
                    $updateContent['section']    = $request->section_name;
                    $updateContent['content']    = $request->section_content;

                    Content::where('id', $contentId)->update( $updateContent );
                    return redirect('admin/content');

                }
        }
        return view('admin.content.edit',['contentRow'=> $contentRow ,'request'=> $request]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $request
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $contentId = $request->id;
        //if request method is post
        if( $request->isMethod('post') ){

            $content = Content::where('id',$contentId);
            $content->delete();
            return response()->json([
    		'error_code' => 200,
    		'msg_string'=> __('messages.PROCESS_SUCCESS'),

            ]);
        }

    }

    /*
     * Change user status
     *
     */
    public function changeStatus(Request $request){
        $contentId = $request->id;
        $contentData = array('is_active'=> $request->status);

        //if request method is post
        if( $request->isMethod('post') ){
            Content::where('id', $contentId)->update($contentData);

            return response()->json([
    		'error_code' => 200,
    		'msg_string'=> __('messages.PROCESS_SUCCESS'),

            ]);
        }

    }


}
