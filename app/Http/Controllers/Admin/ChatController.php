<?php

namespace App\Http\Controllers\Admin;

//use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Chat;

class ChatController extends Controller
{
    /**
     * Display subscription plan of user
     *
     *
     */
     public function __construct()
     {
         $this->middleware('auth:admin');
     }
     
    public function index(Request $request)
    {
        $sortData = $request->only('sorting_order', 'sorting_field');
        $searchData = $request->search_text;

        if( !$sortData['sorting_order'] ) {
            $sortData['sorting_order'] = 'asc';
        }

        if( !$sortData['sorting_field'] ) {
            $sortData['sorting_field'] = 'users.name';
        }

        $chatData= Chat::leftJoin('users','users.id','=','chat_history.sender_id')
                        ->leftJoin('users as u','u.id','=','chat_history.receiver_id')
                        ->Where('u.name','like','%'.$searchData.'%')
                        ->orWhere('users.name','like','%'.$searchData.'%')
                        ->orderBy($sortData['sorting_field'], $sortData['sorting_order'])
                        ->select('chat_history.*','users.name as sender_name','u.name as receiver_name')
                        ->paginate(10);

        return view('admin.chat.index',['chatData'=> $chatData,
                                        'request' => $request ,
                                        'getSortingClass' => function($field) use($sortData) {
                                            if($sortData['sorting_field'] == $field) {
                                                if($sortData['sorting_order'] == 'desc') {
                                                    return "sorting_desc";
                                                } else {
                                                    return "sorting_asc";
                                                }
                                            } else {
                                                return "sorting";
                                            }
                                            },

                                        'getSortingOrder' => function($field) use($sortData) {
                                            if($sortData['sorting_field'] == $field) {
                                                if($sortData['sorting_order'] == 'desc') {
                                                    return "asc";
                                                } else {
                                                    return "desc";
                                                }
                                            } else {
                                                return "asc";
                                            }
                                        }]

                    );
    }

}
