<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\CardType;



class ItemtypeController extends Controller
{
    /**
     * Display subscription plan of user
     *
     *
     */
     public function __construct()
       {
           $this->middleware('auth:admin');
       }
    public function index(Request $request)
    {
       $searchData = $request->search_text;
       $cardTypes= CardType::Where('name', 'like', '%'.$searchData.'%')
                            ->get();

       return view('admin.itemtype.list',['cardTypes'=> $cardTypes,
                                          'request'  => $request ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add(Request $request)
    {
        if( $request->isMethod('post') ){
                $cardType = new CardType;
                $validation = Validator::make($request->all(), [
                    'card_type'  => 'required',
                    'card_image' => 'required|image|mimes:jpeg,png,jpg,gif',

                ]);

                $errors = $validation->errors();

                if( $validation->fails() ){

                   return view('admin.itemtype.add',['errors' => $errors , 'request' => $request]);

                } else {
                    $cardType['name']  = $request->card_type;
                    $image = $request->file('card_image');

                    if(!empty($image)){

                        $imageName = time().'.'.$image->getClientOriginalExtension();
                        //path for uploading image
                        $destinationPath = public_path('admin/uploads/card_items/');

                        $image->move($destinationPath, $imageName );

                        $cardType['image'] =  $imageName;

                    }
                    $cardType->save();
                    return redirect('admin/itemtype');

                }
              }
        return view('admin.itemtype.add',['request'=> $request ]);

    }

    /**
     * Show the form for editing the subscription types
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
       $cardTypeId = $request->id;
       if( !empty( $cardTypeId ) ){
            $cardTypeData = CardType::where('id',$cardTypeId)->first();

            if( $request->isMethod('post') ){
                $validation = Validator::make($request->all(), [
                    'card_type'  => 'required'

                 ]);
                $errors = $validation->errors();

                if( $validation->fails()){

                    return view('admin.itemtype.edit',['errors'=> $errors , 'request' => $request]);

                } else {
                   $cardTypeData['name']  = $request->card_type;
                   $image = $request->file('card_image');

                    if(!empty($image)){

                        $imageName = time().'.'.$image->getClientOriginalExtension();

                        $destinationPath = public_path('admin/uploads/card_items/');

                        $image->move($destinationPath, $imageName );

                        $cardTypeData['image'] =  $imageName;

                    }
                    CardType::where('id', $cardTypeId)->update( $cardTypeData->toArray());
                    return redirect('admin/itemtype');

                }
              }
            return view('admin.itemtype.edit',['cardTypeData'=> $cardTypeData ,'request'=>$request ]);

       }


    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  $request
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $itemTypeId = $request->card_type_id;

        //if request method is post
        if( $request->isMethod('post') ){

            CardType::where('id',$itemTypeId)->delete();

            return response()->json([
    		'error_code' => 200,
    		'msg_string' => __('messages.PROCESS_SUCCESS'),

            ]);
        }

    }

    /*
     * Change subscription status
     *
     */
    public function changeStatus(Request $request){
        $itemTypeId = $request->id;
        $cardTypeData = array('is_active'=> $request->status);

        //If request method is post
        if( $request->isMethod('post') ){
            CardType::where('id', $itemTypeId)->update($cardTypeData);
            return response()->json([
    		'error_code' => 200,
    		'msg_string'=> __('messages.PROCESS_SUCCESS'),

            ]);
        }

    }

}
