<?php

namespace App\Http\Controllers\Admin;

//use Illuminate\Support\Facades\DB;
use J42\LaravelFirebase\LaravelFirebaseFacade as Firebase;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\ContactInvitation;
use App\UserNotification;
use App\UserSubscription;
use App\BlockedContact;
use App\UserChildCard;
use App\ContactGroup;
use App\ReportAbuse;
use App\ChatHistory;
use App\CardType;
use App\UserCard;
use App\Contact;
use App\Profile;
use App\Country;
use App\Region;
use App\Admin;
use App\City;
use App\User;


class UserController extends Controller
{
    /**
     * Display listing of users
     *
     *
     */
    public function __construct()
    {
         $this->middleware('auth:admin');
    }

    public function index(Request $request)
    {
        $sortData = $request->only('sorting_order', 'sorting_field');
        $searchData = $request->search_text;

        if( !$sortData['sorting_order'] ) {
            $sortData['sorting_order'] = 'asc';
        }

        if( !$sortData['sorting_field'] ) {
            $sortData['sorting_field'] = 'users.name';
        }


        $allUsers = User::leftJoin( 'user_subscription', 'user_subscription.id', '=', 'users.subscription_id' )
                        ->Where('name','like','%'.$searchData.'%')
                        ->orWhere('email','like','%'.$searchData.'%')
                        ->orWhere('user_subscription.subscription_name','like','%'.$searchData.'%')
                        ->orderBy($sortData['sorting_field'], $sortData['sorting_order'])
                        ->select( 'users.*', 'user_subscription.subscription_name' )
                        ->paginate(10);

        //Count each user item
        foreach( $allUsers as $value  ){
            $value->user_cards = UserCard::where( 'owner_id',$value['id']  )
                                         ->get()
                                         ->count();
        }

        //Count each user child items
        foreach( $allUsers as $value  ){
            $value->user_child_cards = UserChildCard::where( 'user_id',$value['id']  )
                                                    ->get()
                                                    ->count();
        }


       return view('admin.user.index',['allUsers'=> $allUsers,
                                        'request' => $request,
                                        'getSortingClass' => function($field) use($sortData) {

                                        if($sortData['sorting_field'] == $field) {
                                            if($sortData['sorting_order'] == 'desc') {
                                                return "sorting_desc";
                                            } else {
                                                return "sorting_asc";
                                            }
                                        } else {
                                            return "sorting";
                                        }
                                        },

                                        'getSortingOrder' => function($field) use($sortData) {
                                            if($sortData['sorting_field'] == $field) {
                                                if($sortData['sorting_order'] == 'desc') {
                                                    return "asc";
                                                } else {
                                                    return "desc";
                                                }
                                            } else {
                                                return "asc";
                                            }
                                        }
                                      ]
                    );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
       $userId = $request->user_id;

        if( !empty( $userId ) ){
            $userData= User::leftJoin( 'user_subscription', 'user_subscription.id', '=', 'users.subscription_id' )
                            ->leftJoin('profile','profile.user_id','=','users.id')
                            ->select( 'users.*', 'user_subscription.*', 'profile.*' )
                            ->where('users.id',$userId)
                            ->first();


            $subscriptions = UserSubscription::where('is_active', 1 )
                                               ->get();

            //Get all countries
            $countries = Country::get();

            $countryID = $userData->country_id;
            $stateID   = $userData->region_id;

            //Get all countries
            $stateList = Region::where('country_id', $countryID )->get();

            //Get all cities
            $cityList = City::where('region_id', $stateID )->get();

            if( $request->isMethod('post') ){
                $validation = Validator::make($request->all(), [
                    'user_name'          => 'required',
                    'email'              => 'required|email',
                    'subscription_name'  => 'required',
                    'gender'             => 'required',
                    'dob'                => 'required',
                    'city'               => 'required',
                    'province'           => 'required',
                    'country'            => 'required',
                    'company_name'       => 'required',
                    'website'            => 'required',
                    'phone_no'           => 'required',

                 ]);

                $errors = $validation->errors();

                if( $validation->fails() ){


                   return view('admin.user.edit',[  'subscriptions'=> $subscriptions ,
                                                    'userData'     => $userData ,
                                                    'request'      => $request ,
                                                    'countries'    => $countries ,
                                                    'errors'       => $errors,
                                                    'stateList'    => $stateList,
                                                    'cityList'     => $cityList ] );

                } else {
                    $updateUser['name']             = $request->user_name;
                    $updateUser['email']            = $request->email;
                    $updateUser['subscription_id']  = $request->subscription_name;
                    $profileData['gender']          = $request->gender;
                    $profileData['dob']             = $request->dob;
                    $profileData['city']         = $request->city;
                    $profileData['province']     = $request->province;
                    $profileData['country']      = $request->country;

                    $profileData['company_name']    = $request->company_name;
                    $profileData['website']         = $request->website;
                    $profileData['phone_number']    = $request->phone_no;

                    $profileImage = $request->file('profile_image');
                    $coverImage   = $request->file('cover_image');

                    if(!empty($profileImage)){

                        $profileImageName = 'profile_'.time().'.'.$profileImage->getClientOriginalExtension();
                        $destinationPath = public_path('admin/uploads/user/');

                        $profileImage->move($destinationPath , $profileImageName );

                        $profileData['profile_image'] =  'admin/uploads/user/' . $profileImageName;
                    }


                    if(!empty($coverImageName)){
                        $coverImageName   = 'cover_'.time().'.'.$coverImage->getClientOriginalExtension();
                        $destinationPath = public_path('admin/uploads/user/');
                        $coverImage->move($destinationPath , $coverImageName );
                        $profileData['cover_image']   =  'admin/uploads/user/' . $coverImageName;

                    }


                    // print_r($profileData);
                    // print_r($updateUser);die;
                    User::where('id', $userId)->update( $updateUser );
                    Profile::where('user_id', $userId)->update( $profileData );
                    return redirect('admin/user');

                }
              }

            return view('admin.user.edit',[ 'subscriptions'=> $subscriptions ,
                                            'userData'     => $userData ,
                                            'request'      => $request ,
                                            'countries'    => $countries ,
                                            'stateList'    => $stateList,
                                            'cityList'     => $cityList ]);

       }


    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $userId = $request->user_id;

        //if request method is post
        if( $request->isMethod('post') ){

            //delete firebase user
            $this->deleteFirebaseUser($userId);
            //delete user from db
            $this->deleteUserFromDB($userId);

            return response()->json([
    		'error_code' => 200,
    		'msg_string'=> __('messages.PROCESS_SUCCESS'),

            ]);
        }
    }

    /*
     * Delete user records from datbase
     *
     */
    public function deleteUserFromDB($userId) {
        
        //delete user records from database
            BlockedContact::where('user_id',$userId)
                            ->orWhere('blocked_by',$userId)
                            ->delete();
            CardType::where('owner_id',$userId)->delete();
            ChatHistory::where('sender_id',$userId)
                    ->orWhere('receiver_id',$userId)
                    ->delete();
            Contact::Where('owner_id',$userId)
                    ->orWhere('contact_id',$userId)
                    ->delete();
            ContactGroup::Where('user_id',$userId)->delete();
            ContactInvitation::Where('owner_id',$userId)->delete();
            Profile::Where('user_id',$userId)->delete();
            ReportAbuse::Where( 'user_id',$userId)
                        ->orWhere('report_id',$userId)
                        ->delete();
            UserCard::Where('owner_id',$userId)->delete();
            UserchildCard::Where('user_id',$userId)->delete();
            UserNotification::Where('user_id',$userId)->delete();
            User::where('id',$userId)->delete();

    }

    /*
     *
     * delete firebase user and chat room
     *
     */
    public function deleteFirebaseUser($userId) {
        
        //get user firebase id
        $user = User::where('id',$userId)->first();
        $userFirebaseId = $user->firebase_id;

        //get chat room by firebase id
        if($userFirebaseId) {
            $userList = Firebase::get('/userList/'.$userFirebaseId);
            $userList = (json_decode($userList->getContents()));

            //Iterate users
            if($userList) {
                foreach ($userList->chatRoomMate as $chatroom) {
                    $chatRoomId = $chatroom->chatRoom;

                    //delete chat room
                    $delChatRoom = Firebase::delete('/chatRoom/'.$chatRoomId);

                }
            }

        //delete user
        Firebase::delete('/userList/'.$userFirebaseId);
        }
    }

    /*
     * Change user status
     *
     */
    public function changeStatus(Request $request){
        $userId = $request->user_id;
        $userData = array('is_active'=> $request->status);
        //if request method is post
        if( $request->isMethod('post') ){
            User::where('id', $userId)->update($userData);
            return response()->json([
          		'error_code' => 200,
          		'msg_string'=> __('messages.PROCESS_SUCCESS'),

            ]);
        }

    }

    public function expireCards(Request $request) {
      $expiredCards = UserCard::where(DB::raw("CONCAT(end_date, ' ', end_time)"), "<", DB::raw("NOW()"))
                                ->where('is_active', '=', 1)
                                ->get();
      if($expiredCards) {
        foreach($expiredCards as $expiredCard){
          $expiredCard->is_active = 0;
          $expiredCards->save();
        }
      }
    }
    
    /*
     * Reset Password
     * 
     */
    public function updatePassword(Request $request) {
        
        if($request->isMethod("get")) {
            return view('admin.user.password',[ 'request' => $request ]);
        }
        //check method
        if($request->isMethod("post") && !empty($request->new_password)) {
           
            //get new password
            $new_password = $request->new_password;
            $adminId = Auth::id();
            
            //generate password
            $generatedPassword = Hash::make($new_password);
            
            //update
            Admin::Where('id',$adminId)->update(['password' => $generatedPassword]);
            
            return redirect()->back()->with("message","Password updated successfully.");
            
        }
    }


}
