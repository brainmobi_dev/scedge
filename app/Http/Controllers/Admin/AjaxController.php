<?php

namespace App\Http\Controllers\Admin;

//use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Region;
use App\City;


class AjaxController extends Controller
{

    /**
     * Function to get all states list
     *
     * @param  countryId
     *
     */
     public function __construct()
     {
         $this->middleware('auth:admin');
     }
     
    public function getStates(Request $request){
        $countryId = $request->country_id;
        $html = '';
        $states = Region::where('country_id','=',$countryId)
                         ->get();


        if($states){
            $html .= '<option value="">Select State </option>';
            foreach($states as $stateRow){
                $html .= '<option value="'.$stateRow['id'].'">'.$stateRow['name'].'</option>';
            }
        }

        echo $html;

    }

    /**
     * Function to get all states list
     *
     * @param  countryId
     *
     */
    public function getCities(Request $request){
         $stateId = $request->state_id;
         $html = '';

         // Get all cities of requested state id
         $cities = City::where('region_id','=',$stateId)
                           ->get();

        if($cities){
            $html .= '<option value="">Select City </option>';
            foreach($cities as $cityRow){
                    $html .= '<option value="'.$cityRow['id'].'">'.$cityRow['name'].'</option>';
            }
        }

        echo $html;

    }

}
