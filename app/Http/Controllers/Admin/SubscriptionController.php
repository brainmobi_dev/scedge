<?php

namespace App\Http\Controllers\Admin;

//use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\UserSubscription;

class SubscriptionController extends Controller
{
    /**
     * Display subscription plan of user
     *
     *
     */
     public function __construct()
       {
           $this->middleware('auth:admin');
       }
    public function index(Request $request)
    {

       $sortData = $request->only('sorting_order', 'sorting_field');
       $searchData = $request->search_text;

        if( !$sortData['sorting_order'] ) {
            $sortData['sorting_order'] = 'asc';
        }

        if( !$sortData['sorting_field'] ) {
            $sortData['sorting_field'] = 'user_subscription.subscription_code';
        }

       $subscriptions=  UserSubscription::Where('subscription_code','like','%'.$searchData.'%')
                                        ->orWhere('subscription_name','like','%'.$searchData.'%')
                                        ->orWhere('subscription_amount','like','%'.$searchData.'%')
                                        ->orWhere('subscription_type','like','%'.$searchData.'%')
                                        ->orderBy($sortData['sorting_field'], $sortData['sorting_order'])
                                        ->get();

       return view('admin.subscription.list',['subscriptions'=> $subscriptions,
                                              'request' => $request,
                                              'getSortingClass' => function($field) use($sortData) {

                                                if($sortData['sorting_field'] == $field) {
                                                    if($sortData['sorting_order'] == 'desc') {
                                                        return "sorting_desc";
                                                    } else {
                                                        return "sorting_asc";
                                                    }
                                                } else {
                                                    return "sorting";
                                                }
                                                },

                                            'getSortingOrder' => function($field) use($sortData) {
                                                if($sortData['sorting_field'] == $field) {
                                                    if($sortData['sorting_order'] == 'desc') {
                                                        return "asc";
                                                    } else {
                                                        return "desc";
                                                    }
                                                } else {
                                                    return "asc";
                                                }
                                            }
                                    ]);
    }

    /**
     * Show the form for editing the subscription types
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
       $subId = $request->id;
       if( !empty( $subId ) ){

            $subscription = UserSubscription::where('id',$subId)->select('subscription_amount')->first();

            if( $request->isMethod('post') ){
                $validation = Validator::make($request->all(), [
                    'subscription_amount'  => 'required'

                 ]);
                $errors = $validation->errors();

                if( $validation->fails() ){

                   return view('admin.subscription.edit',['subscription'=> $subscription ,'errors'=>$errors , 'request' => $request]);

                } else {
                    $updateUser['subscription_amount']  = $request->subscription_amount;
                    UserSubscription::where('id', $subId)->update( $updateUser );
                    return redirect('admin/subscription');

                }
              }
            return view('admin.subscription.edit',['subscription'=> $subscription ,'request'=>$request ]);

       }


    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $userId = $request->user_id;
        //if request method is post
        if( $request->isMethod('post') ){

            $user = User::where('id',$userId);
            $user->delete();
            return response()->json([
    		'error_code' => 200,
    		'msg_string'=> __('messages.PROCESS_SUCCESS'),

            ]);
        }

    }

    /*
     * Change subscription status
     *
     */
    public function changeStatus(Request $request){
        $subId = $request->id;
        $subscriptionData = array('is_active'=> $request->status);
        //if request method is post
        if( $request->isMethod('post') ){
            UserSubscription::where('id', $subId)->update($subscriptionData);
            return response()->json([
    		'error_code' => 200,
    		'msg_string'=> __('messages.PROCESS_SUCCESS'),

            ]);
        }

    }

}
