<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\UserCard;
use Illuminate\Support\Facades\Validator;
use App\Country;
use App\Region;
use App\City;

use Illuminate\Support\Facades\DB;

class ItemController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
     public function __construct()
     {
         $this->middleware('auth:admin');
     }

    /**
     * Show the item info
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sortData = $request->only('sorting_order', 'sorting_field');
        $searchData = $request->search_text;

        if( !$sortData['sorting_order'] ) {
            $sortData['sorting_order'] = 'asc';
        }

        if( !$sortData['sorting_field'] ) {
            $sortData['sorting_field'] = 'users.name';
        }

        $userCards = UserCard::leftJoin( 'users', 'users.id', '=', 'user_cards.owner_id' )
                               ->Where('users.name','like','%'.$searchData.'%')
                               ->orderBy($sortData['sorting_field'], $sortData['sorting_order'])
                               ->select( 'user_cards.*', 'users.name' )
                               ->paginate(10);

        return view('admin.item.index',['userCards'=> $userCards ,
                                        'request'  => $request,
                                        'getSortingClass' => function($field) use($sortData) {

                                        if($sortData['sorting_field'] == $field) {
                                            if($sortData['sorting_order'] == 'desc') {
                                                return "sorting_desc";
                                            } else {
                                                return "sorting_asc";
                                            }
                                        } else {
                                            return "sorting";
                                        }
                                        },

                                        'getSortinOrder' => function($field) use($sortData) {
                                            if($sortData['sorting_field'] == $field) {
                                                if($sortData['sorting_order'] == 'desc') {
                                                    return "asc";
                                                } else {
                                                    return "desc";
                                                }
                                            } else {
                                                return "asc";
                                            }
                                        }
                                    ]);
    }

    /*
     * Show item details
     *
     */
    public function view(Request $request){

        $cardID = $request->item_id;

        $card = UserCard::leftJoin( 'users', 'users.id', '=', 'user_cards.owner_id' )
                        ->select( 'user_cards.*', 'users.name' )
                        ->where('user_cards.id',$cardID)
                        ->first();

        if( $card->card_type == 'PROFESSIONAL' ){
                $personal = 'style = display:none';
                $professional = 'style = display:block';

        }elseif($card->card_type == 'FREE' || $card->card_type == 'PERSONAL'){
                $professional = 'style=display:none';
                $personal = 'style=display:block';
        }


        return view('admin.item.view',['card'=> $card ,
                                       'request'=> $request,
                                       'personal'=> $personal,
                                       'professional'=> $professional]);
    }

    /*
     * View edit form of item
     *
     */

     public function edit(Request $request)
    {
        $cardID = $request->item_id;

        //Get all countries
        $countries = Country::get();

        //Get all user card data
        $card = UserCard::leftJoin( 'users', 'users.id', '=', 'user_cards.owner_id' )
                        ->select( 'user_cards.*', 'users.name' )
                        ->where('user_cards.id',$cardID)
                        ->first();

        if(!empty($card->location_country_id)){
            $countryID = $card->location_country_id;
            $stateID   = $card->location_province_id;

            //Get all countries
            $stateList = Region::where('country_id', $countryID )->get();
            //Get all cities
            $cityList = City::where('region_id',$stateID )->get();

        }else {
             //Get all countries
            $stateList = Region::get();
            //Get all cities
            $cityList = City::get();
        }


        if( $card->card_type == 'PROFESSIONAL' ){
                $personal = 'style=display:none';
                $professional = 'style=display:block';

        }elseif($card->card_type == 'FREE' || $card->card_type == 'PERSONAL'){
                $professional = 'style=display:none';
                $personal = 'style=display:block';
        }


        if($request->isMethod('post')){

            $validation = Validator::make($request->all(), [
                            'card_title'       => 'required',
                            'card_date'        => 'required',
                            'card_body'        => 'required',
                            'card_time'        => 'required',
                            'priority'         => 'required',

                        ]);

                //If user subscription is personal or free
                if($card->card_type == 'PERSONAL'|| $card->card_type == 'FREE'){

                    $validation = Validator::make($request->all(), [
                                                    'card_start_date'  => 'required',
                                                    'card_start_time ' => 'required',
                                                    'card_end_date'    => 'required',
                                                    'card_end_time '   => 'required',
                                                    'share_on_showcase'=> 'required',
                                                    'two_way_sync'     => 'required',
                                                    'location_address' => 'required',
                                                    'location_city'    => 'required',
                                                    'location_province'=> 'required',
                                                    'location_country' => 'required'
                                                ]);

                } elseif( $card->card_type == 'PROFESSIONAL' ) {
                    $validation = Validator::make($request->all(), [
                        'spacs_size'       => 'required',
                        'spacs_color'      => 'required',
                        'spacs_modal'      => 'required',
                        'spacs_brand'      => 'required',
                        'share_on_showcase'=> 'required',
                        'two_way_sync'     => 'required',
                        'prof_price'       => 'required',
                        'discount'         => 'required',
                        'for_gender'       => 'required',
                        'for_age'          => 'required',
                        'prof_loc_address' => 'required',
                        'prof_loc_city'    => 'required',
                        'prof_loc_state'   => 'required',
                        'prof_loc_country' => 'required',
                        //'share_on_showcase'=> 'required',
                        //'live_item'        => 'required',
                    ]);
                }

                //Check validation
                if( $validation->fails() ){
                    $errors = $validation->errors();

                    return view('admin.item.edit',[ 'card'     => $card ,
                                                    'request'  => $request,
                                                    'countries'=> $countries,
                                                    'personal' => $personal,
                                                    'errors'   => $errors,
                                                    'professional'=>$professional,
                                                    'stateList'   => $stateList,
                                                    'cityList'    => $cityList
                                                    ]);

                } else {
                     $updateUserCardData = array('title'    => $request->card_title,
                                                'body'     => $request->card_body,
                                                'priority' => $request->priority,
                                                'card_date'=> $request->card_date,
                                                'card_time'=> $request->card_time,
                                                'share_on_showcase'=>$request->share_on_showcase,
                                                'two_way_sync'=> $request->two_way_sync
                                                );

                    if( $card->card_type == 'PROFESSIONAL' ){
                        $updateUserCardData['spacs_size'] = $request->spacs_size;
                        $updateUserCardData['spacs_color'] = $request->spacs_color;
                        $updateUserCardData['spacs_brand'] = $request->spacs_brand;
                        $updateUserCardData['spacs_model'] = $request->spacs_modal;
                        $updateUserCardData['location_address']  = $request->prof_loc_address;
                        $updateUserCardData['location_city_id'] = $request->prof_loc_city;
                        $updateUserCardData['location_province_id'] = $request->prof_loc_state;
                        $updateUserCardData['location_country_id'] = $request->prof_loc_country;
                        $updateUserCardData['price'] = $request->price;
                        $updateUserCardData['discount'] = $request->discount;
                        $updateUserCardData['for_gender'] = $request->for_gender;
                        $updateUserCardData['for_age'] = $request->for_age;
                    } else {
                        $updateUserCardData['start_date']   = $request->card_start_date;
                        $updateUserCardData['start_time']   = $request->card_start_time;
                        $updateUserCardData['end_date']     = $request->card_end_date;
                        $updateUserCardData['end_time']     = $request->card_end_time;
                        $updateUserCardData['location_address']  = $request->location_address;
                        $updateUserCardData['location_city_id']     = $request->location_city;
                        $updateUserCardData['location_province_id'] = $request->location_province;
                        $updateUserCardData['location_country_id']  = $request->location_country;
                    }
                    UserCard::where('id', $cardID)->update( $updateUserCardData );
                    return redirect('/item');
                }
        }

        return view('admin.item.edit',['card'        => $card ,
                                       'request'     => $request,
                                       'countries'   => $countries,
                                       'personal'    => $personal,
                                       'professional'=> $professional,
                                       'stateList'   => $stateList,
                                       'cityList'    => $cityList
                                       ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $itemId = $request->item_id;
        //if request method is post
        if( $request->isMethod('post') ){

            $user = UserCard::where('id',$itemId);
            $user->delete();
            return response()->json([
    		'error_code' => 200,
    		'msg_string'=> __('messages.PROCESS_SUCCESS'),

            ]);
        }

    }

    /*
     * Change user status
     *
     */
    public function changeStatus(Request $request){
        $itemId = $request->item_id;
        $itemData = array('is_active'=> $request->status);
        //if request method is post
        if( $request->isMethod('post') ){
           UserCard::where('id', $itemId)->update($itemData);
           return response()->json([
    		'error_code' => 200,
    		'msg_string'=> __('messages.PROCESS_SUCCESS'),

            ]);
        }

    }

}
