<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Content extends Authenticatable
{
    use Notifiable;

    protected $table = 'site_contents';

    public $timestamps = false;
   
}
