<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class UserCard extends Authenticatable
{
    use Notifiable;

    protected $table = 'user_cards';

    public $timestamps = true;

    protected $fillable = ["owner_id", "card_type_id", "title", "body", "card_image", "card_date", "card_time", "priority", "location_address", "location_city",
      "location_province", "location_country", "start_date", "start_time", "end_date",
      "end_time", "price", "quantity", "discount", "spacs_size", "spacs_brand", "spacs_model",
      "spacs_color", "for_gender", "for_age", "share_on_showcase", "two_way_sync", "live_item",
      "card_type", "allow_notification", "is_active", "card_order", "showcase_order", "currency",
      "url", "category", "contact_info", "card_status", "street_address"

    ];


}
